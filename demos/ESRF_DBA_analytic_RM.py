import commissioningsimulations.config as config

config.lattice_file_for_test = '/machfs/liuzzo/ESRF/StorageRing/LATTICE/AT/ESRF_AT2.mat'
config.lattice_variable_name = 'ring'

# name-pattern to find magnets in lattice
config.normal_quadrupoles_fam_name_pattern = 'Q*'
config.skew_quadrupoles_fam_name_pattern = 'S[461920]*'  # for simplicity steerers and skew at all sextupoles
config.sextupoles_fam_name_pattern = 'S[461920]*'
config.octupoles_fam_name_pattern = ''
config.correctors_fam_name_pattern = 'S[461920]*'
config.bpms_name_patter = 'BPM*'
config.dipoles_fam_name_pattern = 'B[12]*'

from commissioningsimulations.correction.optics_coupling.analytic_orm_with_normal_quad_errors import \
     _test_orm_quad_deriv

import at
ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)
[print(f'{el.FamName} : {el.PolynomB} ') for el in ring if hasattr(el, 'PolynomB')]

# exit()
_test_orm_quad_deriv(m=0, col=[0], row=[0], thick_quadrupole=False, thick_steerers=False)
_test_orm_quad_deriv(m=0, col=[0], row=[0], thick_quadrupole=True, thick_steerers=False)
_test_orm_quad_deriv(m=0, col=[0], row=[0], thick_quadrupole=True, thick_steerers=True)


if __name__=='__main__':
    pass
