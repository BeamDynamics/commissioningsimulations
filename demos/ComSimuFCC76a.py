import os
# INPUT LATTICE SPECIFIC INFORMATIONS
# This needs to be done here before importing any other module of commissioning simulation package.
import commissioningsimulations.config as config

config.venv = '/machfs/liuzzo/FCC/76a/Errors/fcc_com_sim_cluster/bin/activate'  # for slurm

config.lattice_file_for_test = '/machfs/liuzzo/FCC/76a/Errors/FCC_76a_diag.mat'
config.lattice_variable_name = 'r'

config.reference_lattice_file = '/machfs/liuzzo/FCC/76a/Errors/FCC_76a_diag.mat'
config.reference_lattice_variable_name = 'r'


# name-pattern to find magnets in lattice
config.normal_quadrupoles_fam_name_pattern = 'S*'  # for simplicity normal quad at all sextupoles
config.skew_quadrupoles_fam_name_pattern = 'S*'  # for simplicity skew at all sextupoles
config.sextupoles_fam_name_pattern = 'S*'
config.octupoles_fam_name_pattern = 'O*'
config.correctors_fam_name_pattern = 'S*'  # steerers at all sextupoles
config.bpms_name_patter = 'BPM'
config.dipoles_fam_name_pattern = 'B*_*'

# for optics correction
config.correctors_for_optics_RM = 'SD1A_F[LR]'
config.normal_quadrupoles_for_optics_and_coupling_fit = 'S*'
config.skew_quadrupoles_for_optics_and_coupling_fit = 'S*'
config.dipoles_for_optics_and_coupling_fit = 'B*_*'

config.normal_quadrupoles_for_optics_and_coupling_correction = 'S*'
config.skew_quadrupoles_for_optics_and_coupling_correction = 'S*'

# for tune and chromaticity
config.focussing_quadrupoles_for_tune = 'QF2A'  # 'QF[0123456789]A*'
config.defocussing_quadrupoles_for_tune = 'QD1A'  # 'QD[0123456789]A*'
config.focussing_sextupoles_for_chromaticity = 'SF*A'
config.defocussing_sextupoles_for_chromaticity = 'SD*A'

# specify simulation details
lattice_name = 'FCC_76a'
errors_name = 'DQS_30um_noIP_FMA'
correction_name = ''

config.main_folder_data = os.path.abspath('./' + lattice_name + '_Err' + errors_name + '_Cor' + correction_name)
config.folder_matrices = os.path.join(config.main_folder_data, 'Matrices')

## to run only plot (data already computed) uncomment the following 3 lines
# from commissioningsimulations.summary_figures import summary_figures
# summary_figures(data_folder=config.main_folder_data, save_figures=True)
# exit()

config.compute_DA_LT = True
config.compute_sensitivity = True  # sensitivity to alignment errors and to wave of errors
config.compute_survey = True    # survey
config.compute_detuning = True   # detuning with amplitude and momentum and tune footprint
config.tapering_effect = True   # detuning with amplitude and momentum and tune footprint

config.N_seeds = 0

config.submission_mode = 'local'    # can be local (serial seeds, parallelized DA/TLT/IE)
                                    #     or slurm (parallelized seed, serial DA/TLT/IE)

config.slurm_partition_name = 'nice-long,asd'  # additional partition to submit jobs

if config.compute_DA_LT:
    config.slurm_cpus_per_task = 50     # if slurm submission mode, use multiprocessing on N cores for each task.
                                        # config.N_seeds x config.slurm_cpu_per_task cores will be used on the cluster
else:
    config.slurm_cpus_per_task = 1

# to limit the number of cores used in local mode for DA computation
# import at
# at.DConstant.patpass_poolsize = 40

# # Define properties to compute after correction (not fully exploited, still under development)
config.dict_of_evaluations = {
                            'DA': {'DA_max_amplitude': [1.2e-3, 4.0e-5],
                                   'n_turns': 2**9,
                                   'n_points': [31, 31]},
                            'DAdpp': {
                                  'DA_max_amplitude': [0.03, 12.0e-4],
                                  'n_turns': 2**8,
                                  'n_points': [31, 31]},
                            'AngularAcceptance': {
                                  'refpts': list(range(0, 350))[0::10] + list(range(1400, 1900))[0::10],
                                  'n_turns': 2**7},
                            'MA': {
                                'mom_acc_refpts': range(10, 110),
                                'n_turns': 2**2,
                                'emit_v': 10e-12,       # 10 pm
                                'cur_per_bunch': 0.001,  # 1 mA
                                'zn': 0.67},
                            'IE': {},
                            'VacuumLifetime': {},
                            'FrequencyMapAnalysis': {
                                'DA_max_amplitude': [1.2e-3, 4.0e-5],
                                'n_turns': 2**8,
                                'n_points': [51, 51]},
                            'Detunings': {'DA_max_amplitude': [1.2e-3, 4.0e-5],
                                          'n_turns': 2 ** 7,
                                          'n_points': [21]},
                        }

# # Define errors to set
# survey errors
from commissioningsimulations.errors.FromSurveyFile import readSurveyFile
survey_file = '/machfs/liuzzo/FCC/74a/FCC_LongRangeErrorsMH.csv'
s, xs, ys = readSurveyFile(survey_file, survey_scale_factor=0.0, rotate_index=1290)


config.dict_of_errors = {'DL[012345]A': {
                                 'x': 30e-6,
                                 'y': 30e-6,
                                 'rot': 30e-6},
                          'Q[FD][123456]A': {
                                 'x': 30e-6,
                                 'y': 30e-6,
                                 'rot': 30e-6},
                          'S[FD]1A': {
                                 'x': 30e-6,
                                 'y': 30e-6,
                                 'rot': 30e-6},
                          'Survey': {
                             's': s,
                             'x': xs,
                             'y': ys
                          }
                  }


# # Define properties for each correction to run

# reference orbit is the one of the survey misalignments
import at
import numpy as np
r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)
ind_bpms = at.get_refpts(r, config.bpms_name_patter)
sbpm = at.get_s_pos(r, ind_bpms)
x_ref = np.interp(sbpm, s, xs)
y_ref = np.interp(sbpm, s, ys)
reforb = np.zeros((len(ind_bpms), 6))
reforb[:, 0] = x_ref
reforb[:, 2] = y_ref

reftraj = np.concatenate((reforb, reforb, reforb), axis=0)

# trajectory first turns beam threading
traj_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'TRM' + correction_name + '.pkl'),
               'response_mode': 'numeric',
               'number_of_turns': 3,
               'num_singular_vectors': 200,
               'max_trajectory': (10.0e-3, 10.0e-3),
               'reference_trajectory': reftraj.T}

# orbit
orb_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'ORM' + correction_name + '.pkl'),
              'response_mode': 'numeric',
              'num_singular_vectors': 200,
              'num_iterations': 3,
              'zero_steerers_average': False,
              'reference_orbit': reforb}

# optics
fit_optics_params = {
    'use_mp': True,
     'verbose': True,
     'fit_params': {
         'quadrupoles': {
                    'niter': 2,
                    'neigQuad': 30,
                    'neigSkew': 30,
                    'bpms_weights': None,
                    'hor_disp_weight': 1.0,
                    'ver_disp_weight': 1.0,
                    'tune_weight': 1.0,
                    'mode': 'analytic',
                    'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                               'J' + correction_name + '_QUADS_ana.pkl'),
                    'reference_orm': None,
                    'normal': True,
                    'skew': True
                   }
        }  # END OF FIT_PARAMS
}  # END OF OPTICS_PARAMS

# RDTs
rdt_params = {'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                            'RDTs_RM_' + correction_name + '.pkl'),
              'niter': 3,
              'neigQuad': 96,
              'neigSkew': 96,
              'use_mp': False,
              'verbose': True,
              'tune_weight': 1.0,
              'hor_disp_weight': 0.5,
              'ver_disp_weight': 0.8
              }

tune_traj_params = {}
bba_params = {}
noeco_params = {}

# define list of corrections to run (space and digits are ignored to allow for different dictionary keys)
config.dict_of_corrections = {
                        # 'trajectory 0': traj_params,
                        'enable-multipole 0': {'multipole_class': 'sextupoles',
                                               'state': 0.0},
                        'enable-multipole 1': {'multipole_class': 'octupoles',
                                               'state': 0.0},
                        'trajectory 1': traj_params,
                        'enable-multipole 2': {'multipole_class': 'sextupoles',
                                               'state': 0.5},
                        'enable-multipole 3': {'multipole_class': 'octupoles',
                                               'state': 0.5},
                        'trajectory 2': traj_params,
                        'enable-multipole 4': {'multipole_class': 'sextupoles',
                                               'state': 1.0},
                        'enable-multipole 5': {'multipole_class': 'octupoles',
                                               'state': 1.0},
                        'trajectory 3': traj_params,
                        'orbit 1': orb_params,
                        'tune 1': [],
                        'chromaticity 1': [],
                        'orbit 2': orb_params,
                        'errorsmodel 1': fit_optics_params,   # <- fit errors, correct RDTs
                        'optics 1': {'mode': 'fitted_errors'},
                        # 'optics 1': {'mode': rdt_params}, # use fitted model to compute RDTs to correct'orbit 3': orb_params,
                        'tune 2': [],
                        'chromaticity 2': [],
                        'errorsmodel 2': fit_optics_params,   # <- fit errors, correct RDTs
                        'optics 2': {'mode': 'fitted_errors'},
                        # 'optics 2': {'mode': rdt_params}, # use fitted model to compute RDTs to correct'orbit 3': orb_params,
                        'orbit 4': orb_params,
                        'tune 3': [],
                        'chromaticity 3': [],
                        }


"""
# debug: correct one single seed locally
from commissioningsimulations import SingleSeed
correction_parameters_file = os.path.join(config.main_folder_data, 'CorrectionParameters.pkl')
SingleSeed.set_errors_and_correct_one_seed(correction_parameters_file,
                                    1,
                                    ring=None,
                                    indexes=None)
"""


# # RUN SIMULATION

# this import must be here, or above configurations will be ignored
from commissioningsimulations.run import run_commissioning_simulations
run_commissioning_simulations()

# # plot existing data
# from commissioningsimulations.summary_figures import summary_figures
# summary_figures(data_folder=config.main_folder_data, save_figures=True)

# run in pycharm
if __name__ == '__main__':
    pass
