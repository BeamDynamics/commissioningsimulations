import os
# INPUT LATTICE SPECIFIC INFORMATIONS
# This needs to be done here before importing any other module of commissioning simulation package.
import commissioningsimulations.config as config

config.lattice_file_for_test = '/machfs/liuzzo/FCC/V22/FCCeeV22_diag_relaxed.mat'
config.lattice_variable_name = 'r'

# name-pattern to find magnets in lattice
config.normal_quadrupoles_fam_name_pattern = 'Q*'
config.skew_quadrupoles_fam_name_pattern = 'S*'  # for simplicity skew at all sextupoles
config.sextupoles_fam_name_pattern = 'S*'
config.octupoles_fam_name_pattern = 'O*'
config.correctors_fam_name_pattern = 'Q*'  # steerers at all quadrupoles (~2 times more than needed)
config.bpms_name_patter = 'BPM*'
config.dipoles_fam_name_pattern = 'B*'

# for optics correction
config.correctors_for_optics_RM = 'SF[1][1]*'
config.normal_quadrupoles_for_optics_and_coupling_fit = 'QF2_*'
config.skew_quadrupoles_for_optics_and_coupling_fit = 'S[F][0-9]*'
config.dipoles_for_optics_and_coupling_fit = 'B[1]*'

# for tune and chromaticity
config.focussing_quadrupoles_for_tune = 'QF*'
config.defocussing_quadrupoles_for_tune = 'QD*'
config.focussing_sextupoles_for_chromaticity = 'SF*'
config.defocussing_sextupoles_for_chromaticity = 'SD*'

# specify simulation details
lattice_name = 'FCC'
errors_name = 'DQS_50um_noIP'
correction_name = '_traj_to_opt'

config.main_folder_data = os.path.abspath('./' + lattice_name + '_Err' + errors_name + '_Cor' + correction_name)
config.folder_matrices = os.path.join(config.main_folder_data, 'Matrices')

config.N_seeds = 10

config.submission_mode = 'slurm'   # can be local (serial seeds, parallelized DA)
                                   #     or slurm (parallelized seed, serial DA)
slurm_cpus_per_task = 50

config.compute_DA_LT = True


# FOR LATER DEVELOPMENT define properties to compute after correction
config.dict_of_evaluations = {'DA': {'DA_max_amplitude': [0.3e-3, 0.3e-3],
                              'n_turns': 2**7},
                       'DAdpp': {'DA_max_amplitude': [1e-3, 1e-3],
                              'n_turns': 2**7},
                       'MA': {'mom_acc_refpts': range(50),
                              'n_turns': 2**7},
                       'IE': {},
                       }

# define errors to set
config.dict_of_errors = {'DL*': {'x': 50e-6,
                                 'y': 50e-6,
                                 'rot': 50e-6},
                          'QF[0-9]_*': {
                                 'x': 50e-6,
                                 'y': 50e-6,
                                 'rot': 50e-6},
                          'SF[0-9]_*': {
                                 'x': 50e-6,
                                 'y': 50e-6,
                                 'rot': 50e-6},
                  }

# define properties for each correction to run
traj_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'TRM' + correction_name + '.pkl'),
               'response_mode': 'numeric',
               'num_singular_vectors': 500,
               'number_of_turns': 1,
               'max_trajectory': (2.0e-3, 2.0e-3)}

orb_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'ORM' + correction_name + '.mat'),
              'response_mode': 'analytic',
              'num_singular_vectors': 300,
              'num_iterations': 5}

optics_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'J' + correction_name + '.pkl'),
                'response_mode': 'semi-analytic',  # 'numeric',  #'semi-analytic',
                'num_normal_quad_singular_vectors': 64,
                'num_skew_quad_singular_vectors': 64,
                'bpms_weights': None,
                'hor_disp_weight': 0.0,
                'ver_disp_weight': 0.0,
                'tune_weight': 1.0,
                'number_of_iterations': 4
                }

tune_traj_params = {}
bba_params = {}
noeco_params = {}

# define list of corrections to run (space and digits are ignored to allow for different dictionary keys)
config.dict_of_corrections = {'trajectory': traj_params,
                       'tune_from_trajectory': tune_traj_params,
                       'orbit 1': orb_params,
                       'parallel_beam_based_alignment': bba_params,
                       'tune 1': [],
                       'chromaticity 1': [],
                       'orbit 2': orb_params,
                       'optics': optics_params,
                       'orbit 3': orb_params,
                       'tune 2': [],
                       'chromaticity 2': [],
                       'noeco': noeco_params,
                       'optics 2': optics_params,
                        }

## RUN SIMULATION

# this import must be here, or above configurations will be ignored
from commissioningsimulations.run import run_commissioning_simulations

run_commissioning_simulations()

## plot existing data
from commissioningsimulations.summary_figures import summary_figures
# summary_figures(data_folder=config.main_folder_data, save_figures=True)

# run in pycharm
if __name__=='__main__':
    pass