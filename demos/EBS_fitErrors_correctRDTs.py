import os

# INPUT LATTICE SPECIFIC INFORMATIONS
# This needs to be done here before importing any other module of commissioning simulation package.
import commissioningsimulations.config as config

config.lattice_file_for_test = '/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/S28F_all_BM_27Mar2022/betamodel.mat'
config.lattice_variable_name = 'betamodel'

# name-pattern to find magnets in lattice
config.normal_quadrupoles_fam_name_pattern = 'Q*'
config.skew_quadrupoles_fam_name_pattern = 'S[HFDIJ]*'  # for simplicity steerers and skew at all sextupoles
config.sextupoles_fam_name_pattern = 'S[FDIJ]*'
config.octupoles_fam_name_pattern = 'O*'
config.correctors_fam_name_pattern = 'S[HFDIJ]*'
config.bpms_name_patter = 'BPM*'
config.dipoles_fam_name_pattern = '[JD]L*'

# for optics correction
config.correctors_for_optics_RM = 'SH1A'
config.normal_quadrupoles_for_optics_and_coupling_fit = 'Q[FD][345]*'
config.skew_quadrupoles_for_optics_and_coupling_fit = 'S[HFDIJ]*'
config.dipoles_for_optics_and_coupling_fit = '[JD]L*3'

# for tune and chromaticity
config.focussing_quadrupoles_for_tune = 'QF1*'
config.defocussing_quadrupoles_for_tune = 'QD2*'
config.focussing_sextupoles_for_chromaticity = 'SF2*'
config.defocussing_sextupoles_for_chromaticity = 'SD1*'

## INPUT SIMULATION SPECIFIC INFORMATIONS
# DA MA config
config.compute_DA_LT = False

# specify simulation details
lattice_name = 'EBS'
errors_name = 'DQS_50um'
correction_name = ''

config.main_folder_data = os.path.abspath('./' + lattice_name + '_Err' + errors_name + '_Cor' + correction_name)
config.folder_matrices = os.path.join(config.main_folder_data, 'Matrices')

config.N_seeds = 3

config.submission_mode = 'local'    # can be local (serial seeds, parallelized DA)
                                    #     or slurm (parallelized seed, serial DA)

config.slurm_cpus_per_task = 10       # is slurm submission mode, use multiprocessing on N cores for each task.
                                    # config.N_seeds x config.slurm_cpu_per_task cores will be used on the cluster

# to limit the number of cores used in local mode
# at.DConstant.patpass_poolsize = 10

# define properties to compute after correction (not fully exploited, still under development)
config.dict_of_evaluations = { }

# define errors to set
config.dict_of_errors = {
                    'DL*': {'x': 100e-6,
                         'y': 100e-6,
                         'rot': 100e-6},
                    'Q*': {'x': 50e-6,
                         'y': 50e-6,
                         'rot': 100e-6},
                    'S*': {'x': 50e-6,
                         'y': 50e-6,
                         'rot': 100e-6},
                    }

# define properties for each correction to run
traj_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'TRM' + correction_name + '.pkl'),
               'response_mode': 'numeric',
               'number_of_turns': 3,
               'num_singular_vectors': 100,
               'max_trajectory': (2.0e-3, 2.0e-3)}

orb_params = {'response_file': os.path.join(os.path.abspath(config.folder_matrices), 'ORM' + correction_name + '.mat'),
              'response_mode': 'analytic',
              'num_singular_vectors': 120,
              'num_iterations': 5}

fit_optics_params = {'use_mp': True,
                 'verbose': True,
                 'fit_params': {
                 'dipoles': {
                            'niter': 2,
                            'neigDipo': 32,
                            'bpms_weights': None,
                            'mode': 'numeric',
                            'filename_response':os.path.join(os.path.abspath(config.folder_matrices),
                                                               'J' + correction_name + '_DIPS_.pkl'),
                            'reference_disp': None,
                            'normal': True,
                            'skew': False
                            },
             'quadrupoles': {
                            'niter': 2,
                            'neigQuad': 96,
                            'neigSkew': 96,
                            'bpms_weights': None,
                            'hor_disp_weight': 100.0,
                            'ver_disp_weight': 100.0,
                            'tune_weight': 1.0,
                            'mode': 'analytic',
                            'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                                       'J' + correction_name + '_QUADS_analytic.pkl'),
                            'reference_orm': None,
                            'normal': True,
                            'skew': True
                           },
                 'bpms': {'niter': 2,
                          'neig_scale': 320,
                          'neig_rot': 320,
                          'bpms_weights': None,
                          'hor_disp_weight': 1.0,
                          'ver_disp_weight': 1.0,
                          'mode': 'numeric',
                          'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                            'J' + correction_name + '_BPMS_.pkl'),
                          'reference_orm': None,
                          'horizontal': True,
                          'vertical': True,
                          'rotation': True}
                 }  # END OF FIT_PARAMS
    }  # END OF OPTICS_PARAMS

rdt_params = {'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                            'RDTs_RM_' + correction_name + '.pkl'),
              'niter': 3,
              'neigQuad': 96,
              'neigSkew': 96,
              'use_mp': False,
              'verbose': True,
              'tune_weight': 1.0,
              'hor_disp_weight': 0.5,
              'ver_disp_weight': 0.8
              }

tune_traj_params = {}
bba_params = {}
noeco_params = {}

# define list of corrections to run (space and digits are ignored to allow for different dictionary keys)
config.dict_of_corrections = {
                       'trajectory': traj_params,
                       'orbit 1': orb_params,
                       'tune 1': [],
                       'chromaticity 1': [],
                       'orbit 2': orb_params,
                       'errorsmodel': fit_optics_params,  # <- fit optics model and use fitted errors for correction
                       'optics': {'mode': rdt_params}, # use fitted model to compute RDTs to correct
                       'orbit 3': orb_params,
                       'tune 2': [],
                       'chromaticity 2': [],
                       }

## RUN SIMULATION

# this import must be here, or above configurations will be ignored
from commissioningsimulations.run import run_commissioning_simulations

run_commissioning_simulations()

## plot existing data
# from commissioningsimulations.summary_figures import summary_figures
# summary_figures(data_folder=config.main_folder_data, save_figures=True)

# run in pycharm
if __name__ == '__main__':
    pass