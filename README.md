# CommissioningSimulations
1. Assigns **errors** 
1. compute **corrections**
1. compute **DA, MA, TLT, optics parameters**
1. builds figures to summarize the output
1. saves data in an easy to access structure
1. may add/remove/recompute seeds

## running modes: 
**local** mode for parallel DA and serialized seeds

**slurm** mode for parallel seeds and serialized DA

## installation:
1. activate your favourite virtual environement
1. clone this repository to any location
1. navigate to this location, cd CommissioningSimulations
1. `pip install -e .`

## detailed installation steps:
1. cd /your/working/folder/
1. conda deactivate
1. deactivate
1. python3.9 -m venv my_venv
1. source my_venv/bin/activate
1. pip install --upgrade pip
    * -- AT latest release
    pip install accelerator-toolbox
    * -- OR AT MASTER (developpers, latest version)
    pip install git+https://github.com/atcollab/at.git@master
1. install this package (commissioningsimulations)
    1. git clone https://gitlab.esrf.fr/BeamDynamics/commissioningsimulations.git
    1. cd commissioningsimulations/
    1. rm -rf build
    1. rm -rf commissioningsimulations.egg-info
    1. pip install .
    1. cd ..


## to un-install
1. activate your favourite virtual environement
1. `pip uninstall commissioningsimulations` 

## Getting started
edit `commissioningsimulations/config.py` with the parameters of your lattice
run `Example.py`

## Data saved
MAIN DATA FOLDER is structures as below: 

```
.
├── Design                              # FOLDER of data for reference Lattice
│   ├── DATLTDesign.pkl                     # data of DA and LT
│   ├── DADesign.png                        # DA image
│   ├── LatticeDesign.pkl                   # pyAT lattice file
│   ├── TLTDesign.png                       # MA image
│   └── LatticeDesignData.pkl               # optics parameters file
├── Matrices                            # FOLDER of data for response matrices
│   ├── TRM.mat                             # trajectory response
│   ├── ORM.mat                             # orbit response
│   ├── Jnumeric.mat                        # derivative of ORM for optics correction
│   └── ...
├── Seed000                             # FOLDER of data for Seed000
│   ├── Errors.pkl                          # errors assigned to this Seed 0
│   ├── Errors.txt                          # printed verison of errors assigned to seed 0
│   ├── LatticeErrors.pkl                   # pyAT lattice with errors in key rerr
│   ├── LatticeErrorData.pkl                # optics parameters file for lattice with errors
│   ├── LatticeCorrected.pkl                # pyAT lattice with errors and correction in key rcor
│   ├── LatticeCorrectedData.pkl            # optics parameters file for lattice with errors and correction
│   ├── DAErrors.png                        # DA image
│   ├── DATLTErrors.pkl                     # data of DA and LT with errors
│   ├── TLTErrors.png                       # MA image with errors
│   ├── DACorrected.png                     # data of DA and LT with errors and corrections
│   ├── DATLTCorrected.pkl                  # data of DA and LT with errors and correction
│   ├── DA_compare.png                      # DA image comparing design, errors and errors+correction
│   ├── TLTCorrected.png                    # MA image with errors and correction
│   ├── ComparativeData.pkl                 # data compared to Design
│   ├── ComparativeData.txt                 # printed version of data compared to Design
│   └── MA_compare.png                      # MA image comparing design, errors and errors+correction
├── Seed002                             
├── Seed003                             # same structure as Seed000 folder
├── ...
├── Seed###
├── CorrectionParameters.pkl            # global correction parameters
├── params.txt                          # slurm submission file of parameters
├── sbatch.sh                           # slurm submission file
└── SummaryFigures                      # FOLDER of figures summarizing the above data
    ├── DA.png
    ├── LT.png
    ├── MA.png
    ├── Beta.png
    ├── COD.png
    ├── DISP.png
    ├── EMIT.png
    └── TUNE.png
```

# bibtex citation

@article{Liuzzo_2024,
doi = {10.1088/1742-6596/2687/3/032001},
url = {https://dx.doi.org/10.1088/1742-6596/2687/3/032001},
year = {2024},
month = {jan},
publisher = {IOP Publishing},
volume = {2687},
number = {3},
pages = {032001},
author = {S. M. Liuzzo and N. Carmignani and L.R. Carver and L. Hoummi and T. Perron and S. White and I. Agapov and M. Boese and T. Hellert and J. Keil and L. Malina and E. Musa and B. Veglia},
title = {Commissioning simulations tools based on python Accelerator Toolbox},
journal = {Journal of Physics: Conference Series}
}

@inproceedings{franchi:ipac23-mopl069,
    author = {A. Franchi and S. Liuzzo and Z. Martí},
    title = {{Analytic derivative of orbit response matrix and dispersion with thick error sources and thick steerers implemented in python}},
    booktitle = {Proc. IPAC'23},
    pages = {705--707},
    paper = {MOPL069},
    venue = {Venice, Italy},
    series = {International Particle Accelerator Conference},
    number = {14},
    publisher = {JACoW Publishing, Geneva, Switzerland},
    month = {9},
    year = {2023},
    issn = {2673-5490},
    isbn = {978-3-95-450231-8},
    language = {english}
}

