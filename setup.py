from setuptools import setup, find_packages

setup(
    name='commissioningsimulations',
    version='0.1.0',
    description='pyAT commissioning simulations',
    url='https://gitlab.esrf.fr/BeamDynamics/commissioningsimulations.git',
    author='Simone Maria Liuzzo, Lee Robert Carver, Andrea Franchi, Simon White, Lina Hoummi',
    author_email='simone.liuzzo@esrf.fr, lee.carver@esrf.fr, andrea.franchi@esrf.fr, simon.white@esrf.fr, lina.hoummi@esrf.fr',
    license='BSD 2-clause',
    packages=find_packages(),
    python_requires='>=3.8',
    install_requires=['accelerator-toolbox',
                      'matplotlib',
                      'numpy'
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)
