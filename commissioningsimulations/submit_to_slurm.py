import os
import time
import glob
from os.path import exists
import commissioningsimulations.config as config
from commissioningsimulations import SingleSeed


def submit_to_slurm(correction_parameters_file, main_folder_data, N_seeds):
    """
    creates a param.txt file and an sbash.sh file to submit 1 seed to each core in a slurm computing cluster.

    :param correction_parameters_file:
    :param main_folder_data:
    :param N_seeds:
    :return:
    """
    # create a params file
    with open(os.path.join(main_folder_data, 'params.txt'), 'w') as p:
        for seed in range(-1, N_seeds):
            p.write('{} {}\n'.format(os.path.abspath(correction_parameters_file), seed))

    # create a slurm submission script
    os.system('rm -rf {}'.format(os.path.join(main_folder_data, 'slurm*.out')))  # remove previous slurm out files
    os.system('rm -rf {}'.format(os.path.join(main_folder_data, 'sbatch.sh')))  # clear existing submission scripts

    name = main_folder_data.split('/')[-1]

    with open(os.path.join(main_folder_data, 'sbatch.sh'), 'w') as sbatch:
        sbatch.write('#!/bin/bash -l\n')
        sbatch.write(f'#SBATCH --time {config.slurm_time_minutes}\n')
        sbatch.write(f'#SBATCH --partition {config.slurm_partition_name}\n')
        sbatch.write(f'#SBATCH --job-name {name}\n')
        sbatch.write(f'#SBATCH --cpus-per-task {config.slurm_cpus_per_task}\n')
        sbatch.write(f'#SBATCH --array 1-{N_seeds+1}\n')
        sbatch.write(f'source {config.venv} \n')
        sbatch.write(f'python {os.path.abspath(SingleSeed.__file__)} $(sed -n ${{SLURM_ARRAY_TASK_ID}}p params.txt)\n')

    # get present location
    curdir = os.getcwd()

    # move to main directory
    os.chdir(main_folder_data)

    # submit cluster jobs
    submission_start_time = time.time()
    os.chmod('sbatch.sh', 777)
    os.system('sbatch sbatch.sh')
    print('slurm jobs submitted')
    print(f'{(config.N_seeds +1) * config.slurm_cpus_per_task} cores will be used.')

    # back to initial location
    os.chdir(curdir)

    # wait for jobs to finish, looking at data.mat files created
    all_data_found=False
    while not(all_data_found):
        found_data_files = []
        for s in range(N_seeds):
            found_data_files.append(exists( os.path.join(main_folder_data, 'Seed{:03}'.format(s), 'ComparativeData.pkl')))
        found_data_files.append(
            exists(os.path.join(main_folder_data, 'Design', 'LatticeDesignData.pkl')))
        tot_data_files = sum(found_data_files)
        all_data_found = tot_data_files == N_seeds+1
        print('{} / {} seeds completed. Please wait ...(or Ctrl-C and look for saved data later)'.format(tot_data_files, N_seeds+1))
        if tot_data_files<(N_seeds+1):
            time.sleep(10)

    '''
    # not working... 
    # move slurm output to corresponding seed folder
    print(glob.glob(os.path.join(main_folder_data, 'slurm-*.out')))
    slurm_out_files = glob.glob(os.path.join(main_folder_data, 'slurm-*.out'))
    try:
        os.rename(os.path.join(main_folder_data, slurm_out_files[0]),
                  os.path.join(main_folder_data, 'Design'.format(s), slurm_out_files[0]))

        for s in range(N_seeds):
            os.rename(os.path.join(main_folder_data, slurm_out_files[s+1]),
                      os.path.join(main_folder_data, 'Seed{:03}'.format(s), slurm_out_files[s+1]))
    except Exception:
        print('slurm files not found')
    '''

    submission_end_time = time.time()
    print('All slurm jobs for {} are completed in {} seconds'.format(main_folder_data,
                                                                     submission_end_time - submission_start_time))


