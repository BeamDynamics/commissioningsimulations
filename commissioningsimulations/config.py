# python environment
venv = '/machfs/liuzzo/FCC/fcc_py38_venv/bin/activate'

# at DC constants
XYStep = 3.e-8           # Coordinate step for differentiation
DPStep = 3.e-6           # Momentum step for dispersion and chromaticity
OrbConvergence = 1.e-12  # Convergence criterion for orbit
OrbMaxIter = 20          # orbit search iterations
patpass_poolsize = None  # cores used by pyat parallel tracking

# global configurations
N_seeds = 0
submission_mode = 'local'

# slurm
slurm_partition_name = ''  # 'low,asd,nice-long'
slurm_time_minutes = 1439  # 24 hours (-1 minute)
slurm_cpus_per_task = 1  # each slurm task will have these CPUS for parallel computations (python multiprocessing)

# file output
main_folder_data = '.'
folder_matrices = '.'
save_matlab_files = False
save_figures = True
compute_DA_LT = False
compute_sensitivity = False
compute_survey = False
tapering_effect = False

use_mp = True
radiation = None  # force radiation state, regardless of input lattice. if None, do nothing
tapering = False  # force tapering state


# lattice file used for simluations
lattice_file_for_test = '/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/S28F_all_BM_27Mar2022/betamodel.mat'
lattice_variable_name = 'betamodel'

# reference lattice  (this file may be different from the one used for simulations) None means, use lattice_file_for_test
reference_lattice_file = None
reference_lattice_variable_name = None


use_regexp_for_indexes = False   # fnmatch is the default. ALL strings must be either fnmatch or regexp

# name-pattern to find magnets in lattice (can be a list of patterns)
normal_quadrupoles_fam_name_pattern = 'Q*'
skew_quadrupoles_fam_name_pattern = 'S[HFDIJ]*'
sextupoles_fam_name_pattern = 'S[FDIJ]*'
octupoles_fam_name_pattern = 'O*'
correctors_fam_name_pattern = 'S[FDIJH]*'
bpms_name_patter = 'BPM*'
dipoles_fam_name_pattern = '[JD][LQ]*'

# name patterns for tune correction
focussing_quadrupoles_for_tune = 'QF1*'
defocussing_quadrupoles_for_tune = 'QD2*'

# name patterns for chromaticity correction
focussing_sextupoles_for_chromaticity = 'S[FIJ]2*'
defocussing_sextupoles_for_chromaticity = 'S[DIJ]1*'

# name patterns for model optics fit
correctors_for_optics_RM = 'S[IJH]1A*'
normal_quadrupoles_for_optics_and_coupling_fit = 'Q[FD][345]*'
skew_quadrupoles_for_optics_and_coupling_fit = 'S[HFIJ]*'
dipoles_for_optics_and_coupling_fit = '[JD]L*_3'

# quadrupole name patterns for optics correction
normal_quadrupoles_for_optics_and_coupling_correction = 'Q[FD][345]*' #'Q[FD][3]*'
skew_quadrupoles_for_optics_and_coupling_correction = 'S[HFIJ]*' #'S[F]*'

#
dict_of_errors={}
dict_of_corrections={}
dict_of_evaluations={}


# for slurm-cluster submission
def save_configuration_file(filename):

    import pickle
    dict_config = {'N_seeds': N_seeds,
                   'venv': venv,
                   'XYStep': XYStep,
                   'DPStep': DPStep,
                   'OrbConvergence': OrbConvergence,
                   'OrbMaxIter': OrbMaxIter,
                   'patpass_poolsize': patpass_poolsize,
                   'slurm_cpus_per_task': slurm_cpus_per_task,
                   'use_mp': use_mp,
                   'main_folder_data': main_folder_data,
                   'folder_matrices': folder_matrices,
                   'save_matlab_files': save_matlab_files,
                   'save_figures': save_figures,
                   'compute_DA_LT': compute_DA_LT,
                   'compute_sensitivity': compute_sensitivity,
                   'compute_survey': compute_survey,
                   'tapering_effect': tapering_effect,
                   'lattice_file_for_test': lattice_file_for_test,
                   'lattice_variable_name': lattice_variable_name,
                   'reference_lattice_file': reference_lattice_file,
                   'reference_lattice_variable_name': reference_lattice_variable_name,
                   'use_regexp_for_indexes': use_regexp_for_indexes,
                   'normal_quadrupoles_fam_name_pattern': normal_quadrupoles_fam_name_pattern,
                   'skew_quadrupoles_fam_name_pattern': skew_quadrupoles_fam_name_pattern,
                   'correctors_fam_name_pattern': correctors_fam_name_pattern,
                   'bpms_name_patter': bpms_name_patter,
                   'sextupoles_fam_name_pattern': sextupoles_fam_name_pattern,
                   'octupoles_fam_name_pattern': octupoles_fam_name_pattern,
                   'correctors_for_optics_RM': correctors_for_optics_RM,
                   'normal_quadrupoles_for_optics_and_coupling_fit': normal_quadrupoles_for_optics_and_coupling_fit,
                   'skew_quadrupoles_for_optics_and_coupling_fit': skew_quadrupoles_for_optics_and_coupling_fit,
                   'normal_quadrupoles_for_optics_and_coupling_correction': normal_quadrupoles_for_optics_and_coupling_correction,
                   'skew_quadrupoles_for_optics_and_coupling_correction': skew_quadrupoles_for_optics_and_coupling_correction,
                   'dipoles_for_optics_and_coupling_fit': dipoles_for_optics_and_coupling_fit,
                   'dipoles_fam_name_pattern': dipoles_fam_name_pattern,
                   'focussing_sextupoles_for_chromaticity': focussing_sextupoles_for_chromaticity,
                   'defocussing_sextupoles_for_chromaticity': defocussing_sextupoles_for_chromaticity,
                   'focussing_quadrupoles_for_tune': focussing_quadrupoles_for_tune,
                   'defocussing_quadrupoles_for_tune': defocussing_quadrupoles_for_tune,
                   'dict_of_evaluations': dict_of_evaluations,
                   'dict_of_corrections': dict_of_corrections,
                   'dict_of_errors': dict_of_errors}

    pickle.dump(dict_config, open(filename, 'wb'))

    pass


def load_configuration_file(filename, config):
    import pickle

    conf = pickle.load(open(filename, 'rb'))

    config.N_seeds = conf['N_seeds']
    config.venv = conf['venv']
    config.XYStep = conf['XYStep']
    config.DPStep = conf['DPStep']
    config.OrbConvergence = conf['OrbConvergence']
    config.OrbMaxIter = conf['OrbMaxIter']
    config.patpass_poolsize = conf['patpass_poolsize']
    config.slurm_cpus_per_task = conf['slurm_cpus_per_task']
    config.use_mp = conf['use_mp']
    config.N_seeds = conf['N_seeds']
    config.main_folder_data = conf['main_folder_data']
    config.folder_matrices = conf['folder_matrices']
    config.save_matlab_files = conf['save_matlab_files']
    config.save_figures = conf['save_figures']
    config.compute_DA_LT = conf['compute_DA_LT']
    config.compute_sensitivity = conf['compute_sensitivity']
    config.compute_survey = conf['compute_survey']
    config.tapering_effect = conf['tapering_effect']
    config.lattice_file_for_test = conf['lattice_file_for_test']
    config.lattice_variable_name = conf['lattice_variable_name']
    config.reference_lattice_file = conf['reference_lattice_file']
    config.reference_lattice_variable_name = conf['reference_lattice_variable_name']
    config.use_regexp_for_indexes = conf['use_regexp_for_indexes']
    config.normal_quadrupoles_fam_name_pattern = conf['normal_quadrupoles_fam_name_pattern']
    config.skew_quadrupoles_fam_name_pattern = conf['skew_quadrupoles_fam_name_pattern']
    config.correctors_fam_name_pattern = conf['correctors_fam_name_pattern']
    config.bpms_name_patter = conf['bpms_name_patter']
    config.sextupoles_fam_name_pattern = conf['sextupoles_fam_name_pattern']
    config.octupoles_fam_name_pattern = conf['octupoles_fam_name_pattern']
    config.correctors_for_optics_RM = conf['correctors_for_optics_RM']
    config.normal_quadrupoles_for_optics_and_coupling_fit = conf['normal_quadrupoles_for_optics_and_coupling_fit']
    config.skew_quadrupoles_for_optics_and_coupling_fit = conf['skew_quadrupoles_for_optics_and_coupling_fit']
    config.normal_quadrupoles_for_optics_and_coupling_correction = conf['normal_quadrupoles_for_optics_and_coupling_correction']
    config.skew_quadrupoles_for_optics_and_coupling_correction = conf['skew_quadrupoles_for_optics_and_coupling_correction']
    config.dipoles_for_optics_and_coupling_fit = conf['dipoles_for_optics_and_coupling_fit']
    config.dipoles_fam_name_pattern = conf['dipoles_fam_name_pattern']
    config.focussing_sextupoles_for_chromaticity = conf['focussing_sextupoles_for_chromaticity']
    config.defocussing_sextupoles_for_chromaticity = conf['defocussing_sextupoles_for_chromaticity']
    config.focussing_quadrupoles_for_tune = conf['focussing_quadrupoles_for_tune']
    config.defocussing_quadrupoles_for_tune = conf['defocussing_quadrupoles_for_tune']
    config.dict_of_evaluations = conf['dict_of_evaluations']
    config.dict_of_corrections = conf['dict_of_corrections']
    config.dict_of_errors = conf['dict_of_errors']

    return conf