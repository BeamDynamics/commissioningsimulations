import at
from commissioningsimulations.get_indexes import get_lattice_indexes
import numpy
from typing import Optional
from at.lattice import Lattice, Refpts, set_value_refpts
from at.lattice import AtWarning, AtError
from at.physics import get_tune, get_chrom


def correct_tune(reference_ring, ring_to_correct, indexes, KStep=None, jacobian=None):

    if not indexes:
        indexes = get_lattice_indexes(reference_ring)

    desired_tune = at.get_tune(reference_ring, get_integer=True)
    initial_tune = at.get_tune(ring_to_correct, get_integer=True)
    print(f'tunes: initial {initial_tune}')
    print(f'tunes: desired {desired_tune}')

    jacobian = fit_tune(ring_to_correct,
                indexes['focussing_quadrupoles_for_tune'],
                indexes['defocussing_quadrupoles_for_tune'],
                desired_tune[0:2], KStep=KStep, jacobian=jacobian)

    final_tune = at.get_tune(ring_to_correct, get_integer=True)

    # print('tunes: initial {}'.format(initial_tune))
    print(f'tunes: final {final_tune}')
    # print('tunes: desired {}'.format(desired_tune))

    return ring_to_correct, jacobian


def _get_resp(ring: Lattice, index: int, func, refpts, attname,
              delta, dp, regex=False, **kwargs):
    set_value_refpts(ring, refpts, attname, delta, index=index,
                     increment=True, regex=regex)
    datap = func(ring, dp, **kwargs)
    set_value_refpts(ring, refpts, attname, -2 * delta, index=index,
                     increment=True, regex=regex)
    datan = func(ring, dp, **kwargs)
    set_value_refpts(ring, refpts, attname, delta, index=index,
                     increment=True, regex=regex)
    data = numpy.subtract(datap, datan) / (2 * delta)
    return data

def _fit_tune_chrom(ring: Lattice, index: int, func,
                    refpts1: Refpts, refpts2: Refpts, newval,
                    tol: Optional[float] = 1.0e-12,
                    dp: Optional[float] = 0, niter: Optional[int] = 3,
                    delta: Optional[float] = None,
                    regex=False, jacobian=None, **kwargs):
    def _fit(ring, index, func, refpts1, refpts2, newval, J,
             dp: Optional[float] = 0, regex=False, **kwargs):
        val = func(ring, dp, **kwargs)
        dk = numpy.linalg.solve(J, numpy.subtract(newval, val))
        set_value_refpts(ring, refpts1, 'PolynomB', dk[0], index=index,
                         increment=True, regex=regex)
        set_value_refpts(ring, refpts2, 'PolynomB', dk[1], index=index,
                         increment=True, regex=regex)
        val = func(ring, dp, **kwargs)
        sumsq = numpy.sum(numpy.square(numpy.subtract(val, newval)))
        return sumsq

    if delta is None:
        delta = 1.e-6 * 10 ** index

    if jacobian==None:
        dq1 = _get_resp(ring, index, func, refpts1, 'PolynomB',
                        delta, dp, regex=regex, **kwargs)
        dq2 = _get_resp(ring, index, func, refpts2, 'PolynomB',
                        delta, dp, regex=regex, **kwargs)
        J = [[dq1[0], dq2[0]], [dq1[1], dq2[1]]]
    else:
        J = jacobian

    n = 0
    sumsq = tol + 1
    print('Initial value', func(ring, dp, **kwargs))
    while sumsq > tol and n < niter:
        sumsq = _fit(ring, index, func, refpts1, refpts2, newval,
                     J, dp=dp, regex=regex, **kwargs)
        print('iter#', n, 'Res.', sumsq)
        n += 1
    print('Final value', func(ring, dp, **kwargs), '\n')

    return J

def _get_tune(ring: Lattice, dp: float, **kwargs):
    get_integer = kwargs.pop('fit_integer', False)
    return get_tune(ring, dp=dp, get_integer=get_integer)[0:2]


def fit_tune(ring: Lattice, refpts1: Refpts, refpts2: Refpts, newval,
             tol: float = 1.0e-12,
             dp: Optional[float] = 0, niter: int = 3, regex=False,
             KStep: Optional[float] = None,
             jacobian: Optional[list] = None,
             **kwargs) -> None:
    """Fits the tunes using 2 families

    Args:
        ring:       Lattice description
        refpts1:    Selection of the 1st family
        refpts2:    Selection of the 2nd family
        newval:     New tunes, in case an non-zero integer part
                    is provided, fit_integer is set to True

    Keyword arguments:
        tol:        Tolerance for the matching; Default: 1.0e-12
        dp:         Momentum deviation. Default: 0
        niter:      Maximum number of iterations. Default 3
        fit_integer: bool (default=False), use integer tune
        regex:      Using regular expressions for refpt string matching;
                    Default: False
        KStep: gradient variation applied to magnets. Default 1e-5

    Typical usage:
    at.fit_tune(ring, refpts1, refpts2, [0.1,0.25])
    """
    print('\nFitting Tune...')
    if numpy.any(numpy.floor(newval) != 0.0):
        kwargs['fit_integer'] = True
    J = _fit_tune_chrom(ring, 1, _get_tune, refpts1, refpts2, newval, tol=tol,
                        dp=dp, niter=niter, regex=regex, delta=KStep, jacobian=jacobian, **kwargs)
    return J