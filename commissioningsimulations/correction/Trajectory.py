import at
import os
import numpy as np
import pickle
import copy
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy.io import savemat, loadmat
from os.path import exists
import math
from at.plot import plot_beta
import time
import multiprocessing
from itertools import repeat
import commissioningsimulations.config as config
from commissioningsimulations.errors.find_trajectory_bpm_errors import find_trajectory_bpm_errors
from commissioningsimulations.errors.find_orbit_bpm_errors import find_orbit_bpm_errors
from commissioningsimulations.errors.bpm_modifications import bpm_matrices


folder_data='.'
force_matrix_computation = False
filename_response = folder_data+'/TRM.mat'
dcorL = 1e-9  # 1 microrad steerers variation


def compute_analytic_trajectory_response_matrix(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=dcorL,
        verbose=True,
        nturns=3,
        filename_response='AnalyticTRM'):
    """

    DO NOT USE.

    :param ring:
    :param nturns: number of turns for trajectory
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param verbose:
    :param filename_response:
    :return:
    """

    raise Exception('NOT WRITTEN YET')
    
    return MH, MV, uH, sH, vH, uV, sV, vV


def plot_response_matrix(m):

    s = m.shape

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    # Make data.
    X = np.arange(0, s[0])
    Y = np.arange(0, s[1])
    X, Y = np.meshgrid(X, Y)

    # Plot the surface.
    surf = ax.plot_surface(X, Y, m.T, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    plt.show()

    return


def OneSteererResponse(ring, dcorL, orb_ref, ind_bpms, nturns, ic,
                       verbose=True,
                       rel=None, tel=None, trand=None,
                       input_coordinates=(0, 0, 0, 0, 0, 0)
                       ):
    # display status

    L = ring[ic].Length
    thin_flag = ''

    if np.isnan(L) or L==0.0:
        L = 1  # assume thin correctors
        thin_flag='thin '

    # display status
    if verbose:
        print('computing response of {}steerer '
              '{} to Trajectory'
              ''.format(thin_flag, ring[ic].FamName))#, end="\r")

    # vary Horizontal corrector
    initcor = ring[ic].PolynomB[0]
    ring[ic].PolynomB[0] = initcor + dcorL / L

    # compute orbit with steerer
    orb_cor = find_trajectory_bpm_errors(ring, ind_bpms,
                                         nturns=nturns,
                                         rel=rel, tel=tel, trand=trand,
                                         input_coordinates=input_coordinates)

    # restore corrector
    ring[ic].PolynomB[0] = initcor

    # divide by deltacor
    # store in matrix
    H = (orb_cor[0, :] - orb_ref[0, :]) / dcorL

    # vary Vertical corrector
    initcor = ring[ic].PolynomA[0]
    ring[ic].PolynomA[0] = initcor + dcorL / L

    # compute orbit with steerer
    orb_cor = find_trajectory_bpm_errors(ring, ind_bpms,
                                         nturns=nturns,
                                         rel=rel, tel=tel, trand=trand,
                                         input_coordinates=input_coordinates)

    # restore corrector
    ring[ic].PolynomA[0] = initcor

    # divide by deltacor
    # store in matrix
    V = (orb_cor[2, :] - orb_ref[2, :]) / dcorL

    return H, V


def compute_trajectory_response_matrix(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=dcorL,
        verbose=True,
        nturns=3,
        use_mp=False,
        n_processes=None,
        filename_response='./TRM.mat',
        input_coordinates=(0, 0, 0, 0, 0, 0)):
    """

    compute numeric trajectory response matrix

    :param ring:
    :param nturns: number of turns for trajectory
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param verbose:
    :param filename_response:
    :return:
    """
    # compute reference orbit
    rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once
    orb_ref = find_trajectory_bpm_errors(ring, ind_bpms,
                                         nturns=nturns,
                                         rel=rel, tel=tel, trand=trand,
                                         input_coordinates=input_coordinates)

    MH = np.zeros(shape=(len(orb_ref[0, :]), len(ind_cor)))
    MV = np.zeros(shape=(len(orb_ref[0, :]), len(ind_cor)))

    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        if n_processes is None:
            n_processes = n_cpu
        else:
            max_proc = min(n_cpu, len(ind_cor))
            if n_processes > max_proc:
                n_processes = max_proc

        print('parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool(processes=n_processes) as p:
            results = p.starmap(OneSteererResponse,
                                zip(repeat(ring), repeat(dcorL), repeat(orb_ref),
                                    repeat(ind_bpms), repeat(nturns), ind_cor, repeat(verbose),
                                    repeat(rel), repeat(tel), repeat(trand),
                                    repeat(input_coordinates)))

        for count, ic in enumerate(ind_cor):
            MH[:, count] = results[count][0]
            MV[:, count] = results[count][1]

    else:
        for count, ic in enumerate(ind_cor):
            H, V = OneSteererResponse(ring, dcorL, orb_ref, ind_bpms, nturns, ic,
                                      verbose=verbose,
                                      rel=rel, tel=tel, trand=trand,
                                      input_coordinates=input_coordinates)
            MH[:, count] = H
            MV[:, count] = V

    uH, sH, vH = np.linalg.svd(MH, full_matrices=False)
    uV, sV, vV = np.linalg.svd(MV, full_matrices=False)

    mdict = {'uH': uH, 'sH': sH, 'vH': vH,
             'uV': uV, 'sV': sV, 'vV': vV,
             'MH': MH, 'MV': MV,
             'ind_bpms': ind_bpms,
             'cors': ind_cor,
             'ring': ring,
             'nturns': nturns}

    file_no_ext, exten_file = os.path.splitext(filename_response)
    savemat(file_no_ext + '.mat', mdict=mdict)
    pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

    while not(exists(filename_response)):
        print('Wait for file {} to be saved'.format(filename_response))
        time.sleep(1)

    print('Response saved in: {}'.format(filename_response))

    return MH, MV, uH, sH, vH, uV, sV, vV


def correct_trajectory(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=dcorL,
        neig=0,
        nturns=3,
        filename_response=filename_response,
        force_matrix_computation=False,
        max_trajectory=(1e-3, 1e-3),
        reference_trajectory=None,
        maximum_iterations=50,
        input_coordinates=(0, 0, 0, 0, 0, 0),
        check_for_cod_from_iteration=0
        ):
    """
    corrects the trajectory of the first few turns till a closed orbit is found.


    :param ring: AT lattice 
    :param ind_bpms: index of BPMs in ring
    :param ind_cor: index of correctors in ring
    :param dcorL: variation of
    :param neig: number of singular values to use for trajectory correction
    :param filename_response: file with response matrix to use for correction
    :param force_matrix_computation: force to recompute response, even if available in file
    :param max_trajectory: maximum accepted value of trajectory at BPMs
    :param reference_trajectory:
    :return:
    """

    # check that indexes are iterables
    try:
        val = iter(ind_bpms)
    except TypeError as te:
        print(ind_bpms, 'is not iterable')

    try:
        val = iter(ind_cor)
    except TypeError as te:
        print(ind_cor, 'is not iterable')

    # make a copy of the input lattice
    rcor = copy.deepcopy(ring)

    # get trajectory
    rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once
    orb_init = find_trajectory_bpm_errors(rcor, ind_bpms,
                                          nturns=nturns,
                                          rel=rel, tel=tel, trand=trand,
                                          input_coordinates=input_coordinates)

    # check singular values input
    max_neig = min(len(ind_bpms), len(ind_cor))
    if neig > max_neig:
        neig = max_neig
        print('too many singular vectors requested. setting to maximum: {}'.format(max_neig))

    # get Orbit Response Matrix

    if (not exists(filename_response)) or force_matrix_computation:
        # MHa, MVa, _, sHa, _, _, sVa, _ = compute_analytic_orbit_response_matrix(ring, ind_bpms, ind_cor, dcorL,
        #                                                                         'Analytic' + filename_response)
        MH, MV, _,  sH,  _,  _,  sV,  _ = compute_trajectory_response_matrix(ring,
                                                                             ind_bpms=ind_bpms,
                                                                             ind_cor=ind_cor,
                                                                             nturns=nturns,
                                                                             dcorL=dcorL,
                                                                             filename_response=filename_response,
                                                                             input_coordinates=input_coordinates)

    else:

        file_no_ext, exten_file = os.path.splitext(filename_response)

        if exten_file == '.mat':

            mat_contents = loadmat(filename_response)
            MH = mat_contents['MH']
            MV = mat_contents['MV']
            sH = np.transpose(mat_contents['sH'])  # load mat transposes raws/columns for some reason
            sH = sH[:, 0]
            sV = np.transpose(mat_contents['sV'])  # load mat transposes raws/columns for some reason
            sV = sV[:, 0]

        elif exten_file=='.pkl':

            mat_contents = pickle.load(open(filename_response, 'rb'))
            MH = mat_contents['MH']
            MV = mat_contents['MV']
            sH = mat_contents['sH']
            sV = mat_contents['sV']

        else:

            raise Exception(f'files should be .pkl or .mat')

        print(f'Loaded: {filename_response}')

        # check that size of loaded RM is the correct one
        exp_size = (len(orb_init[1, :]), len(ind_cor))
        if MH.shape != exp_size:
            raise ValueError(
                'Horizontal Orbit Response Matrix shape is incorrect. Expected {}, found {}'.format(exp_size, MH.shape))
        if MV.shape != exp_size:
            raise ValueError(
                'Vertical Orbit Response Matrix shape is incorrect. Expected {}, found {}'.format(exp_size, MV.shape))

    # get initial steerers values
    initcorH = at.get_value_refpts(rcor, ind_cor, 'PolynomB', 0)
    initcorV = at.get_value_refpts(rcor, ind_cor, 'PolynomA', 0)
    dcH = np.zeros(len(ind_cor))
    dcV = np.zeros(len(ind_cor))

    if reference_trajectory is None:
        reference_trajectory = np.zeros(orb_init.shape)
        # # use T2 at BPMs as reference orbit: NO! this is done inside find_trajectory_bpm_errors. Reference trajectory is zero.
        # reference_orbit = np.zeros((6, len(ind_bpms)))
        # reference_orbit[0, :] = [el.T2[0] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]
        # reference_orbit[2, :] = [el.T2[2] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]
        # reference_trajectory = copy.deepcopy(reference_orbit)
        # for it in range(nturns - 1):
        #     reference_trajectory = np.concatenate((reference_trajectory, reference_orbit), axis=1)

    # get BPM and steerers s positions
    s_cors = at.get_s_pos(ring, ind_cor)
    s_bpms = at.get_s_pos(ring, ind_bpms)

    # iterate correction till closed orbit is found.
    # for correction_iteration in range(0, niter):
    if check_for_cod_from_iteration == 0:
        _, o0 = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=input_coordinates)

        found_cod_orbit = not np.isnan(o0[0][0])
    else:
        found_cod_orbit = False

    correction_iteration = 0
    while (found_cod_orbit is False) and (correction_iteration < maximum_iterations):

        # get Closed Orbit
        orb_bef = find_trajectory_bpm_errors(rcor, ind_bpms,
                                             nturns=nturns,
                                             rel=rel, tel=tel, trand=trand,
                                             input_coordinates=input_coordinates)
        traj_length = len(orb_bef[0, :])

        # chose bpms to use along trajectory for the first nturns
        use_bpm = [False] * traj_length
        ind = 0
        for x, y in zip(orb_bef[0, :], orb_bef[2, :]):
            # BPMs with signal
            if not(np.isnan(x)) and not(np.isnan(y)) and abs(x) < max_trajectory[0] and abs(y) < max_trajectory[1]:
                use_bpm[ind] = True
            ind = ind + 1

        # find first continuous series of BPMs with accepted signal
        ind_first_false = traj_length
        ind_first_false_ = [i for i, x in enumerate(use_bpm) if not(x)]

        if not(len(ind_first_false_) == 0):
            ind_first_false = ind_first_false_[0]

        max_bpm = traj_length - 1

        if ind_first_false < max_bpm:
            max_bpm = ind_first_false

        # mark NaN all BPMs after first series
        use_bpm[max_bpm+1:] = [False] * (len(use_bpm)-max_bpm-1)

        # chose correctors to use (less than last BPM / 1 turn)
        if max_bpm < len(ind_bpms)-1 and max_bpm>0:
            s_max_bpm = s_bpms[max_bpm]  # s location of last BPM with signal
        elif max_bpm == 0:  # no BPM with signal
            s_max_bpm = 0.0
        else: # all BPMs with signal
            s_max_bpm = ring.circumference

        use_cor = [False] * len(ind_cor)
        for cc, sc in enumerate(s_cors):
            if sc < s_max_bpm:
                use_cor[cc] = True

        # raise Exception('BPS and COR not well selected')
        Nbpms = np.array(use_bpm, dtype=bool).sum()
        Ncors = np.array(use_cor, dtype=bool).sum()
        print('-----')
        print(f'iteration # {correction_iteration + 1}/{maximum_iterations} '
              f'with {Nbpms}/{traj_length} BPMs '
              f'and {Ncors}/{len(ind_cor)} steerers')

        # if no BPM, scan input coordinates
        if Nbpms == 0:
            raise Exception('No Beam at all')

        # compute correction and add to previous iterations
        dcH[use_cor] = dcH[use_cor] + np.matmul(np.linalg.pinv(MH[use_bpm, :][:, use_cor],
                                                               rcond=sH[neig-1] / sH[0]),
                                                orb_bef[0, use_bpm] - reference_trajectory[0, use_bpm])

        dcV[use_cor] = dcV[use_cor] + np.matmul(np.linalg.pinv(MV[use_bpm, :][:, use_cor],
                                                               rcond=sV[neig-1] / sV[0]),
                                                orb_bef[2, use_bpm] - reference_trajectory[2, use_bpm])

        # check that the corrections are <30 microrad
        ooh=(orb_bef[0, use_bpm] - reference_trajectory[0, use_bpm])
        print('Hor. traj. | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(ooh* 1e6),
                                                                     np.min(ooh * 1e6),
                                                                     np.max(ooh * 1e6)))

        oov = (orb_bef[2, use_bpm] - reference_trajectory[2, use_bpm])
        print('Ver. traj. | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(oov * 1e6),
                                                                     np.min(oov * 1e6),
                                                                     np.max(oov * 1e6)))

        print('Hor. cor. | std: {:.3g} microrad  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(dcH * 1e6),
                                                                       np.min(dcH * 1e6),
                                                                       np.max(dcH * 1e6)))
        print('Ver. cor. | std: {:.3g} microrad  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(dcV * 1e6),
                                                                       np.min(dcV * 1e6),
                                                                       np.max(dcV * 1e6)))

        # apply correction
        for count, ic in enumerate(ind_cor):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1
            # set correctors in the lattice
            rcor[ic].PolynomB[0] = initcorH[count] - dcH[count] / L
            rcor[ic].PolynomA[0] = initcorV[count] - dcV[count] / L

        # check closed orbit
        try:
            _, o0 = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=input_coordinates)
        except at.AtError as e:
            print(e)
            o0 = np.nan*np.ones((6, 2))

        # orbit_within_limits_h = (all(abs(o0[0][:]) < max_trajectory[0]))
        # orbit_within_limits_v = (all(abs(o0[2][:]) < max_trajectory[1]))

        not_nan_orbit = not np.isnan(o0[0][0])

        print(f'orbit is found? {not_nan_orbit}')
        # print(f'Hor. orbit max {max(abs(o0[0][:]*1e3))} is below {max_trajectory[0]*1e3} mm ? {orbit_within_limits_h}')
        # print(f'Ver. orbit max {max(abs(o0[2][:]*1e3))} is below {max_trajectory[1]*1e3} mm ? {orbit_within_limits_v}')
        if check_for_cod_from_iteration >= correction_iteration:
            found_cod_orbit = not_nan_orbit and (max_bpm == traj_length-1) # and orbit_within_limits_h and orbit_within_limits_v
        else:
            found_cod_orbit = False

        correction_iteration = correction_iteration +1

    # print(f'Closed Orbit: {found_cod_orbit}')

    # get Closed Orbit after correction
    _, orb_aft = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=input_coordinates)

    ooh = (orb_aft[:, 0])

    print('Final Hor. orb  | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(ooh * 1e6),
                                                                    np.min(ooh * 1e6),
                                                                    np.max(ooh * 1e6)))

    oov = (orb_aft[:, 2])
    print('Final Ver. orb  | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(oov * 1e6),
                                                                    np.min(oov * 1e6),
                                                                    np.max(oov * 1e6)))

    # update input coordinates
    _, input_coordinates_after_correction = find_orbit_bpm_errors(rcor, [0], guess=input_coordinates)

    return rcor, dcH, dcV, input_coordinates_after_correction


def _test_correct_trajectory():

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get correctors indexes
    ind_cor = list(at.get_refpts(ring, config.correctors_fam_name_pattern))

    ind_qua = list(at.get_refpts(ring, 'Q*'))

    # set errors
    rerr = at.Lattice(copy.deepcopy(ring))

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    NQ = int(len(ind_qua))
    np.random.seed(1) # fix seed for reproducibility
    dx[ind_qua] = 40e-6 * np.random.randn(NQ)
    dy[ind_qua] = 40e-6 * np.random.randn(NQ)

    at.set_shift(rerr, dx, dy)

    # add BPM errors
    ind_off = [45, 129]
    ind_read = [100]
    ind_rot = [180]
    ind_scale = [200]

    rerr[ind_bpms[ind_off[0]]].Offset = [-234e-6, 934e-6]

    rerr[ind_bpms[ind_off[1]]].Offset = [-904e-6, -334e-6]

    rerr[ind_bpms[ind_read[0]]].Reading = [10e-6, 6e-6]

    rerr[ind_bpms[ind_rot[0]]].Rotation = [0.1*3.14]

    rerr[ind_bpms[ind_scale[0]]].Scale = [1.001, 1.0]


    # correct trajectory
    if not(exists('./TRM.pkl')):
        compute_trajectory_response_matrix(ring,
                                            ind_bpms=ind_bpms,
                                            ind_cor=ind_cor,
                                            nturns=5,
                                            filename_response='./TRM.pkl')

    rcor, dcH, dcV, input_coordinates_after_correction = \
        correct_trajectory(rerr, ind_bpms, ind_cor,
                                        nturns=5,
                                        neig=100,
                                        max_trajectory=(2e-3, 2e-3),
                                        filename_response='./TRM.pkl')

    # plot results
    rel, tel, trand = bpm_matrices(ring, ind_bpms)

    traj_err = find_trajectory_bpm_errors(rerr, ind_bpms, nturns=5, rel=rel, tel=tel, trand=trand)
    traj_cor = find_trajectory_bpm_errors(rcor, ind_bpms, nturns=5)
    _, orb_cor = find_orbit_bpm_errors(rcor, np.array(ind_bpms), rel=rel, tel=tel, trand=trand)

    # plot cod before after correction
    fig, (axh,axv,axch,axcv) = plt.subplots(nrows=4)
    fig.subplots_adjust(hspace=.5,wspace=.5)
    axh.plot(traj_err[0, :], label='before')
    axh.plot(traj_cor[0, :], label='after')
    axh.plot(orb_cor[:, 0], label='orbit')
    axh.set_xlabel('BPM #')
    axh.set_ylabel('hor')
    plt.legend()

    axv.plot(traj_err[2, :], label='before')
    axv.plot(traj_cor[2, :], label='after')
    axv.plot(orb_cor[:, 2], label='orbit')
    axv.set_xlabel('BPM #')
    axv.set_ylabel('ver')
    plt.legend()

    axch.bar(range(len(dcH)), dcH)
    axch.set_xlabel('corrector #')
    axch.set_ylabel('hor')

    axcv.bar(range(len(dcV)), dcV)
    axcv.set_xlabel('corrector #')
    axcv.set_ylabel('ver')

    plt.show()

    pass


if __name__ == '__main__':

    _test_correct_trajectory()

    pass
