import at
import at.plot
from at.plot.generic import baseplot
import os
import math
import time
import copy
import pickle
import numpy as np
import multiprocessing
from os.path import exists
from itertools import repeat
from at.plot import plot_beta
from matplotlib import pyplot as plt
from scipy.io import savemat, loadmat
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.errors.find_orbit_bpm_errors import find_orbit_bpm_errors
from commissioningsimulations.errors.bpm_modifications import bpm_process, bpm_matrices
from commissioningsimulations.correction.optics_coupling.RDT import EquivalentGradientsFromAlignments6D
from commissioningsimulations.correction.optics_coupling.analytic_orm_with_skew_quad_errors import analytic_orm_variation_with_skew_quadrupole
from commissioningsimulations.correction.optics_coupling.RDT import get_rdts

folder_data = '.'
force_matrix_computation = False

filename_cod_response = folder_data+'/ORM.mat'

dcorL = 1e-8  # 1 microrad steerers variation


def compute_analytic_orbit_response_matrix(
        ring_,
        ind_bpms=None,
        ind_cor=None,
        verbose=True,
        filename_cod_response=None,
        use_bpm_errors=True,
        thick_steerers=True):
    """
    analytic orbit response matrix

    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :param verbose:
    :param filename_cod_response:
    :param use_bpm_errors:
    :return:
    """

    ring = copy.deepcopy(ring_) # or offset modification will propagate.

    # compute reference orbit
    _, orb_ref = at.find_orbit(ring, ind_bpms)  # orbit without BPM errors (may fail for lattices with errors)

    MH = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MH2V = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV2H = np.zeros(shape=(len(orb_ref), len(ind_cor)))

    #
    rad = ring.radiation
    ring.radiation_off()
    alpha_c = ring.get_mcf()
    L0 = ring.circumference

    if use_bpm_errors: # get BPM errors
        for ib in ind_bpms:
            ring[ib].Offset = [0, 0]  # ignore offset errors
            # ring[ib].Reading = 0  # ignore reading errors

        rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once


    if rad:   # if radiation was on, turn it back on.
        ring.radiation_on()

    tunes = at.get_tune(ring, get_integer=True)
    Q = tunes
    # optics at correctors and BPMS
    _, _, opt_all_location = ring.linopt6(range(len(ring)))
    opt_bpm_ = opt_all_location[ind_bpms]
    opt_cor_ = opt_all_location[ind_cor]

    # _, _, opt_bpm_ = ring.linopt6(ind_bpms)
    # _, _, opt_cor_ = ring.linopt6(ind_cor)

    # dispersion
    dh = [o.dispersion[0] for o in opt_bpm_]
    dv = [o.dispersion[2] for o in opt_bpm_]

    if use_bpm_errors:  # if there are BPM errors, apply them also to the analytic response, excluding the offset errors
        dh, dv = bpm_process(dh, dv, rel, tel, trand)

    # would need cavity state, rather than radiation state, but this is not possible apparently
    rad_state = int(ring.radiation)
    mask_rf = at.get_cells(ring, 'Frequency')
    rad_state = +int( ring[mask_rf][0].PassMethod == 'RFCavityPass' )  # this flag is also the sign in equations + as we use PolynomB, (would be - using KickAngle)

    def tau(pl, a, b):
        return dphi(pl, a, b) - math.pi*Q[pl]

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2*math.pi*Q[pl]

        return d

    # thick steerers corrections
    TS = []
    TC = []
    x = 0
    y = 1
    for countw, w in enumerate(range(len(opt_cor_))):
        Lw = ring[ind_cor[w]].Length
        alpha = opt_cor_[w].alpha
        beta = opt_cor_[w].beta
        if thick_steerers:
            TS.append([Lw / (2 * beta[p] ** 0.5) for p in [x, y]])
            TC.append([beta[p] ** 0.5 - Lw * alpha[p] / (2 * beta[p] ** 0.5) for p in [x, y]])
        else:
            TS.append([0 for p in [x, y]])
            TC.append([beta[p] ** 0.5 for p in [x, y]])

    thick_text = 'thin'
    if thick_steerers:
        thick_text = 'thick'

    # formulas for response
    def response(opt_bpm, opt_cor):
        _MH = np.zeros(shape=(len(opt_bpm), len(opt_cor)))
        _MV = np.zeros(shape=(len(opt_bpm), len(opt_cor)))
        for count, j in enumerate(range(len(opt_cor))):
            for counti, i in enumerate(range(len(opt_bpm))):
                if verbose:
                    print(f'{(count*len(ind_cor)+counti) / (len(opt_cor) * len(opt_bpm)) * 100:.3f}% '
                          f'computing response of {thick_text} steerer '
                          f'{ring[ind_cor[j]].FamName} to BPM {ring[ind_bpms[i]].FamName}'
                          )

                if thick_steerers:
                    twj = [tau(p, opt_cor[j], opt_bpm[i]) for p in [x, y]]

                    JCwj = [math.cos(twj[p]) * TC[j][p] + math.sin(twj[p]) * TS[j][p] for p in [x, y]]

                    _MH[i][j] = - (opt_bpm[i].beta[x] ** 0.5) / 2 / math.sin(math.pi * tunes[x]) * JCwj[x] + \
                                rad_state * (opt_bpm[i].dispersion[x] * opt_cor[j].dispersion[x]) / L0 / alpha_c

                    _MV[i][j] = (opt_bpm[i].beta[y] ** 0.5) / 2 / math.sin(math.pi * tunes[y]) * JCwj[y] + \
                                rad_state * (opt_bpm[i].dispersion[2] * opt_cor[j].dispersion[2]) / L0 / alpha_c

                else:

                    _MH[i][j] = - ( (opt_bpm[i].beta[0] * opt_cor[j].beta[0]) ** 0.5 ) / 2 / math.sin(math.pi*tunes[0]) *\
                               math.cos( abs(opt_cor[j].mu[0] - opt_bpm[i].mu[0]) - math.pi*tunes[0]) + \
                               rad_state*(opt_bpm[i].dispersion[0] * opt_cor[j].dispersion[0])/L0/alpha_c

                    _MV[i][j] = ( (opt_bpm[i].beta[1] * opt_cor[j].beta[1]) ** 0.5 ) / 2 / math.sin(math.pi*tunes[1]) *\
                               math.cos( abs(opt_cor[j].mu[1] - opt_bpm[i].mu[1]) - math.pi*tunes[1]) + \
                               rad_state*(opt_bpm[i].dispersion[2] * opt_cor[j].dispersion[2])/L0/alpha_c

        return _MH, _MV

    MH, MV = response(opt_bpm_, opt_cor_)

    # get off-diagonal blocks

    # get skew quadrupoles fields
    modeskew = 'zeros'  # 'analytic'  # 'LF', 'rdts', 'analytic'

    if modeskew == 'LF':

        # get skew-quadrupoles in the lattice: skew, rotated quad, vertical sext shift.
        # for the purpose of this code only explicit skew are needed, PolynomA[1]~=0.
        mask_skew = [hasattr(el, 'PolynomA') and el.MaxOrder >= 1 for el in ring]
        ind_skew = []
        for c, el in enumerate(ring):
            if mask_skew[c]:
                if abs(el.PolynomA[1]) > 0:
                    ind_skew.append(c)
        if verbose:
            print(f'found {len(ind_skew)} skew to compute off-diagonal RM blocks')

        # or use EquivalentGradientsFromAligmnet6D.py
        Kskew = [el.PolynomA[1] for el in ring[ind_skew]]
        Lskew = [el.Length for el in ring[ind_skew]]

        _, _, opt_skew = ring.linopt6(ind_skew)

        H_Skew_Kick, V_Skew_Kick = response(opt_skew, opt_cor_)
        H_BPM_Skew, V_BPM_Skew = response(opt_bpm_, opt_skew)

        for count, k in enumerate(range(len(ind_skew))):

            A = np.atleast_2d(V_BPM_Skew[:, k]).T
            B = np.atleast_2d(H_Skew_Kick[k, :])

            MH2V = MH2V + +1*A @ B * Kskew[k]*Lskew[k]

            A = np.atleast_2d(H_BPM_Skew[:, k]).T
            B = np.atleast_2d(V_Skew_Kick[k, :])

            MV2H = MV2H + -1* A @ B * Kskew[k]*Lskew[k]

    elif modeskew =='zeros':

        MH2V = np.zeros(MH.shape)
        MV2H = np.zeros(MV.shape)

    elif modeskew == 'rdts':
        raise Exception('not yet implemented')
        # compute RDTs at BPMs
        f1001b, f1010b, f2000b, f0020b = get_rdts(ring, ind_bpms)
        # compute RDTs at correctors
        f1001c, f1010c, f2000c, f0020c = get_rdts(ring, ind_cor)


    else: # mode skew = analytic derivative
        raise Exception('DOES NOT work for off diagonal blocks.')
        # convert
        _, _ks_ , _ind_skews_ = EquivalentGradientsFromAlignments6D(ring)

        # find non zero Ks
        ks = [kk for kk, ii in zip(_ks_, _ind_skews_) if abs(kk) > 0]
        ind_skews = [ii for kk, ii in zip(_ks_, _ind_skews_) if abs(kk) > 0]

        # get L of Ks
        Ls = [el.Length for el in ring[ind_skews]]

        if verbose:
            print(f'there are {len(ind_skews)} skew quad sources (vertical orbit at sext., quad. rotations, explicit skews) in ring with std = {np.std(ks)} 1/m')

        # compute derivative for these indexes
        MH2V_over_Ks, MV2H_over_Ks = analytic_orm_variation_with_skew_quadrupole(
            ring,
            ind_bpms=ind_bpms,
            ind_cors=ind_cor,
            ind_skews=np.array(ind_skews, dtype=np.uint32),
            verbose=False,
            thick_skew=True,
            thick_steerer=True,
            opt_all_location=None,
            use_mp=True)

        for s, _ in enumerate(ind_skews):
            MH2V = MH2V + MH2V_over_Ks[:,:,s] * ks[s] * Ls[s]
            MV2H = MV2H + MV2H_over_Ks[:,:,s] * ks[s] * Ls[s]


    # if there are BPM errors, apply them also to the analytic response, excluding the offset errors
    if use_bpm_errors:
        for j, _ in enumerate(ind_cor):
            h, v = bpm_process(MH[:, j], MH2V[:, j], rel, tel, trand)
            MH[:, j] = h
            MH2V[:, j] = v
            h, v = bpm_process(MV2H[:, j], MV[:, j], rel, tel, trand)
            MV2H[:, j] = h
            MV[:, j] = v
            h, v = bpm_process(dh, dv, rel, tel, trand)
            dh = h
            dv = v

    uH, sH, vH = np.linalg.svd(MH, full_matrices=False)
    uV, sV, vV = np.linalg.svd(MV, full_matrices=False)

    if filename_cod_response:
        mdict = {'uH': uH, 'sH': sH, 'vH': vH,
                 'uV': uV, 'sV': sV, 'vV': vV,
                 'MH': MH, 'MV': MV,
                 'MH2V': MH2V, 'MV2H': MV2H,
                 'ind_bpms': ind_bpms,
                 'cors': ind_cor,
                 'ring': ring,
                 'hor_dispersion': dh,
                 'ver_dispersion': dv,
                 'tunes': tunes}

        file_no_ext, exten_file = os.path.splitext(filename_cod_response)
        if config.save_matlab_files:
            savemat(file_no_ext + '.mat', mdict=mdict)
        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        while not(exists(file_no_ext + '.pkl')):
            print('Wait for file {} to be saved'.format(file_no_ext + '.pkl'))
            time.sleep(1)

        print('ORM saved in: {}'.format(file_no_ext + '.pkl'))

    return MH, MV, MH2V, MV2H, uH, sH, vH, uV, sV, vV, dh, dv, tunes


def compute_analytic_average_orbit_response_matrix(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=dcorL,
        verbose=True,
        filename_cod_response=None,
        use_bpm_errors=True):
    """
    analytic orbit response matrix using average optics at correctors

    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param verbose:
    :param filename_cod_response:
    :param use_bpm_errors: apply BPM errors
    :return:
    """
    # compute reference orbit
    _, orb_ref = at.find_orbit(ring, ind_bpms)

    MH = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MH2V = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV2H = np.zeros(shape=(len(orb_ref), len(ind_cor)))

    #
    rad = ring.radiation
    ring.radiation_off()

    alpha_c = ring.get_mcf()
    L0 = ring.circumference

    tunes = at.get_tune(ring, get_integer=True)

    lb = at.avlinopt(ring, 0.0, ind_bpms)
    betab = lb[1]
    mub = lb[2]
    dispb = lb[3]

    lc = at.avlinopt(ring, 0.0, ind_cor)
    betac = lc[1]
    muc = lc[2]
    dispc = lc[3]

    dh = lb[3][:, 0]
    dv = lb[3][:, 2]

    # get BPM errors
    if use_bpm_errors:
        rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once
        trand = [tr*0 for tr in trand]  # ignore reading errors
        tel = [t*0 for t in tel]  # ignore offset errors (cancel in RM measurement)

    if rad:
        ring.radiation_on()

    for count, j in enumerate(range(len(ind_cor))):
        for counti, i in enumerate(range(len(ind_bpms))):
            if verbose:
                print('{:.3f}% computing response of steerer '
                      '{} to BPM {}'
                      ''.format((count + counti*len(ind_cor)) / (len(ind_cor) * len(ind_bpms)) * 100,
                                ring[ind_cor[j]].FamName, ring[ind_bpms[i]].FamName))  # , end="\r")

            MH[i][j] = - ( (betab[i, 0] * betac[j, 0]) ** 0.5 ) / 2 / math.sin(math.pi*tunes[0]) *\
                       math.cos( abs(muc[j, 0] - mub[i, 0]) - math.pi*tunes[0]) + \
                       (dispb[i, 0] * dispc[j, 0])/L0/alpha_c

            MV[i][j] = ( (betab[i, 1] * betac[j, 1]) ** 0.5 ) / 2 / math.sin(math.pi*tunes[1]) *\
                       math.cos( abs(muc[j, 1] - mub[i, 1]) - math.pi*tunes[1]) + \
                       (dispb[i, 2] * dispc[j, 2])/L0/alpha_c

        if use_bpm_errors and (rel is not None): # if there are BPM errors, apply them also to the analytic response, excluding the reading errors
            MH[:][j], MV[:][j] = bpm_process(MH[:][j], MV[:][j], rel, tel, trand)

    uH, sH, vH = np.linalg.svd(MH, full_matrices=True)
    uV, sV, vV = np.linalg.svd(MV, full_matrices=True)
    if filename_cod_response:
        mdict = {'uH': uH, 'sH': sH, 'vH': vH,
                 'uV': uV, 'sV': sV, 'vV': vV,
                 'MH': MH, 'MV': MV,
                 'MH2V': MH2V, 'MV2H': MV2H,
                 'ind_bpms': ind_bpms,
                 'cors': ind_cor,
                 'ring': ring,
                 'hor_dispersion': dh,
                 'ver_dispersion': dv,
                 'tunes': tunes}

        file_no_ext, exten_file = os.path.splitext(filename_cod_response)
        savemat(file_no_ext + '.mat', mdict=mdict)
        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        print('ORM saved in: {}'.format(filename_cod_response))

    return MH, MV, MH2V, MV2H, uH, sH, vH, uV, sV, vV, dh, dv, tunes


def OneSteererResponse(ring, dcorL, orb_ref, ind_bpms, ic, verbose=True, rel=None, tel=None, trand=None):
    # display status

    L = ring[ic].Length
    thin_flag = ''

    if L == 0.0 or np.isnan(L):
        L = 1.0  # assume thin correctors
        thin_flag = 'thin '

    if verbose:
        print('computing response of {}steerer '
              '{} to COD'
              ''.format(thin_flag, ring[ic].FamName))  # , end="\r")

    # vary Horizontal corrector
    initcor = ring[ic].PolynomB[0]
    ring[ic].PolynomB[0] = initcor + dcorL / L

    # compute orbit with steerer
    _, orb_cor_p = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)

    ring[ic].PolynomB[0] = initcor - dcorL / L

    # compute orbit with steerer
    _, orb_cor_m = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)

    # restore corrector
    ring[ic].PolynomB[0] = initcor

    # divide by deltacor
    # store in matrix
    Hcor_Horb = (orb_cor_p[:, 0] - orb_cor_m[:, 0]) / 2 /dcorL
    Hcor_Vorb = (orb_cor_p[:, 2] - orb_cor_m[:, 2]) / 2 / dcorL

    # vary Vertical corrector
    initcor = ring[ic].PolynomA[0]
    ring[ic].PolynomA[0] = initcor + dcorL / L

    # compute orbit with steerer
    _, orb_cor_p = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)

    ring[ic].PolynomA[0] = initcor - dcorL / L

    # compute orbit with steerer
    _, orb_cor_m = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)

    # restore corrector
    ring[ic].PolynomA[0] = initcor

    # divide by deltacor
    # store in matrix
    Vcor_Vorb = (orb_cor_p[:, 2] - orb_cor_m[:, 2]) / 2 / dcorL
    Vcor_Horb = (orb_cor_p[:, 0] - orb_cor_m[:, 0]) / 2 / dcorL

    return Hcor_Horb, Vcor_Vorb, Hcor_Vorb, Vcor_Horb


def compute_orbit_response_matrix(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=dcorL,
        drf=10,  # [Hz]
        verbose=True,
        filename_cod_response=None,
        use_mp=False,
        n_processes=None,
        use_bpm_errors=True):
    """
    numeric orbit response matrix

    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param verbose:
    :param filename_cod_response:
    :return:
    """

    rad = ring.radiation
    ring.radiation_off()
    alpha_c = ring.get_mcf()
    L0 = ring.circumference

    if rad:  # if radiation was on, turn it back on.
        ring.radiation_on()

    # get tune
    tunes = at.get_tune(ring, get_integer=True)

    # compute reference orbit
    if use_bpm_errors:
        rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once
    else:
        rel = [np.array([[1, 0], [0, 1]]) for i in ind_bpms]
        tel = [np.array([0, 0]) for i in ind_bpms]
        trand = np.zeros(len(ind_bpms))

    _, orb_ref = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)

    # dispersion "numeric" via RF shift
    RF0 = ring.get_rf_frequency()
    ring.set_rf_frequency(RF0 + drf)
    _, orb_p_rf = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)
    ring.set_rf_frequency(RF0 - drf)
    _, orb_m_rf = find_orbit_bpm_errors(ring, ind_bpms, rel=rel, tel=tel, trand=trand)
    ring.set_rf_frequency(RF0)  # restore RF frequency

    dh = (np.array(orb_p_rf[:, 0]) - np.array(orb_m_rf[:, 0])) / (2 * drf / RF0) * -alpha_c
    dv = (np.array(orb_p_rf[:, 2]) - np.array(orb_m_rf[:, 2])) / (2 * drf / RF0) * -alpha_c

    # initialize RM
    MH = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MH2V = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV2H = np.zeros(shape=(len(orb_ref), len(ind_cor)))

    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        if n_processes is None:
            n_processes = n_cpu
        else:
            max_proc = min(n_cpu, len(ind_cor))
            if n_processes > max_proc:
                n_processes = max_proc

        print('parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool(processes=n_processes) as p:
            results = p.starmap(OneSteererResponse,
                                zip(repeat(ring),
                                    repeat(dcorL),
                                    repeat(orb_ref),
                                    repeat(ind_bpms),
                                    ind_cor,
                                    repeat(verbose),
                                    repeat(rel),
                                    repeat(tel),
                                    repeat(trand)))

        for count, ic in enumerate(ind_cor):
            MH[:, count] = results[count][0]
            MV[:, count] = results[count][1]
            MH2V[:, count] = results[count][2]
            MV2H[:, count] = results[count][3]

    else:
        for count, ic in enumerate(ind_cor):
            H, V, H2V, V2H = OneSteererResponse(ring, dcorL, orb_ref, ind_bpms, ic,
                                                verbose=verbose,
                                                rel=rel, tel=tel, trand=trand)
            MH[:, count] = H
            MV[:, count] = V
            MH2V[:, count] = H2V
            MV2H[:, count] = V2H

    uH, sH, vH = np.linalg.svd(MH, full_matrices=True)
    uV, sV, vV = np.linalg.svd(MV, full_matrices=True)

    if filename_cod_response:
        mdict = {'uH': uH, 'sH': sH, 'vH': vH,
                 'uV': uV, 'sV': sV, 'vV': vV,
                 'MH': MH, 'MV': MV,
                 'MH2V': MH2V, 'MV2H': MV2H,
                 'ind_bpms': ind_bpms,
                 'cors': ind_cor,
                 'ring': ring,
                 'hor_dispersion': dh,
                 'ver_dispersion': dv,
                 'tunes': tunes}


        file_no_ext, exten_file = os.path.splitext(filename_cod_response)
        if config.save_matlab_files:
            savemat(file_no_ext + '.mat', mdict=mdict)
        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        print('ORM saved in: {}'.format(filename_cod_response))

    return MH, MV, MH2V, MV2H, uH, sH, vH, uV, sV, vV, dh, dv, tunes


def correct_orbit(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL = dcorL,
        neig=0,
        filename_cod_response=filename_cod_response,
        force_matrix_computation=False,
        zero_steerers_average=True,
        niter=2,
        reference_orbit=None,
        guess=(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        verbose=True
    ):
    """

    corrects closed orbit in ring using the correctors in ind_cor and the Orbit response matrix in filename_cod_response

    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param neig:
    :param filename_cod_response:
    :param force_matrix_computation:
    :param niter:
    :param reference_orbit:
    :return:
    """

    # check that inputs are in the correct type
    try:
        val = iter(ind_bpms)
    except TypeError as te:
        print(ind_bpms, 'is not iterable')

    try:
        val = iter(ind_cor)
    except TypeError as te:
        print(ind_cor, 'is not iterable')

    # check neig is possible
    max_neig = min(len(ind_bpms), len(ind_cor))
    if neig>max_neig:
        neig = max_neig
        print('too many singular vectors requested. setting to maximum: {}'.format(max_neig))

    # get Orbit Response Matrix if no file is provided, based on lattice with errors.
    if (not exists(filename_cod_response)) or force_matrix_computation:
        # MHa, MVa, _, sHa, _, _, sVa, _ = compute_analytic_orbit_response_matrix(ring, ind_bpms, ind_cor, dcorL,
        #                                                                         'Analytic' + filename_cod_response)
        MH, MV, _, _, _,  sH,  _,  _,  sV,  _, _, _, _ = compute_orbit_response_matrix(ring,
                                                                        ind_bpms=ind_bpms,
                                                                        ind_cor=ind_cor,
                                                                        dcorL=dcorL,
                                                                        filename_cod_response=filename_cod_response)

    else:

        file_no_ext, exten_file = os.path.splitext(filename_cod_response)

        if exten_file == '.mat':

            mat_contents = loadmat(filename_cod_response)
            MH = mat_contents['MH']
            MV = mat_contents['MV']
            sH = np.transpose(mat_contents['sH'])  # load mat transposes raws/columns for some reason
            sH = sH[:, 0]
            sV = np.transpose(mat_contents['sV'])  # load mat transposes raws/columns for some reason
            sV = sV[:, 0]

        elif exten_file == '.pkl':

            mat_contents = pickle.load(open(filename_cod_response, 'rb'))
            MH = mat_contents['MH']
            MV = mat_contents['MV']
            sH = mat_contents['sH']
            sV = mat_contents['sV']

        else:

            raise Exception(f'files should be .pkl or .mat')

        print(f'Loaded: {filename_cod_response}')
        # check that size of loaded RM is the correct one
        exp_size = (len(ind_bpms), len(ind_cor))
        if MH.shape != exp_size:
            raise ValueError('Horizontal Orbit Response Matrix shape is incorrect. Expected {}, found {}'.format(exp_size, MH.shape))
        if MV.shape != exp_size:
            raise ValueError('Vertical Orbit Response Matrix shape is incorrect. Expected {}, found {}'.format(exp_size, MV.shape))

    # get Closed Orbit
    rcor = copy.deepcopy(ring)

    initcorH = at.get_value_refpts(rcor, ind_cor, 'PolynomB', 0)
    initcorV = at.get_value_refpts(rcor, ind_cor, 'PolynomA', 0)
    dcH = np.zeros(len(ind_cor))
    dcV = np.zeros(len(ind_cor))

    # get Closed Orbit
    rel, tel, trand = bpm_matrices(ring, ind_bpms)  # get bpm errors only once
    guess, orb_init = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=guess)

    # new_guess, orb = at.find_orbit(rcor, ind_bpms, guess=guess)
    # _, (axh, axv) = plt.subplots(nrows=2)
    # plt.subplots_adjust(hspace=0.5)
    # axh.plot(orb[:, 0], label='bare orbit')
    # axh.plot(orb_init[:, 0], label='orbit as measured by offset / misaligned BPMs')
    # axv.plot(orb[:, 2], label='bare orbit')
    # axv.plot(orb_init[:, 2], label='orbit as measured by offset / misaligned BPMs')
    # ref_orbit = np.zeros(orb_init.shape)
    # # # use T2 at BPMs as reference orbit: NO! this is done inside find_orbit_bpm_errors. Reference closed orbit is zero.
    # ref_orbit[:, 0] = [el.T2[0] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]
    # ref_orbit[:, 2] = [el.T2[2] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]
    # axh.plot(ref_orbit[:, 0], label='BPMs misalignments')
    # axh.legend()
    # axh.set_ylabel('Hor.')
    # axv.plot(ref_orbit[:, 2], label='BPMs misalignments')
    # axv.legend()
    # axv.set_xlabel('BPM #')
    # axv.set_ylabel('Ver.')
    #
    # plt.show()

    if reference_orbit is None:
        reference_orbit = np.zeros(orb_init.shape)
        # # use T2 at BPMs as reference orbit: NO! this is done inside find_orbit_bpm_errors. Reference closed orbit is zero.
        # reference_orbit[:, 0] = [el.T2[0] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]
        # reference_orbit[:, 2] = [el.T2[2] if hasattr(el, 'T2') else 0.0 for el in ring[ind_bpms]]

    if zero_steerers_average:
        MH = np.vstack([MH, np.ones(MH[0, :].shape)])
        MV = np.vstack([MV, np.ones(MV[0, :].shape)])

    for correction_iteration in range(0, niter):
        if verbose:
            print('iteration # {}'.format(correction_iteration+1))

        # get Closed Orbit
        guess, orb_bef = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=guess)

        ooh = orb_bef[:, 0] - reference_orbit[:, 0]
        oov = orb_bef[:, 2] - reference_orbit[:, 2]

        if zero_steerers_average:
            ooh = np.append(ooh, [0])
            oov = np.append(oov, [0])

        # compute correction and add to previous iterations
        dcH = dcH + np.linalg.pinv(MH, rcond=sH[neig-1]*(1-1e-9) / sH[0]) @ ooh
        dcV = dcV + np.linalg.pinv(MV, rcond=sV[neig-1]*(1-1e-9) / sV[0]) @ oov

        # check that the corrections are <30 mircorad
        ooh=(orb_bef[:, 0] - reference_orbit[:, 0])
        if verbose:
            print('Hor. orbit | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(ooh* 1e6),
                                                                     np.min(ooh * 1e6),
                                                                     np.max(ooh * 1e6)))

        oov = (orb_bef[:, 2] - reference_orbit[:, 2])
        if verbose:
            print('Ver. orbit | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(oov * 1e6),
                                                                     np.min(oov * 1e6),
                                                                     np.max(oov * 1e6)))

        if verbose:
            print('Hor. cor. | std: {:.3g} microrad  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(dcH * 1e6),
                                                                       np.min(dcH * 1e6),
                                                                       np.max(dcH * 1e6)))
        if verbose:
            print('Ver. cor. | std: {:.3g} microrad  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(dcV * 1e6),
                                                                       np.min(dcV * 1e6),
                                                                       np.max(dcV * 1e6)))

        # apply correction
        for count, ic in enumerate(ind_cor):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0

            # set correctors in the lattice
            rcor[ic].PolynomB[0] = initcorH[count] - dcH[count] / L
            rcor[ic].PolynomA[0] = initcorV[count] - dcV[count] / L

    # get Closed Orbit after correction
    guess, orb_aft = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand, guess=guess)

    ooh = (orb_aft[:, 0] - reference_orbit[:, 0])
    if verbose:
        print('Final Hor. orb | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(ooh * 1e6),
                                                                    np.min(ooh * 1e6),
                                                                    np.max(ooh * 1e6)))

    oov = (orb_aft[:, 2] - reference_orbit[:, 2])
    if verbose:
        print('Final Ver. orb | std: {:.3g} microm  |  (min, max) ({:.3g}, {:.3g})'.format(np.std(oov * 1e6),
                                                                    np.min(oov * 1e6),
                                                                    np.max(oov * 1e6)))
    orbit_0_guess = copy.deepcopy(guess)

    return rcor, dcH, dcV, orbit_0_guess


def _test_correct_orbit(
        mode = 'numeric'   # or 'analytic'
    ):
    """
    test correction of random quadrupole errors
    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :return:
    """

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get correctors indexes
    ind_cor = list(at.get_refpts(ring, config.correctors_fam_name_pattern))

    # set errors
    np.random.seed(1)  # fix seed

    ind_qua = list(at.get_refpts(ring, 'Q*'))

    rerr = at.Lattice(copy.deepcopy(ring))

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    NQ = int(len(ind_qua))
    dx[ind_qua] = 1e-6 * np.random.randn(NQ)
    dy[ind_qua] = 1e-6 * np.random.randn(NQ)

    at.set_shift(rerr, dx, dy)

    # add BPM errors
    ind_off = [45, 129]
    ind_rot = [180]
    ind_scale = [200]
    ind_read = [100]

    rerr[ind_bpms[ind_off[0]]].Offset = [-234e-6, 334e-6]

    rerr[ind_bpms[ind_off[1]]].Offset = [-24e-6, -334e-6]

    rerr[ind_bpms[ind_rot[0]]].Rotation = [0.1*3.14]

    rerr[ind_bpms[ind_scale[0]]].Scale = [1.05, -0.99]

    rerr[ind_bpms[ind_read[0]]].Reading = [10e-6, 2e-6]

    # compute orbit response matrix numeric or analytic
    if not exists('./ORM' + mode + '.pkl'):
        if mode == 'numeric':
            compute_orbit_response_matrix(
                ring,
                ind_bpms=ind_bpms,
                ind_cor=ind_cor,
                dcorL=1e-6,
                verbose=True,
                filename_cod_response='./ORM'+ mode +'.pkl',
                use_mp=True)

        elif mode == 'analytic':
            compute_analytic_orbit_response_matrix(
                ring,
                ind_bpms=ind_bpms,
                ind_cor=ind_cor,
                verbose=True,
                filename_cod_response='./ORM'+ mode +'.pkl',
                use_bpm_errors=True)

    # run correction
    rcor, dcH, dcV, _ = correct_orbit(rerr,
                                   ind_bpms,
                                   ind_cor,
                                   niter=3,
                                   neig=100,
                                   filename_cod_response='./ORM' + mode +'.pkl',
                                   zero_steerers_average=True)

    rel, tel, trand = bpm_matrices(ring, ind_bpms)
    _, orb_err = find_orbit_bpm_errors(rerr, ind_bpms, rel=rel, tel=tel, trand=trand)
    _, orb_cor = find_orbit_bpm_errors(rcor, ind_bpms, rel=rel, tel=tel, trand=trand)

    # plot cod before after correction

    fig, ((axhb, axhc), (axvb, axvc)) = plt.subplots(ncols=2, nrows=2, figsize=(15, 5))
    fig.subplots_adjust(hspace=.5, wspace=.5)

    axhb.plot(orb_err[:, 0], label='before')
    axhb.plot(orb_cor[:, 0], label='after')
    axhb.set_xlabel('BPM #')
    axhb.set_ylabel('hor')
    axhb.legend()
    axhb.set_title(f'orbit correction using {mode} ORM')

    axvb.plot(orb_err[:, 2], label='before')
    axvb.plot(orb_cor[:, 2], label='after')
    axvb.set_xlabel('BPM #')
    axvb.set_ylabel('ver')
    axvb.legend()

    axhc.bar(range(len(dcH)), dcH)
    axhc.set_xlabel('hor cor')

    axvc.bar(range(len(dcV)), dcV)
    axvc.set_xlabel('ver cor')

    plt.show()

    plot_closed_orbit(rerr)
    plot_closed_orbit(rcor)

    plt.show()
    pass


def plot_closed_orbit(ring: at.Lattice, **kwargs):
    """Plot a particle's closed orbit
    Parameters:
        ring:           Lattice object
    Keyword Args:
        refpts (list):   (1,n) array: locations to compute closed orbit
        keep_lattice (bool):   Assume no lattice change since the
          previous tracking. Defaults to :py:obj:`False`
    """

    refpts = range(len(ring))

    # noinspection PyShadowingNames
    def pldata_closed_orbit(ring, refpts, **kwargs):
        _, orbit = at.find_orbit(ring, refpts=refpts, **kwargs)
        s_pos = ring.get_s_pos(refpts)

        xx = [orbit[:, 0] ]
        zz = [orbit[:, 2] ]
        xlabels = [f'hor.']
        zlabels = [f'ver.']
        left = ('position [m]', s_pos, xx+zz, xlabels+zlabels)
        return 'Closed Orbit', left

    return baseplot(ring, pldata_closed_orbit, **kwargs)


def _test_analytic_numeric_orm(testcor=2):

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get correctors indexes
    ind_cor = list(at.get_refpts(ring, config.correctors_fam_name_pattern))

    # add BPM errors
    ind_off = [45, 129]
    ind_rot = [180]
    ind_scale = [200]
    ind_read = [100]

    ring[ind_bpms[ind_off[0]]].Offset = [-234e-6, 334e-6]

    ring[ind_bpms[ind_off[1]]].Offset = [-24e-6, -334e-6]

    ring[ind_bpms[ind_rot[0]]].Rotation = [3.14]

    ring[ind_bpms[ind_scale[0]]].Scale = [10, -10]

    ring[ind_bpms[ind_read[0]]].Reading = [100e-6, 60e-6]

    # compute orbit response matrix numeric or analytic

    compute_orbit_response_matrix(
        ring,
        ind_bpms=ind_bpms,
        ind_cor=[ind_cor[testcor]],
        dcorL=1e-6,
        verbose=True,
        filename_cod_response='./ORM' + 'numeric' + str(testcor)  + '.mat',
        use_mp=True,
        use_bpm_errors=True)

    compute_analytic_orbit_response_matrix(
        ring,
        ind_bpms=ind_bpms,
        ind_cor=[ind_cor[testcor]],
        verbose=True,
        filename_cod_response='./ORM' + 'analytic' + str(testcor) + '.mat',
        use_bpm_errors=True)

    compute_analytic_orbit_response_matrix(
        ring,
        ind_bpms=ind_bpms,
        ind_cor=[ind_cor[testcor]],
        verbose=True,
        filename_cod_response='./ORM' + 'analytic' + 'thick' + str(testcor) + '.mat',
        use_bpm_errors=True, thick_steerers=True)

    num = pickle.load(open('./ORM' + 'numeric' + str(testcor)  + '.pkl', 'rb'))
    ana = pickle.load(open('./ORM' + 'analytic' + str(testcor)  + '.pkl', 'rb'))
    ant = pickle.load(open('./ORM' + 'analytic' + 'thick' + str(testcor)  + '.pkl', 'rb'))

    # plot cod before after correction

    fig, (axhb, axvb) = plt.subplots(ncols=1, nrows=2, figsize=(7, 5))
    fig.subplots_adjust(hspace=.5, wspace=.5)

    axhb.plot(num['MH'][:, 0], 'x', label='numeric')
    axhb.plot(ana['MH'][:, 0], '+:', label='analytic')
    axhb.plot(ant['MH'][:, 0], '+:', label='analytic thick')
    axhb.bar(range(len(ind_bpms)), num['MH'][:, 0] - ana['MH'][:, 0],
             color='g', alpha=0.5, label='num-ana')
    axhb.bar(range(len(ind_bpms)), num['MH'][:, 0] - ant['MH'][:, 0],
             color='y', alpha=0.5, label='num-anathick')
    axhb.set_xlabel('BPM #')
    axhb.set_ylabel('hor. response')
    axhb.legend()
    axhb.set_title(f'orbit reponse for steerer {ring[ind_cor[testcor]].FamName}')
    axhb.text(ind_read[0], max(num['MH'][:, 0]) * 0.7, 'Reading', rotation='vertical')
    axhb.text(ind_scale[0], max(num['MH'][:, 0]) * 0.7, 'Scale', rotation='vertical')
    axhb.text(ind_off[0], max(num['MH'][:, 0]) * 0.7, 'Offset', rotation='vertical')
    axhb.text(ind_off[1], max(num['MH'][:, 0]) * 0.7, 'Offset', rotation='vertical')
    axhb.text(ind_rot[0], max(num['MH'][:, 0]) * 0.7, 'Rotation', rotation='vertical')

    axvb.plot(num['MV'][:, 0], 'x', label='numeric')
    axvb.plot(ana['MV'][:, 0], '+:', label='analytic')
    axvb.plot(ant['MV'][:, 0], '+:', label='analytic thick')
    axvb.bar(range(len(ind_bpms)), num['MV'][:, 0] - ana['MV'][:, 0],
             color='g', alpha=0.5, label='num-ana')
    axvb.bar(range(len(ind_bpms)), num['MV'][:, 0] - ant['MV'][:, 0],
             color='y', alpha=0.5, label='num-ana thick')
    axvb.set_xlabel('BPM #')
    axvb.set_ylabel('ver. response')
    axvb.legend()

    # correlation plot to check that the bpm with errors get the errors
    def text_ax_ind(ax, ind, lab, mat):
        ax.scatter(num[mat][ind, 0], ana[mat][ind, 0], s= 60, edgecolors='r', facecolors=None)
        ax.text(num[mat][ind, 0], ana[mat][ind, 0], lab, rotation='vertical')

    fig, (axhb, axvb) = plt.subplots(ncols=2, nrows=1, figsize=(7, 5))
    fig.subplots_adjust(hspace=.5, wspace=.5)

    axhb.plot(num['MH'][:, 0], ana['MH'][:, 0], '.', label='thin')
    axhb.plot(num['MH'][:, 0], ant['MH'][:, 0], '.', label='thick')
    m = max((max(abs(num['MH'][:, 0])), max(abs(ana['MH'][:, 0]))))
    axhb.plot([-m, m], [-m, m], color='r', label='x=y')
    axhb.set_xlabel('numeric')
    axhb.set_ylabel('analytic')
    axhb.legend()

    text_ax_ind(axhb, ind_read[0], 'Reading', 'MH')
    text_ax_ind(axhb, ind_scale[0], 'Scale', 'MH')
    text_ax_ind(axhb, ind_off[0], 'Offset', 'MH')
    text_ax_ind(axhb, ind_rot[0], 'Rotation', 'MH')


    axhb.set_title(f'hor. orbit reponse for steerer {ring[ind_cor[0]].FamName}')
    axvb.plot(num['MV'][:, 0], ana['MV'][:, 0], 'x',  label='thin')
    axvb.plot(num['MV'][:, 0], ant['MV'][:, 0], 'x', label='thick')
    m = max((max(abs(num['MV'][:, 0])), max(abs(ana['MV'][:, 0]))))
    axvb.plot([-m, m], [-m, m], color='r', label='x=y')
    axvb.set_xlabel('numeric')
    axvb.set_ylabel('analytic')
    axvb.legend()
    axvb.set_title(f'ver. orbit reponse for steerer {ring[ind_cor[0]].FamName}')

    text_ax_ind(axvb, ind_read[0], 'Reading', 'MV')
    text_ax_ind(axvb, ind_scale[0], 'Scale', 'MV')
    text_ax_ind(axvb, ind_off[0], 'Offset', 'MV')
    text_ax_ind(axvb, ind_rot[0], 'Rotation', 'MV')

    plt.show()

    pass


def _test_analytic_numeric_orm_full(errors=False, rad=False):

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # add random errors
    error_label=''
    if errors:
        from commissioningsimulations.errors.SetErrors import SetErrors
        dict_of_errors = {'Q*': {'x': 5e-6, 'y': 5e-6},
                          'S*': {'x': 5e-6, 'y': 5e-6}
                          }
        ring = SetErrors(ring, dict_of_errors, seed=0)
        error_label = 'Err'

    #
    if rad:
        radflag='rad'
        ring.radiation_on()
        ring.enable_6d()
    else:
        radflag=''
        ring.radiation_off()
        ring.disable_6d()

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get correctors indexes
    ind_cor = list(at.get_refpts(ring, config.correctors_fam_name_pattern))

    # set one skew
    # ring[ind_cor[5]].PolynomA[1] = 0.0000001

    # add BPM errors
    ind_off = [45, 129]
    ind_rot = [180]
    ind_scale = [200]
    ind_read = [100]

    # ring[ind_bpms[ind_off[0]]].Offset = [-234e-6, 334e-6]

    # ring[ind_bpms[ind_off[1]]].Offset = [-24e-6, -334e-6]

    # ring[ind_bpms[ind_rot[0]]].Rotation = [0*3.14]

    # ring[ind_bpms[ind_scale[0]]].Scale = [1, 1]

    # ring[ind_bpms[ind_read[0]]].Reading = [0e-6, 0e-6]

    # compute orbit response matrix numeric or analytic
    if not exists('./ORM' + 'numeric' + error_label + 'all' + radflag + '.pkl'):
        compute_orbit_response_matrix(
            ring,
            ind_bpms=ind_bpms,
            ind_cor=ind_cor,
            dcorL=1e-6,
            verbose=True,
            filename_cod_response='./ORM' + 'numeric' + error_label + 'all' + radflag + '.pkl',
            use_mp=True,
            use_bpm_errors=True)

    if not exists('./ORM' + 'analytic' + error_label + 'all' + radflag + '.pkl'):
        compute_analytic_orbit_response_matrix(
            ring,
            ind_bpms=ind_bpms,
            ind_cor=ind_cor,
            verbose=True,
            filename_cod_response='./ORM' + 'analytic' + error_label + 'all' + radflag + '.pkl',
            use_bpm_errors=True, thick_steerers=False)

    if not exists('./ORM' + 'analytic' + error_label + 'all' + radflag + 'thick' + '.pkl'):
        compute_analytic_orbit_response_matrix(
            ring,
            ind_bpms=ind_bpms,
            ind_cor=ind_cor,
            verbose=True,
            filename_cod_response='./ORM' + 'analytic' + error_label + 'all' + radflag + 'thick' + '.pkl',
            use_bpm_errors=True, thick_steerers=True)


    num = pickle.load(open('./ORM' + 'numeric' + error_label + 'all' + radflag + '.pkl', 'rb'))
    ana = pickle.load(open('./ORM' + 'analytic' + error_label + 'all' + radflag + '.pkl', 'rb'))
    ant = pickle.load(open('./ORM' + 'analytic' + error_label + 'all' + radflag + 'thick' + '.pkl', 'rb'))

    # plot cod before after correction

    fig, ((axhb, axvb), (axhvb, axvhb)) = plt.subplots(ncols=2, nrows=2, figsize=(7, 5))
    fig.subplots_adjust(hspace=.5, wspace=.5)

    def flat(MM):
        return np.asarray(np.matrix(MM).flatten().transpose())[:, 0]

    nh = flat(np.abs(num['MH']))
    ah = flat(np.abs(ana['MH']))
    th = flat(np.abs(ant['MH']))

    axhb.plot(nh, 'x', label='numeric')
    axhb.plot(ah-nh, '+:', label='analytic - num')
    axhb.plot(th-nh, '.:', label='analytic thick - num')
    # axhb.bar(range(len(ah)), nh - ah, color='g', alpha=0.5, label='num-ana')
    axhb.set_xlabel('hor BPM #')
    axhb.set_ylabel('hor. response' + error_label )
    axhb.legend()
    axhb.text(ind_read[0], max(num['MH'][:, 0]) * 0.7, 'Reading', rotation='vertical')
    axhb.text(ind_scale[0], max(num['MH'][:, 0]) * 0.7, 'Scale', rotation='vertical')
    axhb.text(ind_off[0], max(num['MH'][:, 0]) * 0.7, 'Offset', rotation='vertical')
    axhb.text(ind_off[1], max(num['MH'][:, 0]) * 0.7, 'Offset', rotation='vertical')
    axhb.text(ind_rot[0], max(num['MH'][:, 0]) * 0.7, 'Rotation', rotation='vertical')

    nhv = flat(np.abs(num['MH2V']))
    ahv = flat(np.abs(ana['MH2V']))
    thv = flat(np.abs(ant['MH2V']))

    axhvb.plot(nhv, 'x', label='numeric')
    axhvb.plot(ahv, '+:', label='analytic')
    axhvb.plot(thv, '.:', label='analytic thick')
    # axhb.bar(range(len(ah)), nh - ah, color='g', alpha=0.5, label='num-ana')
    axhvb.set_xlabel('ver. BPM #')
    axhvb.set_ylabel('response to hor steerer' + error_label )
    axhvb.legend()

    nv = flat(np.abs(num['MV']))
    av = flat(np.abs(ana['MV']))
    tv = flat(np.abs(ant['MV']))

    axvb.plot(nv, 'x', label='numeric')
    axvb.plot(av-nv, '+:', label='analytic - num')
    axvb.plot(tv-nv, '.:', label='analytic thick - num')
    # axvb.bar(range(len(av)), nv-av,  color='g', alpha=0.5, label='num-ana')
    axvb.set_xlabel('ver BPM #')
    axvb.set_ylabel('ver. response' + error_label )
    axvb.legend()

    # correlation plot to check that the bpm with errors get the errors
    def text_ax_ind(ax, ind, lab, mat):
        ax.scatter(num[mat][ind, 0], ana[mat][ind, 0], s= 60, edgecolors='r', facecolors=None)
        ax.text(num[mat][ind, 0], ana[mat][ind, 0], lab, rotation='vertical')

    nvh = flat(np.abs(num['MV2H']))
    avh = flat(np.abs(ana['MV2H']))
    tvh = flat(np.abs(ant['MV2H']))

    axvhb.plot(nvh, 'x', label='numeric')
    axvhb.plot(avh, '+:', label='analytic')
    axvhb.plot(tvh, '.:', label='analytic thick')
    # axhb.bar(range(len(ah)), nh - ah, color='g', alpha=0.5, label='num-ana')
    axvhb.set_xlabel('hor BPM #')
    axvhb.set_ylabel('response to ver kick' + error_label )
    axvhb.legend()


    fig, ((axhb, axvb),(axhvb, axvhb)) = plt.subplots(ncols=2, nrows=2, figsize=(7, 7))
    fig.subplots_adjust(hspace=.5, wspace=.2)

    axhb.plot(nh, ah, '.', label='thin')
    axhb.plot(nh, th, '.', label='thick')
    m = max((max(abs(ah)), max(abs(ah))))
    axhb.plot([-m*0, m], [-m*0, m], color='r', label='x=y')
    axhb.set_xlabel('hor. numeric' + error_label )
    axhb.set_ylabel('hor. analytic' + error_label )
    axhb.legend()

    #text_ax_ind(axhb, ind_read[0], 'Reading', 'MH')
    #text_ax_ind(axhb, ind_scale[0], 'Scale', 'MH')
    #text_ax_ind(axhb, ind_off[0], 'Offset', 'MH')
    #text_ax_ind(axhb, ind_rot[0], 'Rotation', 'MH')


    axvb.plot(nv, av, '.', label='thin')
    axvb.plot(nv, tv, '.', label='thick')
    m = max((max(abs(av)), max(abs(av))))
    axvb.plot([-m*0, m], [-m*0, m], color='r', label='x=y')
    axvb.set_xlabel('ver. numeric')
    axvb.set_ylabel('ver. analytic')
    axvb.legend()

    #text_ax_ind(axvb, ind_read[0], 'Reading', 'MV')
    #text_ax_ind(axvb, ind_scale[0], 'Scale', 'MV')
    #text_ax_ind(axvb, ind_off[0], 'Offset', 'MV')
    #text_ax_ind(axvb, ind_rot[0], 'Rotation', 'MV')


    axhvb.plot(nhv, ahv, '.', label='thin')
    axhvb.plot(nhv, thv, '.', label='thick')
    m = max((max(abs(ahv)), max(abs(ahv))))
    axhvb.plot([-m*0, m], [-m*0, m], color='r', label='x=y')
    axhvb.set_xlabel('hor.2ver numeric')
    axhvb.set_ylabel('hor.2ver analytic')
    axhvb.legend()


    axvhb.plot(nvh, avh, '.', label='thin')
    axvhb.plot(nvh, tvh, '.', label='thick')
    m = max((max(abs(avh)), max(abs(avh))))
    axvhb.plot([-m*0, m], [-m*0, m], color='r', label='x=y')
    axvhb.set_xlabel('ver.2hor numeric')
    axvhb.set_ylabel('ver.2hor analytic')
    axvhb.legend()

    plt.savefig('ORM_ana_vs_num.png', dpi=200)

    plt.show()


    pass


if __name__ == '__main__':

    #_test_correct_orbit(mode='analytic')

    # _test_analytic_numeric_orm()

#    _test_analytic_numeric_orm_full(errors=False, rad=False)
    _test_analytic_numeric_orm_full(errors=False, rad=True)

    pass
