import at
import os
import copy
import time
import math
import socket
import numpy as np
from os.path import exists
from matplotlib import pyplot as plt
import multiprocessing
import pickle
from itertools import repeat
from scipy.io import savemat, loadmat
from scipy.linalg import svdvals
import commissioningsimulations.config as config
from commissioningsimulations.correction.ClosedOrbit import compute_analytic_orbit_response_matrix, \
    compute_orbit_response_matrix
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.correction.optics_coupling.analytic_orm_with_normal_quad_errors import \
    analytic_orm_variation_with_normal_quadrupole
from commissioningsimulations.correction.optics_coupling.analytic_orm_with_skew_quad_errors import \
    analytic_orm_variation_with_skew_quadrupole
from commissioningsimulations.correction.optics_coupling.analytic_dispersion_with_quadrupole_errors import \
    semi_analytic_dispersion_variation_with_normal_quadrupole, analytic_dispersion_variation_with_skew_quadrupole
from commissioningsimulations.correction.optics_coupling.analytic_tune_with_quadrupole_errors import \
    analytic_tune_variation_with_normal_quadrupole


def get_orm_arrays(ring,
                   ind_bpms,
                   ind_cor,
                   bpms_weigths,
                   hor_disp_weight,
                   ver_disp_weight,
                   tune_weight,
                   mode,
                   verbose):
    """
    compute ORM, dispersion and tunes or loads a measured ones.
    apply weigths to BPMS, dispersion and tunes
    returns 2 1D arrays to be used for optics and coupling correction

    :param ring: AT lattice
    :param ind_bpms: index of BPMS in ring
    :param ind_cor: index of correctors in ring
    :param bpms_weigths: 2D numpy array of size (2 (H,V), len(ind_bpms)) to multiply each column of the response matrices
    :param hor_disp_weight: scalar to multiply horizontal dispersion
    :param ver_disp_weight: scalar to multiply vertical dispersion
    :param tune_weight: scalar to multiply tunes
    :param mode: 'numeric' (default), 'analytic', 'measured' (not yet functional)
    :param verbose: boolean. if True print out
    :return: ForQuadCor (1D np array of length(len(H orm)+len(V orm)+len(disph)+len(tunes))
             ForSkewCor (1D np array of length(len(H2V orm)+len(V2H orm)+len(dispv))
    """

    # compute ORM
    if mode == 'numeric':

        H, V, H2V, V2H, _, _, _, _, _, _, dh, dv, tunes = \
            compute_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=ind_cor, verbose=verbose)

    elif mode == 'analytic':

        H, V, H2V, V2H, _, _, _, _, _, _, dh, dv, tunes = \
            compute_analytic_orbit_response_matrix(ring,
                                                   ind_bpms=ind_bpms,
                                                   ind_cor=ind_cor,
                                                   verbose=verbose,
                                                   thick_steerers=True)

    elif mode == 'measured':
        raise NameError('measured mode not yet functional')

    else:
        raise NameError(f'{mode} is not an allowed mode. Use: numeric or analytic')

    # add BPM weigths (hill BPMS get a small weigth)
    if bpms_weigths:  # not None
        if bpms_weigths.shape == (2, len(ind_bpms)):
            # divide each bpm in each column by the corresponding bpm weigth
            H = H * bpms_weigths[0, :].T
            V = V * bpms_weigths[1, :].T
            H2V = H2V * bpms_weigths[1, :].T
            V2H = V2H * bpms_weigths[0, :].T
            dh = dh * bpms_weigths[0, :].T
            dv = dv * bpms_weigths[1, :].T
        else:
            raise ValueError(f'bpm_weigths must be a numpy array of size (2, {len(ind_bpms)})')

    # build arrays for fit
    ForQuadCor = np.concatenate((flat(H),  # Hcor 2 Horb
                                 flat(V),  # Vcor 2 Vorb
                                 hor_disp_weight * flat(dh),  # hor dispersion
                                 tune_weight * flat(tunes[0:2])))  # tunes H and V

    ForSkewCor = np.concatenate((flat(H2V),  # Hcor 2 Vorb
                                 flat(V2H),  # Vcor 2 Horb
                                 ver_disp_weight * flat(dv)))  # ver dispersion

    return ForQuadCor, ForSkewCor


def flat(MM):
    '2D to 1D'
    return np.matrix(MM).flatten().transpose()


def jacobian(ring,
             field,
             index_in_field,
             ring_index_to_modify,
             deltaL,
             ind_bpms,
             ind_cor,
             bpms_weigths,
             hor_disp_weight,
             ver_disp_weight,
             tune_weight,
             mode,
             field_is_integrated=False,
             verbose=False):
    """
    computes the derivative of the (ORM+dispersion+tune) array produced by get_orm_arrays with respect to a variation of
    ring[ring_index_to_modify].field[index_in_field]

    :param ring: AT lattice
    :param field: element field to modify (for example PolynomB)
    :param index_in_field: index in field to modify ( ex.: if field is PolynomB, 0 for dipole, 1 for quadrupole, etc..)
    :param ring_index_to_modify: index in the ring that has to be modified
    :param deltaL: integrated strength variation to apply to ring[ring_index_to_modify].field[index_in_field]
    :param ind_bpms: indexes of BPMs for Orbit Response matrix (get_orm_arrays)
    :param ind_cor: indexes of correctors for Orbit Response matrix (get_orm_arrays)
    :param bpms_weigths: weights to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weight for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weight for vertical dispersion (get_orm_arrays)
    :param tune_weight: weight for tunes (get_orm_arrays)
    :param mode: 'numeric', 'semi-analytic', 'analytic'(not yet functional)
                'numeric' will compute the ORM with tracking
                'semi-analytic' will vary ring[ring_index_to_modify].field[index_in_field] and compute
                                an analytic ORM based on this lattice
    :param field_is_integrated: defualt False. if True, do not divide by length of magnet
    :param verbose: if True, print out text
    :return: JQ = H+delta/2 - H-delta/2 / delta
             JS = V+delta/2 - V-delta/2 / delta
    """

    orm_mode=mode

    if mode == 'semi-analytic':
        orm_mode='analytic'

    if verbose:
        print(f'modify {ring_index_to_modify}th element in ring {ring[ring_index_to_modify].FamName} {field}[{index_in_field}] by {deltaL}')

    # if field_is_integrated is True (BendingAngle, KickAngle, etc..) delta is assumed integrated
    L = ring[ring_index_to_modify].Length

    if field_is_integrated or L == 0:  # for example for PolynomB/A. delta is then not integrated and needs to be multiplied by L
        L = 1

    # get initial vale
    initial_value = ring[ring_index_to_modify].__getattribute__(field)[index_in_field]

    # modify
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value + deltaL / L / 2

    if verbose:
        print(f'get ORM {len(ind_bpms)}x{len(ind_cor)}, dispersion and tunes')

    # get ORM
    q_p, s_p = get_orm_arrays(ring,
                              ind_bpms,
                              ind_cor,
                              bpms_weigths,
                              hor_disp_weight,
                              ver_disp_weight,
                              tune_weight,
                              orm_mode,
                              False)

    # modify
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value - deltaL / L / 2

    # get ORM
    q_m, s_m = get_orm_arrays(ring,
                              ind_bpms,
                              ind_cor,
                              bpms_weigths,
                              hor_disp_weight,
                              ver_disp_weight,
                              tune_weight,
                              orm_mode,
                              False)

    # restore original value
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value

    JQ = (q_p - q_m) / deltaL
    JS = (s_p - s_m) / deltaL

    return np.asarray(JQ), np.asarray(JS)


def analytic_jacobian(ring,
                     indexes=None,
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0):
    """
    compute the jacobian based on:

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param indexes: dictionry output of commissioningsimulations.get_indexes.get_lattice_indexes
    :param use_mp: not used
    :param n_processes: not used
    :param bpms_weigths: weigths to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weigth for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weigth for vertical dispersion (get_orm_arrays)
    :param tune_weight: weigth for tunes (get_orm_arrays)
    :param verbose: print text
    :return:    variation of [H2H, V2V, EtaH, Q] with quad and
                variation of [H2V, V2H, EtaV] with skew quad
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring, verbose=verbose)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']
    ind_quads = indexes['normal_quadrupoles_for_optics_and_coupling_fit']
    ind_skews = indexes['skew_quadrupoles_for_optics_and_coupling_fit']

    # compute optics before and only once
    _, _, opt_all = ring.linopt6(range(len(ring)))

    if verbose:
        print('computing analytic ORM derivative to normal quadrupoles')
    # derivative of diagonal ORM blocks respect to normal quadrupoles
    H_, V_ = analytic_orm_variation_with_normal_quadrupole(ring,
                                                           ind_bpms=ind_bpms,
                                                           ind_cors=ind_cors,
                                                           ind_quads=ind_quads,
                                                           opt_all_location=opt_all,
                                                           verbose=False,
                                                           use_mp=use_mp)

    if verbose:
        print('computing analytic ORM derivative to skew quadrupoles')
    # derivative of off-diagonal ORM blocks respect to skew quadrupoles
    H2V_, V2H_ = analytic_orm_variation_with_skew_quadrupole(ring,
                                                             ind_bpms=ind_bpms,
                                                             ind_cors=ind_cors,
                                                             ind_skews=ind_skews,
                                                             opt_all_location=opt_all,
                                                             verbose=False,
                                                             use_mp=use_mp)

    if verbose:
        print('computing analytic hor. dispersion derivative to normal quadrupoles')
    # analyitic dispersion response
    dh_ = semi_analytic_dispersion_variation_with_normal_quadrupole(ring,
                                                             ind_bpms=ind_bpms,
                                                             ind_quads=ind_quads,
                                                             verbose=False,
                                                             use_mp=use_mp)
    if verbose:
        print('computing analytic ver. dispersion derivative to skew quadrupoles')
    dv_ = analytic_dispersion_variation_with_skew_quadrupole(ring,
                                                            ind_bpms=ind_bpms,
                                                            ind_skews=ind_skews,
                                                            verbose=False,
                                                            opt_all_location=opt_all,
                                                            use_mp=use_mp)
    if verbose:
        print('computing analytic tune derivative to normal quadrupoles')
    # tune analytic response (TO CHECK)
    tunes_ = analytic_tune_variation_with_normal_quadrupole(ring,
                                                            ind_quads=ind_quads,
                                                            verbose=False,
                                                            opt_all_location=opt_all)

    JQ = np.zeros((len(ind_bpms) * len(ind_cors) * 2 + len(ind_bpms) + 2, len(ind_quads)))
    JS = np.zeros((len(ind_bpms) * len(ind_cors) * 2 + len(ind_bpms), len(ind_skews)))

    # apply weigths and flatten to array
    for iq, _ in enumerate(ind_quads):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                H = H_[:, :, iq] * bpms_weights[0, :].T
                V = V_[:, :, iq] * bpms_weights[1, :].T
                dh = dh_[:, iq] * bpms_weights[0, :].T
                tunes = [tunes_[0][iq], tunes_[1][iq]]
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            H = H_[:, :, iq]
            V = V_[:, :, iq]
            dh = dh_[:, iq]
            tunes = [tunes_[0][iq], tunes_[1][iq]]

        # build arrays for fit
        JQ[:, iq] = np.asarray(np.concatenate((flat(H),  # Hcor 2 Horb
                                    flat(V),  # Vcor 2 Vorb
                                    hor_disp_weight * flat(dh),  # hor dispersion
                                    tune_weight * flat(tunes))))[:, 0]  # tunes H and V

    for iq, _ in enumerate(ind_skews):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                H2V = H2V_[:, :, iq] * bpms_weights[1, :].T
                V2H = V2H_[:, :, iq] * bpms_weights[0, :].T
                dv = dv_[:, iq] * bpms_weights[1, :].T
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            H2V = H2V_[:, :, iq]
            V2H = V2H_[:, :, iq]
            dv = dv_[:, iq]

            # build arrays for fit
        JS[:, iq] = np.asarray(np.concatenate((flat(H2V),  # Hcor 2 Vorb
                                    flat(V2H),  # Vcor 2 Horb
                                    ver_disp_weight * flat(dv))))[:, 0]  # ver dispersion

    return JQ, JS


def compute_jacobian(ring,
                     indexes=None,
                     filename_response='./ORMDerivative.pkl',
                     save_matlab_file=False,
                     mode='numeric',  # 'semi-analytic', 'analytic'
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0,
                     DKnL = 1e-4,
                     DKsL = 1e-4,
                     field_is_integrated=False
                     ):
    """
    computes the derivative of the orm+dispersion+tune vectors produced by get_orm_array with respect to the
    list of normal and skew quadrupole correctors specified in config.normal(skew)_quadrupoles_for_optics_fit

    :param ring: AT lattice
    :param indexes: dictionary of indexes obtained with commissioningsimulations.config.get_lattice_indexes
    :param filename_response: file where to store the computed derivatives (large file). if None, no file is saved)
    :param save_matlab_file: save to .mat format
    :param mode: 'numeric' (default), 'analytic', 'semi-analytic'  (see commissioningsimulations.CorrectOptics.jacobian)
    :param use_mp: use multiprocessing on local host
    :param n_processes: number of cores to use. if None, use all
    :param verbose: if True, print text
    :param bpms_weights: see get_orm_array
    :param tune_weight:  see get_orm_array
    :param hor_disp_weight:  see get_orm_array
    :param ver_disp_weight:  see get_orm_array
    :param DKnL: variation of normal quardupoles
    :param DKsL: variation of skew quadrupoles
    :return: JQ derivative of first output of get_orm_array respect to normal quadrupoles,
             JS derivative of first output of get_orm_array respect to skew quadrupoles,
             sQ singular values of JQ
             sS singular values of JS
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']
    ind_quads = indexes['normal_quadrupoles_for_optics_and_coupling_fit']
    ind_skews = indexes['skew_quadrupoles_for_optics_and_coupling_fit']

    NB = len(ind_bpms)
    NC = len(ind_cors)
    NQ = len(ind_quads)
    NS = len(ind_skews)

    if verbose:
        print(f'optics/coupling errors fit\n '
              f'orbit response matrix size: ({NB}, {NC}) \n'
              f'{NQ} {config.normal_quadrupoles_for_optics_and_coupling_fit} normal quadrupoles used for fit\n'
              f'{NS} {config.skew_quadrupoles_for_optics_and_coupling_fit} skew quadrupoles used for fit')

    if mode == 'numeric' or mode == 'semi-analytic':

        # initialize RM, just to get sizes
        H0, V0 = get_orm_arrays(ring,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                tune_weight,
                                'analytic',
                                False)

        JQ = np.zeros((len(H0), NQ))
        JS = np.zeros((len(V0), NS))

        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            if n_processes is None:
                n_processes = n_cpu
            else:
                max_proc = min(n_cpu, NC)
                if n_processes > max_proc:
                    n_processes = max_proc

            print(f'RM derivative parallel computation using {n_processes} cores on {socket.gethostname()}')

            with multiprocessing.Pool(processes=n_processes) as p:

                results_normal = p.starmap(jacobian,
                                        zip(repeat(ring),
                                            repeat('PolynomB'),
                                            repeat(1),
                                            ind_quads,  # loop variable
                                            repeat(DKnL),
                                            repeat(ind_bpms),
                                            repeat(ind_cors),
                                            repeat(bpms_weights),
                                            repeat(hor_disp_weight),
                                            repeat(ver_disp_weight),
                                            repeat(tune_weight),
                                            repeat(mode), repeat(field_is_integrated), repeat(verbose)))

            with multiprocessing.Pool(processes=n_processes) as p:
                results_skew = p.starmap(jacobian,
                                         zip(repeat(ring),
                                             repeat('PolynomA'),
                                             repeat(1),
                                             ind_skews, # loop variable
                                             repeat(DKsL),
                                             repeat(ind_bpms),
                                             repeat(ind_cors),
                                             repeat(bpms_weights),
                                             repeat(hor_disp_weight),
                                             repeat(ver_disp_weight),
                                             repeat(tune_weight),
                                             repeat(mode), repeat(field_is_integrated), repeat(verbose)))

            # collect results
            for count, ic in enumerate(ind_quads):
                JQ[:, count] = results_normal[count][0][:, 0]  # ensure correct dimensionality

            # collect results
            for count, ic in enumerate(ind_skews):
                JS[:, count] = results_skew[count][1][:, 0] # ensure correct dimensionality

        else:

            for count, ic in enumerate(ind_quads):

                jq, _ = jacobian(ring,
                                 'PolynomB',
                                 1,
                                 ic,
                                 DKnL,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 tune_weight,
                                 mode, verbose=True)
                JQ[:, count] = jq[:, 0]  # ensure correct dimensionality

            for count, ic in enumerate(ind_skews):
                _, js = jacobian(ring,
                                 'PolynomA',
                                 1,
                                 ic,
                                 DKsL,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 tune_weight,
                                 mode, verbose=True)

                JS[:, count] = js[:, 0]

    elif mode == 'analytic':

        JQ, JS = analytic_jacobian(ring,
                          indexes=indexes,
                          use_mp=use_mp,
                          n_processes=n_processes,
                          verbose=verbose,
                          bpms_weights=bpms_weights,
                          tune_weight=tune_weight,
                          hor_disp_weight=hor_disp_weight,
                          ver_disp_weight=ver_disp_weight)

    # invert and get singular values
    if verbose:
        print(f'computing inverse of orm derivative')

    #start_time = time.time()
    #_, sQ, _ = np.linalg.svd(JQ, full_matrices=True)
    sQ = svdvals(JQ)
    #print(f'time for SVD of {JQ.shape}: {time.time()-start_time}')

    #start_time = time.time()
    #_, sS, _ = np.linalg.svd(JS, full_matrices=True)
    sS = svdvals(JS)
    #print(f'time for SVD of {JS.shape}: {time.time()-start_time}')

    if filename_response:

        mdict = {'sQ': sQ, 'sS': sS,
                 'JQ': JQ, 'JS': JS,
                 'bpms_weights': bpms_weights,
                 'hor_disp_wight': hor_disp_weight,
                 'ver_disp_wight': hor_disp_weight,
                 'tune_wight': hor_disp_weight,
                 'ind_bpms': ind_bpms,
                 'ind_cors': ind_cors,
                 'ind_quad': ind_quads,
                 'ind_skew': ind_skews}

        file_no_ext, exten_file = os.path.splitext(filename_response)

        if save_matlab_file:
            try:
                savemat(file_no_ext + '.mat', mdict=mdict)
            except Exception as e:
                print(e)
                print('could not save .mat file')

        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        while not(exists(filename_response)):
            print('Wait for file {} to be saved'.format(filename_response))
            time.sleep(1)

        print('ORM derivatives saved in: {}'.format(filename_response))

    return JQ, JS, sQ, sS


def fit_optics(ring_to_correct,
               reference_ring,
               niter=1,
               neigQuad=1,
               neigSkew=1,
               bpms_weights=None,
               hor_disp_weight=1.0,
               ver_disp_weight=1.0,
               tune_weight=1.0,
               mode='numeric',
               indexes=None,
               filename_response=None,
               reference_orm=None,
               use_mp=False,
               verbose=True,
               normal=True,
               skew=True):
    """
    fit quadrupole errors
    these errors added to reference_ring will return an ORM as close as possible to the one of ring_to_correct
    the fitted errors are applied as correction to ring_to_correct to output a corrected lattice rcor

    :param ring_to_correct: ring with optics/coupling errors
    :param reference_ring: reference ring, no errors
    :param niter: number of iteration of correction
    :param neigQuad: # of singular vectors to use for quad correction
    :param neigSkew: # of singular vectors to use for quad correction
    :param bpms_weights: (2, nbpms) array of multiplication factors for each raw of the RM.
    :param hor_disp_weight: multiplication factor for hor disperison
    :param ver_disp_weight: multiplication factor for ver dispersion
    :param tune_weight: multiplication factor for tunes
    :param mode: 'numeric', 'semi-analytic', 'analytic'
    :param indexes: indexes obtained by commissioningsimulations.config.get_lattice_indexes. if none, thecomputed.
    :param filename_response: file where to look for derivative of ORM. if not found, computed
    :param reference_orm: as computed by get_orm_arrays, or None (compute it)
    :param use_mp: use multiprocessing
    :param verbose: if True, print text
    :return: rfit fitted model for ring_to_correct including only quadrupole errors
             rcor corrected AT lattice
             dcQ applied normal quadrupole correctors variations
             dcS applied skew quadrupole correctors variations
    """

    if verbose:
        print('correct optics')

    force_matrix_computation = False

    orm_mode = mode
    if mode == 'semi-analytic':
        orm_mode='analytic'

    # test, overwrite ORM mode to keep always numeric
    orm_mode='numeric'

    if not(indexes):
        indexes = get_lattice_indexes(reference_ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']
    ind_quad = indexes['normal_quadrupoles_for_optics_and_coupling_fit']
    ind_skew = indexes['skew_quadrupoles_for_optics_and_coupling_fit']

    # get copy of ring
    rcor = copy.deepcopy(ring_to_correct)
    rfit = copy.deepcopy(reference_ring)

    # print initial status
    if verbose:
        print('fit-lattice vs input-lattice (should be zero after fit):')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

    #if verbose:
    #    print('Before optics/coupling fit (should be zero after fit):')
    #    get_optics_vs_reference(reference_ring, ring_to_correct, ind_bpms)

    # get Orbit Response Matrix if no file is provided, based on lattice with errors.
    if (not exists(filename_response)) or force_matrix_computation:
        start_time = time.time()
        JQ, JS, sQ, sS = compute_jacobian(reference_ring,
                                          indexes=indexes,
                                          filename_response=filename_response,
                                          save_matlab_file=True,
                                          mode=mode,  # 'semi-analytic', 'analytic'
                                          use_mp=use_mp, #False, #
                                          n_processes=None,
                                          bpms_weights=bpms_weights,
                                          tune_weight=tune_weight,
                                          hor_disp_weight=hor_disp_weight,
                                          ver_disp_weight=ver_disp_weight,
                                          verbose=True)  # all if None
        end_time = time.time()
        if verbose:
            print(f'time for (parallel = {use_mp}) orm derivative computation: {end_time-start_time} seconds')
    else:

        _, file_ext = os.path.splitext(filename_response)
        if file_ext == '.mat':
            mat_contents = loadmat(filename_response)
            JQ = mat_contents['JQ']
            JS = mat_contents['JS']
            sS = np.transpose(mat_contents['sS'])  # load mat transposes raws/columns for some reason
            sS = sS[:, 0]
            sQ = np.transpose(mat_contents['sQ'])  # load mat transposes raws/columns for some reason
            sQ = sQ[:, 0]
        else:
            mat_contents = pickle.load(open(filename_response, 'rb'))
            JQ = mat_contents['JQ']
            JS = mat_contents['JS']
            sS = mat_contents['sS']
            sQ = mat_contents['sQ']
        if verbose:
            print(f'loaded: {filename_response}')

    # fix  singular values to max possible if too many
    if neigQuad > JQ.shape[1]:
        neigQuad = JQ.shape[1] -1
        if verbose:
            print(f'max singular values for normal quadrupoles = {JQ.shape[1]}')
    if neigSkew > JS.shape[1]:
        neigSkew = JS.shape[1] -1
        if verbose:
            print(f'max singular values for skew quadrupoles = {JS.shape[1]}')

    # get initial correctors values
    # Lq = at.get_value_refpts(rcor, ind_quad, 'Length')
    # Ls = at.get_value_refpts(rcor, ind_skew, 'Length')

    initcorQ = at.get_value_refpts(rcor, ind_quad, 'PolynomB', 1)
    initcorS = at.get_value_refpts(rcor, ind_skew, 'PolynomA', 1)

    # get initial correctors values in rfit
    initcorQ0 = at.get_value_refpts(rfit, ind_quad, 'PolynomB', 1)
    initcorS0 = at.get_value_refpts(rfit, ind_skew, 'PolynomA', 1)

    # initialize corrections to zero. INTEGRATED STRENGHTS
    dcQ = np.zeros(len(ind_quad))
    dcS = np.zeros(len(ind_skew))

    # get ORM to correct
    Hinit, Vinit = get_orm_arrays(rcor,
                                  ind_bpms,
                                  ind_cors,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  'numeric',
                                  False)

    # get reference ORM
    if reference_orm==None:
        H0, V0 = get_orm_arrays(reference_ring,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                tune_weight,
                                orm_mode,  # could be analytic
                                False)
    else:
        H0 = reference_orm[0]
        V0 = reference_orm[1]

    for correction_iteration in range(0, niter):
        print('iteration # {}'.format(correction_iteration + 1))

        # get ORM
        if correction_iteration>0:
            H, V = get_orm_arrays(rcor,
                                  ind_bpms,
                                  ind_cors,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  orm_mode,
                                  False)

        else:  # use already available
            H = Hinit
            V = Vinit

        # compute correction and add to previous iterations
        ooh = np.asarray(H - H0)[:, 0]
        oov = np.asarray(V - V0)[:, 0]

        if normal:
            dcQ = dcQ + np.linalg.pinv(JQ, rcond=sQ[neigQuad - 1] / sQ[0]) @ ooh

        if skew:
            dcS = dcS + np.linalg.pinv(JS, rcond=sS[neigSkew - 1] / sS[0]) @ oov

        # check that the corrections are <30 mircorad
        if verbose:
            print(f' ORM for quad | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
            print(f' ORM for skew | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')
            print(f' norm quad cor. | std: {np.std(dcQ):.3g} | (min, max) ({np.min(dcQ):.3g}, {np.max(dcQ):.3g})')
            print(f' skew quad cor. | std: {np.std(dcS):.3g} | (min, max) ({np.min(dcS):.3g}, {np.max(dcS):.3g})')

        # apply correction
        for count, ic in enumerate(ind_quad):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomB[1] = initcorQ[count] - dcQ[count] / L
            # assign errors to optics model
            rfit[ic].PolynomB[1] = initcorQ0[count] + dcQ[count] / L

        for count, ic in enumerate(ind_skew):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomA[1] = initcorS[count] - dcS[count] / L
            # assign errors to optics model
            rfit[ic].PolynomA[1] = initcorS0[count] + dcS[count] / L

    # get Orbit RM after correction
    Haft, Vaft = get_orm_arrays(rcor,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                tune_weight,
                                orm_mode,
                                False)

    ooh = np.asarray(Haft - H0)[:, 0]
    oov = np.asarray(Vaft - V0)[:, 0]
    if verbose:
        print(f'Final ORM for quad | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
        print(f'Final ORM for skew | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')

    if verbose:
        print('fit-lattice vs input-lattice:')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

#    if verbose:
#        print('After optics/coupling fit:')
#        get_optics_vs_reference(reference_ring, rcor, ind_bpms)

    return rfit, rcor, dcQ, dcS


def _test_optics_coupling_correction():
    '''
    script to test optics correction
    :return: None
    '''

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    config.radiation = False

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # shift sextupoles to generate quad and skew quad
    ind_sext = list(at.get_refpts(ring, config.sextupoles_fam_name_pattern))

    ind_to_use = [10, 30]
    ind_sext = ind_sext[ind_to_use]  # chose a single quadrupole

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    NQ = int(len(ind_sext))
    dx[ind_sext] = 10e-6 * np.random.randn(NQ)
    dy[ind_sext] = 10e-6 * np.random.randn(NQ)

    rerr.set_shift(dx, dy)

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)

    # correct optics
    onestep=False
    mode = 'analytic'

    if onestep:
        rfit, rcor, Qcor, Scor = fit_optics(rerr,
                              ring,
                              niter=3,
                              neigQuad=30,
                              neigSkew=30,
                              hor_disp_weight=100,
                              ver_disp_weight=100,
                              mode=mode,
                              indexes=indexes,
                              use_mp=True,
                              filename_response='./J' + mode + '.pkl')

    else:

        # correct optics
        rfit, _, Qcor, _ = fit_optics(
                            rerr,
                            ring,
                            niter=3,
                            neigQuad=30,
                            neigSkew=30,
                            hor_disp_weight=100,
                            ver_disp_weight=100,
                            mode=mode,
                            indexes=indexes,
                            use_mp=True,
                            filename_response='./J' + mode + '.pkl',
                            skew=False)

        # correct coupling
        rfit, _, _, Scor = fit_optics(
                            rerr,  # assume same measured lattice
                            rfit,  # assume quadrupole errors from step before
                            niter=3,
                            neigQuad=30,
                            neigSkew=30,
                            hor_disp_weight=100,
                            ver_disp_weight=100,
                            mode=mode,
                            indexes=indexes,
                            use_mp=True,
                            filename_response='./J' + mode + '.pkl',
                            normal=False)

        # compute rcor
        rcor = copy.deepcopy(rerr)

        # apply correction
        for count, ic in enumerate(indexes['normal_quadrupoles_for_optics_and_coupling_fit']):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomB[1] = rcor[ic].PolynomB[1] - Qcor[count] / L

        for count, ic in enumerate(indexes['skew_quadrupoles_for_optics_and_coupling_fit']):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomA[1] = rcor[ic].PolynomA[1] - Scor[count] / L

    # optics after correction
    # rcor.plot_beta()

    print('BEFORE CORRECTION')
    bbh,  bbv,  ddh,  ddv,  deh,  dev = get_optics_vs_reference(ring, rerr, indexes['bpms'])

    print('AFTER CORRECTION')
    bbha, bbva, ddha, ddva, deha, deva = get_optics_vs_reference(ring, rcor, indexes['bpms'])

    # plot figures
    fig, ((axbh, axdh), (axbv, axdv), (axQ, axS)) = plt.subplots(nrows=3, ncols=2)

    axbh.plot(bbh*100, label=f'before $\delta\epsilon_h$ = {deh*1e12:.2f}')
    axbh.plot(bbha*100, label=f'after $\delta\epsilon_h$ = {deha*1e12:.2f}')
    axbh.set_ylabel(f' hor. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbh.legend()
    axbv.plot(bbv * 100, label=f'before $\delta\epsilon_h$ = {dev*1e12:.2f}')
    axbv.plot(bbva * 100, label=f'after $\delta\epsilon_h$ = {deva*1e12:.2f}')
    axbv.set_ylabel(f' ver. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbv.legend()

    axdh.plot(ddh * 100, label='before')
    axdh.plot(ddha * 100, label='after')
    axdh.set_ylabel(f' hor. $(\eta-\eta_0)/\beta_0$ %')
    axdh.legend()
    axdv.plot(ddv * 100, label='before')
    axdv.plot(ddva * 100, label='after')
    axdv.set_ylabel(f' ver. $(\eta-\eta_0)/\beta_0$ %')
    axdv.legend()

    # axQ.plot(dx*ring[ind_sext].PolynomB[2], label='set errors')
    axQ.plot(Qcor, label='after')
    axQ.set_ylabel(f' Quad cor ')
    axQ.legend()
    # axS.plot(dy*ring[ind_sext].PolynomB[2]/2, label='set error')
    axS.plot(Scor, label='after')
    axS.set_ylabel(f' Skew cor ')
    axS.legend()

    plt.show()

    pass


def _test_compare_jacobian_speed():

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

#    modes = ['analytic', 'semi-analytic', 'numeric']
#    speed_test = [0, 0, 0]
    modes = ['analytic', 'numeric']
    speed_test = [0, 0]

    for c, m in enumerate(modes):
        start_time = time.time()

        compute_jacobian(ring, filename_response='./J' + m + '.pkl', mode=m, verbose=False, use_mp=True, DKnL=1e-8)

        end_time = time.time()

        speed_test[c] = end_time - start_time

        print(f'{m} J took: {speed_test[c]} seconds')

    np.savetxt(f'SpeedTest_analytic_numeric.txt', speed_test, fmt='%10.5f', delimiter=',')

    return speed_test


def _test_compare_jacobians(magnets=[0]):

    import pickle
    from os.path import exists
    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    modes = ['analytic', 'semi-analytic', 'numeric']
    for m in modes:
        if not(exists('./J'+m+'.pkl')):
            compute_jacobian(ring,
            filename_response='./J' + m + '.pkl',
            mode=m,
            verbose=False, use_mp=True)

    numeric = pickle.load(open('./Jnumeric.pkl', 'rb'))
    semiana = pickle.load(open('./Jsemi-analytic.pkl', 'rb'))
    analyti = pickle.load(open('./Janalytic.pkl', 'rb'))

    fig, (axq, axs) = plt.subplots(ncols=2, nrows=1, figsize=(10, 5))
    fig.subplots_adjust(hspace=.5, wspace=.5)

    for count, m in enumerate(magnets):

        axq.plot(numeric['JQ'][:, m], semiana['JQ'][:, m], '.', color=[1, 0, 0], label='semi-analytic')
        axq.plot(numeric['JQ'][:, m], analyti['JQ'][:, m], '.', color=[0, 0, 1], label='analytic')
        axq.set_xlabel('numeric JQ')
        if count==0:
            axq.legend()

        axs.plot(numeric['JS'][:, m], semiana['JS'][:, m], '.', color=[1, 0, 0], label='semi-analytic')
        axs.plot(numeric['JS'][:, m], analyti['JS'][:, m], '.', color=[0, 0, 1], label='analytic')
        axs.set_xlabel('numeric JS')
        if count==0:
            axs.legend()

    v = 100
    axq.plot([-v, v], [-v, v], ':', color='g', label='x=y')
    axs.plot([-v, v], [-v, v], ':', color='g', label='x=y')

    plt.show()
    pass

if __name__=='__main__':


    _test_optics_coupling_correction()
    # _test_compare_jacobian_speed()
    # _test_compare_jacobians(magnets=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    pass
