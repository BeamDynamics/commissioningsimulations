import at
import os
import copy
import time
import math
import socket
import numpy as np
from os.path import exists
from matplotlib import pyplot as plt
import multiprocessing
import pickle
from itertools import repeat
from scipy.io import savemat, loadmat
import commissioningsimulations.config as config
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.correction.ClosedOrbit import \
    compute_orbit_response_matrix, compute_analytic_orbit_response_matrix
from commissioningsimulations.correction.optics_coupling.RDT import \
    normal_quad_rdts_response, skew_quad_rdts_response, EquivalentGradientsFromAlignments6D
from commissioningsimulations.correction.optics_coupling.analytic_dispersion_with_quadrupole_errors import \
    analytic_dispersion_variation_with_skew_quadrupole, semi_analytic_dispersion_variation_with_normal_quadrupole
from commissioningsimulations.correction.optics_coupling.analytic_tune_with_quadrupole_errors import \
    analytic_tune_variation_with_normal_quadrupole


__author__ = 'Simone Maria Liuzzo, Andrea Franchi, Lina Hoummi'


def get_rdt_arrays(ring,
                   ind_bpms,
                   ind_quads,
                   ind_skews,
                   bpms_weigths,
                   hor_disp_weight,
                   ver_disp_weight,
                   tune_weight,
                   mode,
                   normal_rdt_resp=None,
                   skew_rdt_resp=None,
                   verbose=False):
    """
    compute RDT, dispersion and tunes arrays.
    apply weigths to BPMS, dispersion and tunes
    returns 2 1D arrays to be used for optics and coupling correction

    :param ring: AT lattice
    :param ind_bpms: index of BPMS in ring
    :param bpms_weigths: 2D numpy array of size (2 (H,V), len(ind_bpms)) to multiply each column of the response matrices
    :param hor_disp_weight: scalar to multiply horizontal dispersion
    :param ver_disp_weight: scalar to multiply vertical dispersion
    :param tune_weight: scalar to multiply tunes
    :param mode: 'numeric' (default), 'analytic', 'measured' (not yet functional)
    :param verbose: boolean. if True print out
    :return: ForQuadCor (1D np array of length(len(H orm)+len(V orm)+len(disph)+len(tunes))
             ForSkewCor (1D np array of length(len(H2V orm)+len(V2H orm)+len(dispv))
    """

    if normal_rdt_resp is None:
        f2000_rdt_resp, f0020_rdt_resp, _ = normal_quad_rdts_response(ring, ind_bpms, ind_quads)
    else:
        f2000_rdt_resp = normal_rdt_resp[0]
        f0020_rdt_resp = normal_rdt_resp[1]

    if skew_rdt_resp is None:
        f1001_rdt_resp, f1010_rdt_resp, _ = skew_quad_rdts_response(ring, ind_bpms, ind_skews)
    else:
        f1001_rdt_resp = skew_rdt_resp[0]
        f1010_rdt_resp = skew_rdt_resp[1]

    Ln = ring.get_value_refpts(ind_quads, 'Length')
    Ls = ring.get_value_refpts(ind_skews, 'Length')

    if np.array_equal(ind_quads, ind_skews):
        kn, _, _ = EquivalentGradientsFromAlignments6D(ring, ind_quads)
        _, ks, _ = EquivalentGradientsFromAlignments6D(ring, ind_skews)
    else:
        kn, ks, _ = EquivalentGradientsFromAlignments6D(ring, ind_quads)

    KnL = np.array(kn * Ln)
    KsL = np.array(ks * Ls)

    H = np.concatenate((f2000_rdt_resp.real @ KnL,
                        f2000_rdt_resp.imag @ KnL,
                        f0020_rdt_resp.real @ KnL,
                        f0020_rdt_resp.imag @ KnL))

    V = np.concatenate((f1001_rdt_resp.real @ KsL,
                        f1001_rdt_resp.imag @ KsL,
                        f1010_rdt_resp.real @ KsL,
                        f1010_rdt_resp.imag @ KsL))

    # compute ORM
    if mode == 'numeric':

        _, _, _, _, _, _, _, _, _, _, dh, dv, tunes = \
            compute_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=[], verbose=verbose)

    elif mode == 'analytic':

        _, _, _, _, _, _, _, _, _, _, dh, dv, tunes = \
            compute_analytic_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=[], verbose=verbose)

    elif mode == 'measured':
        raise NameError('measured mode not yet functional')

    else:
        raise NameError(f'{mode} is not an allowed mode. Use: numeric or analytic')

    # add BPM weigths (hill BPMS get a small weigth)
    if bpms_weigths:  # not None
        if bpms_weigths.shape == (2, len(ind_bpms)):
            # divide each bpm in each column by the corresponding bpm weigth
            H = H * bpms_weigths[0, :].T
            V = V * bpms_weigths[1, :].T
            dh = dh * bpms_weigths[0, :].T
            dv = dv * bpms_weigths[1, :].T
        else:
            raise ValueError(f'bpm_weigths must be a numpy array of size (2, {len(ind_bpms)})')

    # build arrays for fit
    ForQuadCor = np.concatenate((flat(H),
                                 hor_disp_weight * flat(dh),  # hor dispersion
                                 tune_weight * flat(tunes[0:2])))  # tunes H and V

    ForSkewCor = np.concatenate((flat(V),
                                 ver_disp_weight * flat(dv)))  # ver dispersion

    return ForQuadCor, ForSkewCor


def flat(MM):
    '2D to 1D'
    return np.matrix(MM).flatten().transpose()


def jacobian(ring,
             field,
             index_in_field,
             ring_index_to_modify,
             deltaL,
             ind_bpms,
             ind_cor,
             bpms_weigths,
             hor_disp_weight,
             ver_disp_weight,
             tune_weight,
             mode,
             field_is_integrated=False,
             verbose=False):
    """
    computes the derivative of the (ORM+dispersion+tune) array produced by get_orm_arrays with respect to a variation of
    ring[ring_index_to_modify].field[index_in_field]

    :param ring: AT lattice
    :param field: element field to modify (for example PolynomB)
    :param index_in_field: index in field to modify ( ex.: if field is PolynomB, 0 for dipole, 1 for quadrupole, etc..)
    :param ring_index_to_modify: index in the ring that has to be modified
    :param deltaL: integrated strength variation to apply to ring[ring_index_to_modify].field[index_in_field]
    :param ind_bpms: indexes of BPMs for Orbit Response matrix (get_orm_arrays)
    :param ind_cor: indexes of correctors for Orbit Response matrix (get_orm_arrays)
    :param bpms_weigths: weights to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weight for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weight for vertical dispersion (get_orm_arrays)
    :param tune_weight: weight for tunes (get_orm_arrays)
    :param mode: 'numeric', 'semi-analytic', 'analytic'(not yet functional)
                'numeric' will compute the ORM with tracking
                'semi-analytic' will vary ring[ring_index_to_modify].field[index_in_field] and compute
                                an analytic ORM based on this lattice
    :param field_is_integrated: defualt False. if True, do not divide by length of magnet
    :param verbose: if True, print out text
    :return: JQ = H+delta/2 - H-delta/2 / delta
             JS = V+delta/2 - V-delta/2 / delta
    """

    orm_mode=mode

    if mode == 'semi-analytic':
        orm_mode='analytic'

    if verbose:
        print(f'modify {ring_index_to_modify}th element in ring {ring[ring_index_to_modify].FamName} {field}[{index_in_field}] by {deltaL}')

    # if field_is_integrated is True (BendingAngle, KickAngle, etc..) delta is assumed integrated
    L = ring[ring_index_to_modify].Length

    if field_is_integrated or L == 0:  # for example for PolynomB/A. delta is then not integrated and needs to be multiplied by L
        L = 1

    # get initial vale
    initial_value = ring[ring_index_to_modify].__getattribute__(field)[index_in_field]

    # modify
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value + deltaL / L / 2

    if verbose:
        print(f'get ORM {len(ind_bpms)}x{len(ind_cor)}, dispersion and tunes')

    # get ORM
    q_p, s_p = get_rdt_arrays(ring,
                   ind_bpms,
                   ind_quads,
                   ind_skews,
                   bpms_weigths,
                   hor_disp_weight,
                   ver_disp_weight,
                   tune_weight,
                   orm_mode,
                   verbose=False)

    # modify
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value - deltaL / L / 2

    # get ORM
    q_m, s_m = get_rdt_arrays(ring,
                   ind_bpms,
                   ind_quads,
                   ind_skews,
                   bpms_weigths,
                   hor_disp_weight,
                   ver_disp_weight,
                   tune_weight,
                   orm_mode,
                   verbose=False)

    # restore original value
    ring[ring_index_to_modify].__getattribute__(field)[index_in_field] = initial_value

    JQ = (q_p - q_m) / deltaL
    JS = (s_p - s_m) / deltaL

    return np.asarray(JQ), np.asarray(JS)


def analytic_jacobian(ring,
                     indexes=None,
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0):
    """
    compute the jacobian based on:

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param indexes: dictionry output of commissioningsimulations.get_indexes.get_lattice_indexes
    :param use_mp: not used
    :param n_processes: not used
    :param bpms_weigths: weigths to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weigth for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weigth for vertical dispersion (get_orm_arrays)
    :param tune_weight: weigth for tunes (get_orm_arrays)
    :param verbose: print text
    :return:    variation of [H2H, V2V, EtaH, Q] with quad and
                variation of [H2V, V2H, EtaV] with skew quad
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring, verbose=verbose)

    ind_bpms = indexes['bpms']
    ind_quads = indexes['normal_quadrupoles_for_optics_and_coupling_correction']
    ind_skews = indexes['skew_quadrupoles_for_optics_and_coupling_correction']

    # compute optics before and only once
    _, _, opt_all = ring.linopt6(range(len(ring)))

    if verbose:
        print('computing analytic RDTs derivative to normal quadrupoles')

    f2000_rdt_resp, f0020_rdt_resp, _ = normal_quad_rdts_response(ring, ind_bpms, ind_quads)

    if verbose:
        print('computing analytic RDTs derivative to skew quadrupoles')

    f1001_rdt_resp, f1010_rdt_resp, _ = skew_quad_rdts_response(ring, ind_bpms, ind_skews)

    cH = np.concatenate((f2000_rdt_resp , f0020_rdt_resp ))
    H_ = np.concatenate([cH.real, cH.imag])
    cV = np.concatenate((f1001_rdt_resp , f1010_rdt_resp ))
    V_ = np.concatenate([cV.real, cV.imag])

    if verbose:
        print('computing analytic hor. dispersion derivative to normal quadrupoles')
    # analyitic dispersion response
    dh_ = semi_analytic_dispersion_variation_with_normal_quadrupole(ring,
                                                             ind_bpms=ind_bpms,
                                                             ind_quads=ind_quads,
                                                             verbose=False,
                                                             use_mp=use_mp)
    if verbose:
        print('computing analytic ver. dispersion derivative to skew quadrupoles')
    dv_ = analytic_dispersion_variation_with_skew_quadrupole(ring,
                                                            ind_bpms=ind_bpms,
                                                            ind_skews=ind_skews,
                                                            verbose=False,
                                                            thick=True,
                                                            opt_all_location=opt_all,
                                                            use_mp=use_mp)
    if verbose:
        print('computing analytic tune derivative to normal quadrupoles')
    # tune analytic response (TO CHECK)
    tunes_ = analytic_tune_variation_with_normal_quadrupole(ring,
                                                            ind_quads=ind_quads,
                                                            verbose=False,
                                                            thick=True,
                                                            opt_all_location=opt_all)

    JQ = np.zeros((len(ind_bpms) * 4 + len(ind_bpms) + 2, len(ind_quads)))
    JS = np.zeros((len(ind_bpms) * 4 + len(ind_bpms), len(ind_skews)))

    # apply weigths and flatten to array
    for iq, _ in enumerate(ind_quads):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                H = H_[:, iq] * np.concatenate((bpms_weights[0, :].T, bpms_weights[0, :].T))
                dh = dh_[:, iq] * bpms_weights[0, :].T
                tunes = [tunes_[0][iq], tunes_[1][iq]]
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            H = H_[:, iq]
            dh = dh_[:, iq]
            tunes = [tunes_[0][iq], tunes_[1][iq]]

        # build arrays for fit
        JQ[:, iq] = np.asarray(np.concatenate((flat(H),  # Hcor 2 Horb
                                    hor_disp_weight * flat(dh),  # hor dispersion
                                    tune_weight * flat(tunes))))[:, 0]  # tunes H and V

    for iq, _ in enumerate(ind_skews):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                V = V_[:, iq] * np.concatenate((bpms_weights[1, :].T, bpms_weights[1, :].T))
                dv = dv_[:, iq] * bpms_weights[1, :].T
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            V = V_[:, iq]
            dv = dv_[:, iq]

            # build arrays for fit
        JS[:, iq] = np.asarray(np.concatenate((flat(V),  # Hcor 2 Vorb
                                    ver_disp_weight * flat(dv))))[:, 0]  # ver dispersion

    return JQ, JS


def compute_jacobian(ring,
                     indexes=None,
                     filename_response='./RDTDerivative.pkl',
                     save_matlab_file=False,
                     mode='analytic',  # 'semi-analytic', 'analytic'
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0
                     ):
    """
    computes the derivative of the orm+dispersion+tune vectors produced by get_orm_array with respect to the
    list of normal and skew quadrupole correctors specified in config.normal(skew)_quadrupoles_for_optics_fit

    :param ring: AT lattice
    :param indexes: dictionary of indexes obtained with commissioningsimulations.config.get_lattice_indexes
    :param filename_response: file where to store the computed derivatives (large file). if None, no file is saved)
    :param save_matlab_file: save to .mat format
    :param mode: 'numeric' (default), 'analytic', 'semi-analytic'  (see commissioningsimulations.CorrectOptics.jacobian)
    :param use_mp: use multiprocessing on local host
    :param n_processes: number of cores to use. if None, use all
    :param verbose: if True, print text
    :param bpms_weights: see get_orm_array
    :param tune_weight:  see get_orm_array
    :param hor_disp_weight:  see get_orm_array
    :param ver_disp_weight:  see get_orm_array
    :param DKnL: variation of normal quardupoles
    :param DKsL: variation of skew quadrupoles
    :return: JQ derivative of first output of get_orm_array respect to normal quadrupoles,
             JS derivative of first output of get_orm_array respect to skew quadrupoles,
             sQ singular values of JQ
             sS singular values of JS
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']
    ind_quads = indexes['normal_quadrupoles_for_optics_and_coupling_correction']
    ind_skews = indexes['skew_quadrupoles_for_optics_and_coupling_correction']

    NB = len(ind_bpms)
    NC = len(ind_cors)
    NQ = len(ind_quads)
    NS = len(ind_skews)

    if verbose:
        print(f'optics/coupling errors fit\n '
              f'orbit response matrix size: ({NB}, {NC}) \n'
              f'{NQ} {config.normal_quadrupoles_for_optics_and_coupling_fit} normal quadrupoles used for fit\n'
              f'{NS} {config.skew_quadrupoles_for_optics_and_coupling_fit} skew quadrupoles used for fit')

    if mode == 'numeric' or mode == 'semi-analytic':

        raise Exception('not ready yet. Use mode=analytic')

        # initialize RM, just to get sizes
        H0, V0 = get_rdt_arrays(ring,
                                ind_bpms,
                                ind_quads,
                                ind_skews,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                tune_weight,
                                'analytic',
                                verbose=False,
                                )

        JQ = np.zeros((len(H0), NQ))
        JS = np.zeros((len(V0), NS))

        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            if n_processes is None:
                n_processes = n_cpu
            else:
                max_proc = min(n_cpu, NC)
                if n_processes > max_proc:
                    n_processes = max_proc

            print(f'RM derivative parallel computation using {n_processes} cores on {socket.gethostname()}')

            with multiprocessing.Pool(processes=n_processes) as p:

                results_normal = p.starmap(jacobian,
                                        zip(repeat(ring),
                                            repeat('PolynomB'),
                                            repeat(1),
                                            ind_quads,  # loop variable
                                            repeat(DKnL),
                                            repeat(ind_bpms),
                                            repeat(ind_cors),
                                            repeat(bpms_weights),
                                            repeat(hor_disp_weight),
                                            repeat(ver_disp_weight),
                                            repeat(tune_weight),
                                            repeat(mode), repeat(field_is_integrated), repeat(verbose)))

            with multiprocessing.Pool(processes=n_processes) as p:
                results_skew = p.starmap(jacobian,
                                         zip(repeat(ring),
                                             repeat('PolynomA'),
                                             repeat(1),
                                             ind_skews, # loop variable
                                             repeat(DKsL),
                                             repeat(ind_bpms),
                                             repeat(ind_cors),
                                             repeat(bpms_weights),
                                             repeat(hor_disp_weight),
                                             repeat(ver_disp_weight),
                                             repeat(tune_weight),
                                             repeat(mode), repeat(field_is_integrated), repeat(verbose)))

            # collect results
            for count, ic in enumerate(ind_quads):
                JQ[:, count] = results_normal[count][0][:, 0]  # ensure correct dimensionality

            # collect results
            for count, ic in enumerate(ind_skews):
                JS[:, count] = results_skew[count][1][:, 0] # ensure correct dimensionality

        else:

            for count, ic in enumerate(ind_quads):

                jq, _ = jacobian(ring,
                                 'PolynomB',
                                 1,
                                 ic,
                                 DKnL,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 tune_weight,
                                 mode, verbose=True)
                JQ[:, count] = jq[:, 0]  # ensure correct dimensionality

            for count, ic in enumerate(ind_skews):
                _, js = jacobian(ring,
                                 'PolynomA',
                                 1,
                                 ic,
                                 DKsL,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 tune_weight,
                                 mode, verbose=True)

                JS[:, count] = js[:, 0]

    elif mode == 'analytic':

        JQ, JS = analytic_jacobian(ring,
                          indexes=indexes,
                          use_mp=use_mp,
                          n_processes=n_processes,
                          verbose=verbose,
                          bpms_weights=bpms_weights,
                          tune_weight=tune_weight,
                          hor_disp_weight=hor_disp_weight,
                          ver_disp_weight=ver_disp_weight)

    # invert and get singular values
    if verbose:
        print(f'computing inverse of orm derivative')

    start_time = time.time()
    _, sQ, _ = np.linalg.svd(JQ, full_matrices=True)
    print(f'time for SVD of {JQ.shape}: {time.time()-start_time}')

    start_time = time.time()
    _, sS, _ = np.linalg.svd(JS, full_matrices=True)
    print(f'time for SVD of {JS.shape}: {time.time()-start_time}')

    if filename_response:

        mdict = {'sQ': sQ, 'sS': sS,
                 'JQ': JQ, 'JS': JS,
                 'bpms_weights': bpms_weights,
                 'hor_disp_wight': hor_disp_weight,
                 'ver_disp_wight': hor_disp_weight,
                 'tune_wight': hor_disp_weight,
                 'ind_bpms': ind_bpms,
                 'ind_cors': ind_cors,
                 'ind_quad': ind_quads,
                 'ind_skew': ind_skews}

        file_no_ext, exten_file = os.path.splitext(filename_response)

        if save_matlab_file:
            try:
                savemat(file_no_ext + '.mat', mdict=mdict)
            except Exception as e:
                print(e)
                print('could not save .mat file')

        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        while not(exists(filename_response)):
            print('Wait for file {} to be saved'.format(filename_response))
            time.sleep(1)

        print('ORM derivatives saved in: {}'.format(filename_response))

    return JQ, JS, sQ, sS


def correct_rdt_dispersion(ring_to_correct,
               reference_ring,
               niter=1,
               neigQuad=1,
               neigSkew=1,
               bpms_weights=None,
               hor_disp_weight=1.0,
               ver_disp_weight=1.0,
               tune_weight=1.0,
               mode='analytic',
               indexes=None,
               filename_response=None,
               reference_orm=None,
               use_mp=False,
               verbose=True,
               normal=True,
               skew=True):
    """
    fit quadrupole errors
    these errors added to reference_ring will return an ORM as close as possible to the one of ring_to_correct
    the fitted errors are applied as correction to ring_to_correct to output a corrected lattice rcor

    :param ring_to_correct: ring with optics/coupling errors
    :param reference_ring: reference ring, no errors
    :param niter: number of iteration of correction
    :param neigQuad: # of singular vectors to use for quad correction
    :param neigSkew: # of singular vectors to use for quad correction
    :param bpms_weights: (2, nbpms) array of multiplication factors for each raw of the RM.
    :param hor_disp_weight: multiplication factor for hor disperison
    :param ver_disp_weight: multiplication factor for ver dispersion
    :param tune_weight: multiplication factor for tunes
    :param mode: 'numeric', 'semi-analytic', 'analytic'
    :param indexes: indexes obtained by commissioningsimulations.config.get_lattice_indexes. if none, thecomputed.
    :param filename_response: file where to look for derivative of ORM. if not found, computed
    :param reference_orm: as computed by get_orm_arrays, or None (compute it)
    :param use_mp: use multiprocessing
    :param verbose: if True, print text
    :return: rcor corrected AT lattice
             dcQ applied normal quadrupole correctors variations
             dcS applied skew quadrupole correctors variations
    """

    if verbose:
        print('correct RDTs, Dispersion, Tune')

    force_matrix_computation = False

    orm_mode = mode
    if mode == 'semi-analytic':
        orm_mode='analytic'

    orm_mode = 'numeric' # force numeric, analytic off-diag blocks not ok

    if not(indexes):
        indexes = get_lattice_indexes(reference_ring)

    ind_bpms = indexes['bpms']
    ind_quad = indexes['normal_quadrupoles_for_optics_and_coupling_correction']
    ind_skew = indexes['skew_quadrupoles_for_optics_and_coupling_correction']

    # get copy of ring
    rcor = copy.deepcopy(ring_to_correct)

    # print initial status

    if verbose:
        print('Before RDTs, Dispersion, Tune correction (should be zero after correction):')
        get_optics_vs_reference(reference_ring, ring_to_correct, ind_bpms)

    # get Orbit Response Matrix if no file is provided, based on lattice with errors.
    if (not exists(filename_response)) or force_matrix_computation:
        start_time = time.time()
        JQ, JS, sQ, sS = compute_jacobian(reference_ring,
                                          indexes=indexes,
                                          filename_response=filename_response,
                                          save_matlab_file=True,
                                          mode=mode,  # 'semi-analytic', 'analytic'
                                          use_mp=use_mp, #False, #
                                          n_processes=None,
                                          bpms_weights=bpms_weights,
                                          tune_weight=tune_weight,
                                          hor_disp_weight=hor_disp_weight,
                                          ver_disp_weight=ver_disp_weight,
                                          verbose=True)  # all if None
        end_time = time.time()
        if verbose:
            print(f'time for (parallel = {use_mp}) orm derivative computation: {end_time-start_time} seconds')
    else:

        _, file_ext = os.path.splitext(filename_response)
        if file_ext == '.mat':
            mat_contents = loadmat(filename_response)
            JQ = mat_contents['JQ']
            JS = mat_contents['JS']
            sS = np.transpose(mat_contents['sS'])  # load mat transposes raws/columns for some reason
            sS = sS[:, 0]
            sQ = np.transpose(mat_contents['sQ'])  # load mat transposes raws/columns for some reason
            sQ = sQ[:, 0]
        else:
            mat_contents = pickle.load(open(filename_response, 'rb'))
            JQ = mat_contents['JQ']
            JS = mat_contents['JS']
            sS = mat_contents['sS']
            sQ = mat_contents['sQ']
        if verbose:
            print(f'loaded: {filename_response}')

    # fix  singular values to max possible if too many
    if neigQuad > JQ.shape[1]:
        neigQuad = JQ.shape[1] -1
        if verbose:
            print(f'max singular values for normal quadrupoles = {JQ.shape[1]}')
    if neigSkew > JS.shape[1]:
        neigSkew = JS.shape[1] -1
        if verbose:
            print(f'max singular values for skew quadrupoles = {JS.shape[1]}')

    # get initial correctors values
    # Lq = at.get_value_refpts(rcor, ind_quad, 'Length')
    # Ls = at.get_value_refpts(rcor, ind_skew, 'Length')

    initcorQ = at.get_value_refpts(rcor, ind_quad, 'PolynomB', 1)
    initcorS = at.get_value_refpts(rcor, ind_skew, 'PolynomA', 1)

    # initialize corrections to zero. INTEGRATED STRENGHTS
    dcQ = np.zeros(len(ind_quad))
    dcS = np.zeros(len(ind_skew))

    # compute RDT responses for all normal and skew quads sources in lattice.

    # get lattice elements that are normal or skew quadrupole sources
    _, _, ind_all_ = EquivalentGradientsFromAlignments6D(ring_to_correct)

    # if missing, add correctors to list of indexes
    ind_all = np.unique(np.concatenate((ind_all_, ind_skew, ind_quad)))

    f2000_rdt_resp, f0020_rdt_resp, _ = normal_quad_rdts_response(reference_ring, ind_bpms, ind_all)

    f1001_rdt_resp, f1010_rdt_resp, _ = skew_quad_rdts_response(reference_ring, ind_bpms, ind_all)

    normal_rdt_resp = (f2000_rdt_resp, f0020_rdt_resp)
    skew_rdt_resp = (f1001_rdt_resp, f1010_rdt_resp)

    # get ORM to correct
    Hinit, Vinit = get_rdt_arrays(rcor,
                                  ind_bpms,
                                  ind_all,
                                  ind_all,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  orm_mode,
                                  normal_rdt_resp=normal_rdt_resp,
                                  skew_rdt_resp=skew_rdt_resp,
                                  verbose=False)

    # get reference ORM
    if reference_orm==None:
        H0, V0 = get_rdt_arrays(reference_ring,
                                  ind_bpms,
                                  ind_all,
                                  ind_all,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  orm_mode,
                                  normal_rdt_resp=normal_rdt_resp,
                                  skew_rdt_resp=skew_rdt_resp,
                                  verbose=False)
    else:
        H0 = reference_orm[0]
        V0 = reference_orm[1]

    for correction_iteration in range(0, niter):
        print('iteration # {}'.format(correction_iteration + 1))

        # get ORM
        if correction_iteration > 0:
            H, V = get_rdt_arrays(rcor,
                                  ind_bpms,
                                  ind_all,
                                  ind_all,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  orm_mode,
                                  normal_rdt_resp=normal_rdt_resp,
                                  skew_rdt_resp=skew_rdt_resp,
                                  verbose=False)

        else:  # use already available
            H = Hinit
            V = Vinit

        # compute correction and add to previous iterations
        ooh = np.asarray(H - H0)[:, 0]
        oov = np.asarray(V - V0)[:, 0]

        if normal:
            dcQ = dcQ + np.linalg.pinv(JQ, rcond=sQ[neigQuad - 1] / sQ[0]) @ ooh

        if skew:
            dcS = dcS + np.linalg.pinv(JS, rcond=sS[neigSkew - 1] / sS[0]) @ oov

        # check that the corrections are <30 mircorad
        if verbose:
            rdtq = ooh[0:-len(ind_bpms) - 2]
            rdts = oov[0:-len(ind_bpms)]
            dhq = ooh[- len(ind_bpms) - 2 : -3]
            dvq = oov[- len(ind_bpms): -1]
            dQq = ooh[- 2 : -1]
            def print_std(v, name):
                print(
                    f'{name} | std: {np.std(v):.3g} | (min, max) ({np.min(v):.3g}, {np.max(v):.3g})')
            print_std(rdtq, 'RDT f2000, f0020')
            print_std(rdts, 'RDT f1001, f1010')
            print_std(dhq, 'hor Disp')
            print_std(dvq, 'ver Disp')
            print_std(dQq, 'tune')
            print_std(dcQ, 'norm quad cor.')
            print_std(dcS, 'skew quad cor.')

        # apply correction
        for count, ic in enumerate(ind_quad):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomB[1] = initcorQ[count] - dcQ[count] / L

        for count, ic in enumerate(ind_skew):
            L = rcor[ic].Length
            if L == 0.0:  # thin corrector, PolynomA/B are KL not K
                L = 1.0
            # set correctors in the lattice
            rcor[ic].PolynomA[1] = initcorS[count] - dcS[count] / L

    # get Orbit RM after correction
    Haft, Vaft = get_rdt_arrays(rcor,
                                  ind_bpms,
                                  ind_all,
                                  ind_all,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  tune_weight,
                                  orm_mode,
                                  normal_rdt_resp=normal_rdt_resp,
                                  skew_rdt_resp=skew_rdt_resp,
                                  verbose=False)

    ooh = np.asarray(Haft - H0)[:, 0]
    oov = np.asarray(Vaft - V0)[:, 0]

    if verbose:
        rdtq = ooh[0:-len(ind_bpms) - 2]
        rdts = oov[0:-len(ind_bpms)]
        dhq = ooh[- len(ind_bpms) - 2: -3]
        dvq = oov[- len(ind_bpms): -1]
        dQq = ooh[- 2: ]

        def print_std(v, name):
            print(
                f'Final {name} | std: {np.std(v):.3g} | (min, max) ({np.min(v):.3g}, {np.max(v):.3g})')

        print_std(rdtq, 'RDT f2000, f0020')
        print_std(rdts, 'RDT f1001, f1010')
        print_std(dhq, 'hor Disp')
        print_std(dvq, 'ver Disp')
        print_std(dQq, 'tune')
        print_std(dcQ, 'norm quad cor.')
        print_std(dcS, 'skew quad cor.')

    if verbose:
        print('After RDTs, Dispersion, Tune correction:')
        get_optics_vs_reference(reference_ring, rcor, ind_bpms)

    return rcor, dcQ, dcS


def _test_rdt_dispersion_correction():
    '''
    script to test optics correction
    :return: None
    '''

    from commissioningsimulations.correction.optics_coupling.RDT import get_rdts

    config.normal_quadrupoles_for_optics_and_coupling_fit = 'S[HDFIJ]*'
    config.skew_quadrupoles_for_optics_and_coupling_fit = 'S[HDFIJ]*'
    config.normal_quadrupoles_for_optics_and_coupling_correction = 'S[HDFIJ]*'
    config.skew_quadrupoles_for_optics_and_coupling_correction = 'S[HDFIJ]*'

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    config.radiation = False

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # shift sextupoles to generate quad and skew quad
    ind_sext = list(at.get_refpts(ring, config.sextupoles_fam_name_pattern))

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    # fix seed
    np.random.seed(0)

    NQ = int(len(ind_sext))
    dx[ind_sext] = 50e-6 * np.random.randn(NQ)
    dy[ind_sext] = 50e-6 * np.random.randn(NQ)

    rerr.set_shift(dx, dy)

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)

    # correct optics
    mode = 'analytic'
    try:
        os.remove('./J' + mode + '.pkl')
    except OSError:
        pass

    rcor, Qcor, Scor = correct_rdt_dispersion(rerr,
                          ring,
                          niter=1,
                          neigQuad=200,
                          neigSkew=200,
                          hor_disp_weight=1e2,
                          ver_disp_weight=1e3,
                          mode=mode,
                          indexes=indexes,
                          use_mp=True,
                          filename_response='./J' + mode + '.pkl')

    f1001, f1010, f2000, f0020 = get_rdts(ring, indexes['bpms'])

    print('BEFORE CORRECTION')
    bbh,  bbv,  ddh,  ddv,  deh,  dev = get_optics_vs_reference(ring, rerr, indexes['bpms'])
    f1001e, f1010e, f2000e, f0020e = get_rdts(rerr, indexes['bpms'])

    print('AFTER CORRECTION')
    bbha, bbva, ddha, ddva, deha, deva = get_optics_vs_reference(ring, rcor, indexes['bpms'])
    f1001c, f1010c, f2000c, f0020c = get_rdts(rcor, indexes['bpms'])

    # plot figures
    fig, ((axbh, axdh), (axbv, axdv), (axQ, axS)) = plt.subplots(nrows = 3, ncols=2)
    axbh.plot(bbh*100, label=f'before $\delta\epsilon_h$ = {deh*1e12:.2f}')
    axbh.plot(bbha*100, label=f'after $\delta\epsilon_h$ = {deha*1e12:.2f}')
    axbh.set_ylabel(f' hor. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbh.legend()
    axbv.plot(bbv * 100, label=f'before $\delta\epsilon_h$ = {dev*1e12:.2f}')
    axbv.plot(bbva * 100, label=f'after $\delta\epsilon_h$ = {deva*1e12:.2f}')
    axbv.set_ylabel(f' ver. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbv.legend()

    axdh.plot(ddh * 100, label='before')
    axdh.plot(ddha * 100, label='after')
    axdh.set_ylabel(f' hor. $(\eta-\eta_0)$ %')
    axdh.legend()
    axdv.plot(ddv * 100, label='before')
    axdv.plot(ddva * 100, label='after')
    axdv.set_ylabel(f' ver. $(\eta-\eta_0)$ %')
    axdv.legend()

    axQ.bar(range(len(Qcor)), Qcor, label='after')
    axQ.set_ylabel(f' Quad cor ')
    axQ.legend()
    axS.bar(range(len(Scor)), Scor, label='after')
    axS.set_ylabel(f' Skew cor ')
    axS.legend()

    fig, ((axf10, axf01), (axf20, axf02)) = plt.subplots(nrows=2, ncols=2)
    axf10.plot(f1001e.real - f1001.real, '_', label='Initial $\Delta$ f1001.real')
    axf10.plot(f1001e.imag - f1001.imag, '_', label='Initial $\Delta$ f1001.imag')
    axf10.plot(f1001c.real - f1001.real, ':', label='Corrected $\Delta$ f1001.real')
    axf10.plot(f1001c.imag - f1001.imag, ':', label='Corrected $\Delta$ f1001.imag')
    axf10.legend()

    axf01.plot(f1010e.real - f1010.real, '_', label='Initial $\Delta$ f1010.real')
    axf01.plot(f1010e.imag - f1010.real, '_', label='Initial $\Delta$ f1010.imag')
    axf01.plot(f1010c.real - f1010.real, ':', label='Corrected $\Delta$ f1010.real')
    axf01.plot(f1010c.imag - f1010.real, ':', label='Corrected $\Delta$ f1010.imag')
    axf01.legend()

    axf20.plot(f2000e.real - f2000.real, '_', label='Initial $\Delta$ f2000.real')
    axf20.plot(f2000e.imag - f2000.imag, '_', label='Initial $\Delta$ f2000.imag')
    axf20.plot(f2000c.real - f2000.real, ':', label='Corrected $\Delta$ f2000.real')
    axf20.plot(f2000c.imag - f2000.imag, ':', label='Corrected $\Delta$ f2000.imag')
    axf20.legend()

    axf02.plot(f0020e.real - f0020.real, '_', label='Initial $\Delta$ f0020.real')
    axf02.plot(f0020e.imag - f0020.imag, '_', label='Initial $\Delta$ f0020.imag')
    axf02.plot(f0020c.real - f0020.real, ':', label='Corrected $\Delta$ f0020.real')
    axf02.plot(f0020c.imag - f0020.imag, ':', label='Corrected $\Delta$ f0020.imag')
    axf02.legend()

    plt.show()

    pass


if __name__=='__main__':

    _test_rdt_dispersion_correction()

    pass
