import copy
import time
from os.path import exists
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
import commissioningsimulations.correction.optics_coupling.quadrupoles_fit as quadrupoles
import commissioningsimulations.correction.optics_coupling.dipoles_fit as dipoles
import commissioningsimulations.correction.optics_coupling.beam_position_monitors_fit as bpms
import commissioningsimulations.correction.optics_coupling.steerers_fit as steerers
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference


def fit_optics_model(ring_to_correct, reference_ring,
                     dict_of_fits_kwargs={},
                     indexes=None,
                     use_mp=False,
                     verbose=True,
                     ):
    """

    :param ring_to_correct:
    :param reference_ring:
    :param dict_of_fits_kwargs:
    :return:
    """

    if indexes is None:
        indexes = get_lattice_indexes(reference_ring)

    # initial fitted model is the reference ring
    rfit = copy.deepcopy(reference_ring)

    # corrected lattice using quadrupole correctors as marked in config
    # rcor = copy.deepcopy(reference_ring)

    for k in dict_of_fits_kwargs:

        print(f'Start fit: {k}')

        if k=='bpms':

            if dict_of_fits_kwargs[k]:

                rfit, _, _, _, _ = bpms.fit_optics(
                    ring_to_correct,
                    rfit,
                    indexes=indexes,
                    use_mp=use_mp,
                    verbose=verbose,
                    **dict_of_fits_kwargs[k])

            else:

                rfit, _, _, _, _ = bpms.fit_optics(
                    ring_to_correct,
                    rfit,
                    niter = 1,
                    neig_scale = 1,
                    neig_rot = 1,
                    bpms_weights = None,
                    hor_disp_weight = 1.0,
                    ver_disp_weight = 1.0,
                    mode = 'numeric',
                    indexes = indexes,
                    filename_response = None,
                    reference_orm = None,
                    use_mp = use_mp,
                    verbose = verbose,
                    horizontal = True,
                    vertical = True,
                    rotation = True)
        """
        if k=='steerers':
            
            if dict_of_fits_kwargs[k]:
                
                rfit, _, _, _, _ = steerers.fit_optics(
                    ring_to_correct, 
                    rfit,
                    indexes=indexes,
                    use_mp=use_mp,
                    verbose=verbose,
                    **dict_of_fits_kwargs[k])
            else:
                
                rfit, _, _, _, _ = steerers.fit_optics(
                    ring_to_correct,
                    rfit,
                    niter=1,
                    neig_scale=1,
                    neig_rot=1,
                    bpms_weights=None,
                    hor_disp_weight=1.0,
                    ver_disp_weight=1.0,
                    mode='numeric',
                    indexes=indexes,
                    filename_response=None,
                    reference_orm=None,
                    use_mp=use_mp,
                    verbose=verbose,
                    horizontal=True,
                    vertical=True,
                    rotation=True)
        """

        if k == 'dipoles':

            if dict_of_fits_kwargs[k]:

                rfit, _, _, _ = dipoles.fit_optics(
                    ring_to_correct,
                    rfit,
                    indexes=indexes,
                    use_mp=use_mp,
                    verbose=verbose,
                    **dict_of_fits_kwargs[k])
            else:

                rfit, _, _, _ = dipoles.fit_optics(
                    ring_to_correct,
                    rfit,
                    niter=1,
                    neigDipo=1,
                    bpms_weights=None,
                    mode='numeric',
                    indexes=indexes,
                    filename_response=None,
                    reference_disp=None,
                    use_mp=use_mp,
                    verbose=verbose,
                    normal=True,
                    skew=False)

        if k == 'quadrupoles':

            if dict_of_fits_kwargs[k]:

                rfit, _, _, _ = quadrupoles.fit_optics(
                    ring_to_correct,
                    rfit,
                    indexes=indexes,
                    use_mp=use_mp,
                    verbose=verbose,
                    **dict_of_fits_kwargs[k])

            else:

                rfit, _, _, _ = quadrupoles.fit_optics(
                    ring_to_correct,
                    rfit,
                    niter=1,
                    neigQuad=1,
                    neigSkew=1,
                    bpms_weights=None,
                    hor_disp_weight=1.0,
                    ver_disp_weight=1.0,
                    tune_weight=1.0,
                    mode='numeric',
                    indexes=indexes,
                    filename_response=None,
                    reference_orm=None,
                    use_mp=use_mp,
                    verbose=verbose,
                    normal=True,
                    skew=True)

        print(f'End fit {k}')

    return rfit


def correct_optics_using_fitted_errors(ring, rfit, rcor, indexes=None):
    """
    compares ring and rfit to extract normal and skew quadrupole gradients to be applied to rcor

    :param ring: reference lattice
    :param rfit: fitted optics model
    :param rcor: lattice to be corrected
    :param indexes: output of commissioningsimulations.get_indexes.get_lattice_indexes
    :return: rcor (with corrected normal and skew quadrupole strengths,
             dknL, dksL, the integrated normal and skew quadrupole strength variations applied for correction
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    print('Before - fitted quadrupole errors correction:')
    get_optics_vs_reference(ring, rcor, indexes['bpms'])

    # extract quadrupole correctors from fitted lattice to return a corrected lattice
    ind_quad = indexes['normal_quadrupoles_for_optics_and_coupling_fit']
    ind_skew = indexes['skew_quadrupoles_for_optics_and_coupling_fit']

    Ln = rfit.get_value_refpts(ind_quad, 'Length')
    Ls = rfit.get_value_refpts(ind_skew, 'Length')
    Ln[Ln == 0] = 1.0  # thin magnets
    Ls[Ls == 0] = 1.0  # thin magnets

    dkn = rfit.get_value_refpts(ind_quad, 'PolynomB', 1) - ring.get_value_refpts(ind_quad, 'PolynomB', 1)
    dks = rfit.get_value_refpts(ind_skew, 'PolynomA', 1) - ring.get_value_refpts(ind_skew, 'PolynomA', 1)

    dknL = dkn * Ln
    dksL = dks * Ls

    # print(len(ind_quad))
    # print(len(ind_skew))

    # apply correction
    for count, ic in enumerate(ind_quad):
        # set correctors in the lattice
        rcor[ic].PolynomB[1] = rcor[ic].PolynomB[1] - dkn[count]

    for count, ic in enumerate(ind_skew):
        # set correctors in the lattice
        rcor[ic].PolynomA[1] = rcor[ic].PolynomA[1] - dks[count]

    print('After -fitted quadrupole errors correction:')
    get_optics_vs_reference(ring, rcor, indexes['bpms'])

    return rcor, dknL, dksL


def compute_jacobian(ring,
                     dict_of_fits_kwargs={},
                     indexes=None,
                     use_mp=False,
                     verbose=True,
                     save_matlab_file=False
                     ):

    if indexes is None:
        indexes = get_lattice_indexes(reference_ring)


    for k in dict_of_fits_kwargs:

        start_time = time.time()

        # get jacobian file name
        file_name = dict_of_fits_kwargs[k]['filename_response']
        args = dict_of_fits_kwargs[k]

        if not(exists(file_name)):

            if k == 'bpms':

                bpms.compute_jacobian(
                    ring,
                    indexes=indexes,
                    save_matlab_file=save_matlab_file,
                    use_mp=use_mp,
                    verbose=verbose,
                    mode=args['mode'],
                    filename_response=file_name,
                    bpms_weights=args['bpms_weights'],
                    hor_disp_weight=args['hor_disp_weight'],
                    ver_disp_weight=args['ver_disp_weight'])
            """
            if k == 'steerers':
               
            """

            if k == 'dipoles':

                dipoles.compute_jacobian(
                    ring,
                    indexes=indexes,
                    save_matlab_file=save_matlab_file,
                    use_mp=use_mp,
                    verbose=verbose,
                    filename_response=file_name,
                    mode=args['mode'],
                    bpms_weights=args['bpms_weights'])

            if k == 'quadrupoles':

                quadrupoles.compute_jacobian(
                    ring,
                    indexes=indexes,
                    save_matlab_file=save_matlab_file,
                    use_mp=use_mp,
                    verbose=verbose,
                    filename_response=file_name,
                    mode=args['mode'],
                    bpms_weights=args['bpms_weights'],
                    hor_disp_weight=args['hor_disp_weight'],
                    ver_disp_weight=args['ver_disp_weight'])

            if verbose:
                print(f'{k} Jacobian computation took: {time.time() - start_time} seconds')
                print(f'Saved: {file_name}')

        else:
            if verbose:
                print(f'Loaded: {file_name}')

    pass

def plot_fitted_errors(ring, rfit, indexes = None):

    import matplotlib.pyplot as plt

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    # extract quadrupole correctors from fitted lattice to return a corrected lattice
    ind_quad = indexes['normal_quadrupoles_for_optics_and_coupling_fit']
    ind_skew = indexes['skew_quadrupoles_for_optics_and_coupling_fit']
    ind_bpms = indexes['bpms']
    ind_dips = indexes['dipoles_for_optics_and_coupling_fit']

    Ln = rfit.get_value_refpts(ind_quad, 'Length')
    Ls = rfit.get_value_refpts(ind_skew, 'Length')
    Ln[Ln == 0] = 1.0  # thin magnets
    Ls[Ls == 0] = 1.0  # thin magnets

    # BPMS
    dbsh = rfit.get_value_refpts(ind_bpms, 'Scale',0) - ring.get_value_refpts(ind_bpms, 'Scale', 0)
    dbsv = rfit.get_value_refpts(ind_bpms, 'Scale',1) - ring.get_value_refpts(ind_bpms, 'Scale', 1)
    dbr = rfit.get_value_refpts(ind_bpms, 'Rotation',0) - ring.get_value_refpts(ind_bpms, 'Rotation', 0)

    # dipole
    dda = rfit.get_value_refpts(ind_dips, 'BendingAngle') - ring.get_value_refpts(ind_dips, 'BendingAngle')

    # quadrupole
    dkn = rfit.get_value_refpts(ind_quad, 'PolynomB', 1) - ring.get_value_refpts(ind_quad, 'PolynomB', 1)
    dks = rfit.get_value_refpts(ind_skew, 'PolynomA', 1) - ring.get_value_refpts(ind_skew, 'PolynomA', 1)

    dknL = dkn * Ln
    dksL = dks * Ls

    fig, ((axd, axd2), (axq, axq2),(axb, axb2)) = plt.subplots(nrows=3, ncols=2)
    fig.subplots_adjust(hspace=.5, wspace=0.5)

    axd.bar(range(len(dda)),dda)
    axd.set_ylabel(f'$\Delta\\theta$')
    axd.set_xlabel(f'Dipole')

    axq.bar(range(len(dknL)), dknL, label='normal')
    axq.set_ylabel(f'$\Delta KL$')
    axq.set_xlabel(f'normal Quadrupole')

    axq2.bar(range(len(dksL)), dksL, label='skew')
    axq2.set_ylabel(f'$\Delta KL$')
    axq2.set_xlabel(f'skew Quadrupole')

    axb.bar(range(len(dbsh)), dbsh, label='hor.')
    axb.bar(range(len(dbsv)), dbsv, label='ver.')
    axb.set_ylabel(f'Scale')
    axb.set_xlabel(f'BPM')
    axb.legend()

    axb2.bar(range(len(dbr)), dbr, label='normal')
    axb2.set_ylabel(f'Rotation')
    axb2.set_xlabel(f'BPM')
    axb2.legend()

    plt.show()

    pass

def _test_fit_optics_model():

    import at
    import numpy as np
    import os

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # shift sextupoles to generate quad and skew quad
    ind_sext = list(at.get_refpts(ring, config.sextupoles_fam_name_pattern))

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    NQ = int(len(ind_sext))
    dx[ind_sext] = 30e-6 * np.random.randn(NQ)
    dy[ind_sext] = 30e-6 * np.random.randn(NQ)

    rerr.set_shift(dx, dy)

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)

    # reduce indexes for test
    indexes['normal_quadrupoles_for_optics_and_coupling_fit'] = \
        indexes['normal_quadrupoles_for_optics_and_coupling_fit'][::3]

    indexes['skew_quadrupoles_for_optics_and_coupling_fit'] = \
        indexes['skew_quadrupoles_for_optics_and_coupling_fit'][::3]

    indexes['dipoles_for_optics_and_coupling_fit'] = \
        indexes['dipoles_for_optics_and_coupling_fit'][::3]

    correction_name = '_test'

    dict_of_fit_params = {
        'dipoles': {
            'niter': 2,
            'neigDipo': 32,
            'bpms_weights': None,
            'mode': 'analytic',
            'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                              'J' + correction_name + '_DIPS_analy.pkl'),
            'reference_disp': None,
            'normal': True,
            'skew': False},
        'quadrupoles': {
            'niter': 4,
            'neigQuad': 96,
            'neigSkew': 96,
            'bpms_weights': None,
            'hor_disp_weight': 100.0,
            'ver_disp_weight': 100.0,
            'tune_weight': 1.0,
            'mode': 'analytic',
            'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                              'J' + correction_name + '_QUADS_analy.pkl'),
            'reference_orm': None,
            'normal': True,
            'skew': True
        },
        'bpms': {'niter': 3,
                 'neig_scale': 320,
                 'neig_rot': 320,
                 'bpms_weights': None,
                 'hor_disp_weight': 1.0,
                 'ver_disp_weight': 1.0,
                 'mode': 'analytic',
                 'filename_response': os.path.join(os.path.abspath(config.folder_matrices),
                                                   'J' + correction_name + '_BPMS_analy.pkl'),
                 'reference_orm': None,
                 'horizontal': True,
                 'vertical': True,
                 'rotation': True}
    }  # END OF FIT_PARAMS

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    compute_jacobian( ring,
                      dict_of_fits_kwargs=dict_of_fit_params,
                      indexes=indexes,
                      use_mp=True,
                      verbose=True)

    rfit = fit_optics_model(ring_to_correct=rerr,
                          reference_ring=ring,
                          dict_of_fits_kwargs=dict_of_fit_params,
                          indexes=indexes,
                          use_mp=True,
                          verbose=True)

    plot_fitted_errors(ring, rfit, indexes=indexes)

    rcor, _, _ = correct_optics_using_fitted_errors(ring, rfit, rerr, indexes=None)

    pass

if __name__=='__main__':
    _test_fit_optics_model()
    pass