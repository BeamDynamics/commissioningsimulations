"""
formulas from
A.Franchi (ESRF), Z.Marti (CELLS),
"Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018
"""

import at
import math
import numpy as np
import multiprocessing
from itertools import repeat

__author__='Simone Maria Liuzzo, Andrea Franchi'


def analytic_dispersion_variation_with_dipole(
        ring,
        ind_bpms=None,
        ind_dips=None,
        verbose=True,
        opt_all_location=None,
        thick=False,
        use_mp=False):
    """
    Computes the derivative of the dispersion compared to dipole errors

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_dips: numpy array of np.uint32. indexes of dipoles
    :param verbose: True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :param thick: True/False
    :return: two 2D lists of floats.
    HorDispersion_over_Dip[BPMS][DIP], VerDispersion_over_Dip[BPMS][DIP],
    """

    dDH_dDipHor = np.zeros(shape=(len(ind_bpms), len(ind_dips)))
    dDV_dDipVer = np.zeros(shape=(len(ind_bpms), len(ind_dips)))

    # loop quadrupoles
    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('RM derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap( _analytic_dispersion_variation_with_dipole,
                                 zip(repeat(ring),
                                     repeat(ind_bpms),
                                     ind_dips,    # loop index
                                     repeat(verbose),
                                     repeat(opt_all_location),
                                     repeat(thick)
                                     )
                                 )

        for m, _ in enumerate(ind_dips):
            _MH = results[m][0]
            _MV = results[m][1]
            dDH_dDipHor[:, m] = _MH[:, 0]
            dDV_dDipVer[:, m] = _MV[:, 0]

    else:  # sequential

        dDH_dDipHor, dDV_dDipVer = _analytic_dispersion_variation_with_dipole(
                        ring,
                        ind_bpms=ind_bpms,
                        ind_dips=ind_dips,
                        verbose=verbose,
                        opt_all_location=opt_all_location,
                        thick=thick)

    return dDH_dDipHor, dDV_dDipVer


def _analytic_dispersion_variation_with_dipole(
        ring,
        ind_bpms=None,
        ind_dips=None,
        verbose=True,
        opt_all_location=None, thick=False):
    """
    analytic orbit response matrix derivative with integrated (KL) errors at normal quadrupoles

    :param ring:
    :param ind_bpms:
    :param ind_cors:
    :param verbose:
    :param filename_cod_response:
    :return:
    """
    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    bpm = opt_all_location[ind_bpms]
    dip = opt_all_location[ind_dips]

    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # functions required by later formulas

    def tau(pl, a, b):
        return dphi(pl, a, b) - math.pi * Q[pl]

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2 * math.pi * Q[pl]

        return d

    # formulas for response

    dDH_dDipHor = np.zeros(shape=(len(bpm), len(dip)))
    dDV_dDipVer = np.zeros(shape=(len(bpm), len(dip)))

    x = 0
    y = 1

    for countm, m in enumerate(range(len(dip))):

        if thick:
            Lm = ring[ind_dips[m]].Length
            Km0 = ring[ind_dips[m]].BendingAngle

            TSmx = Lm * math.sin(Km0) / ( 2 * Km0 * dip[m].beta[x] ** 0.5)
            TSmy = Lm * math.sin(Km0) / (2 * Km0 * dip[m].beta[y] ** 0.5)

            TCmx = dip[m].beta[x] ** 0.5 * math.cos(Km0) - dip[m].alpha[x] * Lm * math.sin(Km0) / \
                   (2 * Km0 * dip[m].beta[x] ** 0.5)
            TCmy = dip[m].beta[y] ** 0.5 * math.cos(Km0) - dip[m].alpha[y] * Lm * math.sin(Km0) / \
                   (2 * Km0 * dip[m].beta[y] ** 0.5)

        for countj, j in enumerate(range(len(bpm))):
            if verbose:
                print(f'computing dispersion'
                      f' at BPM {ring[ind_bpms[j]].FamName}'
                      f' with an horizontal or vertical (thick={thick}) bending angle error in {ring[ind_dips[m]].FamName}')

            if thick:
                JCmx = math.cos(tau(x, dip[m], bpm[j])) * TCmx + math.sin(tau(x, dip[m], bpm[j])) * TSmx
                dDH_dDipHor[j][m] = + bpm[j].beta[x] ** 0.5 / 2 / math.sin(math.pi * Q[x]) * JCmx

                JCmy = math.cos(tau(y, dip[m], bpm[j])) * TCmy + math.sin(tau(y, dip[m], bpm[j])) * TSmy
                dDV_dDipVer[j][m] = - bpm[j].beta[y] ** 0.5 / 2 / math.sin(math.pi * Q[y]) * JCmy

            else:
                dDH_dDipHor[j][m] = + bpm[j].beta[x] ** 0.5 / 2 / math.sin(math.pi * Q[x]) \
                                    * (dip[m].beta[x]**0.5) * math.cos(tau(x, dip[m], bpm[j]))

                dDV_dDipVer[j][m] = - bpm[j].beta[y] ** 0.5 / 2 / math.sin(math.pi * Q[y]) \
                                    * (dip[m].beta[y] ** 0.5) * math.cos(tau(y, dip[m], bpm[j]))


    return dDH_dDipHor, dDV_dDipVer


def _test_disp_dipoles_deriv(m=0, thick=True):

    import commissioningsimulations.config as config
    from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix
    import matplotlib.pyplot as plt
    from os.path import exists
    import pickle
    import time

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    ring.radiation_off()
    ring.radiation_on()  # cavity ON, radiation ON or dispersion will not compute
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get dipole indexes
    ind_dip = list(at.get_refpts(ring, config.dipoles_fam_name_pattern))

    # check BETA at DL entrance vs Average beta DL
    _, _, l0 = ring.linopt6(refpts=ind_dip[m])
    ring.radiation_off()
    lave, bave, _, _, _, _, _ = at.avlinopt(ring, 0, ind_dip[m])
    ring.radiation_on()
    print(l0.beta)
    print(lave.beta)
    print(bave)

    # get reference ORM
    orm0 = './Reference.pkl'
    if not exists(orm0):
        MH0, MV0, _, _, _, _, _, _, _, _, dh0, dv0, Q0 = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=orm0)
    else:
        mat_cont = pickle.load(open(orm0, 'rb'))
        MH0 = mat_cont['MH']
        MV0 = mat_cont['MV']
        dh0 = mat_cont['hor_dispersion']
        dv0 = mat_cont['ver_dispersion']
        Q0 = mat_cont['tunes']

    # vary + one dipole
    dKL = 0.0001
    K = ring[ind_dip[m]].BendingAngle
    ring[ind_dip[m]].BendingAngle = K + dKL

    # get modified ORM
    ormq = f'./PlusDip{ring[ind_dip[m]].FamName}.pkl'
    if not exists(ormq):
        MHqp, MVqp, _, _, _, _, _, _, _, _, dhqp, dvqp, Qqp = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqp = mat_cont['MH']
        MVqp = mat_cont['MV']
        dhqp = mat_cont['hor_dispersion']
        dvqp = mat_cont['ver_dispersion']
        Qqp = mat_cont['tunes']

    # vary - one quadrupole
    ring[ind_dip[m]].BendingAngle = K - dKL

    # get modified ORM
    ormq = f'./MinusDip{ring[ind_dip[m]].FamName}.pkl'
    if not exists(ormq):
        MHqm, MVqm, _, _, _, _, _, _, _, _, dhqm, dvqm, Qqm = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqm = mat_cont['MH']
        MVqm = mat_cont['MV']
        dhqm = mat_cont['hor_dispersion']
        dvqm = mat_cont['ver_dispersion']
        Qqm = mat_cont['tunes']

    # restore quadrupole
    ring[ind_dip[m]].BendingAngle = K

    # numeric derivative
    dDH_dDipHor_n = (dhqp - dhqm) / 2 / dKL

    # compute analytic equivalent

    print(f'start analytic orm derivative {thick}')
    start_time = time.time()
    ring.radiation_off()
    dDH_dDipHor_a, dDV_dDipVer_a = analytic_dispersion_variation_with_dipole(ring,
                                                           ind_bpms=ind_bpms,
                                                           ind_dips=[ind_dip[m]],
                                                           thick=thick,
                                                           verbose=False)
    end_time = time.time()
    print(f'time for analytic ORM derivative= {end_time - start_time} seconds')

    # modifications to make it equal to the numeric

    # plot analytic vs numeric
    fig, axch = plt.subplots(figsize=(16, 7))

    if thick:
        thick_text = 'thick'
    else:
        thick_text = 'thin'

    axch.bar(range(len(dDH_dDipHor_n)), dDH_dDipHor_n - dDH_dDipHor_a[:, 0], label=f'num - ana', alpha=0.5, color='g')
    axch.plot(dDH_dDipHor_n, 'x-', label=f'numeric')
    axch.plot(dDH_dDipHor_a[:, 0], label=f'analytic {thick_text}')
    axch.legend()
    axch.set_ylabel('hor. [m/Hz/$\\theta$]')
    axch.set_xlabel('BPMs')
    axch.set_title(f'Dipole: {ring[ind_dip[m]].FamName}')

    # correlation plot
    fig, axch = plt.subplots()
    axch.plot(dDH_dDipHor_n, dDH_dDipHor_a[:, 0], '.', label=f'numeric')
    axch.axis('equal')
    axch.set_ylabel('numeric')
    axch.set_xlabel(f'analytic {thick_text}')
    axch.set_title(f'Dipole: {ring[ind_dip[m]].FamName}')

    fig.tight_layout()

    np.savetxt(f'Dip{ring[ind_dip[m]].FamName}_analytic{thick_text}_dispH.txt', dDH_dDipHor_a[:, 0], fmt='%10.5f', delimiter=',')
    np.savetxt(f'Dip{ring[ind_dip[m]].FamName}_numeric_dispH.txt', dDH_dDipHor_n, fmt='%10.5f', delimiter=',')
    plt.show()

    pass


if __name__ == '__main__':

    _test_disp_dipoles_deriv(m=0, thick=False)  # some discrepancy, may be thick elements correction needed
    #_test_disp_dipoles_deriv(m=100, thick=False)  # some discrepancy, may be thick elements correction needed
    #_test_disp_dipoles_deriv(m=300, thick=False)  # some discrepancy, may be thick elements correction needed

    _test_disp_dipoles_deriv(m=0, thick=True)  # some discrepancy, may be thick elements correction needed
    #_test_disp_dipoles_deriv(m=100, thick=True)  # some discrepancy, may be thick elements correction needed
    #_test_disp_dipoles_deriv(m=300, thick=True)  # some discrepancy, may be thick elements correction needed

    pass