import at
import commissioningsimulations.config as config
import copy
import matplotlib.pyplot as plt
import time
from commissioningsimulations.correction.optics_coupling.RDT import skew_quad_rdts_response
import math
import cmath
import numpy as np

__author__ = 'Simone Maria Liuzzo, Andrea Franchi'


def analytic_f2000_f0020_OTM(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True,
        thick_quadrupole=True,
        opt_all_location=None,
        use_mp=False):
    """
    Computes the derivative of the orbit response matrix compared to normal quadrupoles

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_quads: numpy array of np.uint32. indexes of normal quadrupoles
    :param verbose: True/False
    :param thick_quadrupole:  True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :return: two 2D list of complex.
    resp_f2000[BPM][QUAD], resp_f0020[BPM][QUAD]
    """

    resp_f2000 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))
    resp_f0020 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))

    # loop quadrupoles
    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('RM derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap(_analytic_f2000_f0020_OTM,
                                zip(repeat(ring),
                                    repeat(ind_bpms),
                                    ind_quads,  # loop index
                                    repeat(verbose),
                                    repeat(opt_all_location)
                                    )
                                )

        for m, _ in enumerate(ind_quads):
            _resp_f2000 = results[m][0]
            _resp_f0020 = results[m][1]
            resp_f2000[:, m] = _resp_f2000[:, 0]
            resp_f0020[:, m] = _resp_f0020[:, 0]

    else:  # sequential

        resp_f2000, resp_f0020 = _analytic_f2000_f0020_OTM(
            ring,
            ind_bpms=ind_bpms,
            ind_quads=ind_quads,
            verbose=verbose,
            opt_all_location=opt_all_location)

    return resp_f2000, resp_f0020


def _analytic_f2000_f0020_OTM(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True,
        opt_all_location=None):
    """
    analytic f2000 and f0020 RDTs derivative with integrated (KL) errors at normal quadrupoles from one turn matrix

    :param ring:
    :param ind_bpms:
    :param ind_quads:
    :param verbose:
    :param opt_all_location:
    :return:
    """

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    bpm = opt_all_location[ind_bpms]

    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # print(type(ind_quads))
    if isinstance(ind_quads, np.uint32):
        ind_quads = [ind_quads]

    # define indexes
    x = 0
    y = 1

    # formulas for response

    resp_f2000 = np.zeros(shape=(len(ind_bpms), len(ind_quads))) + complex('j')
    resp_f0020 = np.zeros(shape=(len(ind_bpms), len(ind_quads))) + complex('j')

    for countm, m in enumerate(ind_quads):

        # compute reference One turn matrix at each BPM.
        _M0_, M0 = ring.find_m44(refpts=ind_bpms, full=True)

        # vary +
        dKL = 1e-6
        ring_err = copy.deepcopy(ring)
        K = ring_err[m].PolynomB[1]
        ring_err[m].PolynomB[1] = K + dKL / ring_err[m].Length

        # compute One turn matrix at each BPM with a quadrupole error
        _M_, M = ring_err.find_m44(refpts=ind_bpms, full=True)

        _dM_ = _M_ - _M0_
        dM = np.array([m - m0 for m, m0 in zip(M, M0)])
        ave_dM = np.mean(dM, axis=0)

        for countj, j in enumerate(ind_bpms):

            if verbose:
                print(f'computing f2000 and f0020 RDTs response at'
                      f' BPM {ring[j].FamName}[{countj}]'
                      f' with a normal quadrupole error in {ring[m].FamName}')

            dMj = dM[countj] # + ave_dM

            """
            # compute reference One turn matrix at each BPM rotating lattice.
            ring_rot = ring[j::] + ring[0:j-1]
            ring_err_rot = ring_err[j::] + ring_err[0:j - 1]

            M0j, _ = ring_rot.find_m44()
            Mj, _ = ring_err_rot.find_m44()

            dMj = Mj - M0j
            """

            # translate to dMtilde  (eqs. C90 and C93)
            ax = bpm[countj].alpha[x]
            bx = bpm[countj].beta[x]

            ay = bpm[countj].alpha[y]
            by = bpm[countj].beta[y]

            dMtilde = copy.deepcopy(dMj) # initialize to be equal to dM at this BPM

            dMtilde[0][0] = dMj[0][0] - ax / bx * dMj[0][1]
            dMtilde[0][1] = ax * dMj[0][0] - ax ** 2 / bx * dMj[0][1] + bx * dMj[1][0] - ax * dMj[1][1]
            dMtilde[1][0] = 1 / bx * dMj[1][0]
            dMtilde[1][1] = ax / bx * dMj[0][1] + dMj[1][1]

            dMtilde[2][2] = dMj[2][2] - ay / by * dMj[2][3]
            dMtilde[2][3] = ay * dMj[2][2] - ay ** 2 / by * dMj[2][3] + by * dMj[3][2] - ay * dMj[3][3]
            dMtilde[3][2] = 1 / by * dMj[3][2]
            dMtilde[3][3] = ay / by * dMj[2][3] + dMj[3][3]

            #  equation C88 C91
            sqrtdmtil = math.sqrt((dMtilde[0][1] + dMtilde[1][0])**2 + 4*dMtilde[0][0]**2)

            asin_arg = sqrtdmtil / 2 / math.sin(Q[x]*2*math.pi)
            f2000 = 1/8 * math.asinh(asin_arg)
            # f2000 = 1/8 * (math.log(1 + asin_arg**2) + asin_arg)
            cosq2000 = -2*dMtilde[0][0] / sqrtdmtil
            sinq2000 = (dMtilde[0][1] + dMtilde[1][0]) / sqrtdmtil

            sqrtdmtil = math.sqrt((dMtilde[2][3] + dMtilde[3][2])**2 + 4*dMtilde[2][2]**2)

            f0020 = 1/8 * math.asinh(sqrtdmtil / 2 / math.sin(Q[y]*2*math.pi) )
            cosq0020 = -2*dMtilde[3][3] / sqrtdmtil
            sinq0020 = (dMtilde[2][3] + dMtilde[3][2]) / sqrtdmtil

            resp_f2000[countj][countm] = f2000 / dKL * (cosq2000 + 1j*sinq2000)

            resp_f0020[countj][countm] = f0020 / dKL * (cosq0020 + 1j*sinq0020)

    return resp_f2000, resp_f0020


def _test_f2000_f0020_quad_deriv(
        m=0,  # quadrupole index
        use_quad_indexes=True,
        use_mp=False
):
    import commissioningsimulations.config as config
    import matplotlib.pyplot as plt
    import matplotlib
    import time
    from commissioningsimulations.correction.optics_coupling.RDT import normal_quad_rdts_response
    from commissioningsimulations.correction.optics_coupling.analytic_f2000_f0020_vs_normal_quad import analytic_f2000_f0020

    # use DBA lattice, old ESRF
    import commissioningsimulations.config as config

    config.lattice_file_for_test = '/machfs/liuzzo/ESRF/StorageRing/LATTICE/AT/ESRF_AT2.mat'
    config.lattice_variable_name = 'ring'

    # name-pattern to find magnets in lattice
    config.normal_quadrupoles_fam_name_pattern = 'Q*'
    config.skew_quadrupoles_fam_name_pattern = 'S[461920]*'  # for simplicity steerers and skew at all sextupoles
    config.sextupoles_fam_name_pattern = 'S[461920]*'
    config.octupoles_fam_name_pattern = 'S[461920]*'
    config.correctors_fam_name_pattern = 'S[461920]*'
    config.bpms_name_patter = 'BPM*'
    config.dipoles_fam_name_pattern = 'B[12]*'


    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    ring.radiation_off()
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = ring[mask_rf][0].PassMethod == 'RFCavityPass'
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadruople indexes
    if use_quad_indexes:
        ind_qua = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    else:
        ind_qua = list(at.get_refpts(ring, config.octupoles_fam_name_pattern))  # test thick non-quadrupoles

    # get reference ORM

    # compute analytic equivalent
    thick_text = 'thin'
    print(f'start {thick_text} analytic f2000 f0020 derivative')
    start_time = time.time()
    resp_f2000_ana, resp_f0020_ana = analytic_f2000_f0020(ring,
                                      ind_bpms=ind_bpms,
                                      ind_quads=[ind_qua[m]],
                                      thick_quadrupole=False,
                                      verbose=False,
                                      use_mp=use_mp)
    end_time = time.time()
    tottime=end_time - start_time
    print(f'time for thin analytic f2000 f0020 derivative= {tottime} seconds')

    thick_text = 'thick'
    print(f'start {thick_text} analytic f2000 f0020 derivative')
    start_time = time.time()
    resp_f2000_anat, resp_f0020_anat = analytic_f2000_f0020(ring,
                                                          ind_bpms=ind_bpms,
                                                          ind_quads=[ind_qua[m]],
                                                          thick_quadrupole=True,
                                                          verbose=False,
                                                          use_mp=use_mp)
    end_time = time.time()
    tottime = end_time - start_time
    print(f'time for analytic f2000 f0020 derivative= {tottime} seconds')

    # compute analytic equivalent

    print(f'start analytic OTM f2000 f0020 derivative')
    start_time = time.time()
    resp_f2000, resp_f0020 = analytic_f2000_f0020_OTM(ring,
                                                  ind_bpms=ind_bpms,
                                                  ind_quads=[ind_qua[m]],
                                                  verbose=False,
                                                  use_mp=use_mp)
    end_time = time.time()
    tottime = end_time - start_time
    print(f'time for analytic OTM f2000 f0020 derivative= {tottime} seconds')

    print(f'start thin analytic f2000 f0020 derivative (matlab translation)')
    start_time = time.time()

    resp_f2000_mat, resp_f0020_mat, _ = normal_quad_rdts_response(ring, np.array(ind_bpms), np.array([ind_qua[m]]))

    end_time = time.time()
    tottime = end_time - start_time
    print(f'time for analytic f2000 f0020 derivative= {tottime} seconds (matlab translated)')

    # MODIFY ONE quadrupole
    dKL = 1e-6

    f2000_o = resp_f2000 * dKL
    f0020_o = resp_f0020 * dKL

    f2000_a = resp_f2000_ana * dKL
    f0020_a = resp_f0020_ana * dKL

    f2000_at = resp_f2000_anat * dKL
    f0020_at = resp_f0020_anat * dKL

    f2000_m = resp_f2000_mat * dKL
    f0020_m = resp_f0020_mat * dKL

    # plot analytic vs numeric
    matplotlib.rcParams.update({'font.size': 10})
    fig, ((ax2000r, ax2000i), (ax0020r, ax0020i)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))
    plt.subplots_adjust(hspace=0.3, wspace=0.3)
    ax2000r.plot([f.real for f in f2000_a], label=f'analytic thin {ring[ind_qua[m]].FamName}')
    ax2000r.plot([f.real for f in f2000_at], label=f'analytic thick {ring[ind_qua[m]].FamName}')
    ax2000r.plot([f.real for f in f2000_o], label=f'analytic OTM {ring[ind_qua[m]].FamName}')
    ax2000r.plot([f.real for f in f2000_m], label=f'analytic thin matlab')
    ax2000r.legend()
    ax2000r.set_ylabel('real(f2000)')
    ax2000r.set_xlabel('BPMs')
    ax2000r.set_ylim([-dKL * 10, +dKL * 10])
    ax2000i.plot([f.imag for f in f2000_a], label=f'analytic thin {ring[ind_qua[m]].FamName}')
    ax2000i.plot([f.imag for f in f2000_at], label=f'analytic thick {ring[ind_qua[m]].FamName}')
    ax2000i.plot([f.imag for f in f2000_o], label=f'analytic OTM {ring[ind_qua[m]].FamName}')
    ax2000i.plot([f.imag for f in f2000_m], label=f'analytic thin matlab')
    ax2000i.legend()
    ax2000i.set_ylabel('imag(f2000)')
    ax2000i.set_xlabel('BPMs')
    ax2000i.set_ylim([-dKL * 10, +dKL * 10])

    ax0020r.plot([f.real for f in f0020_a], label=f'analytic thin {ring[ind_qua[m]].FamName}')
    ax0020r.plot([f.real for f in f0020_at], label=f'analytic thick {ring[ind_qua[m]].FamName}')
    ax0020r.plot([f.real for f in f0020_o], label=f'analytic OTM {ring[ind_qua[m]].FamName}')
    ax0020r.plot([f.real for f in f0020_m], label=f'analytic thin matlab')
    ax0020r.legend()
    ax0020r.set_ylabel('real(f0020)')
    ax0020r.set_xlabel('BPMs')
    ax0020r.set_ylim([-dKL * 10, +dKL * 10])

    ax0020i.plot([f.imag for f in f0020_a], label=f'analytic thin {ring[ind_qua[m]].FamName}')
    ax0020i.plot([f.imag for f in f0020_at], label=f'analytic thick {ring[ind_qua[m]].FamName}')
    ax0020i.plot([f.imag for f in f0020_o], label=f'analytic OTM {ring[ind_qua[m]].FamName}')
    ax0020i.plot([f.imag for f in f0020_m], label=f'analytic thin matlab')
    ax0020i.legend()
    ax0020i.set_ylabel('imag(f0020)')
    ax0020i.set_xlabel('BPMs')
    ax0020i.set_ylim([-dKL*10, +dKL*10])

    np.savetxt(f'f2000{ring[ind_qua[m]].FamName}_analytic{thick_text}_H.txt', f2000_a, fmt='%10.5f', delimiter=',')
    np.savetxt(f'f0020{ring[ind_qua[m]].FamName}_analytic{thick_text}_V.txt', f0020_a, fmt='%10.5f', delimiter=',')

    plt.show()

    return



def _test_RDT_from_otm(
        m=0,  # quadrupole index
        use_quad_indexes=True,
        use_mp=False
        ):

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    ring.radiation_off()
    ring.set_cavity(Voltage=0.0)

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadruople indexes
    if use_quad_indexes:
        ind_qua = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    else:
        ind_qua = list(at.get_refpts(ring, config.octupoles_fam_name_pattern))  # test thick non-quadrupoles

    # compute reference One turn matrix at each BPM.
    _M0_, M0 = ring.find_m44(refpts=ind_bpms, full=True)

    # vary + one quadrupole
    dKL = 0.001
    ring_err = copy.deepcopy(ring)
    K = ring_err[ind_qua[m]].PolynomB[1]
    ring_err[ind_qua[m]].PolynomB[1] = K + dKL / ring_err[ind_qua[m]].Length

    # compute One turn matrix at each BPM with a quadrupole error
    _M_, M = ring_err.find_m44(refpts=ind_bpms, full=True)

    _dM_ = _M_ - _M0_
    dM = [m - m0 for m, m0 in zip(M, M0)]

    print(dM[0])

    ind_opt = np.array(range(len(ring)+1))  # range(len(ring))
    _, _, opt_all_location = ring.linopt4(ind_opt)

    opt_obs = opt_all_location[-1]

    x = 0
    y = 1
    mx = np.array([opt_obs.mu[x]])
    my = np.array([opt_obs.mu[y]])
    ax = np.array([opt_obs.alpha[x]])
    ay = np.array([opt_obs.alpha[y]])
    bx = np.array([opt_obs.beta[x]])
    by = np.array([opt_obs.beta[y]])
    gx = [(1 + _ax ** 2) / _bx for _ax, _bx in zip(ax, bx)]
    gy = [(1 + _ay ** 2) / _by for _ay, _by in zip(ay, by)]

    Mtheo = np.zeros((4, 4))
    Mtheo[0, 0] = [math.cos(_mx) + _ax*math.sin(_mx) for _mx, _ax in zip(mx, ax)][0]
    Mtheo[0, 1] = [_bx*math.sin(_mx) for _mx, _bx in zip(mx, bx)][0]
    Mtheo[0, 2] = 0.0
    Mtheo[0, 3] = 0.0

    Mtheo[1, 0] = [-_gx*math.sin(_mx) for _gx, _mx in zip(gx, mx)][0]
    Mtheo[1, 1] = [math.cos(_mx) - _ax*math.sin(_mx)  for _mx, _ax in zip(mx, ax)][0]
    Mtheo[1, 2] = 0.0
    Mtheo[1, 3] = 0.0

    Mtheo[2, 0] = 0.0
    Mtheo[2, 1] = 0.0
    Mtheo[2, 2] = [math.cos(_my) + _ay*math.sin(_my) for _my, _ay in zip(my, ay)][0]
    Mtheo[2, 3] = [_by*math.sin(_my) for _my, _by in zip(my, by)][0]

    Mtheo[3, 0] = 0.0
    Mtheo[3, 1] = 0.0
    Mtheo[3, 2] = [-_gy*math.sin(_my) for _gy, _my in zip(gy, my)][0]
    Mtheo[3, 3] = [math.cos(_my) - _ay*math.sin(_my)  for _my, _ay in zip(my, ay)][0]

    print("analytic no errors")
    print(Mtheo)
    print("find_m44 no errors")
    print(_M0_)

    # with error
    ind_opt = np.array(range(len(ring_err)+1))  # range(len(ring))
    _, _, opt_all_location = ring_err.linopt4(ind_opt)

    opt_obs = opt_all_location[-1]

    x = 0
    y = 1
    mx = np.array([opt_obs.mu[x]])
    my = np.array([opt_obs.mu[y]])
    ax = np.array([opt_obs.alpha[x]])
    ay = np.array([opt_obs.alpha[y]])
    bx = np.array([opt_obs.beta[x]])
    by = np.array([opt_obs.beta[y]])
    gx = [(1 + _ax ** 2) / _bx for _ax, _bx in zip(ax, bx)]
    gy = [(1 + _ay ** 2) / _by for _ay, _by in zip(ay, by)]

    Mtheo = np.zeros((4, 4))
    Mtheo[0, 0] = [math.cos(_mx) + _ax*math.sin(_mx) for _mx, _ax in zip(mx, ax)][0]
    Mtheo[0, 1] = [_bx*math.sin(_mx) for _mx, _bx in zip(mx, bx)][0]
    Mtheo[0, 2] = 0.0
    Mtheo[0, 3] = 0.0

    Mtheo[1, 0] = [-_gx*math.sin(_mx) for _gx, _mx in zip(gx, mx)][0]
    Mtheo[1, 1] = [math.cos(_mx) - _ax*math.sin(_mx)  for _mx, _ax in zip(mx, ax)][0]
    Mtheo[1, 2] = 0.0
    Mtheo[1, 3] = 0.0

    Mtheo[2, 0] = 0.0
    Mtheo[2, 1] = 0.0
    Mtheo[2, 2] = [math.cos(_my) + _ay*math.sin(_my) for _my, _ay in zip(my, ay)][0]
    Mtheo[2, 3] = [_by*math.sin(_my) for _my, _by in zip(my, by)][0]

    Mtheo[3, 0] = 0.0
    Mtheo[3, 1] = 0.0
    Mtheo[3, 2] = [-_gy*math.sin(_my) for _gy, _my in zip(gy, my)][0]
    Mtheo[3, 3] = [math.cos(_my) - _ay*math.sin(_my)  for _my, _ay in zip(my, ay)][0]

    print("analytic with error")
    print(Mtheo)
    print("find_m44 with error")
    print(_M_)

    return


if __name__ == '__main__':
    # _test_RDT_from_otm()
    # _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=False, use_quad_indexes=False)  # not ok
    _test_f2000_f0020_quad_deriv(m=1, use_quad_indexes=True)  # not ok

    # _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=False)  # not ok
    # _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=True)  # not ok

    pass
