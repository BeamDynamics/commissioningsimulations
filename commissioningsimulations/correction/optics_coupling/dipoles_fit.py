import at
import os
import copy
import time
import math
import socket
import numpy as np
from os.path import exists
from matplotlib import pyplot as plt
import multiprocessing
import pickle
from itertools import repeat
from scipy.io import savemat, loadmat
from scipy.linalg import svdvals
import commissioningsimulations.config as config
from commissioningsimulations.correction.ClosedOrbit import compute_analytic_orbit_response_matrix, \
    compute_orbit_response_matrix
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.correction.optics_coupling.analytic_dispersion_with_dipole_errors import \
    analytic_dispersion_variation_with_dipole


def get_disp_arrays(ring,
                    ind_bpms,
                    bpms_weigths,
                    mode,
                    verbose):
    """
    compute ORM, dispersion and tunes or loads a measured ones.
    apply weigths to BPMS, dispersion and tunes
    returns 2 1D arrays to be used for optics and coupling correction

    :param ring: AT lattice
    :param ind_bpms: index of BPMS in ring
    :param bpms_weigths: 2D numpy array of size (2 (H,V), len(ind_bpms)) to multiply each column of the response matrices
    :param mode: 'numeric' (default), 'analytic', 'measured' (not yet functional)
    :param verbose: boolean. if True print out
    :return: ForHorDip (1D np array of length(len(H orm)+len(V orm)+len(disph)+len(tunes))
             ForVerDip (1D np array of length(len(H2V orm)+len(V2H orm)+len(dispv))
    """

    # compute ORM
    if mode == 'numeric':

        _, _, _, _, _, _, _, _, _, _, dh, dv, tunes = \
            compute_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=[], verbose=verbose)

    elif mode == 'analytic':

        _, _, _, _, _, _, _, _, _, _, dh, dv, tunes = \
            compute_analytic_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=[], verbose=verbose)

    elif mode == 'measured':
        raise NameError('measured mode not yet functional')

    else:
        raise NameError(f'{mode} is not an allowed mode. Use: numeric or analytic')

    # add BPM weigths (hill BPMS get a small weigth)
    if bpms_weigths:  # not None
        if bpms_weigths.shape == (2, len(ind_bpms)):
            # divide each bpm in each column by the corresponding bpm weigth
            dh = dh * bpms_weigths[0, :].T
            dv = dv * bpms_weigths[1, :].T
        else:
            raise ValueError(f'bpm_weigths must be a numpy array of size (2, {len(ind_bpms)})')

    # build arrays for fit
    ForHorCor = np.concatenate((flat(dh)))  # hor dispersion

    ForVerCor = np.concatenate((flat(dv)))  # ver dispersion

    return ForHorCor, ForVerCor


def flat(MM):
    '2D to 1D'
    return np.matrix(MM).flatten().transpose()


def jacobian(ring,
             ring_index_to_modify,
             deltaL,
             ind_bpms,
             bpms_weigths,
             mode,
             verbose=False):
    """
    computes the derivative of the (ORM+dispersion+tune) array produced by get_disp_arrays with respect to a variation of
    ring[ring_index_to_modify].field[index_in_field]

    :param ring: AT lattice
    :param integrated_scalar_field: element field to modify (for example PolynomB)
    :param ring_index_to_modify: index in the ring that has to be modified
    :param deltaL: integrated strength variation to apply to ring[ring_index_to_modify].field[index_in_field]
    :param ind_bpms: indexes of BPMs for Orbit Response matrix (get_disp_arrays)
    :param bpms_weigths: weights to apply to each BPM (get_disp_arrays)
    :param mode: 'numeric', 'semi-analytic', 'analytic'(not yet functional)
                'numeric' will compute the ORM with tracking
                'semi-analytic' will vary ring[ring_index_to_modify].field[index_in_field] and compute
                                an analytic ORM based on this lattice
    :param field_is_integrated: defualt False. if True, do not divide by length of magnet
    :param verbose: if True, print out text
    :return: JQ = H+delta/2 - H-delta/2 / delta
             JS = V+delta/2 - V-delta/2 / delta
    """

    orm_mode=mode

    if mode == 'semi-analytic':
        orm_mode='analytic'

    if verbose:
        print(f'modify {ring_index_to_modify}th element in ring {ring[ring_index_to_modify].FamName} BendingAngle by {deltaL}')

    # get initial vale
    initial_value = ring[ring_index_to_modify].BendingAngle

    # modify
    ring[ring_index_to_modify].BendingAngle = initial_value + deltaL / 2

    if verbose:
        print(f'get {len(ind_bpms)}, dispersion')

    # get ORM
    q_p, s_p = get_disp_arrays(ring,
                              ind_bpms,
                              bpms_weigths,
                              orm_mode,
                              False)

    # modify
    ring[ring_index_to_modify].BendingAngle = initial_value - deltaL / 2

    # get ORM
    q_m, s_m = get_disp_arrays(ring,
                               ind_bpms,
                               bpms_weigths,
                               orm_mode,
                               False)

    # restore original value
    ring[ring_index_to_modify].BendingAngle = initial_value

    JQ = (q_p - q_m) / deltaL
    JS = (s_p - s_m) / deltaL

    return np.asarray(JQ), np.asarray(JS)


def analytic_jacobian(ring,
                     indexes=None,
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0):
    """
    compute the jacobian based on:

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param indexes: dictionry output of commissioningsimulations.get_indexes.get_lattice_indexes
    :param use_mp: not used
    :param n_processes: not used
    :param bpms_weigths: weigths to apply to each BPM (get_disp_arrays)
    :param hor_disp_weight: weigth for horizontal dispersion (get_disp_arrays)
    :param ver_disp_weight: weigth for vertical dispersion (get_disp_arrays)
    :param tune_weight: weigth for tunes (get_disp_arrays)
    :param verbose: print text
    :return:    variation of [H2H, V2V, EtaH, Q] with quad and
                variation of [H2V, V2H, EtaV] with skew quad
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring, verbose=verbose)

    ind_bpms = indexes['bpms']
    ind_dips = indexes['dipoles_for_optics_and_coupling_fit']

    # compute optics before and only once
    _, _, opt_all = ring.linopt6(range(len(ring)))

    if verbose:
        print('computing dispersion derivative to normal/skew dipoles')
    # derivative of diagonal ORM blocks respect to normal quadrupoles
    dh_, dv_ = analytic_dispersion_variation_with_dipole(ring,
                                                           ind_bpms=ind_bpms,
                                                           ind_dips=ind_dips,
                                                           opt_all_location=opt_all,
                                                           verbose=False)

    JQ = np.zeros((len(ind_bpms), len(ind_dips)))
    JS = np.zeros((len(ind_bpms), len(ind_dips)))

    # apply weigths and flatten to array
    for iq, _ in enumerate(ind_dips):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                dh = dh_[:, iq] * bpms_weights[0, :].T
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            dh = dh_[:, iq]

        # build arrays for fit
        JQ[:, iq] = np.asarray(np.concatenate( flat(dh) ))[:, 0]  # disp H

    for iq, _ in enumerate(ind_dips):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights:  # not None
            if bpms_weights.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                dv = dv_[:, iq] * bpms_weights[1, :].T
            else:
                raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            dv = dv_[:, iq]

            # build arrays for fit
        JS[:, iq] = np.asarray(np.concatenate( flat(dv) ))[:, 0]  # ver dispersion

    return JQ, JS


def compute_jacobian(ring,
                     indexes=None,
                     filename_response='./DispDipDerivative.pkl',
                     save_matlab_file=False,
                     mode='numeric',  # 'semi-analytic', 'analytic'
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     DBendAng = 1e-4,
                     field_is_integrated=True
                     ):
    """
    computes the derivative of the orm+dispersion+tune vectors produced by get_orm_array with respect to the
    list of normal and skew quadrupole correctors specified in config.normal(skew)_quadrupoles_for_optics_fit

    :param ring: AT lattice
    :param indexes: dictionary of indexes obtained with commissioningsimulations.config.get_lattice_indexes
    :param filename_response: file where to store the computed derivatives (large file). if None, no file is saved)
    :param save_matlab_file: save to .mat format
    :param mode: 'numeric' (default), 'analytic', 'semi-analytic'  (see commissioningsimulations.CorrectOptics.jacobian)
    :param use_mp: use multiprocessing on local host
    :param n_processes: number of cores to use. if None, use all
    :param verbose: if True, print text
    :param bpms_weights: see get_orm_array
    :param DBendAng: variation of BendingAngles
    :return: JH derivative of first output of get_disp_array respect to normal dipoles,
             JV derivative of first output of get_disp_array respect to skew dipoles,
             sH singular values of JQ
             sV singular values of JS
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    ind_bpms = indexes['bpms']
    ind_dips = indexes['dipoles_for_optics_and_coupling_fit']

    NB = len(ind_bpms)
    ND = len(ind_dips)

    if verbose:
        print(f'optics/coupling errors fit\n '
              f'dispersion size: ({NB}, {1}) \n'
              f'{ND} {config.dipoles_for_optics_and_coupling_fit} dipoles used for fit')

    if mode == 'numeric' or mode == 'semi-analytic':

        # initialize RM, just to get sizes
        H0, V0 = get_disp_arrays(ring,
                                indexes['bpms'],
                                bpms_weights,
                                'analytic',
                                False)

        JQ = np.zeros((len(H0), ND))
        JS = np.zeros((len(V0), ND))

        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            if n_processes is None:
                n_processes = n_cpu
            else:
                max_proc = min(n_cpu, NC)
                if n_processes > max_proc:
                    n_processes = max_proc

            print(f'RM derivative parallel computation using {n_processes} cores on {socket.gethostname()}')

            with multiprocessing.Pool(processes=n_processes) as p:

                results_normal = p.starmap(jacobian,
                                        zip(repeat(ring),
                                            ind_dips,  # loop variable
                                            repeat(DBendAng),
                                            repeat(ind_bpms),
                                            repeat(bpms_weights),
                                            repeat(mode), repeat(verbose)))


            # collect results
            for count, ic in enumerate(ind_dips):
                JQ[:, count] = results_normal[count][0][:, 0]  # enusre correct dimensionality
                JS[:, count] = results_normal[count][1][:, 0]  # enusre correct dimensionality

        else:

            for count, ic in enumerate(ind_dips):

                jq, js = jacobian(ring,
                                 'BendingAngle',
                                 ic,
                                 DBendAng,
                                 ind_bpms,
                                 bpms_weights,
                                 mode, verbose=True)

                JQ[:, count] = jq[:, 0]  # ensure correct dimensionality
                JS[:, count] = js[:, 0]

    elif mode == 'analytic':

        JQ, JS = analytic_jacobian(ring,
                          indexes=indexes,
                          use_mp=use_mp,
                          n_processes=n_processes,
                          verbose=verbose,
                          bpms_weights=bpms_weights)

    # invert and get singular values
    if verbose:
        print(f'computing inverse of orm derivative')

    #_, sQ, _ = np.linalg.svd(JQ, full_matrices=True)
    #_, sS, _ = np.linalg.svd(JS, full_matrices=True)
    sQ=svdvals(JQ)
    sS=svdvals(JS)

    if filename_response:

        mdict = {'sH': sQ, 'sV': sS,
                 'JH': JQ, 'JV': JS,
                 'bpms_weights': bpms_weights,
                 'ind_bpms': ind_bpms,
                 'ind_dips': ind_dips}

        file_no_ext, exten_file = os.path.splitext(filename_response)

        if save_matlab_file:
            try:
                savemat(file_no_ext + '.mat', mdict=mdict)
            except Exception as e:
                print(e)
                print('could not save .mat file')

        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        while not(exists(filename_response)):
            print('Wait for file {} to be saved'.format(filename_response))
            time.sleep(1)

        print('ORM derivatives saved in: {}'.format(filename_response))

    return JQ, JS, sQ, sS


def fit_optics(ring_to_correct,
               reference_ring,
               niter=1,
               neigDipo=1,
               bpms_weights=None,
               mode='numeric',
               indexes=None,
               filename_response=None,
               reference_disp=None,
               use_mp=False,
               verbose=True,
               normal=True,
               skew=False):
    """
    fit dispersion with dipole errors
    these errors added to reference_ring will return an ORM as close as possible to the one of ring_to_correct
    the fitted errors are applied as correction to ring_to_correct to output a corrected lattice rcor

    :param ring_to_correct: ring with optics/coupling errors
    :param reference_ring: reference ring, no errors
    :param niter: number of iteration of correction
    :param neigDipo: # of singular vectors to use for quad correction
    :param bpms_weights: (2, nbpms) array of multiplication factors for each raw of the RM.
    :param mode: 'numeric', 'semi-analytic', 'analytic'
    :param indexes: indexes obtained by commissioningsimulations.config.get_lattice_indexes. if none, they are computed.
    :param filename_response: file where to look for derivative of ORM. if not found, computed
    :param reference_disp: as computed by get_disp_arrays, or None (compute it)
    :param use_mp: use multiprocessing
    :param verbose: if True, print text
    :return: rfit fitted model for ring_to_correct including only quadrupole errors
             rcor corrected AT lattice
             dcH applied normal dipoles correctors variations
             dcV (ALWAYS ZERO, not available yet) applied skew dipole correctors variations
    """

    if verbose:
        print('correct optics')

    force_matrix_computation = False

    orm_mode = mode
    if mode == 'semi-analytic':
        orm_mode='analytic'

    # test, overwrite ORM mode to keep always numeric
    orm_mode = 'numeric'

    if not(indexes):
        indexes = get_lattice_indexes(reference_ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']
    ind_dips = indexes['dipoles_for_optics_and_coupling_fit']

    # get copy of ring
    rcor = copy.deepcopy(ring_to_correct)
    rfit = copy.deepcopy(reference_ring)

    # print initial status
    if verbose:
        print('fit-lattice vs input-lattice (should be zero after fit):')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

#    if verbose:
#        print('Before optics/coupling fit (should be zero after fit):')
#        get_optics_vs_reference(reference_ring, ring_to_correct, ind_bpms)

    # get Orbit Response Matrix if no file is provided, based on lattice with errors.
    if (not exists(filename_response)) or force_matrix_computation:
        start_time = time.time()
        JH, JV, sH, sV = compute_jacobian(reference_ring,
                                          indexes=indexes,
                                          filename_response=filename_response,
                                          save_matlab_file=True,
                                          mode=mode,  # 'semi-analytic', 'analytic'
                                          use_mp=use_mp, #False, #
                                          n_processes=None,
                                          bpms_weights=bpms_weights,
                                          verbose=True)  # all if None
        end_time = time.time()
        if verbose:
            print(f'time for (parallel = {use_mp}) orm derivative computation: {end_time-start_time} seconds')
    else:

        _, file_ext = os.path.splitext(filename_response)
        if file_ext == '.mat':
            mat_contents = loadmat(filename_response)
            JH = mat_contents['JH']
            JV = mat_contents['JV']
            sH = np.transpose(mat_contents['sH'])  # load mat transposes raws/columns for some reason
            sH = sH[:, 0]
            sV = np.transpose(mat_contents['sV'])  # load mat transposes raws/columns for some reason
            sV = sV[:, 0]
        else:
            mat_contents = pickle.load(open(filename_response, 'rb'))
            JH = mat_contents['JH']
            JV = mat_contents['JV']
            sH = mat_contents['sH']
            sV = mat_contents['sV']
        if verbose:
            print(f'loaded: {filename_response}')

    # fix  singular values to max possible if too many
    if neigDipo > JH.shape[1]:
        neigDipo = JH.shape[1] -1
        if verbose:
            print(f'max singular values for dipoles = {JH.shape[1]}')

    # get initial correctors values
    # Lq = at.get_value_refpts(rcor, ind_quad, 'Length')
    # Ls = at.get_value_refpts(rcor, ind_skew, 'Length')

    initcorQ = at.get_value_refpts(rcor, ind_dips, 'BendingAngle')
    initcorS = at.get_value_refpts(rcor, ind_dips, 'BendingAngle')

    # get initial correctors values in rfit
    initcorQ0 = at.get_value_refpts(rfit, ind_dips, 'BendingAngle')
    initcorS0 = at.get_value_refpts(rfit, ind_dips, 'BendingAngle')

    # initialize corrections to zero. INTEGRATED STRENGHTS
    dcH = np.zeros(len(ind_dips))
    dcV = np.zeros(len(ind_dips))

    # get ORM to correct
    Hinit, Vinit = get_disp_arrays(rcor,
                                  ind_bpms,
                                  bpms_weights,
                                  'numeric',
                                  False)

    # get reference ORM
    if reference_disp==None:
        H0, V0 = get_disp_arrays(reference_ring,
                                ind_bpms,
                                bpms_weights,
                                orm_mode,
                                False)
    else:
        H0 = reference_disp[0]
        V0 = reference_disp[1]

    for correction_iteration in range(0, niter):
        print('iteration # {}'.format(correction_iteration + 1))

        # get ORM
        if correction_iteration>0:
            H, V = get_disp_arrays(rcor,
                                  ind_bpms,
                                  bpms_weights,
                                  orm_mode,
                                  False)

        else:  # use already available
            H = Hinit
            V = Vinit

        # compute correction and add to previous iterations
        ooh = np.asarray(H - H0)[:, 0]
        oov = np.asarray(V - V0)[:, 0]

        if normal:
            dcH = dcH + np.linalg.pinv(JH, rcond=sH[neigDipo - 1] / sH[0]) @ ooh

        if skew:
            raise Exception('rotation to make a vertical dipole in pyAT not implemented, please wait for next releases')
            dcV = dcV + np.linalg.pinv(JV, rcond=sV[neigDipo - 1] / sV[0]) @ oov

        # check that the corrections are <30 mircorad
        if verbose:
            print(f' disp for hor dipole | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
            print(f' disp for ver dipole | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')
            print(f' norm dipole cor. | std: {np.std(dcH):.3g} | (min, max) ({np.min(dcH):.3g}, {np.max(dcH):.3g})')
            print(f' skew dipole cor. | std: {np.std(dcV):.3g} | (min, max) ({np.min(dcV):.3g}, {np.max(dcV):.3g})')

        # apply correction
        for count, ic in enumerate(ind_dips):

            # set correctors in the lattice
            rcor[ic].BendingAngle = initcorQ[count] - dcH[count]
            # assign errors to optics model
            rfit[ic].BendingAngle = initcorQ0[count] + dcH[count]

        """
        for count, ic in enumerate(ind_dips):
            # set correctors in the lattice
            rcor[ic].BendingAngle = initcorS[count] - dcV[count] / L
            # assign errors to optics model
            rfit[ic].BendingAngle = initcorS0[count] + dcV[count] / L
        """

    # get Orbit RM after correction
    Haft, Vaft = get_disp_arrays(rcor,
                                ind_bpms,
                                bpms_weights,
                                orm_mode,
                                False)

    ooh = np.asarray(Haft - H0)[:, 0]
    oov = np.asarray(Vaft - V0)[:, 0]
    if verbose:
        print(f'Final disp for hor. dipoles | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
        print(f'Final disp for ver. dipoles | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')

    if verbose:
        print('fit-lattice vs input-lattice:')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

    #if verbose:
    #    print('After optics/coupling fit:')
    #    get_optics_vs_reference(reference_ring, rcor, ind_bpms)

    return rfit, rcor, dcH, dcV


def _test_dispersion_fit():
    '''
    script to test dispersion fit with dipole errors
    :return: None
    '''

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # shift sextupoles to generate quad and skew quad
    ind_sext = list(at.get_refpts(ring, config.sextupoles_fam_name_pattern))

    dx = np.zeros(len(rerr))
    dy = np.zeros(len(rerr))

    NQ = int(len(ind_sext))
    dx[ind_sext] = 30e-6 * np.random.randn(NQ)
    dy[ind_sext] = 30e-6 * np.random.randn(NQ)

    rerr.set_shift(dx, dy)

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)

    # correct optics

    mode = 'semi-analytic'

    rfit, rcor, Hcor, Vcor = fit_optics(rerr,
                          ring,
                          niter=3,
                          neigDipo=30,
                          mode=mode,
                          indexes=indexes,
                          use_mp=True,
                          filename_response='./JDip' + mode + '.pkl')

    # optics after correction
    # rcor.plot_beta()

    print('BEFORE CORRECTION')
    bbh,  bbv,  ddh,  ddv,  deh,  dev = get_optics_vs_reference(ring, rerr, indexes['bpms'])

    print('AFTER CORRECTION')
    bbha, bbva, ddha, ddva, deha, deva = get_optics_vs_reference(ring, rcor, indexes['bpms'])

    # plot figures
    fig, ((axbh, axdh), (axbv, axdv), (axQ, axS)) = plt.subplots(nrows = 3, ncols=2)
    fig.subplots_adjust(hspace=.5, wspace=0.5)
    axbh.plot(bbh*100, label=f'before $\delta\epsilon_h$ = {deh*1e12:.2f}')
    axbh.plot(bbha*100, label=f'after $\delta\epsilon_h$ = {deha*1e12:.2f}')
    axbh.set_ylabel(f' hor. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbh.legend()
    axbv.plot(bbv * 100, label=f'before $\delta\epsilon_h$ = {dev*1e12:.2f}')
    axbv.plot(bbva * 100, label=f'after $\delta\epsilon_h$ = {deva*1e12:.2f}')
    axbv.set_ylabel(f' ver. $(\\beta-\\beta_0)/\\beta_0$ %')
    axbv.legend()

    axdh.plot(ddh * 100, label='before')
    axdh.plot(ddha * 100, label='after')
    axdh.set_ylabel(f' hor. $(\eta-\eta_0)/\beta_0$ %')
    axdh.legend()
    axdv.plot(ddv * 100, label='before')
    axdv.plot(ddva * 100, label='after')
    axdv.set_ylabel(f' ver. $(\eta-\eta_0)/\beta_0$ %')
    axdv.legend()

    axQ.bar(range(len(Hcor)), Hcor, label='after')
    axQ.set_ylabel(f' Bending Angles ')
    axQ.legend()
    axS.bar(range(len(Vcor)), Vcor, label='after')
    axS.set_ylabel(f' Vertical Bending Angles ')
    axS.legend()

    plt.show()

    pass


if __name__=='__main__':

    _test_dispersion_fit()

    pass
