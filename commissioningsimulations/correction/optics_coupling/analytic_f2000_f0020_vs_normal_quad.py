"""
formulas from
A.Franchi (ESRF), Z.Marti (CELLS),
"Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018
"""

import at
import math
import cmath
import numpy as np
import multiprocessing
from itertools import repeat

__author__='Simone Maria Liuzzo, Andrea Franchi'

def analytic_f2000_f0020(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True,
        thick_quadrupole=True,
        opt_all_location=None,
        use_mp=False):
    """
    Computes the derivative of the orbit response matrix compared to normal quadrupoles

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_quads: numpy array of np.uint32. indexes of normal quadrupoles
    :param verbose: True/False
    :param thick_quadrupole:  True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :return: two 2D list of complex.
    resp_f2000[BPM][QUAD], resp_f0020[BPM][QUAD]
    """

    resp_f2000 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))
    resp_f0020 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))

    # loop quadrupoles
    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('RM derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap( _analytic_f2000_f0020,
                                 zip(repeat(ring),
                                     repeat(ind_bpms),
                                     ind_quads,    # loop index
                                     repeat(thick_quadrupole),
                                     repeat(verbose),
                                     repeat(opt_all_location)
                                     )
                                 )

        for m, _ in enumerate(ind_quads):
            _resp_f2000 = results[m][0]
            _resp_f0020 = results[m][1]
            resp_f2000[:, m] = _resp_f2000[:, 0]
            resp_f0020[:, m] = _resp_f0020[:, 0]

    else:  # sequential

        resp_f2000, resp_f0020 = _analytic_f2000_f0020(
                        ring,
                        ind_bpms=ind_bpms,
                        ind_quads=ind_quads,
                        verbose=verbose,
                        thick_quadrupole=thick_quadrupole,
                        opt_all_location=opt_all_location)

    return resp_f2000, resp_f0020


def _analytic_f2000_f0020(
        ring,
        ind_bpms=None,
        ind_quads=None,
        thick_quadrupole=False,
        verbose=True,
        opt_all_location=None):
    """
    analytic f2000 and f0020 RDTs derivative with integrated (KL) errors at normal quadrupoles

    :param ring:
    :param ind_bpms:
    :param ind_quads:
    :param thick_quadrupole:
    :param verbose:
    :param opt_all_location:
    :return:
    """

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    """
    
    # lines for test vs matlab equivalent
    if thick_quadrupole == False:
        a = at.avlinopt(ring, 0, range(len(ring)))
        avebeta = a[1]
        avemu = a[2]
        print(f'replace beta and mu by avebeta and avemu')
        for c, o in enumerate(opt_all_location):
            opt_all_location[c].beta = avebeta[c, :]
            opt_all_location[c].mu = avemu[c, :]
    # end of test lines to be removed after tests
    
    """


    # print(type(ind_quads))
    if isinstance(ind_quads, np.uint32):
        ind_quads = [ind_quads]

    bpm = opt_all_location[ind_bpms]
    qua = opt_all_location[ind_quads]

    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # functions required by later formulas

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2*math.pi*Q[pl]

        return d

    # formulas for response

    resp_f2000 = np.zeros(shape=(len(bpm), len(qua))) + complex('j')
    resp_f0020 = np.zeros(shape=(len(bpm), len(qua))) + complex('j')

    x = 0
    y = 1
    _sign = [+1, -1]
    h = 0
    v = 1

    def Ib(qm, p, Km, Lm):
        # Km = abs(Km)
        gamma = (1 + qm.alpha[p] ** 2) / qm.beta[p]
        sKL = 2 * cmath.sqrt(Km) * Lm

        val = 1 / 2 * (qm.beta[p] + gamma / Km) + \
              cmath.sin(sKL) / (2 * sKL) * (qm.beta[p] - gamma / Km) + \
              qm.alpha[p] / (2 * Km * Lm) * (cmath.cos(sKL) - 1)

        return val.real

    def IS(qm, p, Km, Lm):
        #Km = abs(Km)
        sK = 2 * cmath.sqrt(Km)
        val = 1 / (Km * Lm) * (
                1 / 2 * (1 - cmath.cos( sK * Lm)) +
                qm.alpha[p] / qm.beta[p] * (cmath.sin(sK * Lm) / sK - Lm)
              )
        return val.real

    def IC(qm, p, Km, Lm):
        # Km = abs(Km)
        sKL = 2 * cmath.sqrt(Km) * Lm
        val = Ib(qm, p, Km, Lm) - 1 / (Km * qm.beta[p]) * (1 - cmath.sin(sKL) / sKL)
        return val.real


    def Ib_notquad(qm, p, Lm):
        # Km = abs(Km)
        gamma = (1 + qm.alpha[p] ** 2) / qm.beta[p]

        val = qm.beta[p] - qm.alpha[p]*Lm + gamma/3*Lm**2

        return val.real

    def IS_notquad(qm, p, Lm):
        #Km = abs(Km)

        val = Lm - 2/3 * qm.alpha[p] / qm.beta[p] * Lm**2

        return val.real

    def IC_notquad(qm, p, Lm):
        # Km = abs(Km)

        val = Ib_notquad(qm, p, Lm) - 2/3 / qm.beta[p] * Lm**2

        return val.real

    for countm, m in enumerate(range(len(qua))):

        if thick_quadrupole:
            Lm = ring[ind_quads[m]].Length
            # if it is a quadrupole
            if isinstance(ring[ind_quads[m]], at.Quadrupole):
                Km = ring[ind_quads[m]].PolynomB[1]

                # _sign removed compared to analytic_orm_with_normal_quad_errors.
                ISm = [IS(qua[m], p, Km, Lm) for p in [x, y]]
                ICm = [IC(qua[m], p, Km, Lm) for p in [x, y]]

            else:
                # # else, assume drift (only quads change optics)
                ISm = [IS_notquad(qua[m], p, Lm) for p in [x, y]]
                ICm = [IC_notquad(qua[m], p, Lm) for p in [x, y]]
        else:
            # thin quadrupole

            ISm = [0.0 for p in [x, y]]
            ICm = [qua[m].beta[p] for p in [x, y]]

        for countj, j in enumerate(range(len(bpm))):

            if verbose:
                print(f'computing f2000 and f0020 RDTs response at'
                      f' BPM {ring[ind_bpms[j]].FamName}'
                      f' with a normal (thick={thick_quadrupole}) quadrupole error in {ring[ind_quads[m]].FamName}')

            pmj = [dphi(p, qua[m], bpm[j]) for p in [x, y]]

            #  equation C66 C67
            resp_f2000[j][m] = - 1 / (8 * (1 - cmath.exp(4 * math.pi * 1j * Q[x]))) * (
                        cmath.cos(2 * pmj[x]) * (ICm[x] - 1j * ISm[x]) +
                        cmath.sin(2 * pmj[x]) * (ISm[x] + 1j * ICm[x]))

            resp_f0020[j][m] = + 1 / (8 * (1 - cmath.exp(4 * math.pi * 1j * Q[y]))) * (
                        cmath.cos(2 * pmj[y]) * (ICm[y] - 1j * ISm[y]) +
                        cmath.sin(2 * pmj[y]) * (ISm[y] + 1j * ICm[y]))

    return resp_f2000, resp_f0020


def _test_f2000_f0020_quad_deriv(
        m=0,  # quadrupole index
        thick_quadrupole=True,
        use_quad_indexes = True,
        use_mp=False
        ):

    import commissioningsimulations.config as config
    import matplotlib.pyplot as plt
    import time
    from commissioningsimulations.correction.optics_coupling.RDT import normal_quad_rdts_response

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')


    ring.radiation_off()
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = ring[mask_rf][0].PassMethod == 'RFCavityPass'
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadruople indexes
    if use_quad_indexes:
        ind_qua = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    else:
        ind_qua = list(at.get_refpts(ring, config.octupoles_fam_name_pattern))  # test thick non-quadrupoles

    # get reference ORM

    # compute analytic equivalent
    if thick_quadrupole:
        thick_text = 'thick_quad'
    else:
        thick_text = 'thin_quad'

    print(f'start {thick_text} analytic f2000 f0020 derivative')
    start_time = time.time()
    resp_f2000, resp_f0020 = analytic_f2000_f0020(ring,
                                      ind_bpms=ind_bpms,
                                      ind_quads=[ind_qua[m]],
                                      thick_quadrupole=thick_quadrupole,
                                      verbose=False,
                                      use_mp=use_mp)
    end_time = time.time()
    tottime=end_time - start_time
    print(f'time for analytic f2000 f0020 derivative= {tottime} seconds')

    print(f'start thin analytic f2000 f0020 derivative (matlab translation)')
    start_time = time.time()

    resp_f2000_mat, resp_f0020_mat, _ = normal_quad_rdts_response(ring, np.array(ind_bpms), np.array([ind_qua[m]]))

    end_time = time.time()
    tottime = end_time - start_time
    print(f'time for analytic f2000 f0020 derivative= {tottime} seconds (matlab translated)')
    print(f'end thin analytic f2000 f0020 derivative (matlab translation)')

    # MODIFY ONE quadrupole
    dKL = 0.001

    f2000_a = resp_f2000 * dKL
    f0020_a = resp_f0020 * dKL

    f2000_m = resp_f2000_mat * dKL
    f0020_m = resp_f0020_mat * dKL

    # plot analytic vs numeric

    fig, ((ax2000r, ax2000i), (ax0020r, ax0020i)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))
    plt.subplots_adjust(hspace=0.5, wspace=0.5)
    ax2000r.plot([f.real for f in f2000_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax2000r.plot([f.real for f in f2000_m], label=f'analytic thin matlab')
    ax2000r.legend()
    ax2000r.set_ylabel('real(f2000)')
    ax2000r.set_xlabel('BPMs')
    ax2000i.plot([f.imag for f in f2000_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax2000i.plot([f.imag for f in f2000_m], label=f'analytic thin matlab')
    ax2000i.legend()
    ax2000i.set_ylabel('imag(f2000)')
    ax2000i.set_xlabel('BPMs')

    ax0020r.plot([f.real for f in f0020_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax0020r.plot([f.real for f in f0020_m], label=f'analytic thin matlab')
    ax0020r.legend()
    ax0020r.set_ylabel('real(f0020)')
    ax0020r.set_xlabel('BPMs')
    ax0020i.plot([f.imag for f in f0020_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax0020i.plot([f.imag for f in f0020_m], label=f'analytic thin matlab')
    ax0020i.legend()
    ax0020i.set_ylabel('imag(f0020)')
    ax0020i.set_xlabel('BPMs')


    np.savetxt(f'f2000{ring[ind_qua[m]].FamName}_analytic{thick_text}_H.txt', f2000_a, fmt='%10.5f', delimiter=',')
    np.savetxt(f'f0020{ring[ind_qua[m]].FamName}_analytic{thick_text}_V.txt', f0020_a, fmt='%10.5f', delimiter=',')

    plt.show()

    return


if __name__ == '__main__':

    #_test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=False, use_quad_indexes=False)  # not ok
    _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=False, use_quad_indexes=True)  # not ok

    # _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=False)  # not ok
    # _test_f2000_f0020_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=True)  # not ok

    pass