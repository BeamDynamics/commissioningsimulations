"""
formulas from textbooks

"""

import at
import math
import cmath
import numpy as np

__author__='Simone Maria Liuzzo, Andrea Franchi'

def analytic_tune_variation_with_normal_quadrupole(
        ring,
        ind_quads=None,
        verbose=True,
        thick=True,
        opt_all_location=None):
    """
    Compute Tune variation with normal quadrupole errors

    :param ring: AT lattice
    :param ind_quads: numpy array of np.uint32. indexes of skew quadrupoles
    :param verbose: True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :return: 2 1D lists
    QH_over_Kn[QUAD], QV_over_Kn[QUAD]
    """

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    qua = opt_all_location[ind_quads]

    # formulas for response

    dQH_dQuadNorm = np.zeros(shape=(len(qua)))
    dQV_dQuadNorm = np.zeros(shape=(len(qua)))

    def Ib(qm, p, Km, Lm):
        # Km = abs(Km)
        gamma = (1 + qm.alpha[p] ** 2) / qm.beta[p]
        sKL = 2 * cmath.sqrt(Km) * Lm

        val = 1 / 2 * (qm.beta[p] + gamma / Km) + \
              cmath.sin(sKL) / (2 * sKL) * (qm.beta[p] - gamma / Km) + \
              qm.alpha[p] / (2 * Km * Lm) * (cmath.cos(sKL) - 1)

        return val.real

    x = 0
    y = 1

    for countm, m in enumerate(range(len(qua))):
        if verbose:
            print(f'computing tune'
                  f' with an (thick={thick}) normal quadrupole error in {ring[ind_quads[m]].FamName}')

        Km = ring[ind_quads[m]].PolynomB[1]

        if thick and abs(Km)>0:

            Lm = ring[ind_quads[m]].Length

            dQH_dQuadNorm[m] = + Ib(qua[m], x, Km, Lm) / 4 / math.pi
            dQV_dQuadNorm[m] = - Ib(qua[m], y, -Km, Lm) / 4 / math.pi

        else:

            dQH_dQuadNorm[m] = + qua[m].beta[x] / 4 / math.pi
            dQV_dQuadNorm[m] = - qua[m].beta[y] / 4 / math.pi

    return dQH_dQuadNorm, dQV_dQuadNorm


def _test_tune_deriv_quad(m=0, thick=False):

    import commissioningsimulations.config as config
    from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix
    import matplotlib.pyplot as plt
    from os.path import exists
    import pickle
    import time

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    ring.radiation_off()
    ring.radiation_on()  # cavity ON, radiation ON or dispersion will not compute
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadrupole indexes
    #if thick:
    ind_quads = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    #else:
    #    ind_quads = list(at.get_refpts(ring, config.skew_quadrupoles_fam_name_pattern))

    # check BETA at DL entrance vs Average beta DL
    _, _, l0 = ring.linopt6(refpts=ind_quads[m])
    ring.radiation_off()
    lave, bave, _, _, _, _, _ = at.avlinopt(ring, 0, ind_quads[m])
    ring.radiation_on()
    print(l0.beta)
    print(lave.beta)
    print(bave)

    # get reference ORM
    orm0 = './Reference.pkl'
    if not exists(orm0):
        MH0, MV0, _, _, _, _, _, _, _, _, dh0, dv0, Q0 = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=orm0)
    else:
        mat_cont = pickle.load(open(orm0, 'rb'))
        MH0 = mat_cont['MH']
        MV0 = mat_cont['MV']
        dh0 = mat_cont['hor_dispersion']
        dv0 = mat_cont['ver_dispersion']
        Q0 = mat_cont['tunes']

    # vary + one dipole
    dKL = 0.0001
    K = ring[ind_quads[m]].PolynomB[1]
    L = ring[ind_quads[m]].Length
    ring[ind_quads[m]].PolynomB[1] = K + dKL/L

    print(f'DKL = {dKL}, KL = {K*L}, DK/K = {dKL/K/L*100:1.3f} %')

    # get modified ORM
    ormq = f'./PlusQuad{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqp, MVqp, _, _, _, _, _, _, _, _, dhqp, dvqp, Qqp = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqp = mat_cont['MH']
        MVqp = mat_cont['MV']
        dhqp = mat_cont['hor_dispersion']
        dvqp = mat_cont['ver_dispersion']
        Qqp = mat_cont['tunes']

    # vary - one quadrupole
    ring[ind_quads[m]].PolynomB[1] = K - dKL/ring[ind_quads[m]].Length

    # get modified ORM
    ormq = f'./MinusQuad{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqm, MVqm, _, _, _, _, _, _, _, _, dhqm, dvqm, Qqm = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqm = mat_cont['MH']
        MVqm = mat_cont['MV']
        dhqm = mat_cont['hor_dispersion']
        dvqm = mat_cont['ver_dispersion']
        Qqm = mat_cont['tunes']

    # restore quadrupole
    ring[ind_quads[m]].PolynomB[1] = K

    # numeric derivative
    dQH_dQuadNorm_n = (Qqp[0] - Qqm[0]) / 2 / dKL
    dQV_dQuadNorm_n = (Qqp[1] - Qqm[1]) / 2 / dKL

    # formula derivative entrance
    dQH_dQuadNorm_fe = l0[0].beta[0] / 4 / math.pi
    dQV_dQuadNorm_fe = -l0[0].beta[1] / 4 / math.pi

    # formula derivative average
    dQH_dQuadNorm_fa = bave[0][0] / 4 / math.pi
    dQV_dQuadNorm_fa = -bave[0][1] / 4 / math.pi

    # compute analytic equivalent
    print('start analytic dispersion derivative')
    start_time = time.time()
    dQH_dQuadNorm_a, dQV_dQuadNorm_a = analytic_tune_variation_with_normal_quadrupole(
                                                            ring,
                                                            ind_quads=[ind_quads[m]],
                                                            verbose=False,
                                                            thick=thick)
    end_time = time.time()
    print(f'time for analytic dispersion derivative= {end_time - start_time} seconds')

    # modifications to make it equal to the numeric
    print(f'Quad: {ring[ind_quads[m]].FamName} DK/K = {dKL/K/L*100:1.3f} %')

    print(f'QH num: {dQH_dQuadNorm_n:1.4f} beta/4pi: {dQH_dQuadNorm_fe:1.4f} '
          f'diff (num-ana): {(dQH_dQuadNorm_n - dQH_dQuadNorm_fe)/dQH_dQuadNorm_n*100:1.4f} %')
    print(f'QV num: {dQV_dQuadNorm_n:1.4f} beta/4pi: {dQV_dQuadNorm_fe:1.4f} '
          f'diff (num-ana): {(dQV_dQuadNorm_n - dQV_dQuadNorm_fe)/dQV_dQuadNorm_n*100:1.4f} %')

    print(f'QH num: {dQH_dQuadNorm_n:1.4f} <beta>/4pi: {dQH_dQuadNorm_fa:1.4f} '
          f'diff (num-ana): {(dQH_dQuadNorm_n - dQH_dQuadNorm_fa)/dQH_dQuadNorm_n*100:1.4f} %')
    print(f'QV num: {dQV_dQuadNorm_n:1.4f} <beta>/4pi: {dQV_dQuadNorm_fa:1.4f} '
          f'diff (num-ana): {(dQV_dQuadNorm_n - dQV_dQuadNorm_fa)/dQV_dQuadNorm_n*100:1.4f} %')

    print(f'QH num: {dQH_dQuadNorm_n:1.4f} ana: {dQH_dQuadNorm_a[0]:1.4f} '
          f'diff (num-ana): {(dQH_dQuadNorm_n - dQH_dQuadNorm_a[0])/dQH_dQuadNorm_n*100:1.4f} %')
    print(f'QV num: {dQV_dQuadNorm_n:1.4f} ana: {dQV_dQuadNorm_a[0]:1.4f} '
          f'diff (num-ana): {(dQV_dQuadNorm_n - dQV_dQuadNorm_a[0])/dQV_dQuadNorm_n*100:1.4f} %')


    # np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_analytic_tuneH.txt', dQH_dQuadNorm_a[0], fmt='%10.5f', delimiter=',')
    # np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_numeric_tuneH.txt', dQH_dQuadNorm_n, fmt='%10.5f', delimiter=',')

    # np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_analytic_tuneV.txt', dQV_dQuadNorm_a[0], fmt='%10.5f', delimiter=',')
    # np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_numeric_tuneV.txt', dQV_dQuadNorm_n, fmt='%10.5f', delimiter=',')
    plt.show()

    pass


if __name__ == '__main__':

    _test_tune_deriv_quad(m=0)
    _test_tune_deriv_quad(m=0, thick=True)

    pass