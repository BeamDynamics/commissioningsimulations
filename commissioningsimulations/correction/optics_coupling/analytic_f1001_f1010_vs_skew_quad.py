"""
formulas from
A.Franchi (ESRF), Z.Marti (CELLS),
"Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018
"""

import at
import math
import cmath
import numpy as np
import multiprocessing
from itertools import repeat

__author__='Simone Maria Liuzzo, Andrea Franchi'

def analytic_f1001_f1010(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True,
        thick_quadrupole=True,
        opt_all_location=None,
        use_mp=False):
    """
    Computes the derivative of the orbit response matrix compared to normal quadrupoles

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_quads: numpy array of np.uint32. indexes of normal quadrupoles
    :param verbose: True/False
    :param thick_quadrupole:  True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :return: two 2D list of complex.
    resp_f1001[BPM][QUAD], resp_f1010[BPM][QUAD]
    """

    resp_f1001 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))
    resp_f1010 = np.zeros(shape=(len(ind_bpms), len(ind_quads)))

    # loop quadrupoles
    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('RM derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap( _analytic_f1001_f1010,
                                 zip(repeat(ring),
                                     repeat(ind_bpms),
                                     ind_quads,    # loop index
                                     repeat(thick_quadrupole),
                                     repeat(verbose),
                                     repeat(opt_all_location)
                                     )
                                 )

        for m, _ in enumerate(ind_quads):
            _resp_f1001 = results[m][0]
            _resp_f1010 = results[m][1]
            resp_f1001[:, m] = _resp_f1001[:, 0]
            resp_f1010[:, m] = _resp_f1010[:, 0]

    else:  # sequential

        resp_f1001, resp_f1010 = _analytic_f1001_f1010(
                        ring,
                        ind_bpms=ind_bpms,
                        ind_quads=ind_quads,
                        verbose=verbose,
                        thick_quadrupole=thick_quadrupole,
                        opt_all_location=opt_all_location)

    return resp_f1001, resp_f1010


def _analytic_f1001_f1010(
        ring,
        ind_bpms=None,
        ind_quads=None,
        thick_quadrupole=False,
        verbose=True,
        opt_all_location=None):
    """
    analytic f1001 and f1010 RDTs derivative with integrated (KL) errors at normal quadrupoles

    :param ring:
    :param ind_bpms:
    :param ind_quads:
    :param thick_quadrupole:
    :param verbose:
    :param opt_all_location:
    :return:
    """

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    # print(type(ind_quads))
    if isinstance(ind_quads, np.uint32):
        ind_quads = [ind_quads]

    bpm = opt_all_location[ind_bpms]
    qua = opt_all_location[ind_quads]

    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # functions required by later formulas

    def tau(pl, a, b):
        return dphi(pl, a, b) - math.pi*Q[pl]

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2*math.pi*Q[pl]

        return d

    # formulas for response

    resp_f1001 = np.zeros(shape=(len(bpm), len(qua))) + complex('j')
    resp_f1010 = np.zeros(shape=(len(bpm), len(qua))) + complex('j')

    x = 0
    y = 1
    _sign = [+1, -1]
    h = 0
    v = 1


    def _GCm(qu, p, Km0, Lm):
        LsK = Lm * cmath.sqrt(Km0)
        val = +qu.beta[p] ** 0.5 / LsK *cmath.sin(LsK) - qu.alpha[p] * _GSm(qu, p, Km0, Lm)
        return val.real

    def _GSm(qu, p, Km0, Lm):
        LsK = Lm * cmath.sqrt(Km0)
        val = 1 / (Lm * Km0 * qu.beta[p] ** 0.5) * (1 - cmath.cos(LsK))
        return val.real

    for countm, m in enumerate(range(len(qua))):

        if thick_quadrupole:
            Lm = ring[ind_quads[m]].Length
            # if it is a quadrupole
            if isinstance(ring[ind_quads[m]], at.Quadrupole):
                Km = ring[ind_quads[m]].PolynomB[1]

                # _sign removed compared to analytic_orm_with_normal_quad_errors.
                GSm = [_GSm(qua[m], p, Km, Lm) for p in [x, y]]
                GCm = [_GCm(qua[m], p, Km, Lm) for p in [x, y]]

            else:
                beta = qua[m].beta
                alpha = qua[m].alpha
                GSm = [Lm / 2 / beta[p] ** 0.5 for p in [x, y]]
                GCm = [beta[p] ** 0.5 - Lm * alpha[p] / 2 / beta[p] ** 0.5 for p in [x, y]]

        else:
            # thin quadrupole
            # s is unused but gives same shape for all cases
            GSm = [0.0 for p in [x, y]]
            GCm = [qua[m].beta[p] ** 0.5 for p in [x, y]]

        for countj, j in enumerate(range(len(bpm))):

            if verbose:
                print(f'computing f1001 and f1010 RDTs response at'
                      f' BPM {ring[ind_bpms[j]].FamName}'
                      f' with a normal (thick={thick_quadrupole}) quadrupole error in {ring[ind_quads[m]].FamName}')

            pmj = [dphi(p, qua[m], bpm[j]) for p in [x, y]]

            MS = [cmath.sin(pmj[p])*GCm[p] - cmath.cos(pmj[p])*GSm[p] for p in [x, y]]
            MC = [cmath.cos(pmj[p])*GCm[p] + cmath.sin(pmj[p])*GSm[p] for p in [x, y]]

            Rmj = [MC[x] * MC[y] - _sign[s] * MS[x] * MS[y] + 1j * (MS[x] * MC[y] + _sign[s] * MC[x] * MS[y]) for s in [h, v]]

            #  equation C71 C72
            resp_f1001[j][m] = + 1 / (4 * (1 - cmath.exp(2 * math.pi * 1j * (Q[x] - Q[y])))) * Rmj[v]

            resp_f1010[j][m] = + 1 / (4 * (1 - cmath.exp(2 * math.pi * 1j * (Q[x] + Q[y])))) * Rmj[h]

    return resp_f1001, resp_f1010


def _test_f1001_f1010_quad_deriv(
        m=0,  # quadrupole index
        thick_quadrupole=True,
        use_quad_indexes = True,
        use_mp=False
        ):

    import commissioningsimulations.config as config
    import matplotlib.pyplot as plt
    import time
    from commissioningsimulations.correction.optics_coupling.RDT import skew_quad_rdts_response

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')


    ring.radiation_off()
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadruople indexes
    if use_quad_indexes:
        ind_qua = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    else:
        ind_qua = list(at.get_refpts(ring, config.octupoles_fam_name_pattern))  # test thick non-quadrupoles

    # get reference ORM

    # compute analytic equivalent
    if thick_quadrupole:
        thick_text = 'thick_quad'
    else:
        thick_text = 'thin_quad'

    print(f'start {thick_text} analytic f1001 f1010 derivative')
    start_time = time.time()
    resp_f1001, resp_f1010 = analytic_f1001_f1010(ring,
                                      ind_bpms=ind_bpms,
                                      ind_quads=[ind_qua[m]],
                                      thick_quadrupole=thick_quadrupole,
                                      verbose=False,
                                      use_mp=use_mp)
    end_time = time.time()
    tottime=end_time - start_time
    print(f'time for analytic f1001 f1010 derivative= {tottime} seconds')

    print(f'start thin analytic f1001 f1010 derivative (matlab translation)')
    start_time = time.time()

    resp_f1001_mat, resp_f1010_mat, _ = skew_quad_rdts_response(ring, np.array(ind_bpms), np.array([ind_qua[m]]))
    end_time = time.time()
    tottime = end_time - start_time
    print(f'time for analytic f1001 f1010 derivative= {tottime} seconds (matlab translated)')
    print(f'end thin analytic f1001 f1010 derivative (matlab translation)')

    # MODIFY ONE quadrupole
    dKL = 0.0001

    f1001_a = resp_f1001 * dKL
    f1010_a = resp_f1010 * dKL

    f1001_m = resp_f1001_mat * dKL
    f1010_m = resp_f1010_mat * dKL

    # plot analytic vs numeric

    fig, ((ax2000r, ax2000i), (ax0020r, ax0020i)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))
    ax2000r.plot([f.real for f in f1001_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax2000r.plot([f.real for f in f1001_m], label=f'analytic thin matlab')
    ax2000r.legend()
    ax2000r.set_ylabel('real(f1001)')
    ax2000r.set_xlabel('BPMs')
    ax2000i.plot([f.imag for f in f1001_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax2000i.plot([f.imag for f in f1001_m], label=f'analytic thin matlab')
    ax2000i.legend()
    ax2000i.set_ylabel('imag(f1001)')
    ax2000i.set_xlabel('BPMs')

    ax0020r.plot([f.real for f in f1010_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax0020r.plot([f.real for f in f1010_m], label=f'analytic thin matlab')
    ax0020r.legend()
    ax0020r.set_ylabel('real(f1010)')
    ax0020r.set_xlabel('BPMs')
    ax0020i.plot([f.imag for f in f1010_a], label=f'analytic {thick_text} {ring[ind_qua[m]].FamName}')
    ax0020i.plot([f.imag for f in f1010_m], label=f'analytic thin matlab')
    ax0020i.legend()
    ax0020i.set_ylabel('imag(f1010)')
    ax0020i.set_xlabel('BPMs')


    np.savetxt(f'f1001{ring[ind_qua[m]].FamName}_analytic{thick_text}_H.txt', f1001_a, fmt='%10.5f', delimiter=',')
    np.savetxt(f'f1010{ring[ind_qua[m]].FamName}_analytic{thick_text}_V.txt', f1010_a, fmt='%10.5f', delimiter=',')

    plt.show()

    return


if __name__ == '__main__':

    # _test_f1001_f1010_quad_deriv(m=1, thick_quadrupole=False, use_quad_indexes=False)  # not ok
    _test_f1001_f1010_quad_deriv(m=1, thick_quadrupole=False, use_quad_indexes=True)  # not ok

    # _test_f1001_f1010_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=False)  # not ok
    # _test_f1001_f1010_quad_deriv(m=1, thick_quadrupole=True, use_quad_indexes=True)  # not ok

    pass