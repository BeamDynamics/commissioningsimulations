"""
formulas from
A.Franchi (ESRF), Z.Marti (CELLS),
"Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018
"""

import at
import math
import cmath
import numpy as np
import multiprocessing
from itertools import repeat

__author__='Simone Maria Liuzzo, Andrea Franchi'

def analytic_orm_variation_with_normal_quadrupole(
        ring,
        ind_bpms=None,
        ind_cors=None,
        ind_quads=None,
        verbose=True,
        thick_quadrupole=True,
        thick_steerers=True,
        opt_all_location=None,
        use_mp=False):
    """
    Computes the derivative of the orbit response matrix compared to normal quadrupoles

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_cors: numpy array of np.uint32. indexes of correctors
    :param ind_quads: numpy array of np.uint32. indexes of normal quadrupoles
    :param verbose: True/False
    :param thick_quadrupole:  True/False
    :param thick_steerers:  True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :return: two 3D lists of floats.
    ORM_HorSteererToHorBPM_over_Kn[BPMS][CORRECTORS][QUAD], ORM_VerSteererToVerBPM_over_Kn[BPMS][CORRECTORS][QUAD],
    """

    MH = np.zeros(shape=(len(ind_bpms), len(ind_cors), len(ind_quads)))
    MV = np.zeros(shape=(len(ind_bpms), len(ind_cors), len(ind_quads)))

    # loop quadrupoles
    if use_mp:

        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('RM derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap( _analytic_orm_variation_with_normal_quadrupole,
                                 zip(repeat(ring),
                                     repeat(ind_bpms),
                                     repeat(ind_cors),
                                     ind_quads,    # loop index
                                     repeat(thick_quadrupole),
                                     repeat(thick_steerers),
                                     repeat(verbose),
                                     repeat(opt_all_location)
                                     )
                                 )

        for m, _ in enumerate(ind_quads):
            _MH = results[m][0]
            _MV = results[m][1]
            MH[:, :, m] = _MH[:,:,0]
            MV[:, :, m] = _MV[:,:,0]

    else:  # sequential

        MH, MV = _analytic_orm_variation_with_normal_quadrupole(
                        ring,
                        ind_bpms=ind_bpms,
                        ind_cors=ind_cors,
                        ind_quads=ind_quads,
                        verbose=verbose,
                        thick_quadrupole=thick_quadrupole,
                        thick_steerers=thick_steerers,
                        opt_all_location=opt_all_location)

    return MH, MV


def _analytic_orm_variation_with_normal_quadrupole(
        ring,
        ind_bpms=None,
        ind_cors=None,
        ind_quads=None,
        thick_quadrupole=False,
        thick_steerers=False,
        verbose=True,
        opt_all_location=None):
    """
    analytic orbit response matrix derivative with integrated (KL) errors at normal quadrupoles

    :param ring:
    :param ind_bpms:
    :param ind_cors:
    :param verbose:
    :param filename_cod_response:
    :return:
    """

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    # print(type(ind_quads))
    if isinstance(ind_quads, np.uint32):
        ind_quads = [ind_quads]

    bpm = opt_all_location[ind_bpms]
    cor = opt_all_location[ind_cors]
    qua = opt_all_location[ind_quads]


    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # functions required by later formulas

    def PI(a, b):
        val = 1
        if a.s_pos >= b.s_pos:
            val = 0
        return val

    def tau(pl, a, b):
        return dphi(pl, a, b) - math.pi*Q[pl]

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2*math.pi*Q[pl]

        return d

    # formulas for response

    MH = np.zeros(shape=(len(bpm), len(cor), len(qua)))
    MV = np.zeros(shape=(len(bpm), len(cor), len(qua)))

    x = 0
    y = 1
    _sign = [+1, -1]
    h = 0
    v = 1

    def Ib(qm, p, Km, Lm):
        # Km = abs(Km)
        gamma = (1 + qm.alpha[p] ** 2) / qm.beta[p]
        sKL = 2 * cmath.sqrt(Km) * Lm

        val = 1 / 2 * (qm.beta[p] + gamma / Km) + \
              cmath.sin(sKL) / (2 * sKL) * (qm.beta[p] - gamma / Km) + \
              qm.alpha[p] / (2 * Km * Lm) * (cmath.cos(sKL) - 1)

        return val.real

    def IS(qm, p, Km, Lm):
        #Km = abs(Km)
        sK = 2 * cmath.sqrt(Km)
        val = 1 / (Km * Lm) * (
                1 / 2 * (1 - cmath.cos( sK * Lm)) +
                qm.alpha[p] / qm.beta[p] * (cmath.sin(sK * Lm) / sK - Lm)
              )
        return val.real

    def IC(qm, p, Km, Lm):
        # Km = abs(Km)
        sKL = 2 * cmath.sqrt(Km) * Lm
        val = Ib(qm, p, Km, Lm) - 1 / (Km * qm.beta[p]) * (1 - cmath.sin(sKL) / sKL)
        return val.real


    def Ib_notquad(qm, p, Lm):
        # Km = abs(Km)
        gamma = (1 + qm.alpha[p] ** 2) / qm.beta[p]

        val = qm.beta[p] - qm.alpha[p]*Lm + gamma/3*Lm**2

        return val.real

    def IS_notquad(qm, p, Lm):
        #Km = abs(Km)

        val = Lm - 2/3 * qm.alpha[p] / qm.beta[p] * Lm**2

        return val.real

    def IC_notquad(qm, p, Lm):
        # Km = abs(Km)

        val = Ib_notquad(qm, p, Lm) - 2/3 / qm.beta[p] * Lm**2

        return val.real

    # thick steerers corrections
    TS = []
    TC = []
    for countw, w in enumerate(range(len(cor))):
        Lw = ring[ind_cors[w]].Length
        alpha = cor[w].alpha
        beta = cor[w].beta
        TS.append( [Lw / (2 * beta[p]**0.5) for p in [x, y]] )
        TC.append( [beta[p]**0.5 - Lw * alpha[p] / (2 * beta[p]**0.5) for p in [x, y]] )

    for countm, m in enumerate(range(len(qua))):

        if thick_quadrupole:
            Lm = ring[ind_quads[m]].Length
            # if it is a quadrupole
            if isinstance(ring[ind_quads[m]], at.Quadrupole):
                Km = ring[ind_quads[m]].PolynomB[1]

                Ibm = [[Ib(qua[m], p, _sign[s]*Km, Lm) for p in [x, y]] for s in [h, v]]
                ISm = [[IS(qua[m], p, _sign[s]*Km, Lm) for p in [x, y]] for s in [h, v]]
                ICm = [[IC(qua[m], p, _sign[s]*Km, Lm) for p in [x, y]] for s in [h, v]]

            else:
                # # else, assume drift (only quads change optics)
                Ibm = [[Ib_notquad(qua[m], p, Lm) for p in [x, y]] for s in [h, v]]
                ISm = [[IS_notquad(qua[m], p, Lm) for p in [x, y]] for s in [h, v]]
                ICm = [[IC_notquad(qua[m], p, Lm) for p in [x, y]] for s in [h, v]]

        for countj, j in enumerate(range(len(bpm))):

            tmj = [tau(p, qua[m], bpm[j]) for p in [x, y]]

            for countw, w in enumerate(range(len(cor))):
                if verbose:
                    print(f'computing response of steerer {ring[ind_cors[w]].FamName}'
                          f' to BPM {ring[ind_bpms[j]].FamName}'
                          f' with a normal (thick={thick_quadrupole}) quadrupole error in {ring[ind_quads[m]].FamName}')

                twj = [tau(p, cor[w], bpm[j]) for p in [x, y]]
                tmw = [tau(p, qua[m], cor[w]) for p in [x, y]]
                pwj = [dphi(p, cor[w], bpm[j]) for p in [x, y]]

                if thick_quadrupole:
                    # thick quadrupole
                    ISmj = [[math.sin(2 * tmj[p]) * ICm[p][s] - math.cos(2 * tmj[p]) * ISm[p][s] for p in [x, y]] for s in [h, v]]   # eq. C14
                    ICmj = [[math.cos(2 * tmj[p]) * ICm[p][s] + math.sin(2 * tmj[p]) * ISm[p][s] for p in [x, y]] for s in [h, v]]
                    ISmw = [[math.sin(2 * tmw[p]) * ICm[p][s] - math.cos(2 * tmw[p]) * ISm[p][s] for p in [x, y]] for s in [h, v]] # eq. C14
                    ICmw = [[math.cos(2 * tmw[p]) * ICm[p][s] + math.sin(2 * tmw[p]) * ISm[p][s] for p in [x, y]] for s in [h, v]]

                else:
                    # thin quadrupoles
                    Ibm = [[qua[m].beta[p] for p in [x, y]] for s in [h, v]]
                    ISmj = [[qua[m].beta[p] * math.sin(2 * tmj[p]) for p in [x, y]] for s in [h, v]]  # eq. C14
                    ICmj = [[qua[m].beta[p] * math.cos(2 * tmj[p]) for p in [x, y]] for s in [h, v]]
                    ISmw = [[qua[m].beta[p] * math.sin(2 * tmw[p]) for p in [x, y]] for s in [h, v]]  # eq. C14
                    ICmw = [[qua[m].beta[p] * math.cos(2 * tmw[p]) for p in [x, y]] for s in [h, v]]

                if thick_steerers:
                    # thick steerers
                    JSwj = [math.sin(twj[p]) * TC[w][p] - math.cos(twj[p]) * TS[w][p] for p in [x, y]]  # eq. C40
                    JCwj = [math.cos(twj[p]) * TC[w][p] + math.sin(twj[p]) * TS[w][p] for p in [x, y]]
                    JCdwj = [math.cos(pwj[p]) * TC[w][p] + math.sin(pwj[p]) * TS[w][p] for p in [x, y]]
                else:
                    # thin steerers
                    JSwj = [cor[w].beta[p] ** 0.5 * math.sin(twj[p]) for p in [x, y]]
                    JCwj = [cor[w].beta[p] ** 0.5 * math.cos(twj[p]) for p in [x, y]]
                    JCdwj =[cor[w].beta[p] ** 0.5 * math.cos(pwj[p]) for p in [x, y]]

                if thick_quadrupole and thick_steerers:
                    # thick quadrupoles and steerers
                    PCmwj = [[ICmw[p][s] * (math.cos(twj[p])*TC[w][p] + math.sin(twj[p])*TS[w][p] )
                             - 2 * ISmw[p][s]*TS[w][p] * math.cos(twj[p]) for p in [x, y]] for s in [h, v]]

                    PSmwj = [[ISmw[p][s] * (math.sin(twj[p]) * TC[w][p] - math.cos(twj[p]) * TS[w][p])
                             + 2 * ICmw[p][s] * TS[w][p] * math.sin(twj[p]) for p in [x, y]] for s in [h, v]]

                else:
                    #elif (thick_quadrupole and not(thick_steerers)) or (not(thick_quadrupole) and thick_steerers):
                    # thin quads thick steerers or thin quad thick steerers
                    PCmwj = [[JCwj[p] * ICmw[p][s] for p in [x, y]] for s in [h, v]]
                    PSmwj = [[JSwj[p] * ISmw[p][s] for p in [x, y]] for s in [h, v]]

                #else:
                #    # thin quad and thin steerers
                #    PCmwj = [cor[w].beta[p] ** 0.5 * math.cos(twj[p]) * qua[m].beta[p] * math.cos(2*tmw[p]) for p in [x, y]]
                #
                #    PSmwj = [cor[w].beta[p] ** 0.5 * math.sin(twj[p]) * qua[m].beta[p] * math.sin(2*tmw[p]) for p in [x, y]]

                #  V   sign swap intentional to recover numeric ORM. AT sign conventions
                MH[j][w][m] = + ((bpm[j].beta[x]) ** 0.5) \
                              / (2 * math.sin(math.pi * Q[x])) * \
                              (
                              +
                              1 / (4 * math.sin(2 * math.pi * Q[x])) *
                              (JCwj[x]*ICmj[x][h] + PCmwj[x][h] + JSwj[x]*ISmj[x][h] - PSmwj[x][h])
                              +
                              0.5 * Ibm[x][h] * JSwj[x] *
                              (PI(qua[m], bpm[j]) - PI(qua[m], cor[w]) + PI(bpm[j], cor[w]))
                              +
                              Ibm[x][h] * JCdwj[x] /
                              (4 * math.sin(math.pi * Q[x]))
                              )



                MV[j][w][m] = + ((bpm[j].beta[y]) ** 0.5) \
                              / (2 * math.sin(math.pi * Q[y])) * \
                              (
                              +
                              1 / (4 * math.sin(2 * math.pi * Q[y])) *
                              (JCwj[y]*ICmj[y][v] + PCmwj[y][v] + JSwj[y]*ISmj[y][v] - PSmwj[y][v])
                              +
                              0.5 * Ibm[y][v] * JSwj[y] * # * cor[w].beta[y]**0.5 * math.sin(twj[y]) *
                              (PI(qua[m], bpm[j]) - PI(qua[m], cor[w]) + PI(bpm[j], cor[w]))
                              +
                              Ibm[y][v] * JCdwj[y] /  # * cor[w].beta[y]**0.5 * math.cos(dphi(y, cor[w], bpm[j])) /
                              (4 * math.sin(math.pi * Q[y]))
                              )
                """
                # if anything thick (actually works also for thin-thin and it is faster!)
                if thick_quadrupole or thick_steerers:
                else: # all thin
                               #  V   sign swap intentional to recover numeric ORM. AT sign conventions
                    MH[j][w][m] = + ((bpm[j].beta[x] * cor[w].beta[x]) ** 0.5) * qua[m].beta[x] \
                                   / (2 * math.sin(math.pi * Q[x])) * \
                                   (
                                   +
                                   math.cos(twj[x]) /
                                   (4 * math.sin(2 * math.pi * Q[x])) * (math.cos(2 * tmj[x]) + math.cos(2 * tmw[x]))
                                   +
                                   math.sin(twj[x]) /
                                   (4 * math.sin(2 * math.pi * Q[x])) * (math.sin(2 * tmj[x]) - math.sin(2 * tmw[x]))
                                   +
                                   0.5 * math.sin(twj[x]) *
                                   (PI(qua[m], bpm[j]) - PI(qua[m], cor[w]) + PI(bpm[j], cor[w]))
                                   +
                                   math.cos(dphi(x, cor[w], bpm[j])) / (4 * math.sin(math.pi * Q[x]))
                                   )

                    MV[j][w][m] = + ((bpm[j].beta[y] * cor[w].beta[y]) ** 0.5) * qua[m].beta[y] \
                                   / (2 * math.sin(math.pi * Q[y])) * \
                                   (
                                   +
                                   math.cos(twj[y]) /
                                   (4 * math.sin(2 * math.pi * Q[y])) * (math.cos(2 * tmj[y]) + math.cos(2 * tmw[y]))
                                   +
                                   math.sin(twj[y]) /
                                   (4 * math.sin(2 * math.pi * Q[y])) * (math.sin(2 * tmj[y]) - math.sin(2 * tmw[y]))
                                   +
                                   0.5 * math.sin(twj[y]) *
                                   (PI(qua[m], bpm[j]) - PI(qua[m], cor[w]) + PI(bpm[j], cor[w]))
                                   +
                                   math.cos(dphi(y, cor[w], bpm[j])) / (4 * math.sin(math.pi * Q[y]))
                                   )
                """

                """
                if np.isnan(MH[j,w,m]):
                    print(f'MH[{j},{w},{m}] is NaN for '
                          f'Quad {m} {ring[ind_quads[m]].FamName} '
                          f'Cor {w} {ring[ind_cors[w]].FamName} ')
                """

    return MH, MV


def _test_orm_quad_deriv(
        m=0, # quadrupole index
        col=[2], # column and row to plot for comparison
        row=[7],
        thick_quadrupole=True,
        thick_steerers=False,
        use_quad_indexes = True,
        use_mp=False
        ):

    import commissioningsimulations.config as config
    from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix
    import matplotlib.pyplot as plt
    from os.path import exists
    import pickle
    import time


    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')


    ring.radiation_off()
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get correctors indexes
    # ind_cor = list(at.get_refpts(ring, config.correctors_fam_name_pattern))
    ind_cor = list(at.get_refpts(ring, config.correctors_for_optics_RM))

    # get quadruople indexes
    if use_quad_indexes:
        ind_qua = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))
    else:
        ind_qua = list(at.get_refpts(ring, config.octupoles_fam_name_pattern))  # test thick non-quadrupoles

    """
    # get reference ORM
    orm0 = './Reference.pkl'
    if not exists(orm0):
        MH0, MV0, _, _, _, _, _, _, _, _, dh0, dv0, Q0 = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=ind_cor,
                                          filename_cod_response=orm0,
                                          use_mp=use_mp)
    else:
        mat_cont = pickle.load(open(orm0, 'rb'))
        MH0 = mat_cont['MH']
        MV0 = mat_cont['MV']
        dh0 = mat_cont['hor_dispersion']
        dv0 = mat_cont['ver_dispersion']
        Q0 = mat_cont['tunes']

    """

    # vary + one quadrupole
    dKL = 0.00001
    K = ring[ind_qua[m]].PolynomB[1]
    ring[ind_qua[m]].PolynomB[1] = K + dKL / ring[ind_qua[m]].Length

    # get modified ORM
    ormq = f'./PlusQuad{ring[ind_qua[m]].FamName}.pkl'
    if not exists(ormq):
        MHqp, MVqp, _, _, _, _, _, _, _, _, dhqp, dvqp, Qqp = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=ind_cor,
                                          filename_cod_response=ormq,
                                          use_mp=use_mp)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqp = mat_cont['MH']
        MVqp = mat_cont['MV']
        dhqp = mat_cont['hor_dispersion']
        dvqp = mat_cont['ver_dispersion']
        Qqp = mat_cont['tunes']

    # vary - one quadrupole
    ring[ind_qua[m]].PolynomB[1] = K - dKL / ring[ind_qua[m]].Length

    # get modified ORM
    ormq = f'./MinusQuad{ring[ind_qua[m]].FamName}.pkl'
    if not exists(ormq):
        MHqm, MVqm, _, _, _, _, _, _, _, _, dhqm, dvqm, Qqm = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=ind_cor,
                                          filename_cod_response=ormq,
                                          use_mp=use_mp)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqm = mat_cont['MH']
        MVqm = mat_cont['MV']
        dhqm = mat_cont['hor_dispersion']
        dvqm = mat_cont['ver_dispersion']
        Qqm = mat_cont['tunes']

    # restore quadrupole
    ring[ind_qua[m]].PolynomB[1] = K

    # numeric derivative
    dMHn = (MHqp - MHqm) / 2 / dKL
    dMVn = (MVqp - MVqm) / 2 / dKL

    # compute analytic equivalent
    if thick_quadrupole:
        thick_text = 'thick_quad'
    else:
        thick_text = 'thin_quad'

    if thick_steerers:
        thick_text = thick_text + '_thick_steerers'
    else:
        thick_text = thick_text + '_thin_steerers'

    print(f'start {thick_text} analytic orm derivative')
    start_time = time.time()
    dMHa, dMVa = analytic_orm_variation_with_normal_quadrupole(ring,
                                                             ind_bpms=ind_bpms,
                                                             ind_cors=ind_cor,
                                                             ind_quads=[ind_qua[m]],
                                                             thick_quadrupole=thick_quadrupole,
                                                             thick_steerers=thick_steerers,
                                                             verbose=False, use_mp=False)
    end_time = time.time()
    tottime=end_time - start_time
    print(f'time for analytic ORM derivative= {tottime} seconds')

    # modifications to make it equal to the numeric

    # dMHa[:, :, 0] = - dMHa[:, :, 0]  implemented in analytic_orm_variation_with_normal_quadrupole

    # plot std difference by column / row
    h_col_dif = []
    for cc in range(len(ind_cor)):
        h_col_dif.append(np.std((dMHn[:, cc] - dMHa[:, cc, 0])/dMHn[:, cc]))

    h_row_dif = []
    for rr in range(len(ind_bpms)):
        h_row_dif.append(np.std((dMHn[rr, :] - dMHa[rr, :, 0])/dMHn[rr, :]))

    v_col_dif = []
    for cc in range(len(ind_cor)):
        v_col_dif.append(np.std((dMVn[:, cc] - dMVa[:, cc, 0])/dMVn[:, cc]))

    v_row_dif = []
    for rr in range(len(ind_bpms)):
        v_row_dif.append(np.std((dMVn[rr, :] - dMVa[rr, :, 0])/dMVn[rr, :]))


    cornames= [el.FamName for el in ring[[ii for ii in ind_cor]]]

    fig, ((axch, axcv), (axrh, axrv)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))

    fig.subplots_adjust(hspace=0.5)

    axch.bar(range(len(h_col_dif)), h_col_dif, label='hor columns')
    axch.set_xlabel('correctors')
    axch.set_ylabel('hor. std(num-ana)/num')
    axch.set_xticks(range(len(h_col_dif)), labels=cornames)
    plt.setp(axch.get_xticklabels(), rotation=90, ha='right', rotation_mode='anchor')

    axcv.bar(range(len(v_col_dif)), v_col_dif, label='ver columns')
    axcv.set_xlabel('correctors')
    axcv.set_ylabel('ver. std(num-ana)/num')
    axcv.set_xticks(range(len(h_col_dif)), labels=cornames)
    plt.setp(axcv.get_xticklabels(), rotation=90, ha='right', rotation_mode='anchor')

    axrh.bar(range(len(h_row_dif)), h_row_dif, label='hor rows')
    axrh.set_xlabel('BPMs')
    axrh.set_ylabel('hor. std(num-ana)/num')

    axrv.bar(range(len(v_row_dif)), v_row_dif, label='ver rows')
    axrv.set_xlabel('BPMs')
    axrv.set_ylabel('ver. std(num-ana)/num')

    np.savetxt(f'COR_diff_H.txt', h_col_dif, fmt='%10.5f', delimiter=',')
    np.savetxt(f'COR_diff_V.txt', v_col_dif, fmt='%10.5f', delimiter=',')
    np.savetxt(f'BPM_diff_H.txt', h_row_dif, fmt='%10.5f', delimiter=',')
    np.savetxt(f'BPM_diff_V.txt', v_row_dif, fmt='%10.5f', delimiter=',')


    # plot analytic vs numeric
    for cc, rr in zip(col, row):
        fig, ((axch, axcv), (axrh, axrv)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))
        axch.plot(dMHn[:, cc], 'x', label=f'numeric, col={cc}')
        axch.plot(dMHa[:, cc, 0], label=f'analytic {thick_text}, col={cc}')
        axch.plot(dMHn[:, cc] - dMHa[:, cc, 0], ':', label=f'num - ana, col={cc}, cor {ring[ind_cor[cc]].FamName}')
        axch.legend()
        axch.set_ylabel('hor. [m/rad/k1]')
        axch.set_xlabel('BPMs')

        axch.set_title(f'Quadrupole {ring[ind_qua[m]].FamName} modified by {dKL}')
        axcv.plot(dMVn[:, cc], 'x', label=f'numeric, col={cc}')
        axcv.plot(dMVa[:, cc, 0], label=f'analytic {thick_text}, col={cc}')
        axcv.plot(dMVn[:, cc] - dMVa[:, cc, 0], ':', label=f'num - ana, col={cc}, cor {ring[ind_cor[cc]].FamName}')
        axcv.legend()
        axcv.set_ylabel('ver. [m/rad/k1]')
        axcv.set_xlabel('BPMs')

        axrh.plot(dMHn[rr, :], 'x', label=f'numeric, row={rr}')
        axrh.plot(dMHa[rr, :, 0], label=f'analytic {thick_text}, row={rr}')
        axrh.plot(dMHn[rr, :] - dMHa[rr, :, 0], ':', label=f'num - ana, row={rr}, bpm {ring[ind_bpms[rr]].FamName}')
        axrh.legend()
        axrh.set_ylabel('hor. [m/rad/k1]')
        axrh.set_xlabel('correctors')

        axrv.plot(dMVn[rr, :], 'x', label=f'numeric, row={rr}')
        axrv.plot(dMVa[rr, :, 0], label=f'analytic {thick_text}, row={rr}')
        axrv.plot(dMVn[rr, :] - dMVa[rr, :, 0], ':', label=f'num - ana, row={rr}, bpm {ring[ind_bpms[rr]].FamName}')
        axrv.legend()
        axrv.set_ylabel('ver. [m/rad/k1]')
        axrv.set_xlabel('correctors')

        # np.savetxt(f'Data{ring[ind_qua[m]].FamName}_analytic{thick_text}_H_col{cc}_raw{rr}.txt', dMHa[:, :, 0], fmt='%10.5f',
        #            delimiter=',')

    np.savetxt(f'Quad{ring[ind_qua[m]].FamName}_analytic{thick_text}_H.txt', dMHa[:, :, 0], fmt='%10.5f', delimiter=',')
    np.savetxt(f'Quad{ring[ind_qua[m]].FamName}_numeric_H.txt', dMHn, fmt='%10.5f', delimiter=',')
    np.savetxt(f'Quad{ring[ind_qua[m]].FamName}_analytic{thick_text}_V.txt', dMVa[:, :, 0], fmt='%10.5f', delimiter=',')
    np.savetxt(f'Quad{ring[ind_qua[m]].FamName}_numeric_V.txt', dMVn, fmt='%10.5f', delimiter=',')
    plt.show()

    np.savetxt(f'Quad{ring[ind_qua[m]].FamName}_analytic{thick_text}_H_timestdmax.txt',
               (tottime, np.max(h_col_dif), np.max(h_row_dif), np.max(v_col_dif), np.max(v_row_dif),
           np.std(h_col_dif), np.std(h_row_dif), np.std(v_col_dif), np.std(v_row_dif)), fmt='%10.5f', delimiter=',')

    return tottime, (np.max(h_col_dif), np.max(h_row_dif), np.max(v_col_dif), np.max(v_row_dif)), \
           (np.std(h_col_dif), np.std(h_row_dif), np.std(v_col_dif), np.std(v_row_dif))


if __name__ == '__main__':
    """
    _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=False, thick_steerers=False)  # not ok vertical
    _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=False, thick_steerers=True)   # better than thin 
    _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=True, thick_steerers=False)   # better than thin quad
    _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=True, thick_steerers=True)   # perfect
    """

    """
    # test use_mp
    _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=True, thick_steerers=True, use_mp=True)  # perfect
    """

    timett, maxtt, stdtt = _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=False, thick_steerers=False,
                         use_quad_indexes=False)  # not ok 
    timetT, maxtT, stdtT = _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=False, thick_steerers=True,
                         use_quad_indexes=False)   # better than thin steerers
    timeTt, maxTt, stdTt = _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=True, thick_steerers=False,
                         use_quad_indexes=False)   # better than thin quad
    timeTT, maxTT, stdTT = _test_orm_quad_deriv(m=0, col=[10], row=[23], thick_quadrupole=True, thick_steerers=True,
                         use_quad_indexes=False)   # perfect


    import matplotlib.pyplot as plt

    fig, ((axch, axcv), (axrh, axrv)) = plt.subplots(nrows=2, ncols=2, figsize=(10, 7))

    fig.subplots_adjust(hspace=0.5)

    axch.bar(range(4), (maxtt[0], maxtT[0], maxTt[0], maxTT[0]))
    axch.set_xlabel('correctors')
    axch.set_ylabel('max hor. std(num-ana)')
    axch.set_xticks([0, 1, 2, 3], (f'thin-thin {timett:.1f}s',
                                   f'thin-thick {timetT:.1f}s',
                                   f'thick-thin {timeTt:.1f}s',
                                   f'thick-thick {timeTT:.1f}s'), rotation='vertical')

    axrh.bar(range(4), (maxtt[1], maxtT[1], maxTt[1], maxTT[1]))
    axrh.set_xlabel('BPMs')
    axrh.set_ylabel('max hor. std(num-ana)')
    axrh.set_xticks([0, 1, 2, 3], (f'thin-thin {timett:.1f}s',
                                   f'thin-thick {timetT:.1f}s',
                                   f'thick-thin {timeTt:.1f}s',
                                   f'thick-thick {timeTT:.1f}s'), rotation='vertical')

    axcv.bar(range(4), (maxtt[2], maxtT[2], maxTt[2], maxTT[2]))
    axcv.set_xlabel('correctors')
    axcv.set_ylabel('max ver. std(num-ana)')
    axcv.set_xticks([0, 1, 2, 3], (f'thin-thin {timett:.1f}s',
                                   f'thin-thick {timetT:.1f}s',
                                   f'thick-thin {timeTt:.1f}s',
                                   f'thick-thick {timeTT:.1f}s'), rotation='vertical')

    axrv.bar(range(4), (maxtt[3], maxtT[3], maxTt[3], maxTT[3]))
    axrv.set_xlabel('BPMs')
    axrv.set_ylabel('max ver. std(num-ana)')
    axrv.set_xticks([0, 1, 2, 3], (f'thin-thin {timett:.1f}s',
                                   f'thin-thick {timetT:.1f}s',
                                   f'thick-thin {timeTt:.1f}s',
                                   f'thick-thick {timeTT:.1f}s'), rotation='vertical')

    plt.show()

    pass