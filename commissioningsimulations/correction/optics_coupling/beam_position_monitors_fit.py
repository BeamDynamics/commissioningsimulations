import at
import os
import copy
import time
import math
import socket
import numpy as np
from os.path import exists
from matplotlib import pyplot as plt
import multiprocessing
import pickle
from itertools import repeat
from scipy.io import savemat, loadmat
from scipy.linalg import svdvals
import commissioningsimulations.config as config
from commissioningsimulations.correction.ClosedOrbit import compute_analytic_orbit_response_matrix, \
    compute_orbit_response_matrix
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.errors.find_orbit_bpm_errors import plot_bpm_errors


def get_orm_arrays(ring,
                   ind_bpms,
                   ind_cor,
                   bpms_weigths,
                   hor_disp_weight,
                   ver_disp_weight,
                   mode,
                   verbose):
    """
    compute ORM, dispersion and tunes or loads a measured ones.
    apply weigths to BPMS, dispersion and tunes
    returns 2 1D arrays to be used for optics and coupling correction

    :param ring: AT lattice
    :param ind_bpms: index of BPMS in ring
    :param ind_cor: index of correctors in ring
    :param bpms_weigths: 2D numpy array of size (2 (H,V), len(ind_bpms)) to multiply each column of the response matrices
    :param hor_disp_weight: scalar to multiply horizontal dispersion
    :param ver_disp_weight: scalar to multiply vertical dispersion
    :param mode: 'numeric' (default), 'analytic', 'measured' (not yet functional)
    :param verbose: boolean. if True print out
    :return: ForHorScale (1D np array of length(len(H orm)+len(V orm)+len(disph)+len(tunes))
             ForVerScale (1D np array of length(len(H2V orm)+len(V2H orm)+len(dispv))
             ForRotataion (1D np array of length(len(H2V orm)+len(V2H orm)+len(dispv))
    """

    # compute ORM
    if mode == 'numeric':

        H, V, H2V, V2H, _, _, _, _, _, _, dh, dv, _ = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=ind_cor,
                                          verbose=verbose,
                                          use_bpm_errors=True)

    elif mode == 'analytic':

        H, V, H2V, V2H, _, _, _, _, _, _, dh, dv, _ = \
            compute_analytic_orbit_response_matrix(ring,
                                                   ind_bpms=ind_bpms,
                                                   ind_cor=ind_cor,
                                                   verbose=verbose,
                                                   use_bpm_errors=True)

    elif mode == 'measured':
        raise NameError('measured mode not yet functional')

    else:
        raise NameError(f'{mode} is not an allowed mode. Use: numeric or analytic')

    # add BPM weigths (hill BPMS get a small weigth)
    if bpms_weigths is not None:
        if bpms_weigths.any():  # not None
            if bpms_weigths.shape == (2, len(ind_bpms)):
                # divide each bpm in each column by the corresponding bpm weigth
                H = (H.T * bpms_weigths[0, :]).T
                V = (V.T * bpms_weigths[1, :]).T
                H2V = (H2V.T * bpms_weigths[1, :]).T
                V2H = (V2H.T * bpms_weigths[0, :]).T
                dh = (dh.T * bpms_weigths[0, :]).T
                dv = (dv.T * bpms_weigths[1, :]).T
            else:
                raise ValueError(f'bpm_weigths must be a numpy array of size (2, {len(ind_bpms)})')

    # build arrays for fit. use full ORM, to distinguish scale and rotation errors.
    ForHorScale = np.concatenate((flat(H),  # Hcor 2 Horb
                                  flat(H2V),  # Hcor 2 Vorb
                                  flat(V),
                                 flat(V2H),  # Vcor 2 Horb
                                 hor_disp_weight * flat(dh)))   # hor dispersion

    ForVerScale = np.concatenate((flat(H),  # Hcor 2 Horb
                                  flat(H2V),  # Hcor 2 Vorb
                                  flat(V),
                                 flat(V2H),
                                  ver_disp_weight * flat(dv)))  # ver dispersion

    ForRotations = np.concatenate((flat(H),  # Hcor 2 Horb
                                  flat(H2V),  # Hcor 2 Vorb
                                  flat(V),
                                 flat(V2H),
                                   hor_disp_weight * flat(dh),   # hor dispersion
                                 ver_disp_weight * flat(dv)))  # ver dispersion

    return ForHorScale, ForVerScale, ForRotations


def flat(MM):
    '2D to 1D'
    return np.matrix(MM).flatten().transpose()


def jacobian(ring,
             field,
             index_in_field,
             ring_index_to_modify,
             deltaL,
             ind_bpms,
             ind_cor,
             bpms_weigths,
             hor_disp_weight,
             ver_disp_weight,
             mode,
             verbose=False):
    """
    computes the derivative of the (ORM+dispersion+tune) array produced by get_orm_arrays with respect to a variation of
    ring[ring_index_to_modify].field[index_in_field]

    :param ring: AT lattice
    :param field: element field to modify (for example PolynomB)
    :param index_in_field: index in field to modify ( ex.: if field is PolynomB, 0 for dipole, 1 for quadrupole, etc..)
    :param ring_index_to_modify: index in the ring that has to be modified
    :param deltaL: integrated strength variation to apply to ring[ring_index_to_modify].field[index_in_field]
    :param ind_bpms: indexes of BPMs for Orbit Response matrix (get_orm_arrays)
    :param ind_cor: indexes of correctors for Orbit Response matrix (get_orm_arrays)
    :param bpms_weigths: weights to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weight for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weight for vertical dispersion (get_orm_arrays)
    :param tune_weight: weight for tunes (get_orm_arrays)
    :param mode: 'numeric', 'semi-analytic', 'analytic'(not yet functional)
                'numeric' will compute the ORM with tracking
                'semi-analytic' will vary ring[ring_index_to_modify].field[index_in_field] and compute
                                an analytic ORM based on this lattice
    :param field_is_integrated: defualt False. if True, do not divide by length of magnet
    :param verbose: if True, print out text
    :return: JSH = H+delta/2 - H-delta/2 / delta
             JSV = V+delta/2 - V-delta/2 / delta
             JR = V+delta/2 - V-delta/2 / delta
    """

    orm_mode=mode

    if mode == 'semi-analytic':
        orm_mode='analytic'

    if verbose:
        print(f'modify {ring_index_to_modify}th element in ring {ring[ring_index_to_modify].FamName} {field}[{index_in_field}] by {deltaL}')

    # get initial vale
    initial_value = ring[ring_index_to_modify].__getattribute__(field)

    # print(f'Initial {field} = {initial_value}')

    desired = copy.deepcopy(initial_value).astype(np.float64)

    # modify
    if index_in_field is not None:
        desired[index_in_field] = initial_value[index_in_field] + deltaL / 2
    else:
        desired = initial_value + deltaL / 2

    setattr(ring[ring_index_to_modify], field, desired)

    # print(f'desired ring[{ring_index_to_modify}]{field} = {desired}')
    # print(f'Modified ring[{ring_index_to_modify}]{field} = {ring[ring_index_to_modify].__getattribute__(field)}')

    if verbose:
        print(f'get ORM {len(ind_bpms)}x{len(ind_cor)}, dispersion and tunes')

    # get ORM
    q_p, s_p, r_p = get_orm_arrays(ring,
                              ind_bpms,
                              ind_cor,
                              bpms_weigths,
                              hor_disp_weight,
                              ver_disp_weight,
                              orm_mode,
                              False)

    # modify
    if index_in_field is not None:
        desired[index_in_field] = initial_value[index_in_field] - deltaL / 2
    else:
        desired = initial_value - deltaL / 2

    setattr(ring[ring_index_to_modify], field, desired)

    # print(f'Initial {field} = {initial_value}')
    # print(f'desired minus ring[{ring_index_to_modify}]{field} = {desired}')
    # print(f'Modified minus ring[{ring_index_to_modify}]{field} = {ring[ring_index_to_modify].__getattribute__(field)}')

    # get ORM
    q_m, s_m, r_m = get_orm_arrays(ring,
                              ind_bpms,
                              ind_cor,
                              bpms_weigths,
                              hor_disp_weight,
                              ver_disp_weight,
                              orm_mode,
                              False)

    # restore original value
    setattr(ring[ring_index_to_modify], field, initial_value)

    JSH = (q_p - q_m) / deltaL
    JSV = (s_p - s_m) / deltaL
    JR = (r_p - r_m) / deltaL

    return np.asarray(JSH), np.asarray(JSV), np.asarray(JR)


def analytic_jacobian(ring,
                     indexes=None,
                     mode='HorScale',  # VerScale, Rotation
                     orm_data=None,
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     tune_weight=1.0,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0):
    """
    compute the jacobian based on:

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param indexes: dictionry output of commissioningsimulations.get_indexes.get_lattice_indexes
    :param use_mp: not used
    :param n_processes: not used
    :param bpms_weigths: weigths to apply to each BPM (get_orm_arrays)
    :param hor_disp_weight: weigth for horizontal dispersion (get_orm_arrays)
    :param ver_disp_weight: weigth for vertical dispersion (get_orm_arrays)
    :param tune_weight: weigth for tunes (get_orm_arrays)
    :param verbose: print text
    :return:    variation of [H2H, V2V, EtaH, Q] with quad and
                variation of [H2V, V2H, EtaV] with skew quad
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring, verbose=verbose)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']

    JSH = np.zeros((len(ind_bpms) * len(ind_cors) * 4 + len(ind_bpms), len(ind_bpms)))
    JSV = np.zeros((len(ind_bpms) * len(ind_cors) * 4 + len(ind_bpms), len(ind_bpms)))
    JR = np.zeros((len(ind_bpms) * len(ind_cors) * 4 + len(ind_bpms) * 2, len(ind_bpms)))

    H_ = np.zeros((len(ind_bpms), len(ind_cors), len(ind_bpms)))
    H2V_ = np.zeros((len(ind_bpms), len(ind_cors), len(ind_bpms)))
    V_ = np.zeros((len(ind_bpms), len(ind_cors), len(ind_bpms)))
    V2H_ = np.zeros((len(ind_bpms), len(ind_cors), len(ind_bpms)))
    dh_ = np.zeros((len(ind_bpms), len(ind_bpms)))
    dv_ = np.zeros((len(ind_bpms), len(ind_bpms)))

    if verbose:
        print(f'computing ORM derivative to {mode} errors')

    # reference responses
    if not orm_data:
        Hr, Vr, H2Vr, V2Hr, _, _, _, _, _, _, dhr, dvr, _ = \
            compute_analytic_orbit_response_matrix(ring,
                                                   ind_bpms=ind_bpms,
                                                   ind_cor=ind_cors,
                                                   verbose=verbose,
                                                   use_bpm_errors=True)
    else:
        Hr = orm_data[0]
        Vr = orm_data[1]
        H2Vr = orm_data[2]
        V2Hr = orm_data[3]
        dhr = orm_data[-3]
        dvr = orm_data[-2]

    def setscale(h, i):
        # hp[i] = h[i] * (1 + scale)
        # hm[i] = h[i] * (1 - scale)
        # dh[i] = hp[i] - hm[i] / 2 / scale
        # dh[i] = h[i] * (1 + scale) - h[i] * (1 - scale) / 2 / scale
        # dh[i] = h[i] * scale / scale
        h_ = np.zeros(len(ind_bpms))
        h_[i] = h[i]
        return h_

    def setrot(h, v, i):
        # hp[i] = h[i] * cos(rot) - v[i] * sin(rot)
        # vp[i] = h[i] * sin(rot) + v[i] * cos(rot)

        # hm[i] = h[i] * cos(-rot) - v[i] * sin(-rot)
        # vm[i] = h[i] * sin(-rot) + v[i] * cos(-rot)

        # hm[i] = h[i] * cos(rot) + v[i] * sin(rot)
        # vm[i] = -h[i] * sin(rot) + v[i] * cos(rot)

        # dh[i] = hp[i] - hm[i] / 2 / rot
        # dh[i] = h[i] * cos(rot) - v[i] * sin(rot) - h[i] * cos(rot) - v[i] * sin(rot)  / 2 / rot
        # dh[i] = - v[i] * sin(rot) / rot

        # dv[i] = vp[i] - vm[i] / 2 / rot
        # dv[i] = h[i] * sin(rot) + v[i] * cos(rot) +h[i] * sin(rot) - v[i] * cos(rot) / 2 / rot
        # dv[i] = h[i] * sin(rot) / rot

        rot = 1e-4

        h_ = np.zeros(len(ind_bpms))
        v_ = np.zeros(len(ind_bpms))

        h_[i] = v[i] * math.sin(rot) / rot
        v_[i] = -h[i] * math.sin(rot) / rot

        return h_, v_

    if mode == 'HorScale':

        for b,_ in enumerate(ind_bpms):
            dh_[:, b] = setscale(dhr, b)
            for j, _ in enumerate(ind_cors):
                H_[:,j, b] = setscale(Hr[:, j], b)
                V2H_[:,j, b] = setscale(V2Hr[:, j], b)

    elif mode == 'VerScale':

        for b,_ in enumerate(ind_bpms):
            dv_[:, b] = setscale(dvr, b)
            for j, _ in enumerate(ind_cors):
                H2V_[:, j, b] = setscale(H2Vr[:, j], b)
                V_[:, j, b] = setscale(Vr[:, j], b)

    elif mode == 'Rotation':

        for b,_ in enumerate(ind_bpms):
            dh_[:, b], dv_[:, b] = setrot(dhr, dvr, b)
            for j, _ in enumerate(ind_cors):
                H_[:, j, b], H2V_[:, j, b] = setrot(Hr[:, j], H2Vr[:, j], b)
                V2H_[:, j, b], V_[:, j, b] = setrot(V2Hr[:, j], Vr[:, j], b)

    # apply weigths and flatten to array
    for iq, _ in enumerate(ind_bpms):
        # add BPM weigths (hill BPMS get a small weigth)
        if bpms_weights is not None:
            if bpms_weights.any():  # not None
                if bpms_weights.shape == (2, len(ind_bpms)):
                    # divide each bpm in each column by the corresponding bpm weigth
                    H = (H_[:, :, iq].T * bpms_weights[0, :]).T
                    V = (V_[:, :, iq].T * bpms_weights[1, :]).T
                    dh = (dh_[:, iq].T * bpms_weights[0, :]).T
                    H2V = (H2V_[:, :, iq].T * bpms_weights[1, :]).T
                    V2H = (V2H_[:, :, iq].T * bpms_weights[0, :]).T
                    dv = (dv_[:, iq].T * bpms_weights[1, :]).T
                else:
                    raise ValueError(f'bpm_weights must be a numpy array of size (2, {len(ind_bpms)})')
        else:
            H = H_[:, :, iq]
            V = V_[:, :, iq]
            dh = dh_[:, iq]
            H2V = H2V_[:, :, iq]
            V2H = V2H_[:, :, iq]
            dv = dv_[:, iq]

        # build arrays for fit
        JSH[:, iq] = np.asarray(np.concatenate((
            flat(H),  # Hcor 2 Horb
            flat(H2V),  # Hcor 2 Vorb
            flat(V),
            flat(V2H),  # Vcor 2 Horb
            hor_disp_weight * flat(dh))))[:, 0] # hor dispersion

        JSV[:, iq] = np.asarray(np.concatenate((
            flat(H),  # Hcor 2 Horb
            flat(H2V),  # Hcor 2 Vorb
            flat(V),
            flat(V2H),  # Vcor 2 Horb
            ver_disp_weight * flat(dv))))[:, 0]  # ver dispersion

        JR[:, iq] = np.asarray(np.concatenate((
            flat(H),  # Hcor 2 Horb
            flat(H2V),  # Hcor 2 Vorb
            flat(V),
            flat(V2H),  # Vcor 2 Horb
            hor_disp_weight * flat(dh),
            ver_disp_weight * flat(dv))))[:, 0]  # ver dispersion

    return JSH, JSV, JR


def compute_jacobian(ring,
                     indexes=None,
                     filename_response='./ORMDerivative.pkl',
                     save_matlab_file=False,
                     mode='numeric',  # 'semi-analytic', 'analytic'
                     use_mp=False,
                     n_processes=None,
                     verbose=False,
                     bpms_weights=None,
                     hor_disp_weight=1.0,
                     ver_disp_weight=1.0,
                     Dscale = 1e-2,
                     Drot = 1e-4
                     ):
    """
    computes the derivative of the orm+dispersion vectors produced by get_orm_array with respect to the
    list of BPMS scale and rotation errors

    :param ring: AT lattice
    :param indexes: dictionary of indexes obtained with commissioningsimulations.config.get_lattice_indexes
    :param filename_response: file where to store the computed derivatives (large file). if None, no file is saved)
    :param save_matlab_file: save to .mat format
    :param mode: 'numeric' (default), 'analytic', 'semi-analytic'  (see commissioningsimulations.CorrectOptics.jacobian)
    :param use_mp: use multiprocessing on local host
    :param n_processes: number of cores to use. if None, use all
    :param verbose: if True, print text
    :param bpms_weights: see get_orm_array
    :param hor_disp_weight:  see get_orm_array
    :param ver_disp_weight:  see get_orm_array
    :param Dscale: variation of normal quardupoles
    :param Drot: variation of skew quadrupoles
    :return: JSH derivative of first output of get_orm_array respect to normal quadrupoles,
             JSV derivative of first output of get_orm_array respect to skew quadrupoles,
             JR derivative of first output of get_orm_array respect to skew quadrupoles,
             sSH, sSV, sR are the singular values
    """

    if indexes is None:
        indexes = get_lattice_indexes(ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']

    NB = len(ind_bpms)
    NC = len(ind_cors)

    if verbose:
        print(f'optics/coupling errors fit\n '
              f'orbit response matrix size: ({NB}, {NC}) \n'
              f'{NB} {config.bpms_name_patter} BPMS for fit of scale and rotation errors\n')

    if mode == 'numeric' or mode == 'semi-analytic':

        # initialize RM, just to get sizes
        H0, V0, R0 = get_orm_arrays(ring,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                'analytic',
                                False)

        JSH = np.zeros((len(H0), NB))
        JSV = np.zeros((len(V0), NB))
        JR = np.zeros((len(R0), NB))

        # computation using multiprocessing
        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            if n_processes is None:
                n_processes = n_cpu
            else:
                max_proc = min(n_cpu, NC)
                if n_processes > max_proc:
                    n_processes = max_proc

            print(f'RM derivative parallel computation using {n_processes} cores on {socket.gethostname()}')

            with multiprocessing.Pool(processes=n_processes) as p:

                results_HS = p.starmap(jacobian,
                                        zip(repeat(ring),
                                            repeat('Scale'),
                                            repeat(0),
                                            ind_bpms,  # loop variable
                                            repeat(Dscale),
                                            repeat(ind_bpms),
                                            repeat(ind_cors),
                                            repeat(bpms_weights),
                                            repeat(hor_disp_weight),
                                            repeat(ver_disp_weight),
                                            repeat(mode), repeat(verbose)))

            with multiprocessing.Pool(processes=n_processes) as p:

                results_VS = p.starmap(jacobian,
                                        zip(repeat(ring),
                                            repeat('Scale'),
                                            repeat(1),
                                            ind_bpms,  # loop variable
                                            repeat(Dscale),
                                            repeat(ind_bpms),
                                            repeat(ind_cors),
                                            repeat(bpms_weights),
                                            repeat(hor_disp_weight),
                                            repeat(ver_disp_weight),
                                            repeat(mode), repeat(verbose)))

            with multiprocessing.Pool(processes=n_processes) as p:
                results_rot = p.starmap(jacobian,
                                         zip(repeat(ring),
                                             repeat('Rotation'),
                                             repeat(None),
                                             ind_bpms,  # loop variable
                                             repeat(Drot),
                                             repeat(ind_bpms),
                                             repeat(ind_cors),
                                             repeat(bpms_weights),
                                             repeat(hor_disp_weight),
                                             repeat(ver_disp_weight),
                                             repeat(mode), repeat(verbose)))

            # collect results
            for count, ic in enumerate(ind_bpms):
                JSH[:, count] = results_HS[count][0][:, 0]  # ensure correct dimensionality
                JSV[:, count] = results_VS[count][1][:, 0]  # ensure correct dimensionality
                JR[:, count] = results_rot[count][2][:, 0]  # ensure correct dimensionality

        else:   # computing on a single core

            for count, ic in enumerate(ind_bpms):

                jq, _, _ = jacobian(ring,
                                 'Scale',
                                 0,
                                 ic,
                                 Dscale,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 mode, verbose=True)

                JSH[:, count] = jq[:, 0]  # ensure correct dimensionality

                _, js, _ = jacobian(ring,
                                 'Scale',
                                 1,
                                 ic,
                                 Dscale,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 mode, verbose=True)
                JSV[:, count] = js[:, 0]  # ensure correct dimensionality

                _, _, jr = jacobian(ring,
                                 'Rotation',
                                 None,  # scalar
                                 ic,
                                 Drot,
                                 ind_bpms,
                                 ind_cors,
                                 bpms_weights,
                                 hor_disp_weight,
                                 ver_disp_weight,
                                 mode, verbose=True)

                JR[:, count] = jr[:, 0]

    elif mode == 'analytic':

        orm_data = \
            compute_analytic_orbit_response_matrix(ring,
                                                   ind_bpms=ind_bpms,
                                                   ind_cor=ind_cors,
                                                   verbose=verbose,
                                                   use_bpm_errors=True)

        JSH, _, _ = analytic_jacobian(ring,
                          indexes=indexes,
                          mode='HorScale',
                          orm_data= orm_data,
                          use_mp=use_mp,
                          n_processes=n_processes,
                          verbose=verbose,
                          bpms_weights=bpms_weights,
                          hor_disp_weight=hor_disp_weight,
                          ver_disp_weight=ver_disp_weight)

        _, JSV, _= analytic_jacobian(ring,
                                      indexes=indexes,
                                      mode='VerScale',
                                      orm_data=orm_data,
                                      use_mp=use_mp,
                                      n_processes=n_processes,
                                      verbose=verbose,
                                      bpms_weights=bpms_weights,
                                      hor_disp_weight=hor_disp_weight,
                                      ver_disp_weight=ver_disp_weight)

        _, _, JR = analytic_jacobian(ring,
                                      indexes=indexes,
                                      mode='Rotation',
                                      orm_data=orm_data,
                                      use_mp=use_mp,
                                      n_processes=n_processes,
                                      verbose=verbose,
                                      bpms_weights=bpms_weights,
                                      hor_disp_weight=hor_disp_weight,
                                      ver_disp_weight=ver_disp_weight)

    # invert and get singular values
    if verbose:
        print(f'computing inverse of orm derivative')

    start_time=time.time()
    #_, sSH, _ = np.linalg.svd(JSH, full_matrices=True)
    print(f'time for np.linalg.svd of matrix size {JSH.shape}: {time.time()-start_time}')
    sSH = svdvals(JSH)
    start_time=time.time()
    #_, sSV, _ = np.linalg.svd(JSV, full_matrices=True)
    sSV = svdvals(JSV)
    print(f'time for np.linalg.svd of matrix size {JSV.shape}: {time.time()-start_time}')

    start_time=time.time()
    #_, sR, _ = np.linalg.svd(JR, full_matrices=True)
    sR = svdvals(JR)
    print(f'time for np.linalg.svd of matrix size {JR.shape}: {time.time()-start_time}')

    if filename_response:

        mdict = {'sSH': sSH, 'sSV': sSV, 'sR': sR,  # singular values to compute rcond for np.pinv
                 'JSH': JSH, 'JSV': JSV, 'JR': JR,  # jacobians
                 'bpms_weights': bpms_weights,      # following: parameters used
                 'hor_disp_wight': hor_disp_weight,
                 'ver_disp_wight': hor_disp_weight,
                 'ind_bpms': ind_bpms,
                 'ind_cors': ind_cors}

        file_no_ext, exten_file = os.path.splitext(filename_response)

        if save_matlab_file:
            try:
                savemat(file_no_ext + '.mat', mdict=mdict)
            except Exception as e:
                print(e)
                print('could not save .mat file')

        pickle.dump(mdict, open(file_no_ext + '.pkl', 'wb'))

        while not(exists(filename_response)):
            print('Wait for file {} to be saved'.format(filename_response))
            time.sleep(1)

        print('ORM derivatives saved in: {}'.format(filename_response))

    return JSH, JSV, JR, sSH, sSV, sR


def float_bpm_errors(r, ind_bpms):

    for i in ind_bpms:
        try:
            r[i].Scale = r[i].Scale.astype(np.float64)
            r[i].Offset = r[i].Offset.astype(np.float64)
            r[i].Reading = r[i].Reading.astype(np.float64)
            r[i].Rotation = r[i].Rotation.astype(np.float64)
        except Exception as e:
            pass

    return r


def fit_optics(ring_to_correct,
               reference_ring,
               niter=1,
               neig_scale=1,
               neig_rot=1,
               bpms_weights=None,
               hor_disp_weight=1.0,
               ver_disp_weight=1.0,
               mode='numeric',
               indexes=None,
               filename_response=None,
               reference_orm=None,
               use_mp=False,
               verbose=True,
               horizontal=True,
               vertical=True,
               rotation=True):
    """
    fit BPMS scale and rotation errors
    these errors added to reference_ring will return an ORM as close as possible to the one of ring_to_correct
    the fitted errors are applied as correction to ring_to_correct to output a corrected lattice rcor

    :param ring_to_correct: ring with optics/coupling errors
    :param reference_ring: reference ring, no errors
    :param niter: number of iteration of correction
    :param neig_scale: # of singular vectors to use for quad correction
    :param neig_rot: # of singular vectors to use for quad correction
    :param bpms_weights: (2, nbpms) array of multiplication factors for each raw of the RM.
    :param hor_disp_weight: multiplication factor for hor disperison
    :param ver_disp_weight: multiplication factor for ver dispersion
    :param mode: 'numeric', 'semi-analytic', 'analytic'
    :param indexes: indexes obtained by commissioningsimulations.config.get_lattice_indexes. if none, thecomputed.
    :param filename_response: file where to look for derivative of ORM. if not found, computed
    :param reference_orm: as computed by get_orm_arrays, or None (compute it)
    :param use_mp: use multiprocessing
    :param verbose: if True, print text
    :return: rfit fitted model for ring_to_correct including only quadrupole errors
             rcor corrected AT lattice
             dcRot applied BPM Rotation variations
             dcScale applied BPM Scale variations
    """

    if verbose:
        print('correct optics')

    force_matrix_computation = False

    orm_mode = mode
    if mode == 'semi-analytic':
        orm_mode='analytic'

    # test, overwrite ORM mode to keep always numeric
    orm_mode = 'numeric'

    if not(indexes):
        indexes = get_lattice_indexes(reference_ring)

    ind_bpms = indexes['bpms']
    ind_cors = indexes['correctors_for_optics_RM']

    # get copy of ring
    ring_to_correct = float_bpm_errors(ring_to_correct, ind_bpms)
    reference_ring = float_bpm_errors(reference_ring, ind_bpms)

    rcor = copy.deepcopy(ring_to_correct)
    rfit = copy.deepcopy(reference_ring)

    # print initial status
    """
    if verbose:
        print('fit-lattice vs input-lattice (should be zero after fit):')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

    if verbose:
        print('Before optics/coupling correction (should be zero after correction):')
        get_optics_vs_reference(reference_ring, ring_to_correct, ind_bpms)
    """

    # get Orbit Response Matrix if no file is provided, based on lattice with errors.
    if (not exists(filename_response)) or force_matrix_computation:
        start_time = time.time()
        JSH, JSV, JR, sSH, sSV, sR = compute_jacobian(reference_ring,
                                          indexes=indexes,
                                          filename_response=filename_response,
                                          save_matlab_file=True,
                                          mode=mode,  # 'semi-analytic', 'analytic'
                                          use_mp=use_mp, #False, #
                                          n_processes=None,
                                          bpms_weights=bpms_weights,
                                          hor_disp_weight=hor_disp_weight,
                                          ver_disp_weight=ver_disp_weight,
                                          verbose=True)  # all if None
        end_time = time.time()
        if verbose:
            print(f'time for (parallel = {use_mp}) orm derivative computation: {end_time-start_time} seconds')
    else:

        _, file_ext = os.path.splitext(filename_response)
        if file_ext == '.mat':
            mat_contents = loadmat(filename_response)
            JSH = mat_contents['JSH']
            JSV = mat_contents['JSV']
            JR = mat_contents['JR']

            sSH = np.transpose(mat_contents['sSH'])  # load mat transposes raws/columns for some reason
            sSH = sSH[:, 0]
            sSV = np.transpose(mat_contents['sSV'])  # load mat transposes raws/columns for some reason
            sSV = sSV[:, 0]
            sR = np.transpose(mat_contents['sR'])  # load mat transposes raws/columns for some reason
            sR = sR[:, 0]
        else:
            mat_contents = pickle.load(open(filename_response, 'rb'))
            JSH = mat_contents['JSH']
            JSV = mat_contents['JSV']
            JR = mat_contents['JR']

            sSH = mat_contents['sSH']
            sSV = mat_contents['sSV']
            sR = mat_contents['sR']
        if verbose:
            print(f'loaded: {filename_response}')

    # fix  singular values to max possible if too many
    if neig_scale > JSH.shape[1]:
        neig_scale = JSH.shape[1] -1
        if verbose:
            print(f'max singular values for BPM scale errors = {JSH.shape[1]}')
    if neig_rot > JR.shape[1]:
        neig_rot = JR.shape[1] -1
        if verbose:
            print(f'max singular values for BPM rotations = {JR.shape[1]}')

    # get initial correctors values
    # Lq = at.get_value_refpts(rcor, ind_quad, 'Length')
    # Ls = at.get_value_refpts(rcor, ind_skew, 'Length')

    initcorSH = at.get_value_refpts(rcor, ind_bpms, 'Scale', 0)
    initcorSV = at.get_value_refpts(rcor, ind_bpms, 'Scale', 1)
    initcorR = at.get_value_refpts(rcor, ind_bpms, 'Rotation', 0)

    # get initial correctors values in rfit
    initcorSH0 = at.get_value_refpts(reference_ring, ind_bpms, 'Scale', 0)
    initcorSV0 = at.get_value_refpts(reference_ring, ind_bpms, 'Scale', 1)
    initcorR0 = at.get_value_refpts(reference_ring, ind_bpms, 'Rotation', 0)

    # initialize corrections to zero. INTEGRATED STRENGHTS
    dcSH = np.zeros(len(ind_bpms))
    dcSV = np.zeros(len(ind_bpms))
    dcR = np.zeros(len(ind_bpms))

    # get ORM to correct
    Hinit, Vinit, Rinit = get_orm_arrays(rcor,
                                  ind_bpms,
                                  ind_cors,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  orm_mode,
                                  False)

    """
    Hinit, Vinit, Rinit = get_orm_arrays(rcor,
                                  ind_bpms,
                                  ind_cors,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  'numeric',
                                  False)

    Hinita, Vinita, Rinita = get_orm_arrays(rcor,
                                         ind_bpms,
                                         ind_cors,
                                         bpms_weights,
                                         hor_disp_weight,
                                         ver_disp_weight,
                                         'analytic',  # 'numeric',
                                         False)

    def __flat__(MM):
        return np.asarray(np.matrix(MM).flatten().transpose())[:,0]

    fig, (axh, axv, axr) = plt.subplots(ncols=3)
    axh.plot(__flat__(Hinit), __flat__(Hinita), '.')
    axv.plot(__flat__(Vinit), __flat__(Vinita), '.')
    axr.plot(__flat__(Rinit), __flat__(Rinita), '.')
    """

    # get reference ORM
    if reference_orm==None:
        H0, V0, R0 = get_orm_arrays(reference_ring,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                orm_mode,
                                False)
    else:
        H0 = reference_orm[0]
        V0 = reference_orm[1]
        R0 = reference_orm[2]

    for correction_iteration in range(0, niter):
        print('iteration # {}'.format(correction_iteration + 1))

        # plot_bpm_errors(rcor, ind_bpms)

        # get ORM
        if correction_iteration>0:
            H, V, R = get_orm_arrays(rcor,
                                  ind_bpms,
                                  ind_cors,
                                  bpms_weights,
                                  hor_disp_weight,
                                  ver_disp_weight,
                                  orm_mode,
                                  False)

        else:  # use already available
            H = Hinit
            V = Vinit
            R = Rinit

        # compute correction and add to previous iterations
        ooh = np.asarray(H - H0)[:, 0]
        oov = np.asarray(V - V0)[:, 0]
        oor = np.asarray(R - R0)[:, 0]

        """
        fig, (axh, axv, axr) = plt.subplots(nrows=3)
        fig.subplots_adjust(hspace=.5, wspace=.5)
        axh.plot(H0[:,0], label='hor rm ref')
        axh.plot(H[:,0], label='hor rm')
        axh.plot(ooh*100, label='hor scale signal x100')
        axh.legend()
        axv.plot(V0[:, 0], label='ver rm ref')
        axv.plot(V[:, 0], label='ver rm')
        axv.plot(oov*100, label='ver scale signal x100')
        axv.legend()
        axr.plot(R0[:, 0], label='rot rm ref')
        axr.plot(R[:, 0], label='rot rm')
        axr.plot(oor*100, label='rot signal x 100')
        axr.legend()
        plt.show()
        """

        if horizontal:
            # s_time = time.time()
            dcSH = dcSH + np.linalg.pinv(JSH, rcond=sSH[neig_scale - 1] / sSH[0]) @ ooh
            # print(f'time for np.linalg.pinv of matrix size {JSH.shape}: {time.time() - s_time}')

        if vertical:
            dcSV = dcSV + np.linalg.pinv(JSV, rcond=sSV[neig_scale - 1] / sSV[0]) @ oov

        if rotation:
            dcR = dcR + np.linalg.pinv(JR, rcond=sR[neig_rot - 1] / sR[0]) @ oor

        # check that the corrections are <30 mircorad
        if verbose:
            print(f' ORM for BPM Hor. Scale | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
            print(f' ORM for BPM Ver. Scale | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')
            print(f' ORM for BPM Rotations | std: {np.std(oor):.3g} | (min, max) ({np.min(oor):.3g}, {np.max(oor):.3g})')

            print(f' BPM Hor. Scale cor. | std: {np.std(dcSH):.3g} | (min, max) ({np.min(dcSH):.3g}, {np.max(dcSH):.3g})')
            print(f' BPM Ver. Scale cor. | std: {np.std(dcSV):.3g} | (min, max) ({np.min(dcSV):.3g}, {np.max(dcSV):.3g})')
            print(f' BPM Rotation cor. | std: {np.std(dcR):.3g} | (min, max) ({np.min(dcR):.3g}, {np.max(dcR):.3g})')

        # apply correction
        for count, ic in enumerate(ind_bpms):
            # set correctors in the lattice
            rcor[ic].Scale[0] = initcorSH[count] - dcSH[count]
            rcor[ic].Scale[1] = initcorSV[count] - dcSV[count]
            rcor[ic].Rotation[0] = initcorR[count] - dcR[count]
            # assign errors to optics model
            rfit[ic].Scale[0] = initcorSH0[count] + dcSH[count]
            rfit[ic].Scale[1] = initcorSV0[count] + dcSV[count]
            rfit[ic].Rotation[0] = initcorR0[count] + dcR[count]

    # get Orbit RM after correction
    Haft, Vaft, Raft = get_orm_arrays(rcor,
                                ind_bpms,
                                ind_cors,
                                bpms_weights,
                                hor_disp_weight,
                                ver_disp_weight,
                                orm_mode,
                                False)

    ooh = np.asarray(Haft - H0)[:, 0]
    oov = np.asarray(Vaft - V0)[:, 0]
    oor = np.asarray(Raft - R0)[:, 0]

    if verbose:
        print(f'Final ORM for BPM Hor. Scale | std: {np.std(ooh):.3g} | (min, max) ({np.min(ooh):.3g}, {np.max(ooh):.3g})')
        print(f'Final ORM for BPM Ver. Scale | std: {np.std(oov):.3g} | (min, max) ({np.min(oov):.3g}, {np.max(oov):.3g})')
        print(f'Final ORM for BPM Rotations | std: {np.std(oor):.3g} | (min, max) ({np.min(oor):.3g}, {np.max(oor):.3g})')

    """
    if verbose:
        print('fit-lattice vs input-lattice:')
        get_optics_vs_reference(rfit, ring_to_correct, ind_bpms)

    if verbose:
        print('After optics/coupling correction:')
        get_optics_vs_reference(reference_ring, rcor, ind_bpms)
    """

    return rfit, rcor, dcSH, dcSV, dcR


def _test_bpms_errors_fit(
    mode = 'numeric'
    ):
    '''
    script to test optics correction
    :return: None
    '''

    from commissioningsimulations.errors.find_orbit_bpm_errors import find_orbit_bpm_errors

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    config.radiation = True # or no dispersion

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    config.correctors_for_optics_RM = 'SH1A'
    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # add BPM errors
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    ind_off = [129, 135]
    ind_rot = [45]
    ind_scale = [100, 200]
    ind_read = [100]

    rerr[ind_bpms[ind_off[0]]].Offset = [-234e-6, 334e-6]

    rerr[ind_bpms[ind_off[1]]].Offset = [-24e-6, -334e-6]

    rerr[ind_bpms[ind_rot[0]]].Rotation = [0.01*3.14]

    rerr[ind_bpms[ind_scale[0]]].Scale = [1.1, 1.0]
    rerr[ind_bpms[ind_scale[1]]].Scale = [1.0, 0.95]

    # rerr[ind_bpms[ind_read[0]]].Reading = [1e-6, 2e-6]

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)
    # # reduce indexes for test
    # indexes['correctors_for_optics_RM'] =  [indexes['correctors_for_optics_RM'][0]]

    # get bpm weigths from reading errors
    nacq = 100
    oh = np.zeros((len(ind_bpms), nacq))
    ov = np.zeros((len(ind_bpms), nacq))
    bpm_weigths = np.ones((2, len(ind_bpms))) # similar shape of orbit (6,Nbpms)
    for r in range(nacq):
        # read orbit n times (as for a measurement)
        _, o = find_orbit_bpm_errors(rerr, ind_bpms)
        oh[:, r] = o[:,0] # H
        ov[:, r] = o[:,2] # V

    bpm_weigths[0, :] = 1/np.std(oh, axis=1)
    bpm_weigths[1, :] = 1/np.std(ov, axis=1)
    # limit weigths to numerically reasonable values
    bpm_weigths[0, bpm_weigths[0, :] > 1e6] = 1e6  # too good to be true
    bpm_weigths[1, bpm_weigths[1, :] > 1e6] = 1e6
    bpm_weigths[0, bpm_weigths[0, :] < 1e-6] = 1e-6 # too bad to be true
    bpm_weigths[1, bpm_weigths[1, :] < 1e-6] = 1e-6

    # correct optics
    onestep=True
    # mode = 'numeric'  # 'semi-analytic'

    if onestep:
        rfit, rcor, Hcor, Vcor, Rcor = fit_optics(rerr,
                              ring,
                              niter=3,
                              neig_scale=320,
                              neig_rot=320,
                              #  bpms_weights=bpm_weigths,
                              hor_disp_weight=1,
                              ver_disp_weight=1,
                              mode=mode,
                              indexes=indexes,
                              use_mp=True,
                              filename_response='./J_BPMs_' + mode + '.pkl')

    else:
        rfit = copy.deepcopy(ring)

        # correct rotations

        rfit, _, _, _, Rcor= fit_optics(
                            rerr,  # assume same measured lattice
                            rfit,  # assume quadrupole errors from step before
                            niter=3,
                            neig_scale=320,
                            neig_rot=320,
                            hor_disp_weight=100,
                            ver_disp_weight=100,
                            mode=mode,
                            indexes=indexes,
                            use_mp=True,
                            filename_response='./J_BPMs_' + mode + '.pkl',
                            horizontal=False,
                            vertical=False)

        # Rcor = np.zeros(len(ind_bpms))

        # correct scale
        rfit, _, Hcor, Vcor, _ = fit_optics(
                            rerr,
                            rfit,
                            niter=3,
                            neig_scale=320,
                            neig_rot=320,
                            hor_disp_weight=1,
                            ver_disp_weight=1,
                            mode=mode,
                            indexes=indexes,
                            use_mp=True,
                            filename_response='./J_BPMs_' + mode + '.pkl',
                            rotation=False)

        # compute rcor
        rcor = copy.deepcopy(rerr)

        # apply correction
        for count, ic in enumerate(indexes['bpms']):
            rcor[ic].Scale[0] = rcor[ic].Scale[0] - Hcor[count]
            rcor[ic].Scale[1] = rcor[ic].Scale[1] - Vcor[count]
            rcor[ic].Rotation = rcor[ic].Rotation - Rcor[count]

    # optics after correction
    # rcor.plot_beta()

    # plot figures
    fig, (axQ, axS) = plt.subplots(nrows=2, ncols=1)
    fig.subplots_adjust(hspace=.5, wspace=.5)
    axQ.plot(Hcor, label='horizontal')
    axQ.plot(Vcor, label='vertical')
    axQ.set_ylabel(f' Scale errors ')
    axQ.set_title(mode)
    axQ.plot(ind_scale[0], (rerr[ind_bpms[ind_scale[0]]].Scale[0]-1), 'x', label='Hor. Expected')
    axQ.plot(ind_scale[1], (rerr[ind_bpms[ind_scale[1]]].Scale[1]-1), '+', label='Ver. Expected')
    axQ.text(ind_scale[0], max(Hcor) * 0.7, 'Hor. Scale', rotation='vertical')
    axQ.text(ind_scale[1], max(Vcor) * 0.7, 'Ver. Scale', rotation='vertical')
    axQ.legend()

    axS.plot(Rcor, label='')
    axS.plot(ind_rot[0], rerr[ind_bpms[ind_rot[0]]].Rotation[0], 'x',  label='Expected')
    axS.text(ind_rot[0], max(Rcor) * 0.7, 'Rotation', rotation='vertical')

    axS.set_ylabel(f' Rotations ')


    cont=pickle.load(open('./J_BPMs_' + mode + '.pkl','rb'))
    fig, ((axH, axV, axR), (axHb, axVb, axRb), (axHi, axVi, axRi)) = \
        plt.subplots(nrows=3, ncols=3, figsize=(12, 12))

    fig.subplots_adjust(hspace=.5, wspace=.5)

    c = 34
    r = 230
    axH.plot(cont['JSH'][:, c],label=f'col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axH.set_title('Hor. Scale')
    axH.legend()

    axV.plot(cont['JSV'][:, c], label=f'col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axV.set_title('Ver. Scale')
    axV.legend()

    axR.plot(cont['JR'][:, c], label=f'col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axR.set_title('Rotation')
    axR.legend()

    axHb.plot(cont['JSH'][r, :], label=f'row {r}')
    axHb.legend()
    axVb.plot(cont['JSV'][r, :], label=f'row {r}')
    axVb.legend()
    axRb.plot(cont['JR'][r, :], label=f'row {r}')
    axRb.legend()


    axHi.imshow(np.log(cont['JSH']), interpolation='none', aspect='auto')
    axHi.set_xlabel('BPM')
    axHi.set_ylabel('H2H, V2H, DH')

    axVi.imshow(np.log(cont['JSV']), interpolation='none', aspect='auto')
    axVi.set_xlabel('BPM')
    axVi.set_ylabel('V2V, H2V, DV')

    axRi.imshow(np.log(cont['JR']), interpolation='none', aspect='auto')
    axRi.set_xlabel('BPM')
    axRi.set_ylabel('H2H, V2V')

    plt.show()

    pass


def _test_semi_analytic_vs_numeric_jacobian():
    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    config.radiation = True # or no dispersion

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')

    # create a copy of the lattice
    rerr = at.Lattice(copy.deepcopy(ring))

    # add BPM errors
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    ind_off = [129, 135]
    ind_rot = [45]
    ind_scale = [175, 200]
    ind_read = [100]

    rerr[ind_bpms[ind_off[0]]].Offset = [-234e-6, 334e-6]

    rerr[ind_bpms[ind_off[1]]].Offset = [-24e-6, -334e-6]

    rerr[ind_bpms[ind_rot[0]]].Rotation = [0.1*3.14]

    rerr[ind_bpms[ind_scale[0]]].Scale = [1.1, 1.0]
    rerr[ind_bpms[ind_scale[1]]].Scale = [1.0, 1.0]

    rerr[ind_bpms[ind_read[0]]].Reading = [0*100e-6, 0*60e-6]

    # optics before
    # rerr.plot_beta()

    # get indexes once and for all
    indexes = get_lattice_indexes(ring)
    # reduce indexes for test
    indexes['correctors_for_optics_RM'] = [indexes['correctors_for_optics_RM'][0]]

    mode = 'numeric'

    filename_response = './J_BPMs_'+ mode +'_1.pkl'
    if (not exists(filename_response)) :
        start_time = time.time()
        compute_jacobian(ring,
                      indexes=indexes,
                      filename_response=filename_response,
                      save_matlab_file=False,
                      mode=mode,  # 'semi-analytic', 'analytic'
                      use_mp=True,
                      verbose=False)  # all if None
        end_time = time.time()
        print(f'time for {mode} (parallel = {True}) orm derivative computation: {end_time - start_time} seconds')

    mode = 'semi-analytic'

    filename_response = './J_BPMs_' + mode + '_1.pkl'
    if (not exists(filename_response)):
        start_time = time.time()
        compute_jacobian(ring,
                      indexes=indexes,
                      filename_response=filename_response,
                      save_matlab_file=False,
                      mode=mode,  # 'semi-analytic', 'analytic'
                      use_mp=True,
                      verbose=False)  # all if None
        end_time = time.time()
        print(f'time for {mode} (parallel = {True}) orm derivative computation: {end_time - start_time} seconds')

    mode = 'analytic'
    filename_response = './J_BPMs_' + mode + '_1.pkl'
    if (not exists(filename_response)):
        start_time = time.time()
        compute_jacobian(ring,
                         indexes=indexes,
                         filename_response=filename_response,
                         save_matlab_file=False,
                         mode=mode,  # 'semi-analytic', 'analytic'
                         use_mp=True,
                         verbose=False)  # all if None
        end_time = time.time()
        print(f'time for {mode} (parallel = {True}) orm derivative computation: {end_time - start_time} seconds')


    numeric=pickle.load(open('./J_BPMs_' + 'numeric' + '_1.pkl', 'rb'))
    semiana=pickle.load(open('./J_BPMs_' + 'semi-analytic' + '_1.pkl', 'rb'))
    analyti=pickle.load(open('./J_BPMs_' + 'analytic' + '_1.pkl', 'rb'))

    fig, ((axH, axV, axR),
          (axHb, axVb, axRb),
          (axHi, axVi, axRi),
          (axHib, axVib, axRib)) = \
        plt.subplots(nrows=4, ncols=3, figsize=(12, 12))

    fig.subplots_adjust(hspace=.5, wspace=.5)

    c = 34
    r = 230
    axH.plot(numeric['JSH'][:, c], label=f'numeric col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axH.plot(semiana['JSH'][:, c], ':', label=f'semi-ana col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axH.set_title('Hor. Scale')
    axH.legend()

    axV.plot(numeric['JSV'][:, c], label=f'numeric col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axV.plot(semiana['JSV'][:, c], ':', label=f'semi-ana col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axV.set_title('Ver. Scale')
    axV.legend()

    axR.plot(numeric['JR'][:, c], label=f'numeric  {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axR.plot(semiana['JR'][:, c], ':', label=f'semi-ana col {c}, BPM: {ring[ind_bpms[c]].FamName}')
    axR.set_title('Rotation')
    axR.legend()

    axHb.plot(numeric['JSH'][r, :], label=f'numeric row {r}')
    axHb.plot(semiana['JSH'][r, :], ':', label=f'semi-ana row {r}')
    axHb.legend()
    axVb.plot(numeric['JSV'][r, :], label=f'numeric row {r}')
    axVb.plot(semiana['JSV'][r, :], ':', label=f'semi-ana row {r}')
    axVb.legend()
    axRb.plot(numeric['JR'][r, :], label=f'numeric row {r}')
    axRb.plot(semiana['JR'][r, :], ':', label=f'semi-ana row {r}')
    axRb.legend()

    axHi.set_title('numeric')
    axHi.imshow(np.log(numeric['JSH']), interpolation='none', aspect='auto')
    axHi.set_xlabel('BPM')
    axHi.set_ylabel('H2H, V2H, DH')

    axVi.imshow(np.log(numeric['JSV']), interpolation='none', aspect='auto')
    axVi.set_xlabel('BPM')
    axVi.set_ylabel('V2V, H2V, DV')

    axRi.imshow(np.log(numeric['JR']), interpolation='none', aspect='auto')
    axRi.set_xlabel('BPM')
    axRi.set_ylabel('H2H, V2V')

    axHib.set_title('semi-analytic')
    axHib.imshow(np.log(semiana['JSH']), interpolation='none', aspect='auto')
    axHib.set_xlabel('BPM')
    axHib.set_ylabel('H2H, V2H, DH')

    axVib.imshow(np.log(semiana['JSV']), interpolation='none', aspect='auto')
    axVib.set_xlabel('BPM')
    axVib.set_ylabel('V2V, H2V, DV')

    axRib.imshow(np.log(semiana['JR']), interpolation='none', aspect='auto')
    axRib.set_xlabel('BPM')
    axRib.set_ylabel('H2H, V2V')

    # correlation figures
    fig, ((axHic, axVic, axRic),(axHid, axVid, axRid)) = \
        plt.subplots(nrows=2, ncols=3, figsize=(12, 8))

    fig.subplots_adjust(hspace=.5, wspace=.5)
    axHic.plot(flat(numeric['JSH']),flat(semiana['JSH']), '.', label='semi-analytic')
    axHic.plot(flat(numeric['JSH']),flat(analyti['JSH']), '.', label='analytic')
    axHic.set_xlabel('numeric')
    axHic.legend()

    axVic.plot(flat(numeric['JSV']),flat(semiana['JSV']), '.', label='semi-analytic')
    axVic.plot(flat(numeric['JSV']),flat(analyti['JSV']), '.', label='analytic')
    axVic.set_xlabel('numeric')
    axRic.legend()

    axRic.plot(flat(numeric['JR']),flat(semiana['JR']), '.', label='semi-analytic')
    axRic.plot(flat(numeric['JR']),flat(analyti['JR']), '.', label='analytic')
    axRic.set_xlabel('numeric')
    axRic.legend()

    nbin =  int(len(flat(numeric['JSH']))**0.5)

    axHid.hist(flat(numeric['JSH'])-flat(semiana['JSH']), nbin, alpha=0.5, label='semi-analytic')
    axHid.hist(flat(numeric['JSH'])-flat(analyti['JSH']), nbin, alpha=0.5, label='analytic')
    axHid.set_xlabel('numeric-[ana/semi]')
    axHid.legend()

    NN = len(flat(numeric['JSV']))
    nbin = int(NN ** 0.5)

    axVid.bar(range(NN), flat(numeric['JSV'])-flat(semiana['JSV']),alpha=0.5, label='semi-analytic')
    axVid.bar(range(NN), flat(numeric['JSV'])-flat(analyti['JSV']), alpha=0.5, label='analytic')
    axVid.set_xlabel('numeric-[ana/semi]')
    axRid.legend()

    nbin = int(len(flat(numeric['JR'])) ** 0.5)

    axRid.hist(flat(numeric['JR'])-flat(semiana['JR']), nbin, alpha=0.5, label='semi-analytic')
    axRid.hist(flat(numeric['JR'])-flat(analyti['JR']), nbin, alpha=0.5, label='analytic')
    axRid.set_xlabel('numeric-[ana/semi]')
    axRid.legend()

    plt.show()

    pass


if __name__ == '__main__':

    #_test_semi_analytic_vs_numeric_jacobian()

    # _test_bpms_errors_fit(mode='numeric')  # ok
    #  _test_bpms_errors_fit(mode='semi-analytic') # not ok Hor scale
    _test_bpms_errors_fit(mode='analytic')  # not tested

    pass
