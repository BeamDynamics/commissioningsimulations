"""
formulas from
A.Franchi (ESRF), Z.Marti (CELLS),
"Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018
"""

import at
import math
import numpy as np
import multiprocessing
from itertools import repeat

__author__='Simone Maria Liuzzo, Andrea Franchi'


def analytic_dispersion_variation_with_skew_quadrupole(
        ring,
        ind_bpms=None,
        ind_skews=None,
        verbose=True,
        thick=False,
        opt_all_location=None,
        use_mp=False):
    """
    Computes the derivative of the vertical dispersion compared to skew quadrupole errors

    A.Franchi (ESRF), Z.Marti (CELLS),
    "Analytic formulas for the rapid evaluation of the orbit response matrix and chromatic functions from lattice
    parameters in circular accelerators" arXiv:1711:06589v2 17 Apr 2018

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_skews: numpy array of np.uint32. indexes of skew quadrupoles
    :param verbose: True/False
    :param thick: True/False
    :param opt_all_location: output of at.linopt6 at all ring elements
    :param use_mp: True/False use multiprocessing
    :return: one 2D lists of floats.
    VerDispersion_over_Skew[BPMS][SKEW],
    """

    dDV_dQuadSkew = np.zeros(shape=(len(ind_bpms), len(ind_skews)))

    # loop quadrupoles
    if use_mp:
        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('Dispersion derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap(_analytic_dispersion_variation_with_skew_quadrupole,
                                zip(repeat(ring),
                                    repeat(ind_bpms),
                                    ind_skews,  # loop index
                                    repeat(verbose),
                                    repeat(thick),
                                    repeat(opt_all_location)
                                    )
                                )

        for m, _ in enumerate(ind_skews):
            _MH = results[m]
            dDV_dQuadSkew[:, m] = _MH[:, 0]

    else:

        dDV_dQuadSkew = _analytic_dispersion_variation_with_skew_quadrupole(
            ring,
            ind_bpms=ind_bpms,
            ind_skews=ind_skews,
            verbose=verbose,
            thick=thick,
            opt_all_location=opt_all_location)

    return dDV_dQuadSkew


def _analytic_dispersion_variation_with_skew_quadrupole(
        ring,
        ind_bpms=None,
        ind_skews=None,
        verbose=True,
        thick=False,
        opt_all_location=None):

    # optics at correctors and BPMS
    if opt_all_location is None:
        _, _, opt_all_location = ring.linopt6(range(len(ring)))

    if isinstance(ind_skews, np.uint32):
        ind_skews = [ind_skews]

    bpm = opt_all_location[ind_bpms]
    qua = opt_all_location[ind_skews]

    # full tunes
    Q = at.get_tune(ring, get_integer=True)

    # functions required by later formulas

    def tau(pl, a, b):
        return dphi(pl, a, b) - math.pi * Q[pl]

    def dphi(pl, w, j):

        d = j.mu[pl] - w.mu[pl]

        if j.mu[pl] < w.mu[pl]:
            d = d + 2 * math.pi * Q[pl]

        return d

    # formulas for response
    x = 0
    xp = 1
    y = 1

    dDV_dQuadSkew = np.zeros(shape=(len(bpm), len(qua)))
    for countm, m in enumerate(range(len(qua))):
        is_a_quad = True

        Lm = ring[ind_skews[m]].Length
        Km0 = ring[ind_skews[m]].PolynomB[1]

        if Km0 == 0:
            is_a_quad = False
            if verbose:
                print(f'{ring[ind_skews[m]].FamName} is not a quadrupole. '
                      f'PolynomA[1] = {ring[ind_skews[m]].PolynomA[1]:.3f}, '
                      f'PolynomB[1] = {ring[ind_skews[m]].PolynomB[1]:.3f}')

        if is_a_quad:
            sKLm = abs(Km0) ** 0.5 * Lm

            foc = 1
            if Km0 < 0:
                foc = -1

            TSmy = qua[m].dispersion[x] / (2 * abs(Km0) * Lm * qua[m].beta[y] ** 0.5) * (
                           math.sin(sKLm) * math.sinh(sKLm) + foc * math.cos(sKLm) * math.cosh(sKLm) - foc) + \
                   qua[m].dispersion[xp] / (2 * abs(Km0) ** (3 / 2) * Lm * qua[m].beta[y] ** 0.5) * (
                           math.sin(sKLm) * math.cosh(sKLm) - math.cos(sKLm) * math.sinh(sKLm))

            TCmy = + qua[m].beta[y] ** 0.5 * qua[m].dispersion[x] / (2 * sKLm) * ( \
                           math.sin(sKLm) * math.cosh(sKLm) + math.cos(sKLm) * math.sinh(sKLm)) + \
                   - qua[m].alpha[y] * qua[m].dispersion[xp] / (2 * abs(Km0) ** (3 / 2) * Lm * qua[m].beta[y] ** 0.5) * (
                           math.sin(sKLm) * math.cosh(sKLm) - math.cos(sKLm) * math.sinh(sKLm)) + \
                   - qua[m].alpha[y] * qua[m].dispersion[x] / (2 * abs(Km0) * Lm * qua[m].beta[y] ** 0.5) * (
                           math.sin(sKLm) * math.sinh(sKLm) + foc * math.cos(sKLm) * math.cosh(sKLm) - foc) + \
                   - qua[m].beta[y] ** 0.5 * qua[m].dispersion[xp] / (2 * abs(Km0) * Lm) * (
                           math.sin(sKLm) * math.sinh(sKLm) - foc * math.cos(sKLm) * math.cosh(sKLm) + foc)

        for countj, j in enumerate(range(len(bpm))):
            if verbose:
                print(f'computing dispersion'
                      f' at BPM {ring[ind_bpms[j]].FamName}'
                      f' with a skew (thick={thick}) quadrupole error in {ring[ind_skews[m]].FamName}')
            if thick and is_a_quad:

                JCmjDx = math.cos(tau(y, qua[m], bpm[j]))*TCmy + math.sin(tau(y, qua[m], bpm[j]))*TSmy
                
                dDV_dQuadSkew[j][m] = (bpm[j].beta[y] ** 0.5) / 2 / math.sin(math.pi * Q[y]) * JCmjDx

            else:
                dDV_dQuadSkew[j][m] = (bpm[j].beta[y] ** 0.5) / 2 / math.sin(math.pi * Q[y]) \
                                      * (qua[m].beta[x] ** 0.5) \
                                      * qua[m].dispersion[x] \
                                      * math.cos(tau(y, qua[m], bpm[j]))

    return dDV_dQuadSkew


def semi_analytic_dispersion_variation_with_normal_quadrupole(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True,
        use_mp=False):
    """
    Computes the SEMI-ANALYTIC derivative of the horizontal dispersion compared to normal quadrupole errors

    :param ring: AT lattice
    :param ind_bpms: numpy array of np.uint32. indexes of BPMs
    :param ind_quads: numpy array of np.uint32. indexes of skew quadrupoles
    :param verbose: True/False
    :param use_mp: True/False use multiprocessing
    :return: one 2D lists of floats.
    HorDispersion_over_QUAD[BPMS][QUAD],
    """


    dDH_dQuadNorm = np.zeros(shape=(len(ind_bpms), len(ind_quads)))

    # loop quadrupoles
    if use_mp:
        n_cpu = multiprocessing.cpu_count()
        n_processes = n_cpu

        if verbose:
            print('Dispersion derivative parallel computation using {} cores'.format(n_processes))

        with multiprocessing.Pool() as p:
            results = p.starmap(_semi_analytic_dispersion_variation_with_normal_quadrupole,
                                zip(repeat(ring),
                                    repeat(ind_bpms),
                                    ind_quads,  # loop index
                                    repeat(verbose)
                                    )
                                )

        for m, _ in enumerate(ind_quads):
            _MH = results[m]
            dDH_dQuadNorm[:, m] = _MH[:, 0]

    else:

        dDH_dQuadNorm = _semi_analytic_dispersion_variation_with_normal_quadrupole(
            ring,
            ind_bpms=ind_bpms,
            ind_quads=ind_quads,
            verbose=verbose)

    return dDH_dQuadNorm


def _semi_analytic_dispersion_variation_with_normal_quadrupole(
        ring,
        ind_bpms=None,
        ind_quads=None,
        verbose=True):

    dKL = 0.00000001

    if isinstance(ind_quads, np.uint32):
        ind_quads = [ind_quads]

    dDH_dQuadNorm = np.zeros(shape=(len(ind_bpms), len(ind_quads)))

    for ciq, iq in enumerate(ind_quads):
        K0 = ring[iq].PolynomB[1]
        ring[iq].PolynomB[1] = K0 + dKL / ring[iq].Length
        _, _, opt_plus = ring.linopt6(ind_bpms)
        ring[iq].PolynomB[1] = K0 - dKL / ring[iq].Length
        _, _, opt_minus = ring.linopt6(ind_bpms)
        ring[iq].PolynomB[1] = K0
        dDH_dQuadNorm[:, ciq] = np.array([(p.dispersion[0] - m.dispersion[0])/2/dKL
                                          for p, m in zip(opt_plus, opt_minus)])

    return dDH_dQuadNorm


def _test_dispersion_deriv_quad(m=0):

    import commissioningsimulations.config as config
    from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix
    import matplotlib.pyplot as plt
    from os.path import exists
    import pickle
    import time

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    ring.radiation_off()
    ring.radiation_on()  # cavity ON, radiation ON or dispersion will not compute
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadrupole indexes
    ind_quads = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))

    # check BETA at DL entrance vs Average beta DL
    _, _, l0 = ring.linopt6(refpts=ind_quads[m])
    ring.radiation_off()
    lave, bave, _, _, _, _, _ = at.avlinopt(ring, 0, ind_quads[m])
    ring.radiation_on()
    print(l0.beta)
    print(lave.beta)
    print(bave)

    # get reference ORM
    orm0 = './Reference.pkl'
    if not exists(orm0):
        MH0, MV0, _, _, _, _, _, _, _, _, dh0, dv0, Q0 = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=orm0)
    else:
        mat_cont = pickle.load(open(orm0, 'rb'))
        MH0 = mat_cont['MH']
        MV0 = mat_cont['MV']
        dh0 = mat_cont['hor_dispersion']
        dv0 = mat_cont['ver_dispersion']
        Q0 = mat_cont['tunes']

    # vary + one dipole
    dKL = 0.0001
    K = ring[ind_quads[m]].PolynomB[1]
    ring[ind_quads[m]].PolynomB[1] = K + dKL/ring[ind_quads[m]].Length

    # get modified ORM
    ormq = f'./PlusQuad{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqp, MVqp, _, _, _, _, _, _, _, _, dhqp, dvqp, Qqp = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqp = mat_cont['MH']
        MVqp = mat_cont['MV']
        dhqp = mat_cont['hor_dispersion']
        dvqp = mat_cont['ver_dispersion']
        Qqp = mat_cont['tunes']

    # vary - one quadrupole
    ring[ind_quads[m]].PolynomB[1] = K - dKL/ring[ind_quads[m]].Length


    # get modified ORM
    ormq = f'./MinusQuad{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqm, MVqm, _, _, _, _, _, _, _, _, dhqm, dvqm, Qqm = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqm = mat_cont['MH']
        MVqm = mat_cont['MV']
        dhqm = mat_cont['hor_dispersion']
        dvqm = mat_cont['ver_dispersion']
        Qqm = mat_cont['tunes']

    # restore quadrupole
    ring[ind_quads[m]].PolynomB[1] = K

    # numeric derivative
    dDH_dQuadNorm_n = (dhqp - dhqm) / 2 / dKL

    # compute analytic equivalent
    print('start analytic dispersion derivative')
    start_time = time.time()
    dDH_dQuadNorm_a = semi_analytic_dispersion_variation_with_normal_quadrupole(
                                                            ring,
                                                           ind_bpms=ind_bpms,
                                                           ind_quads=[ind_quads[m]],
                                                           verbose=False)
    end_time = time.time()
    print(f'time for analytic dispersion derivative= {end_time - start_time} seconds')

    # modifications to make it equal to the numeric

    # plot analytic vs numeric
    fig, axch = plt.subplots(figsize=(16, 7))

    axch.plot(dDH_dQuadNorm_n, 'x-', label=f'numeric')
    axch.plot(dDH_dQuadNorm_a[:, 0], label=f'analytic')
    axch.bar(range(len(dDH_dQuadNorm_n)), dDH_dQuadNorm_n - dDH_dQuadNorm_a[:, 0], label=f'num - ana', alpha=0.5)
    axch.legend()
    axch.set_ylabel('hor. [m/Hz/$KL_{quad}$]')
    axch.set_xlabel('BPMs')
    axch.set_title(f'Quad: {ring[ind_quads[m]].FamName}')

    # correlation plot
    fig, axch = plt.subplots()
    axch.plot(dDH_dQuadNorm_n, dDH_dQuadNorm_a[:, 0], '.', label=f'numeric')
    axch.axis('equal')
    axch.set_ylabel('numeric')
    axch.set_xlabel('analytic')
    axch.set_title(f'Quad: {ring[ind_quads[m]].FamName}')

    fig.tight_layout()

    np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_analytic_dispH.txt', dDH_dQuadNorm_a[:, 0], fmt='%10.5f', delimiter=',')
    np.savetxt(f'Quad{ring[ind_quads[m]].FamName}_numeric_dispH.txt', dDH_dQuadNorm_n, fmt='%10.5f', delimiter=',')
    plt.show()

    pass


def _test_dispersion_deriv_skew(m=0, thick=False):

    import commissioningsimulations.config as config
    from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix
    import matplotlib.pyplot as plt
    from os.path import exists
    import pickle
    import time

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    ring.radiation_off()
    ring.radiation_on()  # cavity ON, radiation ON or dispersion will not compute
    mask_rf = at.get_cells(ring, 'Frequency')
    cav_state = int(ring[mask_rf][0].PassMethod == 'RFCavityPass')
    print(f'cavity is active?: {cav_state}')
    print(f'radiation is active?: {cav_state}')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, config.bpms_name_patter))

    # get quadrupole indexes
    # ind_quads = list(at.get_refpts(ring, config.skew_quadrupoles_fam_name_pattern))
    ind_quads = list(at.get_refpts(ring, config.normal_quadrupoles_fam_name_pattern))

    # check BETA at DL entrance vs Average beta DL
    _, _, l0 = ring.linopt6(refpts=ind_quads[m])
    ring.radiation_off()
    lave, bave, _, _, _, _, _ = at.avlinopt(ring, 0, ind_quads[m])
    ring.radiation_on()
    print(l0.beta)
    print(lave.beta)
    print(bave)

    # get reference ORM
    orm0 = './Reference.pkl'
    if not exists(orm0):
        MH0, MV0, _, _, _, _, _, _, _, _, dh0, dv0, Q0 = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=orm0)
    else:
        mat_cont = pickle.load(open(orm0, 'rb'))
        MH0 = mat_cont['MH']
        MV0 = mat_cont['MV']
        dh0 = mat_cont['hor_dispersion']
        dv0 = mat_cont['ver_dispersion']
        Q0 = mat_cont['tunes']

    # vary + one dipole
    dKL = 0.0001
    K = ring[ind_quads[m]].PolynomA[1]
    ring[ind_quads[m]].PolynomA[1] = K + dKL/ring[ind_quads[m]].Length

    # get modified ORM
    ormq = f'./PlusSkew{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqp, MVqp, _, _, _, _, _, _, _, _, dhqp, dvqp, Qqp = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqp = mat_cont['MH']
        MVqp = mat_cont['MV']
        dhqp = mat_cont['hor_dispersion']
        dvqp = mat_cont['ver_dispersion']
        Qqp = mat_cont['tunes']

    # vary - one quadrupole
    ring[ind_quads[m]].PolynomA[1] = K - dKL/ring[ind_quads[m]].Length

    # get modified ORM
    ormq = f'./MinusSkew{ring[ind_quads[m]].FamName}.pkl'
    if not exists(ormq):
        MHqm, MVqm, _, _, _, _, _, _, _, _, dhqm, dvqm, Qqm = \
            compute_orbit_response_matrix(ring,
                                          ind_bpms=ind_bpms,
                                          ind_cor=[],
                                          filename_cod_response=ormq)
    else:
        mat_cont = pickle.load(open(ormq, 'rb'))
        MHqm = mat_cont['MH']
        MVqm = mat_cont['MV']
        dhqm = mat_cont['hor_dispersion']
        dvqm = mat_cont['ver_dispersion']
        Qqm = mat_cont['tunes']

    # restore quadrupole
    ring[ind_quads[m]].PolynomA[1] = K

    # numeric derivative
    dDV_dQuadSkew_n = (dvqp - dvqm) / 2 / dKL

    # compute analytic equivalent
    if thick:
        thick_text = 'thick'
    else:
        thick_text = 'thin'

    print(f'start analytic {thick_text} dispersion derivative')
    start_time = time.time()
    dDV_dQuadSkew_a = analytic_dispersion_variation_with_skew_quadrupole(
                                                            ring,
                                                            ind_bpms=ind_bpms,
                                                            ind_skews=[ind_quads[m]],
                                                            thick=thick,
                                                            verbose=False)
    end_time = time.time()
    print(f'time for analytic dispersion derivative= {end_time - start_time} seconds')

    # modifications to make it equal to the numeric

    # plot analytic vs numeric
    fig, axch = plt.subplots(figsize=(16, 7))

    axch.plot(dDV_dQuadSkew_n, 'x-', label=f'numeric')
    axch.plot(dDV_dQuadSkew_a[:, 0], label=f'analytic {thick_text}')
    axch.bar(range(len(dDV_dQuadSkew_n)), dDV_dQuadSkew_n - dDV_dQuadSkew_a[:, 0], label=f'num - ana', alpha=0.5)
    axch.legend()
    axch.set_ylabel('hor. [m/Hz/$KL_{skew}$]')
    axch.set_xlabel('BPMs')
    axch.set_title(f'Skew: {ring[ind_quads[m]].FamName}')

    # correlation plot
    fig, axch = plt.subplots()
    axch.plot(dDV_dQuadSkew_n, dDV_dQuadSkew_a[:, 0], '.', label=f'numeric')
    axch.axis('equal')
    axch.set_ylabel('numeric')
    axch.set_xlabel('analytic  {thick_text}')
    axch.set_title(f'Skew: {ring[ind_quads[m]].FamName}')

    fig.tight_layout()

    np.savetxt(f'Skew{ring[ind_quads[m]].FamName}_analytic_{thick_text}_dispV.txt', dDV_dQuadSkew_a[:, 0], fmt='%10.5f', delimiter=',')
    np.savetxt(f'Skew{ring[ind_quads[m]].FamName}_numeric_dispV.txt', dDV_dQuadSkew_n, fmt='%10.5f', delimiter=',')
    plt.show()

    pass


if __name__ == '__main__':

#    _test_dispersion_deriv_quad(m=0)  # sharp (semi analytic)

    _test_dispersion_deriv_skew(m=0, thick=False)  # factor
    _test_dispersion_deriv_skew(m=0, thick=True)  # perfect

#    _test_dispersion_deriv_skew(m=100)   # factor
#    _test_dispersion_deriv_skew(m=200)   # factor

    pass