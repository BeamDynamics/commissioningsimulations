import at
from commissioningsimulations.get_indexes import get_lattice_indexes
import numpy
from typing import Optional
from at.lattice import Lattice, Refpts, set_value_refpts
from at.lattice import AtWarning, AtError
from at.physics import get_tune, get_chrom


def correct_chromaticty(reference_ring, ring_to_correct, indexes, HStep=None, jacobian=None):

    desired_chrom = at.get_chrom(reference_ring)
    initial_chrom = at.get_chrom(ring_to_correct)

    if not indexes:
        indexes = get_lattice_indexes(reference_ring)

    jacobian = fit_chrom(ring_to_correct,
                 indexes['focussing_sextupoles_for_chromaticity'],
                 indexes['defocussing_sextupoles_for_chromaticity'],
                 desired_chrom[0:2], HStep=HStep, jacobian=jacobian)

    final_chrom = at.get_chrom(ring_to_correct)

    print('chrom: initial {}'.format(initial_chrom))
    print('chrom: final {}'.format(final_chrom))
    print('chrom: desired {}'.format(desired_chrom))

    return ring_to_correct, jacobian


def _get_resp(ring: Lattice, index: int, func, refpts, attname,
              delta, dp, regex=False, **kwargs):
    set_value_refpts(ring, refpts, attname, delta, index=index,
                     increment=True, regex=regex)
    datap = func(ring, dp, **kwargs)
    set_value_refpts(ring, refpts, attname, -2 * delta, index=index,
                     increment=True, regex=regex)
    datan = func(ring, dp, **kwargs)
    set_value_refpts(ring, refpts, attname, delta, index=index,
                     increment=True, regex=regex)
    data = numpy.subtract(datap, datan) / (2 * delta)
    return data


def _fit_tune_chrom(ring: Lattice, index: int, func,
                    refpts1: Refpts, refpts2: Refpts, newval,
                    tol: Optional[float] = 1.0e-12,
                    dp: Optional[float] = 0, niter: Optional[int] = 3,
                    delta: Optional[float] = None,
                    regex=False, jacobian=None, **kwargs):
    def _fit(ring, index, func, refpts1, refpts2, newval, J,
             dp: Optional[float] = 0, regex=False, **kwargs):
        val = func(ring, dp, **kwargs)
        dk = numpy.linalg.solve(J, numpy.subtract(newval, val))
        set_value_refpts(ring, refpts1, 'PolynomB', dk[0], index=index,
                         increment=True, regex=regex)
        set_value_refpts(ring, refpts2, 'PolynomB', dk[1], index=index,
                         increment=True, regex=regex)
        val = func(ring, dp, **kwargs)
        sumsq = numpy.sum(numpy.square(numpy.subtract(val, newval)))
        return sumsq

    if delta is None:
        delta = 1.e-6 * 10 ** index

    if jacobian == None:
        dq1 = _get_resp(ring, index, func, refpts1, 'PolynomB',
                        delta, dp, regex=regex, **kwargs)
        dq2 = _get_resp(ring, index, func, refpts2, 'PolynomB',
                        delta, dp, regex=regex, **kwargs)
        J = [[dq1[0], dq2[0]], [dq1[1], dq2[1]]]
    else:
        J = jacobian

    n = 0
    sumsq = tol + 1
    print('Initial value', func(ring, dp, **kwargs))
    while sumsq > tol and n < niter:
        sumsq = _fit(ring, index, func, refpts1, refpts2, newval,
                     J, dp=dp, regex=regex, **kwargs)
        print('iter#', n, 'Res.', sumsq)
        n += 1
    print('Final value', func(ring, dp, **kwargs), '\n')

    return J


def _get_chrom(ring: Lattice, dp: float, **kwargs):
    return get_chrom(ring, dp=dp)[0:2]


def fit_chrom(ring: Lattice, refpts1: Refpts, refpts2: Refpts, newval,
              tol: Optional[float] = 1.0e-12,
              dp: Optional[float] = 0, niter: Optional[int] = 3, regex=False,
              HStep: Optional[float] = None,
              jacobian: Optional[list] = None,
              **kwargs) -> None:
    """Fit the chromaticities using 2 families

    Args:
        ring:       Lattice description
        refpts1:    Selection of the 1st family
        refpts2:    Selection of the 2nd family
        newval:     New tunes

    Keyword arguments:
        tol:        Tolerance for the matching; Default: 1.0e-12
        dp:         Momentum deviation. Default: 0
        niter:      Maximum number of iterations. Default 3
        regex:      Using regular expressions for refpt string matching;
                    Default: False
        HStep: gradient variation applied to magnets. Default 1e-4

    Typical usage:
    at.fit_chrom(ring, refpts1, refpts2, [10,5])
    """
    print('\nFitting Chromaticity...')
    J = _fit_tune_chrom(ring, 2, _get_chrom, refpts1, refpts2, newval, tol=tol,
                    dp=dp, niter=niter, regex=regex, delta=HStep, jacobian=jacobian, **kwargs)
    return J