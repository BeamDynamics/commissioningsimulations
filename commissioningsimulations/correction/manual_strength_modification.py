import at
from commissioningsimulations.get_indexes import get_lattice_indexes


def disable_multipole(ring, multipole_class, indexes=None, verbose=True):

    if not indexes:
        indexes = get_lattice_indexes(ring)

    ring.set_value_refpts(indexes[multipole_class], 'MaxOrder', 0)

    if verbose:
        print(f'{multipole_class} has been disabled')
    return ring


def enable_multipole(ring, multipole_class, indexes=None, fraction=None, reference_ring=None, verbose=True):

    if not indexes:
        indexes = get_lattice_indexes(ring)

    ord = None

    if multipole_class == 'dipoles' or multipole_class == 'correctors':
        ord = 0
    elif multipole_class == 'normal_quadrupoles' or multipole_class == 'skew_quadrupoles':
        ord = 1
    elif multipole_class == 'sextupoles':
        ord = 2
    elif multipole_class == 'octupoles':
        ord = 3

    if ord:
        ring.set_value_refpts(indexes[multipole_class], 'MaxOrder', ord)
    else:
        _mult_classes = ('dipoles', 'correctors', 'normal_quadrupoles', 'skew_quadrupoles', 'sextupoles', 'octupoles')
        print(f'not a correct multipole class. Chose among: {_mult_classes}. SKIP this step.')
        return ring

    if fraction != None:
        if reference_ring:
            pa = reference_ring.get_value_refpts(indexes[multipole_class], 'PolynomA', ord)
            pb = reference_ring.get_value_refpts(indexes[multipole_class], 'PolynomB', ord)
            ring.set_value_refpts(indexes[multipole_class], 'PolynomA', [_pa*fraction for _pa in pa], index=int(ord))
            ring.set_value_refpts(indexes[multipole_class], 'PolynomB', [_pb*fraction for _pb in pb], index=int(ord))
        else:
            print(f'fraction {fraction} may not be applied. Missing reference_ring optional input.')
            fraction = 1

    if verbose:
        if fraction:
            print(f'{multipole_class} have been enabled to {fraction*100}%')
        else:
            print(f'{multipole_class} have been enabled')

    return ring


if __name__ == '__main__':

    import commissioningsimulations.config as config
    from commissioningsimulations.get_indexes import get_lattice_indexes
    import matplotlib.pyplot as plt
    import copy

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    indexes = get_lattice_indexes(ring)

    rcor = copy.deepcopy(ring)

    K0 = ring.get_value_refpts(indexes['sextupoles'], 'PolynomB', 2)
    plt.plot(K0, 'x', label=' initial')

    frac = [1.0, 0.7, 0.3, .1]
    for f in frac:
        enable_multipole(rcor, 'sextupoles', indexes=indexes, fraction=f, reference_ring=ring)

        Kfrac = rcor.get_value_refpts(indexes['sextupoles'], 'PolynomB', 2)

        plt.plot(Kfrac, label=f'fraction {f*100}%')

    plt.xlabel('sextupoles')
    plt.ylabel('strengths 1/m2')
    plt.legend()
    plt.show()

    # test disable
    enable_multipole(rcor, 'sextupoles', indexes=indexes, fraction=1.0, reference_ring=ring)

    c0 = at.get_chrom(rcor)
    disable_multipole(rcor, 'sextupoles', indexes=indexes)
    c1 = at.get_chrom(rcor)

    print(f'initial chrom= {c0[0]:.2f}, {c0[1]:.2f}')
    print(f'sext OFF chrom= {c1[0]:.2f}, {c1[1]:.2f}')

    pass


