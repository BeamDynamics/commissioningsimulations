import at
import numpy as np
from itertools import repeat
import copy
from scipy.io import savemat, loadmat
from os.path import exists
import math
import time
from multiprocessing import Pool, Queue, Process


def OneSteererResponse(ring, dcorL, orb_ref, ic, verbose=True):

    # display status
    if verbose:
        print('computing response of steerer '
              '{} to COD'
              ''.format(ring[ic].FamName))  # , end="\r")

    L = ring[ic].Length

    if np.isnan(L):
        L = 1  # assume thin correctors

    # vary Horizontal corrector
    initcor = ring[ic].PolynomB[0]
    ring[ic].PolynomB[0] = initcor + dcorL / L

    # compute orbit with steerer
    _, orb_cor = at.find_orbit(ring, ind_bpms)

    # restore corrector
    ring[ic].PolynomB[0] = initcor

    # divide by deltacor
    # store in matrix
    H = (orb_cor[:, 0] - orb_ref[:, 0]) / dcorL

    # vary Vertical corrector
    initcor = ring[ic].PolynomA[0]
    ring[ic].PolynomA[0] = initcor + dcorL / L

    # compute orbit with steerer
    _, orb_cor = at.find_orbit(ring, ind_bpms)

    # restore corrector
    ring[ic].PolynomA[0] = initcor

    # divide by deltacor
    # store in matrix
    V = (orb_cor[:, 2] - orb_ref[:, 2]) / dcorL

    #    q.put((H, V))

    return H, V


def compute_orbit_response_matrix(
        ring,
        ind_bpms=None,
        ind_cor=None,
        dcorL=1e-6,
        verbose=True,
        filename_cod_response='./ORM.mat',
        use_mp=False):
    """
    numeric orbit response matrix

    :param ring:
    :param ind_bpms:
    :param ind_cor:
    :param dcorL:
    :param verbose:
    :param filename_cod_response:
    :return:
    """

    rad = ring.radiation
    ring.radiation_off()
    alpha_c = ring.get_mcf()
    L0 = ring.circumference

    if rad:  # if radiation was on, turn it back on.
        ring.radiation_on()

    tunes = at.get_tune(ring, get_integer=True)

    # compute reference orbit
    _, orb_ref = at.find_orbit(ring, ind_bpms)

    # dispersion "numeric" via RF shift
    drf = 100  # [Hz]
    RF0 = ring.get_rf_frequency()
    ring.set_rf_frequency(RF0+drf)
    _, orb_p_rf = at.find_orbit(ring, ind_bpms)
    ring.set_rf_frequency(RF0-drf)
    _, orb_m_rf = at.find_orbit(ring, ind_bpms)
    ring.set_rf_frequency(RF0) # restore RF frequency

    dh = (np.array(orb_p_rf[:, 0])-np.array(orb_m_rf[:, 0])) / (2*drf/RF0) * -alpha_c
    dv = (np.array(orb_p_rf[:, 2])-np.array(orb_m_rf[:, 2])) / (2*drf/RF0) * -alpha_c

    # initialize RM
    MH = np.zeros(shape=(len(orb_ref), len(ind_cor)))
    MV = np.zeros(shape=(len(orb_ref), len(ind_cor)))

    if use_mp:

        with Pool(processes=10) as p:
            results = p.starmap(OneSteererResponse, zip(repeat(ring), repeat(dcorL), repeat(orb_ref), ind_cor, repeat(verbose)))

        for count, ic in enumerate(ind_cor):
            MH[:, count] = results[count][0]
            MV[:, count] = results[count][1]

    else:
        for count, ic in enumerate(ind_cor):
            H, V =OneSteererResponse(ring, dcorL, orb_ref, ic, verbose=verbose)
            MH[:, count] = H
            MV[:, count] = V

    uH, sH, vH = np.linalg.svd(MH, full_matrices=True)
    uV, sV, vV = np.linalg.svd(MV, full_matrices=True)

    mdict = {'uH': uH, 'sH': sH, 'vH': vH,
             'uV': uV, 'sV': sV, 'vV': vV,
             'MH': MH, 'MV': MV,
             'ind_bpms': ind_bpms,
             'cors': ind_cor,
             'ring': ring,
             'hor_dispersion': dh,
             'ver_dispersion': dv,
             'tunes': tunes}

    savemat(filename_cod_response, mdict=mdict)
    print('ORM saved in: {}'.format(filename_cod_response))

    return MH, MV, uH, sH, vH, uV, sV, vV, dh, dv, tunes



if __name__ == '__main__':
    # load lattice
    ring = at.load_mat('/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/S28F_all_BM_27Mar2022/betamodel.mat',
                       mat_key='betamodel')

    # get BPM indexes
    ind_bpms = list(at.get_refpts(ring, 'BPM*'))

    # get correctors indexes
    ind_cor = list(at.get_refpts(ring, 'S[FDIJH]*'))

    compute_orbit_response_matrix(ring, ind_bpms=ind_bpms, ind_cor=ind_cor)