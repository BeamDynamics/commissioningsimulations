import pickle

import at
import at.plot
import copy
import time
import numpy as np
from os.path import exists
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.evaluation.get_optics_vs_reference import get_optics_vs_reference
from commissioningsimulations.correction.ClosedOrbit import compute_orbit_response_matrix, \
    correct_orbit, compute_analytic_average_orbit_response_matrix, compute_analytic_orbit_response_matrix
from commissioningsimulations.correction.Trajectory import correct_trajectory, compute_trajectory_response_matrix
from commissioningsimulations.correction.Chromaticity import correct_chromaticty
from commissioningsimulations.correction.Tune import correct_tune
from commissioningsimulations.correction.manual_strength_modification import enable_multipole, disable_multipole
from commissioningsimulations.correction.optics_coupling.fit_optics_model import fit_optics_model, compute_jacobian, \
    correct_optics_using_fitted_errors
import commissioningsimulations.correction.optics_coupling.resonance_driving_terms as cor_rdts
import commissioningsimulations.correction.optics_coupling.phase_dispersion as phase_dispersion

correction_name = ''

folder_data='.'

# example dict_of_corrections
dict_of_corrections = {'trajectory': {'response_file': folder_data + '/TRM' + correction_name + '.mat',
                                 'response_mode': 'numeric',
                                 'num_singular_vectors': 0,
                                 'number_of_turns': 1,
                                 'max_trajectory': (2e-3, 2e-3),
                                 'reference_trajectory': None,
                                 'check_for_cod_from_iteration': 0.0},
                       'orbit': {'response_file': folder_data + '/ORM' + correction_name + '.mat',
                                 'response_mode': 'analytic',
                                 'num_singular_vectors': 0,
                                 'num_iterations': 3,
                                 'reference_orbit': None},
                       'tune': {'KStep': 1e-9, 'response_file': folder_data + '/tuneJ' + correction_name + '.pkl'},
                       'chromaticity': {'HStep': 1e-7, 'response_file': folder_data + '/chromJ' + correction_name + '.pkl'},
                       'enable-multipole': {'multipole_class': 'sextuoples',
                                            'state': 'ON'}, # 'ON', 'OFF', float in range [0-1]
                       'manual-strength': {'ind':None, 'field':None, 'ind_in_field':None, 'new_value':None}, # 'ON', 'OFF', float in range [0-1]
                       'optic': {'mode': 'fitted_errors'},
                       'phase-dispersion': {'response_file': None,
                                            'tune_weight': 0.0,
                                            'hor_disp_weight': 0.0,
                                            'ver_disp_weight': 0.0,
                                            'verbose': True},
                       'errorsmodel': {'response_file': folder_data + '/ORMDeriv' + correction_name + '.mat',
                                  'response_mode': 'analytic',
                                  'num_normal_quad_singular_vectors': 0,
                                  'num_skew_quad_singular_vectors': 0,
                                  'bpms_weights': None,
                                  'hor_disp_weight': 1.0,
                                  'ver_disp_weight': 1.0,
                                  'tune_weight': 1.0,
                                  'number_of_iterations': 3
                                  }
                       }


def compute_correction_responses(ring,  # ideal lattice (for RM computation if needed)
                                 indexes=None,
                                 dict_of_corrections=dict_of_corrections,
                                 use_mp=False):
    """
    compute response matrices necessary for corrections in dict_of_corrections

    :param ring: AT lattice
    :param indexes: if None it is computed. dictionary of indexes from get_lattice_indexes
    :param dict_of_corrections: dictionary of corrections and correction parameters
           Example: dict_of_corrections = {'trajectory': {
                                 'response_file': folder_data + '/TRM' + correction_name + '.mat',
                                 'response_mode':'numeric',
                                 'num_singular_vectors':0,
                                 'max_trajectory': (2e-3, 2e-3)},
                       'orbit': {'response_file': folder_data + '/ORM' + correction_name + '.mat',
                                 'response_mode':'analytic',
                                 'num_singular_vectors':0,
                                 'num_iterations': 3},
                       'tune': [],
                       'chromaticity':[],
                       }
    :return: nothing. files are saved in the locations specified in dict_of_corrections
    """

    # get indexes
    if indexes == None:
        indexes = get_lattice_indexes(ring)

    # loop corrections list and compute response if needed
    for k in dict_of_corrections:

        # replace _# by nothing
        k_sel = copy.deepcopy(k)
        k = ''.join([i for i in k_sel if not i.isdigit()])
        k = k.replace(" ", "")

        print('computing response for {}'.format(k_sel))
        start_time = time.time()

        cor = dict_of_corrections[k_sel]

        if k == 'trajectory':

            if not (exists(cor['response_file'])):

                start_time = time.time()
                if cor['response_mode'] == 'numeric':

                    orbit_at_entrance_of_ref_lat, _ = at.find_orbit(ring, 0)

                    compute_trajectory_response_matrix(
                        ring,
                        ind_bpms=indexes['bpms'],
                        ind_cor=indexes['correctors'],
                        nturns=cor['number_of_turns'],
                        verbose=True,
                        use_mp=use_mp,
                        filename_response=cor['response_file'],
                        input_coordinates=orbit_at_entrance_of_ref_lat)

                print('{} Trajectory Response Matrix computation took: {} s'.format(
                    cor['response_mode'], time.time() - start_time))

            else:
                file_name = cor['response_file']
                print(f'Loaded: {file_name}')

        elif k == 'orbit':

            if not (exists(cor['response_file'])):

                start_time = time.time()
                if cor['response_mode'] == 'analytic':

                    """
                    compute_analytic_average_orbit_response_matrix(
                        ring,
                        ind_bpms=indexes['bpms'],
                        ind_cor=indexes['correctors'],
                        verbose=True,
                        filename_cod_response=cor['response_file'])
                    """
                    compute_analytic_orbit_response_matrix(
                        ring,
                        ind_bpms=indexes['bpms'],
                        ind_cor=indexes['correctors'],
                        verbose=True,
                        filename_cod_response=cor['response_file'])

                elif cor['response_mode'] == 'numeric':

                    compute_orbit_response_matrix(
                        ring,
                        ind_bpms=indexes['bpms'],
                        ind_cor=indexes['correctors'],
                        verbose=True,
                        use_mp=use_mp,
                        filename_cod_response=cor['response_file'])

                print('{} Orbit Response Matrix computation took: {} s'.format(cor['response_mode'],
                                                                               time.time() - start_time))
            else:
                file_name = cor['response_file']
                print(f'Loaded: {file_name}')

        elif k == 'tune':
            if 'response_file' not in cor:
                print('no tune RM, re-compute at each step based on lattice with errors and corrections')
            else:
                if not exists(cor['response_file']):
                    print('RM for tune')
                    _, J = correct_tune(ring, ring, indexes, KStep=cor['KStep'], jacobian=None)
                    cor['jacobian'] = J
                    pickle.dump({'J': J}, open(cor['response_file'], 'wb'))
                else:
                    print('RM for tune exists')

        elif k == 'chromaticity':
            if 'response_file' not in cor:
                print('no RM for chromticity, re-compute at each step based on lattice with errors and corrections')
            else:
                if not exists(cor['response_file']):
                    print('RM for chromticity')
                    _, J = correct_chromaticty(ring, ring, indexes, HStep=cor['HStep'], jacobian=None)
                    cor['jacobian'] = J
                    pickle.dump({'J': J}, open(cor['response_file'], 'wb'))
                else:
                    print('RM for chrom exists ')

        elif k == 'phase-dispersion':

            if not exists(cor['response_file']):
                start_time = time.time()

                phase_dispersion.compute_jacobian(ring,
                              indexes=indexes,
                              filename_response=cor['response_file'],
                              save_matlab_file=False,
                              mode='numeric',  # 'semi-analytic', 'analytic'
                              use_mp=use_mp,  # False, #
                              n_processes=None,
                              bpms_weights=None,
                              tune_weight=cor['tune_weight'],
                              hor_disp_weight=cor['hor_disp_weight'],
                              ver_disp_weight=cor['ver_disp_weight'],
                              verbose=cor['verbose'])  # all if None

                print('Jacobian computation took: {} s'.format(time.time() - start_time))

            else:
                file_name = cor['response_file']
                print(f'Loaded: {file_name}')

        elif k == 'errorsmodel':

            start_time = time.time()

            compute_jacobian(ring,
                             dict_of_fits_kwargs=cor['fit_params'],
                             indexes=indexes,
                             save_matlab_file=False,
                             use_mp=use_mp,
                             verbose=True)

            print('Jacobian computation took: {} s'.format(time.time() - start_time))

        elif k == 'optics':

            if not cor['mode'] == 'fitted_errors':  # if not fitted errors, assume RDTs
                start_time = time.time()

                if not (exists(cor['mode']['filename_response'])):
                    cor['mode'].pop('niter', None) # remove if it exist, not used
                    cor['mode'].pop('neigQuad', None) # remove if it exist, not used
                    cor['mode'].pop('neigSkew', None) # remove if it exist, not used
                    cor_rdts.compute_jacobian(ring, indexes=indexes, **cor['mode'])

                print('RDTs response computation took: {} s'.format(time.time() - start_time))

        elif k == 'resonance_driving_terms':

            start_time = time.time()

            # correct RDTs
            if not (exists(cor['filename_response'])):
                cor['mode'].pop('niter', None) # remove if it exist, not used
                cor['mode'].pop('neigQuad', None)  # remove if it exist, not used
                cor['mode'].pop('neigSkew', None)  # remove if it exist, not used
                cor_rdts.compute_jacobian(ring, indexes=indexes, **cor)

            print('RDTs response computation took: {} s'.format(time.time() - start_time))

        elif k == 'parallel_beam_based_alignment':
            start_time = time.time()
            print('... TO DO ...')

        elif k == 'beam_based_alignment':
            start_time = time.time()
            print('... TO DO ...')

        elif k == 'tune_from_trajectory':
            print('... TO DO ...')
        elif k == 'noeco':
            print('... TO DO ...')

        else:
            print(f'nothing to compute for correction {k}')

        end_time = time.time()
        print('time to compute RMs {} : {:.2f} s'.format(k, (end_time - start_time)))

    return


def correction_sequence(ring,  # ideal lattice (for RM computation if needed)
               rerr,  # lattice to be corrected
               indexes=None,
               dict_of_corrections=dict_of_corrections):
    """

    :param ring: AT lattice without errors and without corrections. used to compute reference tunes, chrom, etc...
    :param rerr: AT lattice with errors to be corrected
    :param indexes: if None it is computed. dictionary of indexes from get_lattice_indexes
    :param dict_of_corrections: dictionary of corrections and correction parameters
           Example: dict_of_corrections = {'trajectory': {
                                 'response_file': folder_data + '/TRM' + correction_name + '.mat',
                                 'response_mode':'numeric',
                                 'num_singular_vectors':0,
                                 'max_trajectory': (2e-3, 2e-3)},
                       'orbit': {'response_file': folder_data + '/ORM' + correction_name + '.mat',
                                 'response_mode':'analytic',
                                 'num_singular_vectors':0,
                                 'num_iterations': 3},
                       'tune': [],
                       'chromaticity':[],
                       }
    :return: rcor (AT lattice with same errors as rerr but corrected following the sequence described in dict_of_corrections
    """

    # make a copy of the input lattice to store corrected lattice
    rcor = at.Lattice(copy.deepcopy(rerr))

    # make a copy of the input lattice to store the fitted lattice model
    rfit = at.Lattice(copy.deepcopy(ring))

    # make a copy of the input reference ring
    rref = at.Lattice(copy.deepcopy(ring))

    # get indexes
    if indexes==None:
        indexes = get_lattice_indexes(ring)

    print('# BPMS: {}'.format(len(indexes['bpms'])))
    print('# correctors: {}'.format(len(indexes['correctors'])))
    print('# normal quad: {}'.format(len(indexes['normal_quadrupoles'])))
    print('# skew quad: {}'.format(len(indexes['skew_quadrupoles'])))
    print('# correctors (optics): {}'.format(len(indexes['correctors_for_optics_RM'])))
    print('# normal quad (optics): {}'.format(len(indexes['normal_quadrupoles_for_optics_and_coupling_fit'])))
    print('# skew quad (optics): {}'.format(len(indexes['skew_quadrupoles_for_optics_and_coupling_fit'])))

    # get input orbit for initial guess if not zero
    orbit_at_entrance_of_ref_lat, _ = at.find_orbit(ring, 0)

    # orbit_guess = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    orbit_guess = orbit_at_entrance_of_ref_lat

    # loop corrections list
    for k in dict_of_corrections:

        # replace _# by nothing
        k_sel = copy.deepcopy(k)
        k = ''.join([i for i in k_sel if not i.isdigit()])
        k = k.replace(" ", "")

        unstable_lattice = False
        check_stability = True

        try:

            print('- * -'*20)
            print('correcting {}'.format(k))
            print('- - -'*20)

            start_time = time.time()

            # save a lattice copy to be restored in case of correction failure
            rcor_init = copy.deepcopy((rcor))

            # get all relevant parameters of the present correction
            cor = dict_of_corrections[k_sel]

            if k == 'trajectory':
                if 'reference_trajectory' in cor.keys():
                    ref_traj = cor['reference_trajectory']
                else:
                    ref_traj = None

                if 'check_for_cod_from_iteration' in cor.keys():
                    check_cod = cor['check_for_cod_from_iteration']
                else:
                    check_cod = 0.0

                rcor, dch_err, dcv_err, input_coordinates_after_correction = \
                    correct_trajectory(rcor,
                                    indexes['bpms'],
                                    indexes['correctors'],
                                    nturns=cor['number_of_turns'],
                                    neig=cor['num_singular_vectors'],
                                    max_trajectory=cor['max_trajectory'],
                                    filename_response=cor['response_file'],
                                    reference_trajectory=ref_traj,
                                    input_coordinates=orbit_at_entrance_of_ref_lat,
                                    check_for_cod_from_iteration=check_cod)

                # update input coordinates guess
                orbit_guess = copy.deepcopy(input_coordinates_after_correction)
                orbit_guess.squeeze()

            elif k == 'orbit':
                if 'reference_orbit' in cor.keys():
                    ref_orb = cor['reference_orbit']
                else:
                    ref_orb = None

                rcor, dch_err, dcv_err, orbit_guess = \
                    correct_orbit(rcor,
                                  indexes['bpms'],
                                  indexes['correctors'],
                                  neig=cor['num_singular_vectors'],
                                  niter=cor['num_iterations'],
                                  filename_cod_response=cor['response_file'],
                                  zero_steerers_average=cor['zero_steerers_average'],
                                  reference_orbit=ref_orb,
                                  guess=orbit_guess.squeeze())

            elif k == 'tune':
                if 'KStep' not in cor:
                    cor['KStep'] = None
                J=None
                if 'response_file' in cor:
                    data=pickle.load(open(cor['response_file'], 'rb'))
                    J = data['J']

                rcor, _ = correct_tune(rref, rcor, indexes, KStep=cor['KStep'], jacobian=J)

            elif k == 'chromaticity':
                if 'HStep' not in cor:
                    cor['HStep'] = None
                J = None
                if 'response_file' in cor:
                    data = pickle.load(open(cor['response_file'], 'rb'))
                    J = data['J']

                rcor, _ = correct_chromaticty(rref, rcor, indexes, HStep=cor['HStep'], jacobian=J)

            elif k == 'phase-dispersion':

                rcor, _, _ = phase_dispersion.correct_phase_dispersion(rcor,
                                                               rref,
                                                               niter=cor['niter'],
                                                               neigQuad=cor['neigQuad'],
                                                               neigSkew=cor['neigSkew'],
                                                               bpms_weights=None,
                                                               hor_disp_weight=cor['hor_disp_weight'],
                                                               ver_disp_weight=cor['ver_disp_weight'],
                                                               tune_weight=cor['tune_weight'],
                                                               mode='numeric',
                                                               indexes=indexes,
                                                               filename_response=cor['response_file'],
                                                               reference_orm=None,
                                                               use_mp=False,
                                                               verbose=cor['verbose'],
                                                               normal=True,
                                                               skew=True)
            elif k == 'errorsmodel':

                # this action does not correct, it updates the fitted optics model.
                if 'assume_perfect_fit' not in cor:
                    cor['assume_perfect_fit'] = False

                if cor['assume_perfect_fit'] == True:
                    rfit = copy.deepcopy(rerr)
                else:
                    rfit = fit_optics_model(
                        rcor,
                        rfit,
                        indexes=indexes,
                        use_mp=cor['use_mp'],
                        verbose=cor['verbose'],
                        dict_of_fits_kwargs=cor['fit_params'])

            elif k == 'optics':

                # correct optics
                if cor['mode'] == 'fitted_errors':  # use fit values as correction gradients

                    rcor, _, _ = correct_optics_using_fitted_errors(rref, rfit, rcor, indexes=indexes)

                else:  # compute correction on model and apply it to the lattice with errors.

                    _, dKLn, dKLs = cor_rdts.correct_rdt_dispersion(rfit, rref, indexes=indexes, **cor['mode'])

                    ind_kn = indexes['normal_quadrupoles_for_optics_and_coupling_correction']
                    ind_ks = indexes['skew_quadrupoles_for_optics_and_coupling_correction']

                    Kn = rcor.get_value_refpts(ind_kn, 'PolynomB', index=1)
                    Ks = rcor.get_value_refpts(ind_ks, 'PolynomA', index=1)
                    Ln = rcor.get_value_refpts(ind_kn, 'Length')
                    Ln[Ln == 0] = 1.0  # thin correctors
                    Ls = rcor.get_value_refpts(ind_ks, 'Length')
                    Ls[Ls == 0] = 1.0  # thin correctors

                    rcor.set_value_refpts(ind_kn, 'PolynomB', Kn - dKLn/Ln, index=1)
                    rcor.set_value_refpts(ind_ks, 'PolynomA', Ks - dKLs/Ls, index=1)

                    print('After RDT+Disp.+Tune correction:')
                    get_optics_vs_reference(rref, rcor, indexes['bpms'])

                # reset fitted model to zero, or iterating this correction step the errors will add up.
                rfit = copy.deepcopy(rref)

            elif k == 'resonance_driving_terms':

                # correct RDTs as if we could measure them directly
                rcor, _, _ = cor_rdts.correct_rdt_dispersion(rcor, rref, **cor)

            elif k == 'parallel_beam_based_alignment':
                print('... TO DO ...')

            elif k == 'beam_based_alignment':
                print('... TO DO ...')

            elif k == 'tune_from_trajectory':
                print('... TO DO ...')

            elif k == 'noeco':
                print('... TO DO ...')

            elif k == 'manual-strengths':

                rcor = at.set_field_values(rcor, cor['ind'], cor['field'], cor['ind_in_field'], cor['new_value'])

            elif k == 'enable-multipole':

                check_stability=False # this action is not a real correction, lattice may be unstable

                if cor['state'] == 'ON':
                    enable_multipole(rcor, cor['multipole_class'], indexes=indexes)
                elif cor['state'] == 'OFF':
                    disable_multipole(rcor, cor['multipole_class'], indexes=indexes)
                elif isinstance(cor['state'], float):
                    frac = cor['state']
                    if frac>=0.0 and frac<=1.0:
                        enable_multipole(rcor, cor['multipole_class'], indexes=indexes,
                                         fraction=frac, reference_ring=ring)
                    else:
                        print(f'{frac} fraction is out of [0-1] range')
            else:
                raise Exception('{} is not a possible correction.'.format(k))

            end_time = time.time()
            print('time to correct {} : {:.2f} s'.format(k, (end_time-start_time)))

            # correction success, but final lattice is unstable

            # check if lattice is stable, if not, revert to previous correction or return lattice without correction
            if check_stability:
                try:
                    # _, o0 = at.find_orbit(rcor, indexes['bpms'], guess=tuple([o for o in orbit_guess[0]]))
                    # if any(o0) is None:
                    #     unstable_lattice = True
                    r_test = copy.deepcopy(rcor)  # make sure disable 6d has no effect on other steps
                    r_test.disable_6d()
                    opt, rd, _ = at.get_optics(rcor, indexes['bpms'], guess=tuple([o for o in orbit_guess.flatten()]))
                    del r_test
                    for o in opt.dtype.names:
                        # print(o)
                        # print(opt[o])
                        if any(opt[o].flatten() == None):
                            print(f'{o} has NaN: {any(opt[o].flatten() == None)}')
                            unstable_lattice = True
                except Exception as e:
                    print(e)
                    unstable_lattice = True

        except Exception as e:
            print(e)
            print(f'correction {k} failed')
            unstable_lattice = True

        if unstable_lattice:
            print(f'lattice is unstable after correction step: {k}')
            print('revert to previous step and stop correction now')
            rcor = rcor_init
            break

    return rcor, rfit, indexes


def get_correction_strengths(r, indexes=None):
    """
    get correction strengths for the indexes in indexes
    :param r:
    :param indexes: if None, compute them. dictionary of indexes from get_lattice_indexes
    :return: dictionary of correctors strengths at indexes, with keys: K1L, K1sL,K2L, K2sL,K3L, K3sL, K4L, K4sL
    """

    if indexes==None:
        indexes = get_lattice_indexes(r)

    correctors = {}
    correctors['K1L'] = at.get_value_refpts(r,
                                            indexes['correctors'],
                                            'PolynomB', 0) * \
                        at.get_value_refpts(r,
                                            indexes['correctors'],
                                            'Length')
    correctors['K1sL'] = at.get_value_refpts(r,
                                             indexes['correctors'],
                                             'PolynomA', 0) * \
                         at.get_value_refpts(r,
                                             indexes['correctors'],
                                             'Length')
    correctors['K2L'] = at.get_value_refpts(r,
                                            indexes['normal_quadrupoles'],
                                            'PolynomB', 1) * \
                        at.get_value_refpts(r,
                                            indexes['normal_quadrupoles'],
                                            'Length')
    correctors['K2sL'] = at.get_value_refpts(r,
                                             indexes['skew_quadrupoles'],
                                             'PolynomA', 1) * \
                         at.get_value_refpts(r,
                                             indexes['skew_quadrupoles'],
                                             'Length')
    correctors['K3L'] = at.get_value_refpts(r,
                                            indexes['sextupoles'],
                                            'PolynomB', 2) * \
                        at.get_value_refpts(r, indexes['sextupoles'],
                                            'Length')
    correctors['K3sL'] = at.get_value_refpts(r,
                                             indexes['sextupoles'],
                                             'PolynomA', 2) * \
                         at.get_value_refpts(r, indexes['sextupoles'],
                                             'Length')
    correctors['K4L'] = at.get_value_refpts(r,
                                            indexes['octupoles'],
                                            'PolynomB', 3) * \
                        at.get_value_refpts(r, indexes['octupoles'],
                                            'Length')
    correctors['K4sL'] = at.get_value_refpts(r,
                                             indexes['octupoles'],
                                             'PolynomA', 3) * \
                         at.get_value_refpts(r, indexes[ 'octupoles'],
                                             'Length')

    return correctors
