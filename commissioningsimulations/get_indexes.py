import at
import commissioningsimulations.config as config
import numpy as np
import warnings


def get_lattice_indexes(ring,
                        verbose=False
                        ):
    """
    compute lattice indexes matching given input strings for FamName search

    :param ring:
    :param steerers_search_string:
    :param quad_search_string:
    :param skew_search_string:
    :param sext_search_string:
    :param oct_search_string:
    :param bpms_search_string:
    :return: dictionary of indexes with keys:
    normal_quadrupoles,
    skew_quadrupoles,
    sextupoles,
    octupoles,
    bpms,
    correctors
    """
    indexes = {}

    def get_inds_list(strings):
        # string or list of strings

        # sort if string or list of strings
        if type(strings) is list:
            inds = np.array([])
            for s in strings:
                _ind = at.get_uint32_index(ring, s,
                                           regex=config.use_regexp_for_indexes)
                inds = np.concatenate((inds, _ind))

            refpts = np.array(np.sort(inds), dtype=np.uint32)
        else:
            refpts = at.get_uint32_index(ring, strings,
                                         regex=config.use_regexp_for_indexes)

        if len(refpts) == 0:
            warnings.warn(f'0 elements match {strings}')

        return np.array(refpts, dtype=np.uint32)

    # global indexes
    indexes['normal_quadrupoles'] = get_inds_list(config.normal_quadrupoles_fam_name_pattern)
    indexes['skew_quadrupoles'] = get_inds_list(config.skew_quadrupoles_fam_name_pattern)
    indexes['sextupoles'] = get_inds_list(config.sextupoles_fam_name_pattern)
    indexes['octupoles'] = get_inds_list(config.octupoles_fam_name_pattern)
    indexes['bpms'] = get_inds_list(config.bpms_name_patter)
    indexes['dipoles'] = get_inds_list(config.dipoles_fam_name_pattern)

    indexes['correctors'] = get_inds_list(config.correctors_fam_name_pattern)

    # subset for tune
    indexes['focussing_quadrupoles_for_tune'] = get_inds_list(config.focussing_quadrupoles_for_tune)
    indexes['defocussing_quadrupoles_for_tune'] = get_inds_list(config.defocussing_quadrupoles_for_tune)

    # subset for chrom
    indexes['focussing_sextupoles_for_chromaticity'] = get_inds_list(config.focussing_sextupoles_for_chromaticity)
    indexes['defocussing_sextupoles_for_chromaticity'] = get_inds_list(config.defocussing_sextupoles_for_chromaticity)

    # subset of indexes for optics/coupling fit
    indexes['correctors_for_optics_RM'] = get_inds_list(config.correctors_for_optics_RM)
    indexes['normal_quadrupoles_for_optics_and_coupling_fit'] = \
        get_inds_list(config.normal_quadrupoles_for_optics_and_coupling_fit)
    indexes['skew_quadrupoles_for_optics_and_coupling_fit'] = \
        get_inds_list(config.skew_quadrupoles_for_optics_and_coupling_fit)
    indexes['dipoles_for_optics_and_coupling_fit'] = get_inds_list(config.dipoles_for_optics_and_coupling_fit)

    # subset of indexes for optics/coupling correction
    indexes['normal_quadrupoles_for_optics_and_coupling_correction'] = \
        get_inds_list(config.normal_quadrupoles_for_optics_and_coupling_correction)
    indexes['skew_quadrupoles_for_optics_and_coupling_correction'] = \
        get_inds_list(config.skew_quadrupoles_for_optics_and_coupling_correction)

    if verbose:
        nq = len(indexes['dipoles'])
        print(f'dip: {nq} elements match {config.dipoles_fam_name_pattern}')
        nq = len(indexes['normal_quadrupoles'])
        print(f'norm quad: {nq} elements match {config.normal_quadrupoles_fam_name_pattern}')
        nq = len(indexes['skew_quadrupoles'])
        print(f'skew Quad: {nq} elements match {config.skew_quadrupoles_fam_name_pattern}')
        nq = len(indexes['sextupoles'])
        print(f'sext: {nq} elements match {config.sextupoles_fam_name_pattern}')
        nq = len(indexes['octupoles'])
        print(f'oct: {nq} elements match {config.octupoles_fam_name_pattern}')
        nq = len(indexes['correctors'])
        print(f'cor: {nq} elements match {config.correctors_fam_name_pattern}')
        nq = len(indexes['bpms'])
        print(f'bpms: {nq} elements match {config.bpms_name_patter}')
        nq = len(indexes['correctors_for_optics_RM'])
        print(f'steerers for optics: {nq} elements match {config.correctors_for_optics_RM}')
        nq = len(indexes['focussing_quadrupoles_for_tune'])
        print(f'QF for tune: {nq} elements match {config.focussing_quadrupoles_for_tune}')
        nq = len(indexes['defocussing_quadrupoles_for_tune'])
        print(f'QD for tune: {nq} elements match {config.defocussing_quadrupoles_for_tune}')
        nq = len(indexes['focussing_sextupoles_for_chromaticity'])
        print(f'SF for chrom: {nq} elements match {config.focussing_sextupoles_for_chromaticity}')
        nq = len(indexes['defocussing_sextupoles_for_chromaticity'])
        print(f'SD for chrom: {nq} elements match {config.defocussing_sextupoles_for_chromaticity}')
        nq = len(indexes['dipoles_for_optics_and_coupling_fit'])
        print(f'dipoles for optics: {nq} elements match {config.dipoles_for_optics_and_coupling_fit}')
        nq = len(indexes['normal_quadrupoles_for_optics_and_coupling_fit'])
        print(f'normal Quad for optics: {nq} elements match {config.normal_quadrupoles_for_optics_and_coupling_fit}')
        nq = len(indexes['skew_quadrupoles_for_optics_and_coupling_fit'])
        print(f'skew Quad for optics: {nq} elements match {config.skew_quadrupoles_for_optics_and_coupling_fit}')
        nq = len(indexes['normal_quadrupoles_for_optics_and_coupling_correction'])
        print(f'normal Quad for corr: {nq} elements match {config.normal_quadrupoles_for_optics_and_coupling_correction}')
        nq = len(indexes['skew_quadrupoles_for_optics_and_coupling_correction'])
        print(f'skew Quad for corr: {nq} elements match {config.skew_quadrupoles_for_optics_and_coupling_correction}')

    return indexes


def _test_get_indexes():

    # load lattice
    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    config.use_regexp_for_indexes = False

    indexes = get_lattice_indexes(ring, verbose=True)

    config.use_regexp_for_indexes = True

    config.sextupoles_fam_name_pattern = ['S(F|D|I|J).*(A|B|D|E)', 'SH.*']
    indexes = get_lattice_indexes(ring, verbose=True)

    # regex = fnmatch.translate('(?s:.*\\.txt)\\Z')
    # print(regex)
    pass


if __name__=='__main__':
    _test_get_indexes()
    pass