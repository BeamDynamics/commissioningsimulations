# import modules
import at
import os
import sys
from scipy.io import savemat
import pickle
import commissioningsimulations.config as config
from commissioningsimulations.correction.Sequence import compute_correction_responses
from commissioningsimulations.summary_figures import summary_figures
from commissioningsimulations.SingleSeed import set_errors_and_correct_one_seed
from commissioningsimulations.submit_to_slurm import submit_to_slurm
from commissioningsimulations.get_indexes import get_lattice_indexes

"""
    # define errors to set
    dict_of_errors = {'DL*': {'x': 100e-6,
                             'y': 100e-6,
                             'rot': 100e-6},
                      'Q*': {'x': 50e-6,
                             'y': 50e-6,
                             'rot': 100e-6},
                      'S*': {'x': 50e-6,
                             'y': 50e-6,
                             'rot': 100e-6},
                      }
    
    # define properties for each correction to run
    traj_params = {'response_file': os.path.join(os.path.abspath(fold_matrices), 'TRM' + correction_name + '.pkl'),
                   'response_mode': 'numeric',
                   'num_singular_vectors': 100,
                   'max_trajectory': (2.0e-3, 2.0e-3)}
    
    orb_params = {'response_file': os.path.join(os.path.abspath(fold_matrices), 'ORM' + correction_name + '.mat'),
                  'response_mode': 'analytic',
                  'num_singular_vectors': 120,
                  'num_iterations': 5}
    
    optics_params = {'response_file': os.path.join(os.path.abspath(fold_matrices), 'J' + correction_name + '.pkl'),
                    'response_mode': 'analytic',  # 'numeric',  #'semi-analytic',
                    'num_normal_quad_singular_vectors': 96,
                    'num_skew_quad_singular_vectors': 96,
                    'bpms_weights': None,
                    'hor_disp_weight': 0.0,
                    'ver_disp_weight': 0.0,
                    'tune_weight': 1.0,
                    'number_of_iterations': 4
                    }
    
    tune_traj_params = {}
    bba_params = {}
    noeco_params = {}
    
    # define list of corrections to run (space and digits are ignored to allow for different dictionary keys)
    dict_of_corrections = {'trajectory': traj_params,
                           'tune_from_trajectory': tune_traj_params,
                           'orbit 1': orb_params,
                           'parallel_beam_based_alignment': bba_params,
                           'tune 1': [],
                           'chromaticity 1': [],
                           'orbit 2': orb_params,
                           'optics': optics_params,
                           'orbit 3': orb_params,
                           'tune 2': [],
                           'chromaticity 2': [],
                           'noeco': noeco_params,
                           }
"""

import sys

class Logger(object):
    def __init__(self, f):
        self.terminal = sys.stdout
        self.log = f

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


def run_commissioning_simulations():
    """

    1. creates the required folders
    2. saves the configuration
    3. get indexes
    4. runs computations of required response matrices
    5. runs the computations in a series or submits the jobs to the slurm cluster
    6. produces figures of the final results

    :return: None
    """

    # get values from config.py module
    lattice_file = config.lattice_file_for_test
    ring_var_name = config.lattice_variable_name

    # create main folder for data
    try:
        os.mkdir(config.main_folder_data)
    except FileExistsError:
        print(f'Data folder {config.main_folder_data} already exsits')

    # create folder to store response matrices
    fold_matrices = os.path.join(config.main_folder_data, 'Matrices')
    try:
        os.mkdir(config.folder_matrices)
    except FileExistsError:
        print(f'Data folder {config.folder_matrices} already exsits')

    # save correction parameters/configuration
    correction_parameters_file = os.path.join(config.main_folder_data, 'CorrectionParameters.pkl')
    config.save_configuration_file(correction_parameters_file)

    # load starting, ideal lattice
    ring = at.load_lattice(lattice_file, mat_key=ring_var_name)

    # force radiation state if needed
    if config.radiation is not None:
        if config.radiation:
            ring.radiation_on()
            print('radiation on')
        else:
            ring.radiation_off()
            print('radiation off')
    else:
        print(f'radiation state is: {ring.radiation}')

    # get indexes of relevant elements in the lattice
    indexes = get_lattice_indexes(ring, verbose=True)  # this function is specific to each lattice.

    # compute required RM ahead
    compute_correction_responses(ring,  # ideal lattice (for RM computation if needed)
                                 indexes=None,
                                 dict_of_corrections=config.dict_of_corrections,
                                 use_mp=True)  # run parallel RM

    # run computations
    if config.submission_mode == 'local':  # parallel DA MA computation, serial seeds

        std_out_init = sys.stdout

        # loop all requested seeds (seed -1 is the design lattice)
        for seed in range(-1, config.N_seeds):

            # save output to file
            if seed >= 0:
                print('simulations for seed #{} out of {}'.format(seed, config.N_seeds))
                out_file_name = 'RunOutputSeed{:03}.out'.format(seed)
            else:
                print('simulations for Design lattice')
                out_file_name = 'RunOutputDesign.out'

            with open(os.path.join(config.main_folder_data, out_file_name), 'w') as f:
                # sys.stdout = f
                sys.stdout = Logger(f)

                set_errors_and_correct_one_seed(correction_parameters_file,
                                                seed,
                                                ring=ring,
                                                indexes=indexes)

                sys.stdout = std_out_init

    elif config.submission_mode == 'slurm':  # for large number of seeds: serial DA MA computation, parallel seeds

        submit_to_slurm(correction_parameters_file, config.main_folder_data, config.N_seeds)

    else:
        raise Exception(f'submission mode must be: local or slurm')

    # plot all seeds results stored in data.mat in summary graphs
    summary_figures(data_folder=config.main_folder_data, save_figures=True)

    pass
