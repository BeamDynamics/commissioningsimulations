import os
import pickle
import numpy as np
from scipy.io import loadmat
from matplotlib import pyplot as plt

plt.rcParams.update({'font.size': 10})

import commissioningsimulations.config as config


def one_seed_figures(data_folder='.', save_figures=True):
    
    l0 = pickle.load(open(os.path.join(data_folder, '..', 'Design', 'LatticeDesignData.pkl'), 'rb'))
    opt0 = l0['optics']
    s = np.array([x.s_pos for x in l0['optics']])

    mat_cont = pickle.load(open(os.path.join(data_folder, 'LatticeCorrectedData.pkl'), 'rb'))
    # datac.append(mat_cont['parameters'])
    optcor = mat_cont['optics']
    corc = mat_cont['correctors']
    mat_cont = pickle.load(open(os.path.join(data_folder, 'LatticeErrorData.pkl'), 'rb'))
    # datae.append(mat_cont['parameters'])
    opterr = mat_cont['optics']
    core = mat_cont['correctors']
    
    
    # plot beta-beating dispersion deviation and Delta COD
    fig, ax = plt.subplots(3, 2, figsize=(10, 7))
    fig.subplots_adjust(hspace=0.5, wspace=0.5)
    ax[0, 0].plot(s, [h[0]*1e6 for h in opterr.closed_orbit], label='before')
    ax[0, 0].plot(s, [h[0]*1e6 for h in optcor.closed_orbit], label='after')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 0].set_ylabel('hor. orbit \n [$\mu$m]')
    ax[0, 0].legend()
    ax[0, 1].plot(s, [h[2]*1e6 for h in opterr.closed_orbit], label='before')
    ax[0, 1].plot(s, [h[2]*1e6 for h in optcor.closed_orbit], label='after')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 1].set_ylabel('ver. orbit \n [$\mu$m]')
    
    ax[1, 0].plot(s, [(h[0]-h0[0])*1e3 for h, h0 in zip(opterr.dispersion, opt0.dispersion)], label='before')
    ax[1, 0].plot(s, [(h[0]-h0[0])*1e3 for h, h0 in zip(optcor.dispersion, opt0.dispersion)], label='after')
    ax[1, 0].legend()
    ax[1, 0].set_ylabel('hor. $\Delta\eta$ \n [mm]')
    ax[1, 1].plot(s, [(h[2]-h0[2])*1e3 for h, h0 in zip(opterr.dispersion, opt0.dispersion)], label='before')
    ax[1, 1].plot(s, [(h[2]-h0[2])*1e3 for h, h0 in zip(optcor.dispersion, opt0.dispersion)], label='after')
    ax[1, 1].set_ylabel('ver. $\Delta\eta$ \n [mm]')
    
    
    # ax[2, 0].plot([h[0] for h in opt0.beta], label='no rad')
    ax[2, 0].plot(s, [(h[0]-h0[0])/h0[0] *100 for h, h0 in zip(opterr.beta, opt0.beta)], label='before')
    ax[2, 0].plot(s, [(h[0]-h0[0])/h0[0] *100 for h, h0 in zip(optcor.beta, opt0.beta)], label='after')
    ax[2, 0].set_ylabel('hor. $\Delta\\beta / \\beta_0$ \n [%]')
    ax[2, 0].legend()
    ax[2, 0].set_xlabel('s [m]')
    # ax[2, 1].plot([h[1] for h in opt0.beta], label='no rad')
    ax[2, 1].plot(s, [(h[1]-h0[1])/h0[1] *100 for h, h0 in zip(opterr.beta, opt0.beta)], label='before')
    ax[2, 1].plot(s, [(h[1]-h0[1])/h0[1] *100 for h, h0 in zip(optcor.beta, opt0.beta)], label='after')
    ax[2, 1].set_ylabel('ver. $\Delta\\beta / \\beta_0$ \n [%]')
    ax[2, 1].set_xlabel('s [m]')
    
    plt.savefig('')
    
    if save_figures:
        plt.savefig(os.path.join(data_folder, 'OpticsBeforeAfterCorrection' + '.png'))
    plt.close()


    # plot correctors steerers, quad, skew, sext, oct

    fig, ax = plt.subplots(3, 2, figsize=(10, 7))
    fig.subplots_adjust(hspace=0.5, wspace=0.5)
    ax[0, 0].plot((corc['K1L'] - core['K1L'])*1e6, label='K1L')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 0].set_ylabel('hor. steerers \n [$\mu$rad]')
    ax[0, 0].legend()
    ax[0, 1].plot((corc['K1sL'] - core['K1sL'])*1e6, label='K1sL')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 1].set_ylabel('ver. steerers \n [$\mu$rad]')

    ax[1, 0].plot((corc['K2L'] - core['K2L'])*1e3, label='K2L')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[1, 0].set_ylabel('normal quad. \n [1e-3 1/m]')
    ax[1, 0].legend()
    ax[1, 1].plot((corc['K2sL'] - core['K2sL']), label='K2sL')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[1, 1].set_ylabel('skew quad. \n [1e-3 $m^-1$]')

    ax[2, 0].plot((corc['K3L'] - core['K3L']) * 1e3, label='K3L')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[2, 0].set_ylabel('normal sext. \n [1e-3 $m^-2$]')
    ax[2, 0].legend()
    ax[2, 1].plot((corc['K4L'] - core['K4L']), label='K4L')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[2, 1].set_ylabel('normal oct. \n [1e-3 $m^-3$]')

    plt.savefig('')

    if save_figures:
        plt.savefig(os.path.join(data_folder, 'Correctors' + '.png'))
    plt.close()


    return



if __name__ == '__main__':
    one_seed_figures(data_folder='/machfs/liuzzo/FCC/commissioningsimulations/'
                                 'demos/FCC_V22_z_ErrDQS_10um_noIP_Cor_optics_8cor/'
                                 'Seed001',
                     save_figures=True)
    pass