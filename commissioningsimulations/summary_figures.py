import os
import pickle
import numpy as np
from os.path import exists
from scipy.io import loadmat
from matplotlib import pyplot as plt
from commissioningsimulations.plots_for_one_seed import one_seed_figures
import at

plt.rcParams.update({'font.size': 12})


def summary_figures(data_folder='.', seed_folder='Seed', save_figures=True):
    """
    navigate in the subdirectory of data_folder to find the relevant files to build summary plots.

    figures produced are saved in the folder data_folder/SummaryFigures/*.png

    :param data_folder: main folder with data
    :param seed_folder: name of folder with data for 1 seed
    :return: nothing
    """

    try:
        os.mkdir(os.path.join(data_folder, 'SummaryFigures'))
    except FileExistsError:
        print('Summary figures folder already exists')

    list_seed_folders = [d for d in [x[0] for x in os.walk(data_folder)] if seed_folder in d]

    # stop if there are no seed folders
    if list_seed_folders == None:
        raise FileNotFoundError('{} do not exist in {}'.format(seed_folder, os.path.abspath(data_folder)))

    da = []
    dadp = []
    ma = []
    xya = []
    datae = []
    datac = []
    core = []
    corc = []
    opte = []
    optc = []
    comp = []
    # loop folders, load data and add it to the plots
    for c, s in enumerate(list_seed_folders):
        try:
            comp.append(pickle.load(open(os.path.join(s, 'ComparativeData.pkl'), 'rb')))
            mat_cont = pickle.load(open(os.path.join(s, 'LatticeCorrectedData.pkl'), 'rb'))
            datac.append(mat_cont['parameters'])
            optc.append(mat_cont['optics'])
            corc.append(mat_cont['correctors'])
            mat_cont = pickle.load(open(os.path.join(s, 'LatticeErrorData.pkl'), 'rb'))
            datae.append(mat_cont['parameters'])
            opte.append(mat_cont['optics'])
            core.append(mat_cont['correctors'])

            try:
                da.append(pickle.load(open(os.path.join(s, 'DACorrected.pkl'), 'rb')))
            except FileNotFoundError as fe:
                print('NO DA data in folder: {}'.format(s))

            try:
                dadp.append(pickle.load(open(os.path.join(s, 'DAdppCorrected.pkl'), 'rb')))
            except FileNotFoundError as fe:
                print('NO DAdpp data in folder: {}'.format(s))

            try:
                ma.append(pickle.load(open(os.path.join(s, 'MACorrected.pkl'), 'rb')))
            except FileNotFoundError as fe:
                print('NO MA and Lifetime data in folder: {}'.format(s))

            try:
                xya.append(pickle.load(open(os.path.join(s, 'AngAccCorrected.pkl'), 'rb')))
            except FileNotFoundError as fe:
                print('NO AngAcc data in folder: {}'.format(s))

            print('loaded data in: {}'.format(s))

            if not (exists(os.path.join(s, 'OpticsBeforeAfterCorrection' + '.png'))):
                one_seed_figures(data_folder=s)

        except Exception as e:  # folder is created, but data is not yet saved
            print(e)
            print('data.mat is not (yet) available in {}'.format(s))

    # count failed seeds
    eh = [d.__dict__['emittances'][0] for d in datac]
    failed_seeds = np.isnan(eh)
    number_failed_seeds = np.count_nonzero(failed_seeds)
    good_seeds = [not (f) for f in failed_seeds]

    # get sigmas
    mat_cont = pickle.load(open(os.path.join(data_folder, 'Design', 'LatticeDesignData.pkl'), 'rb'))
    params = mat_cont['parameters']
    # get global lattice parameters
    emittance_h = params.emittances[0]
    emittance_v = params.emittances[1]
    sigma_delta = params.sigma_e
    ring = at.load_lattice(os.path.join(data_folder, 'Design', 'LatticeDesign.mat'), mat_key='ring')
    l, _, _ = ring.linopt6()
    # l = mat_cont['optics'][0]
    sig_h = (emittance_h * l.beta[0] + (sigma_delta * l.dispersion[0]) ** 2) ** 0.5
    sig_v = (emittance_h / 1000 * l.beta[1] + (sigma_delta * l.dispersion[2]) ** 2) ** 0.5

    sig_hp = (emittance_h / l.beta[0] + (sigma_delta * l.dispersion[1]) ** 2) ** 0.5
    sig_vp = (emittance_h / 1000 / l.beta[1] + (sigma_delta * l.dispersion[3]) ** 2) ** 0.5

    # no DA/Lifetime data.
    figures_DA = True
    if len(da) == 0:
        print(f'No DA data to plot in {data_folder}')
        figures_DA = False

    figures_DAdpp = True
    if len(dadp) == 0:
        print(f'No DAdpp data to plot in {data_folder}')
        figures_DAdpp = False

    figures_MA = True
    if len(ma) == 0:
        print(f'No MA/LT data to plot in {data_folder}')
        figures_MA = False

    figures_XYA = True
    if len(xya) == 0:
        print(f'No Xacc Yacc data to plot in {data_folder}')
        figures_XYA = False

    if figures_MA:
        # design lattice data
        mm = pickle.load(open(os.path.join(data_folder, 'Design', 'MADesign.pkl'), 'rb'))

        # TLT histogram
        #  MATLAB --> #  tt = np.concatenate([d['tau_touschek'][0]/3600 for d in da])
        tt = np.array([d['touschek_lifetime'] / 3600 for d in ma])

        fig, ax = plt.subplots()
        if not(np.isnan(tt)).any(): # at least one non NaN value
            plt.hist(tt)
        mean_tt = np.mean(np.array(tt))
        plt.text(0.8, 0.9,
                 '<$\\tau$> = {:.2f} h \n $\\tau_0$ = {:.2f} h \n{} seeds \n{} failed'.format(
                     mean_tt, mm['touschek_lifetime'] / 3600,
                     len(list_seed_folders), number_failed_seeds),
                 ha='center', va='top', transform=ax.transAxes)
        plt.xlabel('Touschek lifetime [h]')
        plt.ylabel('counts')
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'LT' + '.png'), bbox_inches='tight')
        plt.close()

        # MA figure
        fig, ax_MA = plt.subplots()
        for c, d in enumerate(ma):
            print('plotting MA data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            plt.plot(d['ind_mom_acc'].T, d['momentum_acceptance'][:, 0] * 1e2,
                     ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label=lab)
            plt.plot(d['ind_mom_acc'].T, d['momentum_acceptance'][:, 1] * 1e2,
                     ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label='_')

        plt.plot(mm['ind_mom_acc'].T, mm['momentum_acceptance'][:, 0] * 1e2, '-', color=[0.0, 0.0, 0.7], label='Design')
        plt.plot(mm['ind_mom_acc'].T, mm['momentum_acceptance'][:, 1] * 1e2, '-', color=[0.0, 0.0, 0.7], label='_')
        ax_MA.legend()
        plt.xlabel('index in lattice')
        plt.ylabel('$\delta$ [%]')
        ax_MA.text(0.01, 0.6,
                   '6D tracking\nrad {}\n{} turns \n{} seeds'.format(mm['rad'] == 1,
                                                                     mm['n_turns'],
                                                                     len(list_seed_folders)),
                   ha='left', va='top',
                   transform=ax_MA.transAxes)
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'MA' + '.png'), bbox_inches='tight')
        plt.close()

    if figures_XYA:
        # design lattice data
        mm = pickle.load(open(os.path.join(data_folder, 'Design', 'AngAccDesign.pkl'), 'rb'))

        # sigma at acceptance location for reference lattice
        _, _, l = ring.linopt6(refpts=mm['ind_ang_acc'])
        sig_hp_ = [(emittance_h / bh + (sigma_delta * dxp) ** 2) ** 0.5 for bh, dxp in
                   zip([ll.beta[0] for ll in l], [ll.dispersion[1] for ll in l])]
        sig_vp_ = [(emittance_h / 1000 / bv + (sigma_delta * dyp) ** 2) ** 0.5 for bv, dyp in
                   zip([ll.beta[1] for ll in l], [ll.dispersion[3] for ll in l])]

        # Xacc YAcc figures over sigma xp py
        fig, (ax_XAcc, ax_YAcc) = plt.subplots(nrows=2)

        s = ring.get_s_pos(
            mm['ind_ang_acc'].T)  # at.get_s_pos(ring,indsMA) returns wrong size, (2,300) instead of (134,)

        for c, d in enumerate(xya):
            print('plotting AngAcc data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            ax_XAcc.plot(s, [m / y for m, y in zip(d['Xp_acceptance'][:, 0], sig_hp_)],
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label=lab)
            ax_XAcc.plot(s, [m / y for m, y in zip(d['Xp_acceptance'][:, 1], sig_hp_)],
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label='_')
            ax_YAcc.plot(s, [m / y for m, y in zip(d['Yp_acceptance'][:, 0], sig_vp_)],
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label=lab)
            ax_YAcc.plot(s, [m / y for m, y in zip(d['Yp_acceptance'][:, 1], sig_vp_)],
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label='_')

        ax_XAcc.plot(s, [m / y for m, y in zip(mm['Xp_acceptance'][:, 0], sig_hp_)], '.', color=[0.0, 0.0, 0.7],
                     label='Design')
        ax_XAcc.plot(s, [m / y for m, y in zip(mm['Xp_acceptance'][:, 1], sig_hp_)], '.', color=[0.0, 0.0, 0.7],
                     label='_')
        ax_YAcc.plot(s, [m / y for m, y in zip(mm['Yp_acceptance'][:, 0], sig_vp_)], '.', color=[0.0, 0.0, 0.7],
                     label='Design')
        ax_YAcc.plot(s, [m / y for m, y in zip(mm['Yp_acceptance'][:, 1], sig_vp_)], '.', color=[0.0, 0.0, 0.7],
                     label='_')
        plt.xlabel('s [m]')
        ax_XAcc.set_ylabel(f'xp/$\sigma(s)$ \n [$\sigma_{{IP}}$ = {sig_hp * 1e6:.2f} $\mu$rad]')
        ax_YAcc.set_ylabel(f'yp/$\sigma(s)$ \n [$\sigma_{{IP}}$ = {sig_vp * 1e6:.2f} $\mu$rad]')
        ax_XAcc.set_ylim([-50, 50])
        ax_YAcc.set_ylim([-200, 200])

        ax_XAcc.text(0.5, 0.6,
                     '6D tracking\nrad {}\n{} turns \n{} seeds'.format(mm['rad'] == 1,
                                                                       mm['n_turns'],
                                                                       len(list_seed_folders)),
                     ha='left', va='top',
                     transform=ax_XAcc.transAxes)

        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'AngAcc' + 'Sigma' + '.png'), bbox_inches='tight')
        plt.close()

        # XAcc YAcc figure
        fig, (ax_XAcc, ax_YAcc) = plt.subplots(nrows=2)
        for c, d in enumerate(xya):
            print('plotting AngAcc data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            ax_XAcc.plot(s, d['Xp_acceptance'][:, 0] * 1e6,
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label=lab)
            ax_XAcc.plot(s, d['Xp_acceptance'][:, 1] * 1e6,
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label='_')
            ax_YAcc.plot(s, d['Yp_acceptance'][:, 0] * 1e6,
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label=lab)
            ax_YAcc.plot(s, d['Yp_acceptance'][:, 1] * 1e6,
                         ':', linewidth=0.8, color=[0.3, 0.3, 0.3], label='_')

        ax_XAcc.plot(s, mm['Xp_acceptance'][:, 0] * 1e6, '-', color=[0.0, 0.0, 0.7], label='Design')
        ax_XAcc.plot(s, mm['Xp_acceptance'][:, 1] * 1e6, '-', color=[0.0, 0.0, 0.7], label='_')
        ax_YAcc.plot(s, mm['Yp_acceptance'][:, 0] * 1e6, '-', color=[0.0, 0.0, 0.7], label='Design')
        ax_YAcc.plot(s, mm['Yp_acceptance'][:, 1] * 1e6, '-', color=[0.0, 0.0, 0.7], label='_')
        plt.xlabel('s [m]')
        ax_XAcc.set_ylabel('xp [$\mu$rad]')
        ax_YAcc.set_ylabel('yp [$\mu$rad]')
        ax_XAcc.set_ylim([-100, 100])
        ax_YAcc.set_ylim([-100, 100])

        ax_XAcc.text(0.5, 0.6,
                     '6D tracking\nrad {}\n{} turns \n{} seeds'.format(mm['rad'] == 1,
                                                                       mm['n_turns'],
                                                                       len(list_seed_folders)),
                     ha='left', va='top',
                     transform=ax_XAcc.transAxes)

        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'AngAcc' + '.png'), bbox_inches='tight')
        plt.close()

    if figures_DA:
        dd = pickle.load(open(os.path.join(data_folder, 'Design', 'DADesign.pkl'), 'rb'))

        # DA figure
        fig_DA, ax_DA = plt.subplots()

        # DA errors + corrections
        for c, d in enumerate(da):
            print('plotting DA data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            plt.plot(d['border'][0, :] * 1e3, d['border'][1, :] * 1e3, 's:', linewidth=0.5, color=[0.3, 0.3, 0.3],
                     label=lab)
        plt.xlabel('x [mm]')
        plt.ylabel('y [mm]')
        # design DA
        ax_DA.plot(dd['border'][0, :] * 1e3, dd['border'][1, :] * 1e3, '-', label='Design')
        # info about simulations
        ax_DA.text(0.01, 0.99,
                   '6D tracking\nrad {}\n{} turns \n{} seeds \n{} failed'.format(dd['rad'] == 1,
                                                                                 dd['n_turns'],
                                                                                 len(list_seed_folders),
                                                                                 number_failed_seeds),
                   ha='left', va='top',
                   transform=ax_DA.transAxes)

        ax_DA.legend()
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'DA' + '.png'), bbox_inches='tight')
        plt.close()

        # DA figure
        fig_DA, ax_DA = plt.subplots()

        # DA errors + corrections
        for c, d in enumerate(da):
            print('plotting DA data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            plt.plot(d['border'][0, :] / sig_h, d['border'][1, :] / sig_v, 's:', linewidth=0.5, color=[0.3, 0.3, 0.3],
                     label=lab)
        plt.xlabel(f'x [sigmas = {sig_h * 1e6:.2f} $\mu$m]')
        plt.ylabel(f'y [sigmas = {sig_v * 1e6:.2f} $\mu$m $\epsilon_v=0.001\epsilon_h$]')
        # design DA
        ax_DA.plot(dd['border'][0, :] / sig_h, dd['border'][1, :] / sig_v, '-', label='Design')
        # info about simulations
        ax_DA.text(0.01, 0.99,
                   '6D tracking\nrad {}\n{} turns \n{} seeds \n{} failed'.format(dd['rad'] == 1,
                                                                                 dd['n_turns'],
                                                                                 len(list_seed_folders),
                                                                                 number_failed_seeds),
                   ha='left', va='top',
                   transform=ax_DA.transAxes)

        ax_DA.legend()
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'DA_sigmas' + '.png'), bbox_inches='tight')
        plt.close()

    if figures_DAdpp:
        pp = pickle.load(open(os.path.join(data_folder, 'Design', 'DAdppDesign.pkl'), 'rb'))

        # DAdpp figure
        fig_DA, ax_DA = plt.subplots()

        # DAdpp errors + corrections
        for c, d in enumerate(dadp):
            print('plotting DAdpp data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            plt.plot(d['border'][0, :] * 1e2, d['border'][1, :] * 1e3, 's:', linewidth=0.5, color=[0.3, 0.3, 0.3],
                     label=lab)
        plt.xlabel('$\delta$ [%]')
        plt.ylabel('max (x>0) [mm]')
        # design DA
        ax_DA.plot(pp['border'][0, :] * 1e2, pp['border'][1, :] * 1e3, '-', label='Design')
        # info about simulations
        ax_DA.text(0.01, 0.99,
                   '6D tracking\nrad {}\n{} turns \n{} seeds \n{} failed'.format(pp['rad'] == 1,
                                                                                 pp['n_turns'],
                                                                                 len(list_seed_folders),
                                                                                 number_failed_seeds),
                   ha='left', va='top',
                   transform=ax_DA.transAxes)

        ax_DA.legend()
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'DAdpp' + '.png'), bbox_inches='tight')
        plt.close()

        # DAdpp figure sigmas
        fig_DA, ax_DA = plt.subplots()

        # DAdpp errors + corrections
        for c, d in enumerate(dadp):
            print('plotting DAdpp data in: {}'.format(list_seed_folders[c]))
            lab = ''
            if c == 0:
                lab = 'Err. + Cor.'
            plt.plot(d['border'][0, :] * 1e2, d['border'][1, :] / sig_h, 's:', linewidth=0.5, color=[0.3, 0.3, 0.3],
                     label=lab)
        plt.xlabel('$\delta$ [%]')
        plt.ylabel(f'max (x>0) [sigmas = {sig_h * 1e6:.2f} $\mu$m]')
        # design DA
        ax_DA.plot(pp['border'][0, :] * 1e2, pp['border'][1, :] / sig_h, '-', label='Design')
        # info about simulations
        ax_DA.text(0.01, 0.99,
                   '6D tracking\nrad {}\n{} turns \n{} seeds \n{} failed'.format(pp['rad'] == 1,
                                                                                 pp['n_turns'],
                                                                                 len(list_seed_folders),
                                                                                 number_failed_seeds),
                   ha='left', va='top',
                   transform=ax_DA.transAxes)

        ax_DA.legend()
        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'DAdpp_sigmas' + '.png'), bbox_inches='tight')
        plt.close()

    # orbit histogram
    def hist_of_local_optics(type_plot='beta',
                             label_plot='$\\beta$-beating [%]',
                             name_of_figure='beta_vs_s',
                             hxlim=(0, 1000),
                             vxlim=(0, 1000),
                             hylim=(-100, 100),
                             vylim=(-100, 100)):

        l0 = pickle.load(open(os.path.join(data_folder, 'Design', 'LatticeDesignData.pkl'), 'rb'))
        s_pos = np.array([x.s_pos for x in l0['optics']])
        co = np.array([x.closed_orbit[0] for x in l0['optics']])

        nseed = len(opte)
        nbpm = len(co)

        bbhe = np.empty(shape=(nseed, nbpm))
        bbve = np.empty(shape=(nseed, nbpm))
        bbhc = np.empty(shape=(nseed, nbpm))
        bbvc = np.empty(shape=(nseed, nbpm))

        fig, (axh, axv) = plt.subplots(nrows=2)  # , figsize=(6.4,6.4) )
        fig.subplots_adjust(hspace=0.5)
        count = 0
        for lc, le in zip(optc, opte):

            if type_plot == 'beta':
                # beta
                beta_h_0 = np.array([x.beta[0] for x in l0['optics']])
                beta_h_e = np.array([x.beta[0] for x in le])
                beta_h_c = np.array([x.beta[0] for x in lc])

                bbhe[count, :] = (beta_h_e - beta_h_0) / beta_h_0 * 1e2  # %
                bbhc[count, :] = (beta_h_c - beta_h_0) / beta_h_0 * 1e2
                std_Dbeta_h_e = np.std(bbhe)
                std_Dbeta_h_c = np.std(bbhc)

                beta_v_0 = np.array([x.beta[1] for x in l0['optics']])
                beta_v_e = np.array([x.beta[1] for x in le])
                beta_v_c = np.array([x.beta[1] for x in lc])

                bbve[count, :] = (beta_v_e - beta_v_0) / beta_v_0 * 1e2  # %
                bbvc[count, :] = (beta_v_c - beta_v_0) / beta_v_0 * 1e2

            if type_plot == 'dispersion':
                # dispersion
                beta_h_0 = np.array([x.dispersion[0] for x in l0['optics']])
                beta_h_e = np.array([x.dispersion[0] for x in le])
                beta_h_c = np.array([x.dispersion[0] for x in lc])

                bbhe[count, :] = (beta_h_e - beta_h_0) * 1e3  # mm
                bbhc[count, :] = (beta_h_c - beta_h_0) * 1e3

                beta_v_0 = np.array([x.dispersion[2] for x in l0['optics']])
                beta_v_e = np.array([x.dispersion[2] for x in le])
                beta_v_c = np.array([x.dispersion[2] for x in lc])

                bbve[count, :] = (beta_v_e - beta_v_0) * 1e3  # mm
                bbvc[count, :] = (beta_v_c - beta_v_0) * 1e3

            std_Dbeta_h_e = np.std(bbhe)
            std_Dbeta_h_c = np.std(bbhc)
            std_Dbeta_v_e = np.std(bbve)
            std_Dbeta_v_c = np.std(bbvc)
            if count == 1:
                axh.plot(s_pos, bbhe[count, :], 'x', color='r', label='errors')
                axh.plot(s_pos, bbhc[count, :], 'o', color='b', label='corrected', fillstyle=None)
                axv.plot(s_pos, bbve[count, :], 'x', color='r', label='errors')
                axv.plot(s_pos, bbvc[count, :], 'o', color='b', label='corrected', fillstyle=None)
            else:
                axh.plot(s_pos, bbhe[count, :], 'x', color='r')
                axh.plot(s_pos, bbhc[count, :], 'o', color='b', fillstyle=None)
                axv.plot(s_pos, bbve[count, :], 'x', color='r')
                axv.plot(s_pos, bbvc[count, :], 'o', color='b', fillstyle=None)
            count += 1

        # axh.plot(np.mean(bbhe, axis=1), ':', color='r', label='errors')
        # axh.plot(np.mean(bbhc, axis=1), ':', color='r', label='corrected')
        # axv.plot(np.mean(bbve, axis=1), '-', color='b', label='errors')
        # axv.plot(np.mean(bbvc, axis=1), '-', color='b', label='corrected')
        axh.plot(s_pos, np.std(bbhe, axis=0), '-', color='m', label='std errors')
        axh.plot(s_pos, np.std(bbhc, axis=0), '-', color='c', label='std corrected')
        axv.plot(s_pos, np.std(bbve, axis=0), '-', color='m', label='std errors')
        axv.plot(s_pos, np.std(bbvc, axis=0), '-', color='c', label='std corrected')

        axh.legend()
        axh.set_ylabel('Hor. ' + label_plot)
        # axh.set_xlabel(f'#')
        axh.set_xlim(hxlim)
        axh.set_ylim(hylim)

        axv.legend()
        axv.set_ylabel('Ver. ' + label_plot)
        axv.set_xlabel(f's [m]')
        axv.set_xlim(vxlim)
        axv.set_ylim(vylim)
        print(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'))

        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'), bbox_inches='tight')
        plt.close()
        return fig, (axh, axv)

    ## beta plot
    hist_of_local_optics(type_plot='beta',
                         label_plot='$\\beta$-beating [%]',
                         name_of_figure='beta_vs_s',
                         hxlim=(0, 25000),
                         vxlim=(0, 25000),
                         hylim=(-100, 100),
                         vylim=(-100, 100))  # fails to plot, overflow error.
    hist_of_local_optics(name_of_figure='beta_vs_s_IP',
                         hxlim=(0, 100),
                         vxlim=(0, 100),
                         hylim=(-10, 10),
                         vylim=(-10, 10))  # fails to plot, overflow error.
    hist_of_local_optics(type_plot='dispersion',
                         label_plot='$\\eta$-$\\eta_0$ [mm]',
                         name_of_figure='eta_vs_s',
                         hxlim=(0, 25000),
                         vxlim=(0, 25000),
                         hylim=(-100, 100),
                         vylim=(-100, 100))
    hist_of_local_optics(type_plot='dispersion',
                         label_plot='$\\eta$-$\\eta_0$ [mm]',
                         name_of_figure='eta_vs_s_IP',
                         hxlim=(0, 100),
                         vxlim=(0, 100),
                         hylim=(-100, 100),
                         vylim=(-100, 100))

    # orbit histogram
    def hist_of_optics(type_plot='oo', label_plot='std(Closed Orbit) [$\mu$m]', name_of_figure='COD', hxlim=(0, 10),
                       vxlim=(0, 10)):

        '''
        # matlab
        h_e = np.concatenate([d[type_plot + '_h_e'][0] for d in comp])
        h_c = np.concatenate([d[type_plot + '_h_c'][0] for d in comp])
        v_e = np.concatenate([d[type_plot + '_v_e'][0] for d in comp])
        v_c = np.concatenate([d[type_plot + '_v_c'][0] for d in comp])
        '''
        h_e = np.array([d[type_plot + '_h_e'] for d in comp])
        h_c = np.array([d[type_plot + '_h_c'] for d in comp])
        v_e = np.array([d[type_plot + '_v_e'] for d in comp])
        v_c = np.array([d[type_plot + '_v_c'] for d in comp])

        mean_h_e = np.mean(np.array(h_e[good_seeds]))
        mean_h_c = np.mean(np.array(h_c[good_seeds]))
        mean_v_e = np.mean(np.array(v_e[good_seeds]))
        mean_v_c = np.mean(np.array(v_c[good_seeds]))
        n_bins = int(np.sqrt(len(h_e)))
        # binw = 1
        # n_bins = range(hxlim[0], hxlim[1]-binw, binw)

        fig, (axh, axv) = plt.subplots(nrows=2)  # , figsize=(6.4,6.4) )
        fig.subplots_adjust(hspace=0.5)
        axh.hist(h_c, bins=n_bins, label='<Corrected> = {:.2f}'.format(mean_h_c), ec=None, alpha=0.5)
        try:
            axh.hist(h_e, bins=n_bins, label='<errors> = {:.2f}'.format(mean_h_e),
                     fc=(255 / 255, 127 / 255, 0),
                     ec='black', alpha=0.5)
        except Exception as e:
            print(e)
            print('no data with errors')
        axh.legend()
        axh.set_xlabel('Hor. ' + label_plot)
        axh.set_ylabel(f'counts over {len(h_c)}')
        axh.set_xlim(hxlim)
        axv.hist(v_c, bins=n_bins, label='<Corrected> = {:.2f}'.format(mean_v_c), ec=None, alpha=0.5)
        try:
            axv.hist(v_e, bins=n_bins, label='<errors> = {:.2f}'.format(mean_v_e),
                     fc=(255 / 255, 127 / 255, 0),
                     ec='black', alpha=0.5)
        except Exception as e:
            print(e)
            print('no data with errors')
        axv.legend()
        axv.set_xlabel('Ver. ' + label_plot)
        axv.set_ylabel(f'counts over {len(h_c)}')
        axv.set_xlim(vxlim)

        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'), bbox_inches='tight')
        plt.close()
        return fig, (axh, axv)

    # close orbit histogram
    hist_of_optics(type_plot='oo', label_plot='std(Closed Orbit) [$\mu$m]', name_of_figure='COD',
                   hxlim=(0, 800), vxlim=(0, 800))
    # dispersion histogram
    hist_of_optics(type_plot='dd', label_plot='std(Dispersion) [mm]', name_of_figure='DISP',
                   hxlim=(0, 25), vxlim=(0, 25))
    # beta beating histogram
    hist_of_optics(type_plot='bb', label_plot='std($\\beta$-beating) [%]', name_of_figure='Beta',
                   hxlim=(0, 25), vxlim=(0, 25))

    # emittance histogram
    def hist_of_parameters(type_plot='emittance', units_scale=1.0, label_plot='$\epsilon$ [pm]', name_of_figure='EMIT',
                           hxlim=(0, 10), vxlim=(0, 10)):

        '''matlab
        h_e = np.array([d[type_plot][0][0][0][0]*units_scale for d in datae])
        h_c = np.array([d[type_plot][0][0][0][0]*units_scale for d in datac])
        v_e = np.array([d[type_plot][0][0][0][1]*units_scale for d in datae])
        v_c = np.array([d[type_plot][0][0][0][1]*units_scale for d in datac])
        '''

        h_e = np.array([d.__dict__[type_plot][0] * units_scale for d in datae])
        h_c = np.array([d.__dict__[type_plot][0] * units_scale for d in datac])
        v_e = np.array([d.__dict__[type_plot][1] * units_scale for d in datae])
        v_c = np.array([d.__dict__[type_plot][1] * units_scale for d in datac])

        mean_h_e = np.mean(np.array(h_e[good_seeds]))
        mean_h_c = np.mean(np.array(h_c[good_seeds]))
        mean_v_e = np.mean(np.array(v_e[good_seeds]))
        mean_v_c = np.mean(np.array(v_c[good_seeds]))
        n_bins = int(np.sqrt(len(h_e)))

        fig, (axh, axv) = plt.subplots(nrows=2)  # , figsize=(6.4,6.4) )
        fig.subplots_adjust(hspace=0.5)

        _, _b, _ = axh.hist(h_c, bins=n_bins, label='<Corrected> = {:.2f}'.format(mean_h_c), ec=None, alpha=0.5)
        try:
            axh.hist(h_e,
                     bins=_b,
                     label='<errors> = {:.2f}'.format(mean_h_e), ec=None, alpha=0.5)
        except Exception as e:
            print(e)
            print('no data with errors')

        axh.legend()
        axh.set_xlabel('Hor. ' + label_plot)
        axh.set_ylabel(f'counts over {len(h_c)}')
        # axh.set_xlim(hxlim)

        _, _b, _ = axv.hist(v_c, label='<Corrected> = {:.2f}'.format(mean_v_c), ec=None, alpha=0.5, zorder=2)
        try:
            axv.hist(v_e,
                     bins=_b,
                     label='<errors> = {:.2f}'.format(mean_v_e), ec=None, alpha=0.5, zorder=0)
        except Exception as e:
            print(e)
            print('no data with errors')

        axv.legend()
        axv.set_xlabel('Ver. ' + label_plot)
        axv.set_ylabel(f'counts over {len(h_c)}')
        # axv.set_xlim(vxlim)

        if save_figures:
            plt.savefig(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'), bbox_inches='tight')
        plt.close()

        return fig, (axh, axv)

    hist_of_parameters(type_plot='emittances', units_scale=1e12, label_plot='$\epsilon$ [pm]', name_of_figure='EMIT',
                       hxlim=(0, 20),
                       vxlim=(0, 10))

    hist_of_parameters(type_plot='tunes6', label_plot='Tune []', name_of_figure='TUNE',
                       hxlim=(0, 0.5),
                       vxlim=(0, 0.5))

    # correctors strengths histogram
    fig, ((ax1, axs1), (ax2, axs2)) = plt.subplots(nrows=2, ncols=2, figsize=(10.4, 8.4))  # )
    fig.subplots_adjust(hspace=0.7)

    mK1L = []
    mK1sL = []
    mK2L = []
    mK2sL = []

    for err, cor in zip(core, corc):
        mK1L.append(np.std(cor['K1L'] - err['K1L']) * 1e6)
        mK1sL.append(np.std(cor['K1sL'] - err['K1sL']) * 1e6)
        mK2L.append(np.std(cor['K2L'] - err['K2L']) * 1e6)
        mK2sL.append(np.std(cor['K2sL'] - err['K2sL']) * 1e6)

    ax1.hist(mK1L)
    ax1.set_xlabel('std(Hor. steerer) [$\mu$rad]')
    ax1.set_ylabel(f'counts / {len(list_seed_folders)}')
    axs1.hist(mK1sL)
    axs1.set_xlabel('std(Ver. steerer) [$\mu$rad]')
    axs1.set_ylabel(f'counts / {len(list_seed_folders)}')
    ax2.hist(mK2L)
    ax2.set_xlabel('std(normal quad. cor.) [1e-6 1/m]')
    ax2.set_ylabel(f'counts / {len(list_seed_folders)}')
    axs2.hist(mK2sL)
    axs2.set_xlabel('std(skew quad. cor.) [1e-6 1/m]')
    axs2.set_ylabel(f'counts / {len(list_seed_folders)}')

    if save_figures:
        plt.savefig(os.path.join(data_folder, 'SummaryFigures', 'correctors' + '' + '.png'), bbox_inches='tight')
    plt.close()

    pass


def recompute_optics_summary_figures(
        data_folder='.',
        seed_folder='Seed',
        save_figures=True):
    """
    navigate in the subdirectory of data_folder to find the relevant files to build summary plots.

    this method is slower has it loads lattice files and recomputes the optics

    figures produced are saved in the folder data_folder/SummaryFigures/*.png

    :param data_folder: main folder with data
    :param seed_folder: name of folder with data for 1 seed
    :return: nothing
    """

    try:
        os.mkdir(os.path.join(data_folder, 'SummaryFigures'))
    except FileExistsError:
        print('Summary figures folder already exists')

    list_seed_folders = [d for d in [x[0] for x in os.walk(data_folder)] if seed_folder in d]

    # stop if there are no seed folders
    if list_seed_folders == None:
        raise FileNotFoundError('{} do not exist in {}'.format(seed_folder, os.path.abspath(data_folder)))

    da = []
    ma = []
    datae = []
    datac = []
    core = []
    corc = []
    opte = []
    optc = []
    comp = []
    refpts = range(0, 500)
    # loop folders, load data and add it to the plots
    for c, s in enumerate(list_seed_folders):
        # if not (exists(lat_cor_name))
        try:
            rerr = at.load_lattice(os.path.join(s, 'LatticeErrors.mat'), mat_key='rerr')
            rcor = at.load_lattice(os.path.join(s, 'LatticeCorrected.mat'), mat_key='rcor')

            _, _, opte_ = rerr.linopt6(refpts=refpts)
            _, _, optc_ = rcor.linopt6(refpts=refpts)

            opte.append(opte_)
            optc.append(optc_)

            print('loaded data in: {}'.format(s))
        except Exception as e:  # folder is created, but data is not yet saved
            print(e)
            print('data.mat is not (yet) available in {}'.format(s))

    label_plot = '$\\beta$-beating [%]'
    name_of_figure = 'beta_vs_s_firstNelements'
    hxlim = (0, 600)
    vxlim = (0, 600)

    r0 = at.load_lattice(os.path.join(data_folder, 'Design', 'LatticeDesign.mat'), mat_key='ring')
    _, _, l0 = r0.linopt6(refpts=refpts)

    s_pos = np.array([x.s_pos for x in l0])
    co = np.array([x.closed_orbit[0] for x in l0])

    nseed = len(opte)
    nbpm = len(co)

    bbhe = np.empty(shape=(nseed, nbpm))
    bbve = np.empty(shape=(nseed, nbpm))
    bbhc = np.empty(shape=(nseed, nbpm))
    bbvc = np.empty(shape=(nseed, nbpm))

    fig, (axh, axv) = plt.subplots(nrows=2)  # , figsize=(6.4,6.4) )
    fig.subplots_adjust(hspace=0.5)
    count = 0
    for lc, le in zip(optc, opte):

        # beta
        beta_h_0 = np.array([x.beta[0] for x in l0])
        beta_h_e = np.array([x.beta[0] for x in le])
        beta_h_c = np.array([x.beta[0] for x in lc])

        bbhe[count, :] = (beta_h_e - beta_h_0) / beta_h_0 * 1e2
        bbhc[count, :] = (beta_h_c - beta_h_0) / beta_h_0 * 1e2

        beta_v_0 = np.array([x.beta[1] for x in l0])
        beta_v_e = np.array([x.beta[1] for x in le])
        beta_v_c = np.array([x.beta[1] for x in lc])

        bbve[count, :] = (beta_v_e - beta_v_0) / beta_v_0 * 1e2
        bbvc[count, :] = (beta_v_c - beta_v_0) / beta_v_0 * 1e2

        if count == 1:
            axh.plot(s_pos, bbhe[count, :], 'x', color='r', label='errors')
            axh.plot(s_pos, bbhc[count, :], '.', color='b', label='corrected', fillstyle=None)
            axv.plot(s_pos, bbve[count, :], 'x', color='r', label='errors')
            axv.plot(s_pos, bbvc[count, :], '.', color='b', label='corrected', fillstyle=None)
        else:
            axh.plot(s_pos, bbhe[count, :], 'x', color='r')
            axh.plot(s_pos, bbhc[count, :], '.', color='b', fillstyle=None)
            axv.plot(s_pos, bbve[count, :], 'x', color='r')
            axv.plot(s_pos, bbvc[count, :], '.', color='b', fillstyle=None)
        count += 1

    # axh.plot(np.mean(bbhe, axis=1), ':', color='r', label='errors')
    # axh.plot(np.mean(bbhc, axis=1), ':', color='r', label='corrected')
    # axv.plot(np.mean(bbve, axis=1), '-', color='b', label='errors')
    # axv.plot(np.mean(bbvc, axis=1), '-', color='b', label='corrected')
    axh.plot(s_pos, np.std(bbhe, axis=0), '-', color='m', label='std errors')
    axh.plot(s_pos, np.std(bbhc, axis=0), '-', color='c', label='std corrected')
    axv.plot(s_pos, np.std(bbve, axis=0), '-', color='m', label='std errors')
    axv.plot(s_pos, np.std(bbvc, axis=0), '-', color='c', label='std corrected')

    axh.legend()
    axh.set_ylabel('Hor. ' + label_plot)
    # axh.set_xlabel(f'#')
    axh.set_xlim(hxlim)

    axv.legend()
    axv.set_ylabel('Ver. ' + label_plot)
    axv.set_xlabel(f's [m]')
    axv.set_xlim(vxlim)
    print(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'))
    plt.show()

    if save_figures:
        plt.savefig(os.path.join(data_folder, 'SummaryFigures', name_of_figure + '' + '.png'), bbox_inches='tight')
    plt.close()
    return fig, (axh, axv)
