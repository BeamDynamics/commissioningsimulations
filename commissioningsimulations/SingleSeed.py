import os
import at
import sys
import time
import copy
import pickle
import numpy as np
from pprint import pprint
from os.path import exists
from matplotlib import pyplot as plt
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.errors.SetErrors import SetErrors, print_errors, print_ring_errors
from commissioningsimulations.correction.Sequence import correction_sequence
from commissioningsimulations.evaluation.compute_lattice_parameters import print_lattice_parameters, compute_lattice_parameters
from commissioningsimulations.evaluation.get_DA_MA_TLT import plot_DA_comparison, plot_mom_acc_comparison
from commissioningsimulations.evaluation.Sequence import evaluations_sequence
from commissioningsimulations.plots_for_one_seed import one_seed_figures
from commissioningsimulations.errors.sensitivity import sensitivity
from commissioningsimulations.errors.ErrorWaves import error_waves_sensitivity
from commissioningsimulations.evaluation.tapering_effect import plot_tapering_effect


def exception_handler(func):
    def inner_function(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            print(e)
            print(f"{func.__name__} failed for seed {args[1]}")
    return inner_function


#@exception_handler
def set_errors_and_correct_one_seed(correction_parameters_file,
                                    seed,
                                    ring=None,
                                    indexes=None):
    """
    sets errors and performs the correction of an SR lattice
    the errors are described by the dictionary dict_of_errors in file correction_parameters_file
    the corrections are described by the dictionary dict_of_corrections in file correction_parameters_file

    MORE...

    :param correction_parameters_file: a file saved by commissioningsimulations.config.save_configuration
    :param seed: seed to set random number generator to obtain errors
    :param ring: AT lattice without errors and without corrections. REFERENCE. if not give, use correction_parameters_file
    :param indexes: output of correction.get_lattice_indexes
    :return: nothing. all files are saved in an arborescence of foders below main_folder_data. if not give, compute them
    """

    # load input file (this is done like that because not all inputs to this script may be strings)
    # mat_content = loadmat(correction_parameters_file)
    '''
    mat_content = pickle.load(open(correction_parameters_file, 'rb'))
    lattice_file = mat_content['lattice_file']
    ring_var_name = mat_content['ring_var_name']
    main_folder_data = mat_content['main_folder_data']
    dict_of_evaluations = mat_content['dict_of_evaluations']
    dict_of_errors = mat_content['dict_of_errors']
    dict_of_corrections = mat_content['dict_of_corrections']
    DA_max_amplitude = mat_content['DA_max_amplitude']
    DA_n_turns = mat_content['DA_TL_n_turns']
    mom_acc_refpts = mat_content['mom_acc_refpts']
    compute_DA_LT = mat_content['compute_DA_LT']
    save_matlab_files = mat_content['save_matlab_files']
    save_figures = mat_content['save_figures']
'''

    # load configuration file (this allows use in slurm cluster)
    config.load_configuration_file(correction_parameters_file, config)

    print(f'List of configurations in: {correction_parameters_file}')
    # pprint(vars(config))

    # error if no DA/MA not in dict of
    if not('DA' in config.dict_of_evaluations):
        raise('DA missing from dict of evaluations')
    if not('MA' in config.dict_of_evaluations):
        raise('MA missing from dict of evaluations')

    # assign at.options.DConstants
    at.lattice.options.DConstant.OrbMaxIter = config.OrbMaxIter
    at.lattice.options.DConstant.DPStep = config.DPStep
    at.lattice.options.DConstant.XYStep = config.XYStep
    at.lattice.options.DConstant.OrbConvergence = config.OrbConvergence
    at.lattice.options.DConstant.patpass_poolsize = config.patpass_poolsize

    # define use of multiprocessing on the defined cores reserved by slurm
    use_mp = config.use_mp
    if use_mp and config.slurm_cpus_per_task > 1:
        at.DConstant.patpass_poolsize = config.slurm_cpus_per_task  # or it will use all the cores of the host, even if slurm has reserved only config.slurm_cpus_per_task
        print(f'using {config.slurm_cpus_per_task} cores')

    if ring == None:
        ring = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    if indexes == None:
        indexes = get_lattice_indexes(ring, verbose=True)

    # if there is a reference lattice specified, use it.
    if not (config.reference_lattice_file is None):
        print(f'Using reference lattice: {config.reference_lattice_file}')
        refring = at.load_lattice(config.reference_lattice_file, mat_key=config.reference_lattice_variable_name)
    else:
        print(f'Using reference lattice == test lattice')
        refring = copy.deepcopy(ring)

    # create a folder to store the data belonging to each seed
    folder_design = 'Design'
    if seed == -1:
        folder_seed = folder_design
    else:
        folder_seed = 'Seed{:03}'.format(seed)
    try:
        os.mkdir(os.path.join(config.main_folder_data, folder_seed))
    except (FileExistsError, PermissionError):
        print('Data folder {} already exsits'.format(folder_seed))

    # do not run any calculation if data already exists.
    data_name = os.path.join(config.main_folder_data, folder_seed, 'ComparativeData.pkl')
    if exists(data_name):
        print('data already exists for seed {}'.format(seed))
    else:
        if seed == -1:
            # save reference lattice in main_data_folder
            lat_design_name = os.path.join(config.main_folder_data, folder_design, 'LatticeDesign.mat')

            # NOT WORKING #
            at.save_lattice(refring, lat_design_name, mat_key='ring')
            pickle.dump({'ring': refring},
                        open(os.path.join(config.main_folder_data, folder_design, 'LatticeDesign.pkl'), 'wb'))

            # compute DA/LT/MA etc...
            if config.compute_DA_LT:
                print('Compute Design DA, TLT and lattice parameters')

                b, ma, tau, indsMA, b_dpp , inj_eff = evaluations_sequence(
                    refring,
                    config.dict_of_evaluations,
                    specname='Design',
                    use_mp=use_mp,
                    folder_data=os.path.join(config.main_folder_data, folder_design))

            # compute lattice patameter (always)
            compute_lattice_parameters(refring, indexes=indexes,
                                       save_matlab_file=config.save_matlab_files,
                                       file_name=os.path.join(config.main_folder_data,
                                                              folder_design, './LatticeDesignData.pkl'))

            # compute 2D sensitivity maps
            if config.compute_sensitivity:
                print('Compute Design errors sensitivity plots')
                if not (exists(os.path.join(config.main_folder_data, folder_design, 'hbbeatquad.png'))):
                    print('Quadrupole errors sensitivity plots')
                    sensitivity(r=refring,
                                Nseeds=3,
                                refpts=indexes['normal_quadrupoles'],
                                figure_label='quad',
                                datafolder=os.path.join(config.main_folder_data, folder_design))

                if not (exists(os.path.join(config.main_folder_data, folder_design, 'hbbeatsext.png'))):
                    print('Sextupole errors sensitivity plots')
                    sensitivity(r=refring,
                                Nseeds=3,
                                refpts=indexes['sextupoles'],
                                figure_label='sext',
                                datafolder=os.path.join(config.main_folder_data, folder_design))

                if not (exists(os.path.join(config.main_folder_data, folder_design, 'hbbeatWave.png'))):
                    print('DX/DY Wave errors sensitivity plots')
                    error_waves_sensitivity(r=refring,
                                            datafolder=os.path.join(config.main_folder_data, folder_design))
            else:
                print('skipped sensitivity comptuations for design lattice')

            if config.compute_survey:
                if not (exists(os.path.join(config.main_folder_data, folder_design, 'Survey.png'))):
                    print('compute survey')
                    _, _, ax = refring.plot_geometry()
                    plt.savefig(os.path.join(config.main_folder_data, folder_design, 'Survey' + '.png'), bbox_inches='tight')
                    os.path.join(config.main_folder_data, folder_design)

            if config.tapering_effect:
                if not (exists(os.path.join(config.main_folder_data, folder_design, 'tapering_effect.png'))):
                    print('compute tapering effect')
                    plot_tapering_effect(r=refring,
                                         datafolder=os.path.join(config.main_folder_data, folder_design))

        else:
            print('Assign errors for seed {}, correct, computed DA, TLT and lattice parameters'.format(seed))
            # set errors
            lat_err_name = os.path.join(config.main_folder_data, folder_seed, 'LatticeErrors.mat')
            if not (exists(lat_err_name)):  # do not repeat error setting if already done

                rerr = SetErrors(ring, config.dict_of_errors, seed=seed)

                print_errors(config.dict_of_errors,
                             file_data=os.path.join(config.main_folder_data, folder_seed, 'errors.txt'),
                             save_matlab_file=config.save_matlab_files)

                print_ring_errors(rerr, file_data=os.path.join(config.main_folder_data, folder_seed, 'Errors_by_Element.txt'))

                # save lattice with errors
                # NOT WOPRKING #
                at.save_lattice(rerr, lat_err_name, mat_key='rerr')

                # also PICKLE not working
                # with open(lat_err_name.replace('.mat','.pickle'), 'w') as handle:
                #    pickle.dump(rerr, handle, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump({'rerr': rerr}, open(os.path.join(config.main_folder_data, folder_seed, 'LatticeErrors.pkl'), 'wb'))

            else:
                print('lattice with errors for seed {} already exists'.format(seed))
                rerr = at.load_lattice(lat_err_name, mat_key='rerr')

            compute_lattice_parameters(rerr, indexes=indexes,
                                       save_matlab_file=config.save_matlab_files,
                                       file_name=os.path.join(config.main_folder_data, folder_seed, 'LatticeErrorData.pkl'))

            # perform correction
            lat_cor_name = os.path.join(config.main_folder_data, folder_seed, 'LatticeCorrected.mat')
            lat_fit_name = os.path.join(config.main_folder_data, folder_seed, 'LatticeFittedErrors.mat')

            if not (exists(lat_cor_name)):  # do not repeat error setting if already done

                rcor, rfit, _ = correction_sequence(refring,
                                                    rerr,
                                                    dict_of_corrections=config.dict_of_corrections,
                                                    indexes=indexes)

                # save lattice with errors and corrections
                # NOT WORKING #
                at.save_lattice(rcor, lat_cor_name, mat_key='rcor')
                pickle.dump({'rcor': rcor},
                            open(os.path.join(config.main_folder_data, folder_seed, 'LatticeCorrected.pkl'), 'wb'))

                # save fitted error model (if any)
                at.save_lattice(rfit, lat_fit_name, mat_key='rfit')
                pickle.dump({'rfit': rfit},
                            open(os.path.join(config.main_folder_data, folder_seed, 'LatticeFittedErrors.pkl'), 'wb'))

            else:
                print('corrected lattice for seed {} already exists'.format(seed))
                rcor = at.load_lattice(lat_cor_name, mat_key='rcor')

            compute_lattice_parameters(rcor, indexes=indexes,
                                       save_matlab_file=config.save_matlab_files,
                                       file_name=os.path.join(config.main_folder_data,
                                                              folder_seed, 'LatticeCorrectedData.pkl'))

            # compute DA
            if config.compute_DA_LT:
                try:
                    # errors

                    b_e, ma_e, tau_e, indsMA_e, b_dpp_e, inj_eff_e = evaluations_sequence(
                        rerr,
                        config.dict_of_evaluations,
                        specname='errors',
                        use_mp=use_mp,
                        folder_data=os.path.join(config.main_folder_data, folder_seed))

                except:
                    b_e = np.nan*np.zeros((2, 2))
                    ma_e = np.nan*np.zeros((2, 2))
                    tau_e = np.nan
                    indsMA_e = np.array([0, 1])

                try:                     
                    # errors + correction

                    b_c, ma_c, tau_c, indsMA_c, b_dpp_c, inj_eff_c = evaluations_sequence(
                        rcor,
                        config.dict_of_evaluations,
                        specname='Corrected',
                        use_mp=use_mp,
                        folder_data=os.path.join(config.main_folder_data, folder_seed))
                except:
                    b_c = np.nan * np.zeros((2, 2))
                    ma_c = np.nan * np.zeros((2, 2))
                    tau_c = np.nan
                    indsMA_c = np.array([0, 1])

                # load DA Design, or wait for it to be available
                if 'DA' in config.dict_of_evaluations:
                    while not(exists(os.path.join(config.main_folder_data, folder_design, 'DADesign.pkl'))):
                        print('Wait for Design DA data')
                        time.sleep(10)
                # load MA Design, or wait for it to be available
                if 'MA' in config.dict_of_evaluations:
                    while not(exists(os.path.join(config.main_folder_data, folder_design, 'MADesign.pkl'))):
                        print('Wait for Design MA data')
                        time.sleep(10)

                while not(exists(os.path.join(config.main_folder_data, folder_design, 'LatticeDesignData.pkl'))):
                    print('Wait for Design optics data')
                    time.sleep(10)

                # mat_content = loadmat(os.path.join(main_folder_data, folder_design, 'DATLTDesign.mat'))
                mat_content = pickle.load(open(os.path.join(config.main_folder_data, folder_design, 'DADesign.pkl'), 'rb'))
                b = mat_content['border']

                mat_content = pickle.load(
                    open(os.path.join(config.main_folder_data, folder_design, 'MADesign.pkl'), 'rb'))

                ma = mat_content['momentum_acceptance']
                tau = mat_content['touschek_lifetime']
                indsMA = mat_content['ind_mom_acc']

                # plot comparison of DAs for a given seed
                plot_DA_comparison(ring, b, b_e, b_c,
                                   save_figures=config.save_figures,
                                   folder_data= os.path.join(config.main_folder_data, folder_seed))

                # plot comparison of MAs for a given seed
                plot_mom_acc_comparison(ring, indsMA, indsMA_e, indsMA_c, ma, ma_e, ma_c, tau, tau_e, tau_c,
                                        save_figures=config.save_figures,
                                        folder_data=os.path.join(config.main_folder_data, folder_seed))

            # print results compared to design and save comparative data (ex beta-beating)
            print_lattice_parameters(data_file_r0=os.path.join(config.main_folder_data,
                                                               folder_design, 'LatticeDesignData.pkl'),
                                     data_file_r_errors=os.path.join(config.main_folder_data,
                                                                     folder_seed, 'LatticeErrorData.pkl'),
                                     data_file_r_corrected=os.path.join(config.main_folder_data,
                                                                        folder_seed, 'LatticeCorrectedData.pkl'),
                                     file_name= os.path.join(config.main_folder_data, folder_seed, 'ComparativeData.txt'))

            try:
                one_seed_figures(data_folder=os.path.join(config.main_folder_data, folder_seed))
            except Exception:
                print(f'could not produce plots for folder {s}')


    return


if __name__ == '__main__':

    # this is used to submit jobs to the slurm cluster

    correction_parameters_file = sys.argv[1]
    seed = int(sys.argv[2])

    # runs correction and saves data. if data exists, it skips.
    # parallelization is done on the seeds, not on DA/MA computations (can be turned on)

    set_errors_and_correct_one_seed(correction_parameters_file, seed)

    pass