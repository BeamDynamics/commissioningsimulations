import at
import numpy as np
import copy
from scipy.io import savemat
import pickle
import math

# example default list of errors
dict_of_errors = {'Q*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6},
                  'S*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6},
                  'BPM*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6,
                           'H_offset': 50e-6, 'V_offset': 50e-6, 'Rotation': 50e-6, 'Reading': 1e-6},
                  'COR*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6,
                           'H_KickAngleScale': 50e-6, 'V_KickAngleScale': 50e-6, 'KickAngleRotation': 50e-6},
                  'Wave': {'x': {'wavelengths': [1, 800], 'amplitudes': [50e-6, 1e-3]},
                           'y': {'wavelengths': [100, 400], 'amplitudes': [100e-6, 500e-3]},
                           'tilt': {'wavelengths': [1, 800], 'amplitudes': [50e-6, 1e-3]},
                           },
                  'Survey': {'s': [],
                             'x': [],
                             'y': []
                             }
                  }


def SetErrors(ring, dict_of_errors, seed=None):
    """
    Set errors in ring
    :param ring: AT lattice
    :param dict_of_errors: dictionary of dictionary of errors. Each key is a fam name search string.
            example:
            dict_of_errors = {'Q*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6},
                  'S*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6},
                  'BPM*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6,
                           'H_offset': 50e-6, 'V_offset': 50e-6, 'Rotation': 50e-6, 'Reading': 1e-6},
                  'COR*': {'x': 50e-6, 'y': 50e-6, 'tilt': 50e-6,
                           'H_KickAngleScale': 50e-6, 'V_KickAngleScale': 50e-6, 'KickAngleRotation': 50e-6},
                  }

            MORE....

    :param seed: seed for random number generator
    :return: rerr, AT lattice with errors
    """
    ### assign quadrupole alignment errors and store their values
    rerr = at.Lattice(copy.deepcopy(ring))
    n_elements = len(rerr)

    dr = np.zeros(n_elements)
    dx = np.zeros(n_elements)
    dy = np.zeros(n_elements)

    if seed is not None:
        # fix initial random errors seed
        np.random.seed(seed)

    for fam in dict_of_errors.keys():
        if (fam != 'Wave') and (fam != 'Survey'):
            # random errors
            ind = list(at.get_refpts(ring, fam))  # +list(at.get_refpts(ring, 'DQ*')) Not using DQ, atreduce not found in doc
            Nel = int(len(ind))

            for err in dict_of_errors[fam].keys():

                err_ampl = dict_of_errors[fam][err]

                if err == 'x':
                    dx[ind] = dx[ind] + err_ampl * np.random.randn(Nel)
                elif err == 'y':
                    dy[ind] = dy[ind] + err_ampl * np.random.randn(Nel)
                elif err == 'tilt':
                    dr[ind] = dr[ind] + err_ampl * np.random.randn(Nel)

        if fam == 'Wave':
            # wave error
            ind = list(range(len(ring)))  # define T1, T2, R1, R2 also in BPMs, Drifts, Markers.
            Nel = int(len(ind))
            s = at.get_s_pos(ring, ind)

            for err in dict_of_errors[fam].keys():

                err_ampl = dict_of_errors[fam][err]['amplitudes']
                err_wavelengths = dict_of_errors[fam][err]['wavelengths']

                err_wave = errors_wave(s, err_ampl, err_wavelengths)

                if err == 'x':
                    dx[ind] = dx[ind] + err_wave
                elif err == 'y':
                    dy[ind] = dy[ind] + err_wave
                elif err == 'tilt':
                    dr[ind] = dr[ind] + err_wave

        if fam == 'Survey':
            # survey errors

            ind = list(range(len(ring)))  # define T1, T2, R1, R2 also in BPMs, Drifts, Markers.
            Nel = int(len(ind))
            s = at.get_s_pos(ring, ind)

            # get survey table values
            s_surv = dict_of_errors[fam]['s']
            x_surv = dict_of_errors[fam]['x']
            y_surv = dict_of_errors[fam]['y']

            # interpolate survey data at every lattice element
            sinterp = [_s + d for _s, d in zip(s, np.array(range(Nel))*1e-15)]  # make all s monotonically increasing
            err_x_surv = np.interp(sinterp, s_surv, x_surv)
            err_y_surv = np.interp(sinterp, s_surv, y_surv)

            dx[ind] = dx[ind] + err_x_surv
            dy[ind] = dy[ind] + err_y_surv

    # apply errors
    at.set_shift(rerr, dx, dy)
    at.set_tilt(rerr, dr)

    return rerr


def print_errors(dict_of_errors, file_data='./errors.txt', save_matlab_file=False):
    """

    print dicitonary of errors and save a txt and mat file.

    :param dict_of_errors:
    :param file_data:
    :return:
    """
    with open(file_data, 'w') as f:

        for fam in dict_of_errors.keys():
            t = 'errors for: {}\n'.format(fam)
            print(t)
            f.write(t)
            for err in dict_of_errors[fam].keys():
                if fam != 'Survey' and fam != 'Wave':
                    t = '\t {} : {} um\n'.format(err, dict_of_errors[fam][err] * 1e6)
                elif fam == 'Wave':
                    t = '\t Wave std({}) : Amplitudes {} um\n'.format(err,
                                                                      np.std(dict_of_errors[fam][err]['amplitudes']))
                    t = t + '\t Wave std({}) : Wavelength {} um\n'.format(err, np.std(
                        dict_of_errors[fam][err]['wavelengths']))
                elif fam == 'Survey':
                    t = '\t Survey std({}) : {} um\n'.format(err, np.std(dict_of_errors[fam][err]))
                print(t)
                f.write(t)

    if save_matlab_file:
        savemat(file_data.replace('.txt', '.mat'), mdict={'errors': dict_of_errors})
    pickle.dump({'errors': dict_of_errors}, open(file_data.replace('.txt', '.pkl'), 'wb'))

    pass


def print_ring_errors(ring, file_data='./Errors_by_Element.txt'):
    """

    print errors in ring and save a txt and mat file.

    :param dict_of_errors:
    :param file_data:
    :return:
    """
    with open(file_data, 'w') as f:

        for el in ring:
            if hasattr(el, 'T2'):
                t = f'{el.FamName} \t {el.T2[0]} \t {el.T2[2]} \t {el.R2[1,2]}\n'
            else:
                t = f'{el.FamName} \t {0.0} \t {0.0} \t {0.0}\n'
            # print(t)
            f.write(t)

    pass


def errors_wave(s, amplitudes, wavelengths):

    # initialize wave array
    wave_s = np.zeros(s.shape)
    if type(wavelengths) is list:
        wavelengths = np.array(wavelengths)

    if type(amplitudes) is list:
        amplitudes = np.array(amplitudes)

    # give error if size is incorrect
    if amplitudes.shape != wavelengths.shape:
        raise ValueError(f'amplitudes ({amplitudes.shape}) and '
                         f'wavelengths ({wavelengths.shape}) must be the same shape')

    # sum wavelengths and corresponding amplitudes
    for a, w in zip(amplitudes, wavelengths):
        wave_s += np.array([a * math.sin(2 * math.pi * _s/w) for _s in s])

    return wave_s