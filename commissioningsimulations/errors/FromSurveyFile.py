import at
import numpy as np
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.errors.SetErrors import SetErrors
import copy
import multiprocessing
from itertools import repeat
import matplotlib.pyplot as plt
import matplotlib
import pickle
from os.path import exists
import os
from commissioningsimulations.correction.ClosedOrbit import correct_orbit, compute_analytic_orbit_response_matrix
import csv


def readSurveyFile(survey_file, survey_scale_factor=1.0, rotate_index=0):
    s = []
    x = []
    y = []

    with open(survey_file, 'r') as csvfile:
        surv = csv.reader(csvfile, delimiter=',')

        # skip header
        next(surv, None)

        for row in surv:
            s.append(float(row[0]))
            x.append(float(row[2]))
            y.append(float(row[3]))

    def rotate(l, n):
        return l[n:] + l[:n]

    # rotate data to start in a location that is close to zero for both planes, index 1290
    xx = rotate(x, rotate_index)
    yy = rotate(y, rotate_index)

    xs = [_x * survey_scale_factor for _x in xx]
    ys = [_x * survey_scale_factor for _x in yy]

    return s, xs, ys


def plot_errors(rerr):
    s = at.get_s_pos(rerr, range(len(rerr)))
    x = [el.T2[0] for el in rerr]
    y = [el.T2[2] for el in rerr]
    fig, ax = plt.subplots()
    ax.plot(s, [_x*1e3 for _x in x], label='x')
    ax.plot(s, [_y*1e3 for _y in y], label='y')
    ax.set_xlabel('s [m]')
    ax.set_ylabel('position [mm]')
    ax.set_title('in AT lattice elements')
    ax.legend()
    plt.show()
    return


def _test_survey_errors():

    import commissioningsimulations.config as config
    import csv
    import matplotlib.pyplot as plt

    config.lattice_file_for_test = '/machfs/liuzzo/FCC/74a/Errors/FCC_74a_diag.mat'
    config.lattice_variable_name = 'r'

    r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    rerr = at.Lattice(copy.deepcopy(r))

    survey_file = '/machfs/liuzzo/FCC/74a/FCC_LongRangeErrorsMH.csv'
    s, x, y = readSurveyFile(survey_file, rotate_index=1290)

    f, a = plt.subplots()
    a.plot(s, [_x*1e3 for _x in x], label='h')
    a.plot(s, [_x*1e3 for _x in y], label='v')
    a.legend()
    a.set_xlabel('s [m]')
    a.set_ylabel('position [mm]')
    a.set_title(f'from file {survey_file}')


    f, a = plt.subplots()
    a.plot([_x*1e3 for _x in x], label='h')
    a.plot([_x*1e3 for _x in y], label='v')
    a.legend()
    a.set_xlabel('index')
    a.set_ylabel('position [mm]')
    a.set_title(f'from file {survey_file}')


    # scale down to make them keep lattice stable
    scale_factors = [1e-3, 5e-3, 1e-2, 5e-2, 1e-1, 2e-1, 3e-1, 4e-1, 5e-1, 1.0]
    oh_scale = []
    ov_scale = []

    for scale_factor in scale_factors:

        xs = [_x * scale_factor for _x in xx]
        ys = [_x * scale_factor for _x in yy]

        try:
            # define errors
            dict_of_errors = {
                'Survey': {
                    's': s,
                    'x': xs,
                    'y': ys
                          }
            }

            # assign survey errors
            rerr = SetErrors(rerr, dict_of_errors)

            # plot errors
            # plot_errors(rerr)

            # get BPM indexess
            ind_bpms = list(at.get_refpts(r, config.bpms_name_patter))

            _, _, opt0 = at.linopt6(rerr, ind_bpms)

            # refernce orbit is the one of the misalignments
            x_ref = [el.T2[0] for el in rerr[ind_bpms]]
            y_ref = [el.T2[2] for el in rerr[ind_bpms]]

            reforb = np.zeros((len(ind_bpms), 6))
            reforb[:, 0] = x_ref
            reforb[:, 2] = y_ref

            # get correctors indexes
            ind_cor = list(at.get_refpts(r, config.correctors_fam_name_pattern))

            orm_file = './ORM' + 'analytic' + 'all' + 'thick' + '.pkl'
            if not exists(orm_file):
                compute_analytic_orbit_response_matrix(
                    r,
                    ind_bpms=ind_bpms,
                    ind_cor=ind_cor,
                    verbose=True,
                    filename_cod_response='./ORM' + 'analytic' + 'all' + 'thick' + '.pkl',
                    use_bpm_errors=False, thick_steerers=True)

            # run orbit correction
            rerr, dcH, dcV, orbit_0_guess = correct_orbit(rerr,
                                           ind_bpms,
                                           ind_cor,
                                           niter=1,
                                           neig=100,
                                           filename_cod_response=orm_file,
                                           zero_steerers_average=False,
                                           reference_orbit=reforb)


            # compute optics
            _, _, opterr = at.linopt6(rerr, range(len(rerr)))

            oh = [o.closed_orbit[0] - d for o, d in zip(opterr, xs)]
            ov = [o.closed_orbit[2] - d for o, d in zip(opterr, ys)]

            oh_scale.append(np.std(oh))
            ov_scale.append(np.std(ov))

        #try:
            print(f'try survey errors x {scale_factor}')
        except Exception:
            oh_scale.append(0.0)
            ov_scale.append(0.0)

    f, a = plt.subplots()
    a.plot(scale_factors, oh_scale, label='hor')
    a.plot(scale_factors, ov_scale, label='ver')
    a.set_xlabel('scale factor')
    a.set_xscale('log')
    a.set_ylabel('std(COD-Survey) [m]')
    a.grid()
    a.legend()
    plt.show()
    
    pass


if __name__ == '__main__':

    _test_survey_errors()

    pass
