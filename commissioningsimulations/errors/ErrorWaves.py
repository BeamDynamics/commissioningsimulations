import at
import numpy as np
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.errors.SetErrors import SetErrors
import copy
import multiprocessing
from itertools import repeat
import matplotlib.pyplot as plt
import matplotlib
import pickle
from os.path import exists
import os
from commissioningsimulations.correction.ClosedOrbit import correct_orbit, compute_analytic_orbit_response_matrix


def plot_errors(rerr):
    s = at.get_s_pos(rerr, range(len(rerr)))
    x = [el.T2[0] for el in rerr]
    y = [el.T2[2] for el in rerr]
    fig, ax = plt.subplots()
    ax.plot(s, x, label='x')
    ax.plot(s, y, label='y')
    ax.legend()
    # plt.show()
    return


def assign_error_get_optics(r, err, Wavelengths, DXY, Nseed, opt0, emit0, run_correct_orbit=False):
    cod = [[], []]
    bb = [[], []]
    deta = [[], []]
    demit = [[], []]
    print(f'{Nseed} seeds with rms alignment errors: ({Wavelengths[err]}, {DXY[err]})')
    for seed in range(Nseed):

        rerr = at.Lattice(copy.deepcopy(r))

        dict_of_errors = {'Wave': {
            'x': {'wavelengths': np.array([Wavelengths[err]]),
                  'amplitudes': np.array([DXY[err]])},
            'y': {'wavelengths': np.array([Wavelengths[err]]),
                  'amplitudes': np.array([DXY[err]])}
                                   }
                          }
        # assign wave errors
        rerr = SetErrors(rerr, dict_of_errors, seed=seed)

        # get expected reference orbit (T2[0], T2[2] at BPMs)
        opt0.closed_orbit[:, 0] += [el.T2[0] for el in rerr]
        opt0.closed_orbit[:, 2] += [el.T2[2] for el in rerr]

        print(f'{opt0.closed_orbit[0, :]} is the orbit at injection')

        # compute optics
        try:

            # correct orbit
            if run_correct_orbit:
                # get BPM indexes
                ind_bpms = list(at.get_refpts(r, config.bpms_name_patter))

                # get correctors indexes
                ind_cor = list(at.get_refpts(r, config.correctors_fam_name_pattern))

                orm_file = './ORM' + 'analytic' + 'all' + 'thick' + '.pkl'
                if not exists(orm_file):
                    compute_analytic_orbit_response_matrix(
                        r,
                        ind_bpms=ind_bpms,
                        ind_cor=ind_cor,
                        verbose=True,
                        filename_cod_response='./ORM' + 'analytic' + 'all' + 'thick' + '.pkl',
                        use_bpm_errors=False, thick_steerers=True)

                # run orbit correction
                rerr, dcH, dcV, _ = correct_orbit(rerr,
                                               ind_bpms,
                                               ind_cor,
                                               niter=3,
                                               neig=160,
                                               filename_cod_response=orm_file,
                                               zero_steerers_average=False,
                                               reference_orbit=opt0.closed_orbit[ind_bpms, :],
                                               guess=opt0.closed_orbit[0, :],
                                               verbose=False)

            # compute optics
            _, _, opterr = at.linopt6(rerr, range(len(rerr)))

            # get emittance
            radstate = rerr.is_6d
            if not rerr.is_6d:
                rerr.enable_6d()
            emit = rerr.envelope_parameters()
            if not radstate:
                rerr.disable_6d()

        except Exception:

            opterr = np.recarray(len(rerr),
                                      dtype=[('alpha', '<f8', (2,)),
                                             ('beta', '<f8', (2,)),
                                             ('mu', '<f8', (3,)),
                                             ('R', '<f8', (3, 6, 6)),
                                             ('A', '<f8', (6, 6)),
                                             ('dispersion', '<f8', (4,)),
                                             ('closed_orbit', '<f8', (6,)),
                                             ('M', '<f8', (6, 6)),
                                             ('s_pos', '<f8')])

            emit = at.RingParameters()
            emit.__dict__ = {'E0': np.nan,
                                     'U0': np.nan,
                                     'tunes6': [np.nan] * 3,
                                     'emittances': [np.nan] * 3,
                                     'J': [np.nan] * 3,
                                     'Tau': [np.nan] * 3,
                                     'sigma_e': np.nan,
                                     'sigma_l': np.nan,
                                     'voltage': np.nan,
                                     'phi_s': np.nan,
                                     'fs': np.nan
                                     }


        # save
        cod[0].append(np.std([(err.closed_orbit[0] - ref.closed_orbit[0]) * 1e6 for err, ref in zip(opterr, opt0)]))
        cod[1].append(np.std([(err.closed_orbit[2] - ref.closed_orbit[2]) * 1e6 for err, ref in zip(opterr, opt0)]))

        deta[0].append(np.std([(err.dispersion[0] - ref.dispersion[0]) * 1e3 for err, ref in zip(opterr, opt0)]))
        deta[1].append(np.std([(err.dispersion[2] - ref.dispersion[2]) * 1e3 for err, ref in zip(opterr, opt0)]))

        bb[0].append(np.std([(err.beta[0] - ref.beta[0]) / ref.beta[0] * 1e2 for err, ref in zip(opterr, opt0)]))
        bb[1].append(np.std([(err.beta[1] - ref.beta[1]) / ref.beta[1] * 1e2 for err, ref in zip(opterr, opt0)]))

        demit[0].append((emit.emittances[0] - emit0.emittances[0])*1e12)
        demit[1].append((emit.emittances[1] - emit0.emittances[1])*1e12)

        print(f'seed {seed} ({Wavelengths[err]}, {DXY[err]})' +
              f' cod: ({cod[0][-1]}, {cod[1][-1]})' +
              f' eta: ({deta[0][-1]}, {deta[1][-1]})' +
              f' bb: ({bb[0][-1]}, {bb[1][-1]})' +
              f' eta: ({demit[0][-1]}, {demit[1][-1]})')

    return np.mean(np.array(cod), axis=1), \
           np.mean(np.array(bb), axis=1), \
           np.mean(np.array(deta), axis=1), \
           np.mean(np.array(demit), axis=1)


def plot_contour(dx, dy, val, limit, figure_label, xlabel, ylabel, param_name, units, N, par_save_name, datafolder):

    V = np.array(val).reshape(len(dx), len(dy))

    _DX, _DY = np.meshgrid(dx, dy)

    fig, ax = plt.subplots()
    c = ax.pcolormesh(_DX, _DY, V,
                      norm=matplotlib.colors.LogNorm())
    fig.colorbar(c, ax=ax)
    cs = ax.contour(dx, dy, V)
    # labp = ax.plot(0, 0, label=f'<{param_name}>$_{N}$ {units}')
    ax.clabel(cs, inline=True, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(dx[0::3])
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_title(f'<{param_name}>$_{N}$ {units}')
    # ax.legend(handles=[labp])

    # draw contour lines at desired levels
    try:

        csl = ax.contour(dx, dy, V, levels=[limit], colors='k')
        ax.clabel(csl, inline=True, fontsize=12)
        p = csl.collections[0].get_paths()[0]
        if p:
            maxx = csl.collections[0].get_paths()[1].vertices[-1, 0]
            maxy = csl.collections[0].get_paths()[0].vertices[0, 1]

            #ax.set_title(figure_label +
            #             f'\n Wavelength = {maxx:1.2f} m for {param_name} = {limit} {units}' +
            #             f'\n Amplitude = {maxy*1e6:1.2f}$\mu$m for {param_name} = {limit} {units}')
        else:
            ax.set_title(figure_label + '\n ' + f'>{max(dx)}' + '\n ' + f'>{max(dy)}')

    except Exception as ex:
        print(ex)

    plt.savefig(os.path.join(datafolder, par_save_name + figure_label + '.png'))

    return


def error_waves_sensitivity(r=None,
                            Nseeds=1,
                            errors_range=1e-3,
                            points_in_range=(17, 17),
                            cod_level_um=(10, 10),
                            beta_level_perc=(0.1, 0.1),
                            eta_level_mm=(0.1, 0.1),
                            emit_level_pm=(0.01, 0.01),
                            run_correct_orbit=False,
                            figure_label='',
                            datafolder='./',
                            use_mp=True):

    if r is None:
        r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    # get inital optics
    _, _, opt0 = at.linopt6(r, range(len(r)))
    radstate = r.is_6d
    if not r.is_6d:
        r.enable_6d()
    emit0 = r.envelope_parameters()
    if not radstate:
        r.disable_6d()

    if run_correct_orbit:
        # get BPM indexes
        ind_bpms = list(at.get_refpts(r, config.bpms_name_patter))

        # get correctors indexes
        ind_cor = list(at.get_refpts(r, config.correctors_fam_name_pattern))

        orm_file = './ORM' + 'analytic' + 'all' + 'thick' + '.pkl'
        if not exists(orm_file):
            compute_analytic_orbit_response_matrix(
                r,
                ind_bpms=ind_bpms,
                ind_cor=ind_cor,
                verbose=True,
                filename_cod_response='./ORM' + 'analytic' + 'all' + 'thick' + '.pkl',
                use_bpm_errors=False, thick_steerers=True)

    # define errors
    # dw = np.linspace(r.circumference/len(r), r.circumference, points_in_range[0])
    #dw = np.array([r.circumference / 1.3 ** (x - 1) for x in np.linspace(1, points_in_range[0], points_in_range[0])])
    dw = np.array([r.circumference / x for x in np.linspace(1, points_in_range[0], points_in_range[0])])
    # dy = np.linspace(errors_range/100, errors_range, points_in_range[1])
    dy = errors_range * (10 ** np.linspace(-1.7, 1.7, points_in_range[1])) /10

    _DW, _DY = np.meshgrid(dw, dy)

    # flatten
    DW =[]
    for row in _DW:
        for el in row:
            DW.append(el)
    DY =[]
    for row in _DY:
        for el in row:
            DY.append(el)

    codh = np.zeros(len(DW)) * np.nan
    bbh = copy.deepcopy(codh)
    detah = copy.deepcopy(codh)
    demith = copy.deepcopy(codh)

    codv = copy.deepcopy(codh)
    bbv = copy.deepcopy(codh)
    detav = copy.deepcopy(codh)
    demitv = copy.deepcopy(codh)

    figure_label = 'Wave' + figure_label

    datafilename = os.path.join(datafolder, figure_label + 'data.pkl')

    if not(exists(datafilename)):

        # loop errors
        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            n_processes = n_cpu

            if True:
                print('parallel computation using {} cores'.format(n_processes))

            with multiprocessing.Pool() as p:
                results = p.starmap(assign_error_get_optics,
                                     zip(repeat(r),
                                         range(len(DW)),  # loop index
                                         repeat(DW),
                                         repeat(DY),
                                         repeat(Nseeds),
                                         repeat(opt0),
                                         repeat(emit0),
                                         repeat(run_correct_orbit))
                                     )

            for m, _ in enumerate(DW):

                codh[m] = results[m][0][0]  # cod
                bbh[m] = results[m][1][0]  # bb
                detah[m] = results[m][2][0]  # deta
                demith[m] = results[m][3][0]  # demit

                codv[m] = results[m][0][1]  # cod
                bbv[m] = results[m][1][1]  # bb
                detav[m] = results[m][2][1]  # deta
                demitv[m] = results[m][3][1]  # demit

        else:  # sequential
            for m, _ in enumerate(DW):
                results = assign_error_get_optics(r,
                    m,  # loop index
                    DW, DY, Nseeds, opt0, emit0, run_correct_orbit)

                codh[m] = results[0][0]  # cod
                bbh[m] = results[1][0]  # bb
                detah[m] = results[2][0]  # deta
                demith[m] = results[3][0]  # demit

                codv[m] = results[0][1]  # cod
                bbv[m] = results[1][1]  # bb
                detav[m] = results[2][1]  # deta
                demitv[m] = results[3][1]  # demit

        # save data
        m_dict = {'DW' : DW,
                  'DY' : DY,
                  'codh': codh,
                  'codv': codv,
                  'bbh': bbh,
                  'bbv': bbv,
                  'detah': detah,
                  'detav': detav,
                  'demith': demith,
                  'demitv': demitv,
                  }

        pickle.dump(m_dict, open(datafilename, 'wb'))

    else:
        # data exists, load file
        print(f'Data file {datafilename} already exists. Loading...')
        data = pickle.load(open(datafilename, 'rb'))
        codh = data['codh']
        codv = data['codv']
        bbh = data['bbh']
        bbv = data['bbv']
        detah = data['detah']
        detav = data['detav']
        demith = data['demith']
        demitv = data['demitv']



    # plot data
    xlabel = 'wavelength'
    if correct_orbit:
        ylabel = 'max. Amplitude. Errors in $\Delta$x, $\Delta$y. COD cor.'
    else:
        ylabel = 'max. Amplitude. Errors in $\Delta$x, $\Delta$y'

    try:
        plot_contour(dw, dy, codh, cod_level_um[0], figure_label, xlabel, ylabel,
                    'x', '$\mu$m', Nseeds, 'horb', datafolder)
        plot_contour(dw, dy, codv, cod_level_um[1], figure_label, xlabel, ylabel,
                    'y', '$\mu$m', Nseeds, 'vorb', datafolder)
    except ValueError:
        print('no orbit plots')

    try:
        plot_contour(dw, dy, detah, eta_level_mm[0], figure_label, xlabel, ylabel,
                     '$\Delta\eta_h$', '$mm$', Nseeds, 'hdisp', datafolder)
        plot_contour(dw, dy, detav, eta_level_mm[1], figure_label, xlabel, ylabel,
                     '$\Delta\eta_v$', '$mm$', Nseeds, 'vdisp', datafolder)
    except ValueError:
        print('no disperison plots')

    try:
        plot_contour(dw, dy, bbh, beta_level_perc[0], figure_label, xlabel, ylabel,
                     '$\Delta\\beta_h/\\beta_{h,0}$', '$\%$', Nseeds, 'hbbeat', datafolder)
        plot_contour(dw, dy, bbv, beta_level_perc[1], figure_label, xlabel, ylabel,
                     '$\Delta\\beta_v/\\beta_{v,0}$', '$\%$', Nseeds, 'vbbeat', datafolder)
    except ValueError:
        print('no beta beating plots')

    try:
        plot_contour(dw, dy, demith, emit_level_pm[0], figure_label, xlabel, ylabel,
                     '$\Delta\epsilon_h$', '$pmrad$', Nseeds, 'hemit', datafolder)
        plot_contour(dw, dy, demitv, emit_level_pm[1], figure_label, xlabel, ylabel,
                     '$\Delta\epsilon_v$', '$pmrad$', Nseeds, 'vemit', datafolder)
    except ValueError:
        print('no emittance plots')

    return codh, bbh, detah, demith, codv, bbv, detav, demitv


def _test_wave_errors():

    import commissioningsimulations.config as config

    r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    rerr = at.Lattice(copy.deepcopy(r))

    dict_of_errors = {'Wave': {
        'x': {'wavelengths': np.array([r.circumference / 3, r.circumference / 30]),
              'amplitudes': np.array([1e-4, 1e-5])},
        'y': {'wavelengths': np.array([r.circumference / 12, r.circumference / 2]),
              'amplitudes': np.array([1e-4, 1e-3])}
    }
    }

    # assign wave errors
    rerr = SetErrors(rerr, dict_of_errors)

    plot_errors(rerr)

    # get BPM indexes
    ind_bpms = list(at.get_refpts(r, config.bpms_name_patter))

    _, _, opt0 = at.linopt6(rerr, ind_bpms)

    # get correctors indexes
    ind_cor = list(at.get_refpts(r, config.correctors_fam_name_pattern))

    orm_file = './ORM' + 'analytic' + 'all' + 'thick' + '.pkl'
    if not exists(orm_file):
        compute_analytic_orbit_response_matrix(
            r,
            ind_bpms=ind_bpms,
            ind_cor=ind_cor,
            verbose=True,
            filename_cod_response='./ORM' + 'analytic' + 'all' + 'thick' + '.pkl',
            use_bpm_errors=False, thick_steerers=True)

    # run orbit correction
    rerr, dcH, dcV = correct_orbit(rerr,
                                   ind_bpms,
                                   ind_cor,
                                   niter=3,
                                   neig=100,
                                   filename_cod_response=orm_file,
                                   zero_steerers_average=True,
                                   reference_orbit=opt0.closed_orbit)


    # compute optics
    _, _, opterr = at.linopt6(rerr, range(len(rerr)))
    pass


if __name__ == '__main__':

    error_waves_sensitivity(use_mp=True,
                            run_correct_orbit=True,
                            errors_range=1e-3,
                            points_in_range=(15, 15))
    """
    _test_wave_errors()
    """

    pass
