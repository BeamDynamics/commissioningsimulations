import at
import numpy as np
import commissioningsimulations.config as config
from commissioningsimulations.get_indexes import get_lattice_indexes
import copy
import multiprocessing
from itertools import repeat
import matplotlib.pyplot as plt
import pickle
from os.path import exists
import os



def assign_error_get_optics(r, refpts, err, DX, DY, Nseed, opt0, emit0):
    cod = [[], []]
    bb = [[], []]
    deta = [[], []]
    demit = [[], []]
    print(f'{Nseed} seeds with rms alignment errors: ({DX[err]}, {DY[err]})')
    for seed in range(Nseed):
        rerr = at.Lattice(copy.deepcopy(r))
        # set seed
        np.random.seed(seed)

        # assign random errors at refpts
        n_elements = len(r)
        n_elements_err = len(refpts)

        dx = np.zeros(n_elements)
        dy = np.zeros(n_elements)

        dx[refpts] = np.random.normal(0.0, DX[err], n_elements_err)
        dy[refpts] = np.random.normal(0.0, DY[err], n_elements_err)

        at.set_shift(rerr, dx, dy)

        nan_envelope = at.RingParameters()
        nan_envelope.__dict__ = {'E0': np.nan,
                                 'U0': np.nan,
                                 'tunes6': [np.nan] * 3,
                                 'emittances': [np.nan] * 3,
                                 'J': [np.nan] * 3,
                                 'Tau': [np.nan] * 3,
                                 'sigma_e': np.nan,
                                 'sigma_l': np.nan,
                                 'voltage': np.nan,
                                 'phi_s': np.nan,
                                 'fs': np.nan
                                 }

        nan_linopt6 = np.recarray((len(rerr), ),
                                  dtype=[('alpha', '<f8', (2,)),
                                         ('beta', '<f8', (2,)),
                                         ('mu', '<f8', (3,)),
                                         ('R', '<f8', (3, 6, 6)),
                                         ('A', '<f8', (6, 6)),
                                         ('dispersion', '<f8', (4,)),
                                         ('closed_orbit', '<f8', (6,)),
                                         ('M', '<f8', (6, 6)),
                                         ('s_pos', '<f8')])


        # compute optics
        try:
            _, _, opterr = at.linopt6(rerr, range(len(rerr)))
        except at.lattice.utils.AtError as aterr:
            print(aterr)
            opterr = copy.deepcopy(nan_linopt6)

        #emit = rerr.envelope_parameters()
        radstate = rerr.is_6d
        if not rerr.is_6d:
            rerr.enable_6d()

        try:
            emit = rerr.envelope_parameters()
        except at.lattice.utils.AtError as aterr:
            print(aterr)
            emit = copy.deepcopy(nan_envelope)

        if not radstate:
            rerr.disable_6d()

        # save
        cod[0].append(np.std([(err.closed_orbit[0] - ref.closed_orbit[0]) * 1e6 for err, ref in zip(opterr, opt0)]))
        cod[1].append(np.std([(err.closed_orbit[2] - ref.closed_orbit[2]) * 1e6 for err, ref in zip(opterr, opt0)]))

        deta[0].append(np.std([(err.dispersion[0] - ref.dispersion[0]) * 1e3 for err, ref in zip(opterr, opt0)]))
        deta[1].append(np.std([(err.dispersion[2] - ref.dispersion[2]) * 1e3 for err, ref in zip(opterr, opt0)]))

        bb[0].append(np.std([(err.beta[0] - ref.beta[0]) / ref.beta[0] * 1e2 for err, ref in zip(opterr, opt0)]))
        bb[1].append(np.std([(err.beta[1] - ref.beta[1]) / ref.beta[1] * 1e2 for err, ref in zip(opterr, opt0)]))

        demit[0].append((emit.emittances[0] - emit0.emittances[0])*1e12)
        demit[1].append((emit.emittances[1] - emit0.emittances[1])*1e12)

        print(f'seed {seed} ({DX[err]}, {DY[err]})' +
              f' cod: ({cod[0][-1]}, {cod[1][-1]})' +
              f' eta: ({deta[0][-1]}, {deta[1][-1]})' +
              f' bb: ({bb[0][-1]}, {bb[1][-1]})' +
              f' eta: ({demit[0][-1]}, {demit[1][-1]})')

    return np.mean(np.array(cod), axis=1), \
           np.mean(np.array(bb), axis=1), \
           np.mean(np.array(deta), axis=1), \
           np.mean(np.array(demit), axis=1)


def plot_contour(dx, dy, val, limit, figure_label, xlabel, ylabel, param_name, units, N, par_save_name, datafolder):

    V = np.array(val).reshape(len(dx), len(dy))

    fig, ax = plt.subplots()
    cs = ax.contour(dx, dy, V)
    ax.clabel(cs, inline=True, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    ax.legend(f'<{param_name}>$_{N}$ {units}')

    # draw contour lines at desired levels
    try:

        csl = ax.contour(dx, dy, V, levels=[limit], colors='k')
        ax.clabel(csl, inline=True, fontsize=12)
        p = csl.collections[0].get_paths()[0]
        if p:
            maxx = csl.collections[0].get_paths()[1].vertices[-1, 0]
            maxy = csl.collections[0].get_paths()[0].vertices[0, 1]

            ax.set_title(figure_label +
                         f'\n $\Delta$ x = {maxx*1e6:1.2f}$\mu$m for {param_name} = {limit} {units}' +
                         f'\n $\Delta$ y = {maxy*1e6:1.2f}$\mu$m for {param_name} = {limit} {units}')
        else:
            ax.set_title(figure_label + '\n ' + f'>{max(dx)}' + '\n ' + f'>{max(dy)}')

    except Exception as ex:
        print(ex)

    plt.savefig(os.path.join(datafolder, par_save_name + figure_label + '.png'))

    # close figures
    plt.close('all')

    return


def sensitivity(r=None, refpts=None,
                Nseeds=10,
                errors_range=(1e-5, 1e-5),
                points_in_range=(5, 5),
                cod_level_um=(100, 100),
                beta_level_perc=(1, 1),
                eta_level_mm=(1, 0.5),
                emit_level_pm=(0.01, 0.01),
                figure_label='',
                datafolder='./',
                use_mp=True):

    if r is None:
        r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)

    if refpts is None:
        inds = get_lattice_indexes(r)
        refpts = inds['normal_quadrupoles']

    # get inital optics
    _, _, opt0 = at.linopt6(r, range(len(r)))
    radstate = r.is_6d
    if not r.is_6d:
        r.enable_6d()
    emit0 = r.envelope_parameters()
    if not radstate:
        r.disable_6d()

    # define errors
    dx = np.linspace(0, errors_range[0], points_in_range[0])
    dy = np.linspace(0, errors_range[1], points_in_range[1])

    _DX, _DY = np.meshgrid(dx, dy)

    # flatten
    DX =[]
    for row in _DX:
        for el in row:
            DX.append(el)
    DY =[]
    for row in _DY:
        for el in row:
            DY.append(el)

    codh = np.zeros(len(DX)) * np.nan
    bbh = copy.deepcopy(codh)
    detah = copy.deepcopy(codh)
    demith = copy.deepcopy(codh)

    codv = copy.deepcopy(codh)
    bbv = copy.deepcopy(codh)
    detav = copy.deepcopy(codh)
    demitv = copy.deepcopy(codh)

    datafilename = os.path.join(datafolder, figure_label + 'data.pkl')

    if not(exists(datafilename)):

        # loop errors
        if use_mp:

            n_cpu = multiprocessing.cpu_count()
            n_processes = n_cpu

            if True:
                print('parallel computation using {} cores'.format(n_processes))

            with multiprocessing.Pool() as p:
                results = p.starmap(assign_error_get_optics,
                                     zip(repeat(r),
                                         repeat(refpts),
                                         range(len(DX)),  # loop index
                                         repeat(DX),
                                         repeat(DY),
                                         repeat(Nseeds),
                                         repeat(opt0),
                                         repeat(emit0))
                                     )

            for m, _ in enumerate(DX):

                codh[m] = results[m][0][0]  # cod
                bbh[m] = results[m][1][0]  # bb
                detah[m] = results[m][2][0]  # deta
                demith[m] = results[m][3][0]  # demit

                codv[m] = results[m][0][1]  # cod
                bbv[m] = results[m][1][1]  # bb
                detav[m] = results[m][2][1]  # deta
                demitv[m] = results[m][3][1]  # demit

        else:  # sequential
            for m, _ in enumerate(DX):
                results = assign_error_get_optics(r,
                    refpts,
                    m,  # loop index
                    DX, DY, Nseeds, opt0, emit0)

                codh[m] = results[0][0]  # cod
                bbh[m] = results[1][0]  # bb
                detah[m] = results[2][0]  # deta
                demith[m] = results[3][0]  # demit

                codv[m] = results[0][1]  # cod
                bbv[m] = results[1][1]  # bb
                detav[m] = results[2][1]  # deta
                demitv[m] = results[3][1]  # demit

        # save data
        m_dict = {'codh': codh,
                  'codv': codv,
                  'bbh': bbh,
                  'bbv': bbv,
                  'detah': detah,
                  'detav': detav,
                  'demith': demith,
                  'demitv': demitv,
                  }

        pickle.dump(m_dict, open(datafilename, 'wb'))

    else:
        # data exists, load file
        print(f'Data file {datafilename} already exists. Loading...')
        data = pickle.load(open(datafilename, 'rb'))
        codh = data['codh']
        codv = data['codv']
        bbh = data['bbh']
        bbv = data['bbv']
        detah = data['detah']
        detav = data['detav']
        demith = data['demith']
        demitv = data['demitv']



    # plot data
    xlabel = '$\Delta$x @ refpts'
    ylabel = '$\Delta$y @ refpts'

    plot_contour(dx, dy, codh, cod_level_um[0], figure_label, xlabel, ylabel,
                 'x', '$\mu$m', Nseeds, 'horb', datafolder)
    plot_contour(dx, dy, codv, cod_level_um[1], figure_label, xlabel, ylabel,
                 'y', '$\mu$m', Nseeds, 'vorb', datafolder)

    plot_contour(dx, dy, detah, eta_level_mm[0], figure_label, xlabel, ylabel,
                 '$\Delta\eta_h$', '$mm$', Nseeds, 'hdisp', datafolder)
    plot_contour(dx, dy, detav, eta_level_mm[1], figure_label, xlabel, ylabel,
                 '$\Delta\eta_v$', '$mm$', Nseeds, 'vdisp', datafolder)

    plot_contour(dx, dy, bbh, beta_level_perc[0], figure_label, xlabel, ylabel,
                 '$\Delta\\beta_h/\\beta_{h,0}$', '$\%$', Nseeds, 'hbbeat', datafolder)
    plot_contour(dx, dy, bbv, beta_level_perc[1], figure_label, xlabel, ylabel,
                 '$\Delta\\beta_v/\\beta_{v,0}$', '$\%$', Nseeds, 'vbbeat', datafolder)

    plot_contour(dx, dy, demith, emit_level_pm[0], figure_label, xlabel, ylabel,
                 '$\Delta\epsilon_h$', '$pmrad$', Nseeds, 'hemit', datafolder)
    plot_contour(dx, dy, demitv, emit_level_pm[1], figure_label, xlabel, ylabel,
                 '$\Delta\epsilon_v$', '$pmrad$', Nseeds, 'vemit', datafolder)

    return codh, bbh, detah, demith, codv, bbv, detav, demitv


if __name__ == '__main__':
    sensitivity(use_mp=True)
    pass
