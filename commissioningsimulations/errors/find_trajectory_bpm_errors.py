import at
import numpy as np
from commissioningsimulations.errors.bpm_modifications import bpm_matrices, bpm_process


def find_trajectory(ring, refpts, input_coordinates=(0, 0, 0, 0, 0, 0), nturns=3):
    """
    returns a (6,nbpmxnturns) trajectory starting at given input_coordinates (default [0,0,0,0,0,0])

    :param ring:
    :param refpts:
    :param input_coordinates:
    :param nturns:
    :return: (6,nbpmxnturns) array
    """
    # one singke particle
    if len(input_coordinates) != 6:
        raise ValueError

    r_out = at.tracking.lattice_pass(ring,
                                   r_in=np.array([float(i) for i in input_coordinates]),
                                   refpts=refpts,
                                   nturns=nturns)

    # 6 x (nbpmxnturns) array
    trajectory = np.concatenate([r_out[:,0,:,t] for t in range(nturns)], axis=1)

    """
    plt.plot(trajectory[0, :], label='hor.')
    plt.plot(trajectory[2, :], label='hor.')
    plt.xlabel('BPM #')
    plt.legend
    plt.show()
    """

    return trajectory  # (6,NbpmxNturns)


def find_trajectory_bpm_errors(ring, ind_bpms,
                               input_coordinates=(0, 0, 0, 0, 0, 0),
                               nturns=3,
                               rel=None, tel=None, trand=None):

    t2d = np.zeros((2, int(nturns), len(ind_bpms)))

    if (rel is None) or (tel is None) or (trand is None):
        rel, tel, trand = bpm_matrices(ring, ind_bpms)

    coord = at.tracking.lattice_pass(ring,
                                     r_in=np.array([float(i) for i in input_coordinates]),
                                     refpts=ind_bpms,
                                     nturns=nturns)

    # APPLY BPM ERRORS
    h_coord_err = coord[0, 0, 0:, 0:]
    v_coord_err = coord[2, 0, 0:, 0:]

    # apply BPM noise (random each turn) and BPM errors (same at each turn)
    for iturn in range(0, int(nturns), 1):

        h_err, v_err = bpm_process(
            coord[0, 0, 0:, iturn],
            coord[2, 0, 0:, iturn],
            rel, tel, trand)

        t2d[0, iturn, 0:] = h_err
        t2d[1, iturn, 0:] = v_err

        h_coord_err[0:, iturn] = h_err
        v_coord_err[0:, iturn] = v_err

    coord[0, 0, 0:, 0:] = h_coord_err
    coord[2, 0, 0:, 0:] = v_coord_err

    # 6 x (nbpmxnturns) array
    trajectory = np.concatenate([coord[:, 0, :, t] for t in range(nturns)], axis=1)

    return trajectory
