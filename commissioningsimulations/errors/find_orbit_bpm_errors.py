import at
from commissioningsimulations.errors.bpm_modifications import bpm_matrices, bpm_process
import copy
import numpy


def find_orbit_bpm_errors(ring, ind_bpms, rel=None, tel=None, trand=None, guess=(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)):

    if (rel is None) or (tel is None) or (trand is None):
        rel, tel, trand = bpm_matrices(ring, ind_bpms)

    new_guess, orb = at.find_orbit(ring, ind_bpms, guess=guess)
    # ^ the guess for closed orbit should not contain BPM errors.

    # if nan orbit with guess input, try no input
    if numpy.isnan(new_guess).any():
        new_guess, orb = at.find_orbit(ring, ind_bpms)

    h_err, v_err = bpm_process(
        orb[:, 0],
        orb[:, 2],
        rel, tel, trand)

    orb[:, 0] = h_err
    orb[:, 2] = v_err

    # remember: new_guess may be different from orb[:, 0].
    # one is the orbit at the first BPM the other the orbit at the entrance of the ring.
    # if the entrance fo the ring is a BPM then the difference could be the errors applied in this function
    return copy.deepcopy(new_guess), copy.deepcopy(orb)


def plot_bpm_errors(ring, ind_bpms):

    o = [el.Offset for el in ring[ind_bpms]]
    s = [el.Scale for el in ring[ind_bpms]]
    r = [el.Rotation for el in ring[ind_bpms]]
    e = [el.Reading for el in ring[ind_bpms]]

    fig, (axo, axs, axr, axe) = plt.subplots(nrows=4)
    fig.subplots_adjust(hspace=0.5)

    axo.plot(o)
    axo.set_ylabel('Offsets')
    axs.plot(s)
    axs.set_ylabel('Scale')
    axr.plot(r)
    axr.set_ylabel('Rotation')
    axe.plot(e)
    axe.set_ylabel('Reading')
    axe.set_xlabel('BPM #')

    plt.show()

    pass