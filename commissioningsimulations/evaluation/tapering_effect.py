import at
import matplotlib.pyplot as plt
import copy
import os


def plot_tapering_effect(r=None, latname='', datafolder='./'):

    r0 = copy.deepcopy(r)

    ind_bpms = range(len(r0))

    # compute optics
    r0.disable_6d()

    _, _, opt0 = r0.linopt4(ind_bpms)

    # compute optics with radiation on
    r0.enable_6d()
    _, _, optrad = r0.linopt6(ind_bpms)

    # apply tapering
    r0.tapering()
    r0.radiation_on()
    # compute optics with radiation on and tapering
    _, _, optradtap = r0.linopt6(ind_bpms)

    s = at.get_s_pos(r0, ind_bpms)
    # plot beta-beating dispersion deviation and Delta COD
    fig, ax = plt.subplots(ncols=3, nrows=2, figsize=(10, 7))
    fig.subplots_adjust(hspace=0.5, wspace=0.5)
    ax[0, 0].plot(s, [h[0]*1e6 for h in optrad.closed_orbit], label='rad')
    ax[0, 0].plot(s, [h[0]*1e6 for h in optradtap.closed_orbit], label='rad + tapering')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 0].set_ylabel('hor. closed orbit [$\mu$m]')
    ax[0, 0].legend()
    ax[0, 0].set_title(latname)
    ax[0, 1].plot(s, [h[2]*1e6 for h in optrad.closed_orbit], label='rad')
    ax[0, 1].plot(s, [h[2]*1e6 for h in optradtap.closed_orbit], label='rad + tapering')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 1].set_ylabel('ver. closed orbit [$\mu$m]')

    ax[1, 0].plot(s, [(h[0]-h0[0])*1e3 for h, h0 in zip(optrad.dispersion, opt0.dispersion)], label='rad')
    ax[1, 0].plot(s, [(h[0]-h0[0])*1e3 for h, h0 in zip(optradtap.dispersion, opt0.dispersion)], label='rad + tapering')
    ax[1, 0].legend()
    ax[1, 0].set_ylabel('hor. $\Delta\eta$ [mm]')
    ax[1, 1].plot(s, [(h[2]-h0[2])*1e3 for h, h0 in zip(optrad.dispersion, opt0.dispersion)], label='rad')
    ax[1, 1].plot(s, [(h[2]-h0[2])*1e3 for h, h0 in zip(optradtap.dispersion, opt0.dispersion)], label='rad + tapering')
    ax[1, 1].set_ylabel('ver. $\Delta\eta$ [mm]')


    # ax[2, 0].plot([h[0] for h in opt0.beta], label='no rad')
    ax[2, 0].plot(s, [(h[0]-h0[0])/h0[0] *100 for h, h0 in zip(optrad.beta, opt0.beta)], label='rad')
    ax[2, 0].plot(s, [(h[0]-h0[0])/h0[0] *100 for h, h0 in zip(optradtap.beta, opt0.beta)], label='rad + tapering')
    ax[2, 0].set_ylabel('hor. $\Delta\\beta / \\beta_0$ [%]')
    ax[2, 0].legend()
    ax[2, 0].set_xlabel('s [m]')
    # ax[2, 1].plot([h[1] for h in opt0.beta], label='no rad')
    ax[2, 1].plot(s, [(h[1]-h0[1])/h0[1] *100 for h, h0 in zip(optrad.beta, opt0.beta)], label='rad')
    ax[2, 1].plot(s, [(h[1]-h0[1])/h0[1] *100 for h, h0 in zip(optradtap.beta, opt0.beta)], label='rad + tapering')
    ax[2, 1].set_ylabel('ver. $\Delta\\beta / \\beta_0$')
    ax[2, 1].set_xlabel('s [m]')

    plt.savefig(os.path.join(datafolder, 'tapering_effect' + '.png'))

    pass