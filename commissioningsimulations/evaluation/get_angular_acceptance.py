from at.acceptance import get_1d_acceptance
from at.lattice import Lattice


def get_angular_acceptance(ring: Lattice,
                            resolution: float, amplitude: float,
                            *args, **kwargs):
    r"""Computes the 1D momentum acceptance at refpts observation points

    See :py:func:`get_acceptance`

    Parameters:
        ring:           Lattice definition
        resolution:     Minimum distance between 2 grid points
        amplitude:      Search range:

          * :py:attr:`GridMode.CARTESIAN/RADIAL <.GridMode.RADIAL>`:
            max. amplitude
          * :py:attr:`.GridMode.RECURSIVE`: initial step

    Keyword Args:
        nturns:         Number of turns for the tracking
        refpts:         Observation points. Default: start of the machine
        dp:             static momentum offset
        offset:         initial orbit. Default: closed orbit
        grid_mode:      defines the evaluation grid:

          * :py:attr:`.GridMode.CARTESIAN`: full [:math:`\:x, y\:`] grid
          * :py:attr:`.GridMode.RADIAL`: full [:math:`\:r, \theta\:`] grid
          * :py:attr:`.GridMode.RECURSIVE`: radial recursive search
        use_mp:         Use python multiprocessing (:py:func:`.patpass`,
          default use :py:func:`.lattice_pass`). In case multi-processing is
          not enabled, ``grid_mode`` is forced to
          :py:attr:`.GridMode.RECURSIVE` (most efficient in single core)
        verbose:        Print out some information
        divider:        Value of the divider used in
          :py:attr:`.GridMode.RECURSIVE` boundary search
        shift_zero: Epsilon offset applied on all 6 coordinates
        start_method:   Python multiprocessing start method. The default
          ``None`` uses the python default that is considered safe.
          Available parameters: ``'fork'``, ``'spawn'``, ``'forkserver'``.
          The default for linux is ``'fork'``, the default for MacOS and
          Windows is ``'spawn'``. ``'fork'`` may used for MacOS to speed-up
          the calculation or to solve runtime errors, however  it is considered
          unsafe.

    Returns:
        boundary:   (len(refpts),2) array: 1D acceptance
        tracked:    (n,) array: Coordinates of tracked particles
        survived:   (n,) array: Coordinates of surviving particles

    In case of multiple ``tracked`` and ``survived`` are lists of arrays,
    with one array per ref. point.

    .. note::

       * When``use_mp=True`` all the available CPUs will be used.
         This behavior can be changed by setting
         ``at.DConstant.patpass_poolsize`` to the desired value
    """
    xp_acc, _, _ = get_1d_acceptance(ring, 'xp', resolution, amplitude,
                      *args, **kwargs)

    yp_acc, _, _ = get_1d_acceptance(ring, 'yp', resolution, amplitude,
                      *args, **kwargs)

    return xp_acc, yp_acc