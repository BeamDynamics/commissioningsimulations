import at
import numpy as np


def get_optics_vs_reference(r0, r, ind_bpms):
    """
    computes optics parameters and print them
    """

    nan_envelope = at.RingParameters()
    nan_envelope.__dict__ = {'E0': np.nan,
                             'U0': np.nan,
                             'tunes6': [np.nan]*3,
                             'emittances': [np.nan]*3,
                             'J': [np.nan]*3,
                             'Tau': [np.nan]*3,
                             'sigma_e': np.nan,
                             'sigma_l': np.nan,
                             'voltage': np.nan,
                             'phi_s': np.nan,
                             'fs': np.nan
                             }

    nan_linopt6 = np.recarray(ind_bpms.shape,
                                 dtype=[('alpha', '<f8', (2,)),
                                         ('beta', '<f8', (2,)),
                                         ('mu', '<f8', (3,)),
                                         ('R', '<f8', (3, 6, 6)),
                                         ('A', '<f8', (6, 6)),
                                         ('dispersion', '<f8', (4,)),
                                         ('closed_orbit', '<f8', (6,)),
                                         ('M', '<f8', (6, 6)),
                                         ('s_pos', '<f8')])

    radstate = r0.is_6d

    try:
        if radstate:
            # print('r0 6D')
            _, _, opt0 = r0.linopt6(ind_bpms)
        else:
            # print('r0 4D')
            _, _, opt0 = r0.linopt4(ind_bpms)
    except Exception:
        opt0 = nan_linopt6

    # force 6D for emittance
    r0.enable_6d()
    try:
        params = r0.envelope_parameters()
    except Exception:
        params = nan_envelope

    # restore if initial was 4d state
    if not radstate:
        r0.disable_6d()

    bh0 = np.array([l.beta[0] for l in opt0])
    bv0 = np.array([l.beta[1] for l in opt0])
    dh0 = np.array([l.dispersion[0] for l in opt0])
    dv0 = np.array([l.dispersion[2] for l in opt0])
    eh0 = params.emittances[0]
    ev0 = params.emittances[1]

    radstate = r.is_6d

    try:
        if radstate:
            # print('r 6D')
            _, _, opt = r.linopt6(ind_bpms)
        else:
            # print('r 4D')
            _, _, opt = r.linopt4(ind_bpms)
    except Exception:
        opt = nan_linopt6

    r.enable_6d()  # force 6D for emittance computation
    try:
        params = r.envelope_parameters()
    except Exception:
        params = nan_envelope

    if not radstate:
        r.disable_6d()

    bh = np.array([l.beta[0] for l in opt])
    bv = np.array([l.beta[1] for l in opt])
    dh = np.array([l.dispersion[0] for l in opt])
    dv = np.array([l.dispersion[2] for l in opt])
    eh = params.emittances[0]
    ev = params.emittances[1]

    bbh = (bh - bh0) / bh0
    bbv = (bv - bv0) / bv0
    ddh = (dh - dh0)
    ddv = (dv - dv0)
    deh = (eh - eh0)
    dev = (ev - ev0)

    print(f'beta-beating:     H {np.std(bbh)*100:.2f}%      V {np.std(bbv)*100:.2f}% ')
    print(f'delta dispersion: H {np.std(ddh)*1000:.2f}mm     V {np.std(ddv)*1000:.2f}mm ')
    print(f'emittance:  H {eh * 1e12:.2f}pmrad  V {ev * 1e12:.2f}pmrad ')

    return bbh, bbv, ddh, ddv, eh, ev


def _test_get_opt_vs_reference():

    import commissioningsimulations.config as config
    import matplotlib.pyplot as plt

    r0 = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)
    r = at.load_lattice(config.lattice_file_for_test, mat_key=config.lattice_variable_name)
    r0.disable_6d()
    r.enable_6d()

    """
    lattice_ref = '/machfs/liuzzo/FCC/V22_z/Errors/FCCeeV22_diag_4d.mat'
    lattice = '/machfs/liuzzo/FCC/V22_z/ANALYSIS/tapering/' \
                               'FCCeeV22_diag_radon.mat'

    r0 = at.load_lattice(lattice_ref, mat_key='r')
    r = at.load_lattice(lattice, mat_key='r')
    """

    # all indexes
    ind_bpms = np.array(range(len(r)))

    # print output of get_optics_vs_reference
    get_optics_vs_reference(r0, r, ind_bpms)

    # recompute and plot
    radstate = r0.is_6d
    if radstate:
        _, _, opt0 = r0.linopt6(ind_bpms)
    else:
        _, _, opt0 = r0.linopt4(ind_bpms)

    radstate = r.is_6d
    if radstate:
        _, _, opterr = r.linopt6(ind_bpms)
    else:
        _, _, opterr = r.linopt4(ind_bpms)



    s = np.array([x.s_pos for x in opt0])

    # plot beta-beating dispersion deviation and Delta COD
    fig, ax = plt.subplots(3, 2, figsize=(10, 7))
    fig.subplots_adjust(hspace=0.5, wspace=0.5)
    ax[0, 0].plot(s, [h[0] * 1e6 for h in opterr.closed_orbit], label='rad on')
    ax[0, 0].plot(s, [h[0] * 1e6 for h in opt0.closed_orbit], label='reference (rad off)')
    # ax[0, 0].plot(s, [h[0]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 0].set_ylabel('hor. orbit \n [$\mu$m]')
    ax[0, 0].legend()
    ax[0, 1].plot(s, [h[2] * 1e6 for h in opterr.closed_orbit], label='rad on')
    ax[0, 1].plot(s, [h[2] * 1e6 for h in opt0.closed_orbit], label='reference (rad off)')
    # ax[0, 1].plot(s, [h[2]*1e6 for h in opt0.closed_orbit], label='no rad')
    ax[0, 1].set_ylabel('ver. orbit \n [$\mu$m]')

    ax[1, 0].plot(s, [(h[0] - h0[0]) * 1e3 for h, h0 in zip(opterr.dispersion, opt0.dispersion)], label='rad on')
    ax[1, 0].plot(s, [(h[0] - h0[0]) * 1e3 for h, h0 in zip(opt0.dispersion, opt0.dispersion)], label='reference (rad off)')
    ax[1, 0].legend()
    ax[1, 0].set_ylabel('hor. $\Delta\eta$ \n [mm]')
    ax[1, 1].plot(s, [(h[2] - h0[2]) * 1e3 for h, h0 in zip(opterr.dispersion, opt0.dispersion)], label='rad on')
    ax[1, 1].plot(s, [(h[2] - h0[2]) * 1e3 for h, h0 in zip(opt0.dispersion, opt0.dispersion)], label='reference (rad off)')
    ax[1, 1].set_ylabel('ver. $\Delta\eta$ \n [mm]')

    # ax[2, 0].plot([h[0] for h in opt0.beta], label='no rad')
    ax[2, 0].plot(s, [(h[0] - h0[0]) / h0[0] * 100 for h, h0 in zip(opterr.beta, opt0.beta)], label='rad on')
    ax[2, 0].plot(s, [(h[0] - h0[0]) / h0[0] * 100 for h, h0 in zip(opt0.beta, opt0.beta)], label='reference (rad off)')
    ax[2, 0].set_ylabel('hor. $\Delta\\beta / \\beta_0$ \n [%]')
    ax[2, 0].legend()
    ax[2, 0].set_xlabel('s [m]')
    # ax[2, 1].plot([h[1] for h in opt0.beta], label='no rad')
    ax[2, 1].plot(s, [(h[1] - h0[1]) / h0[1] * 100 for h, h0 in zip(opterr.beta, opt0.beta)], label='rad on')
    ax[2, 1].plot(s, [(h[1] - h0[1]) / h0[1] * 100 for h, h0 in zip(opt0.beta, opt0.beta)], label='reference (rad off)')
    ax[2, 1].set_ylabel('ver. $\Delta\\beta / \\beta_0$ \n [%]')
    ax[2, 1].set_xlabel('s [m]')

    plt.show()

    pass

if __name__ == '__main__':
    _test_get_opt_vs_reference()
