import at
import os
import copy
import time
import numpy as np
from scipy.io import savemat
import scipy.io as spio
import pickle
from commissioningsimulations.get_indexes import get_lattice_indexes
from commissioningsimulations.correction.Sequence import get_correction_strengths


def compute_lattice_parameters(r0, indexes=None, file_name='./LatticeData.pkl', save_matlab_file=False):
    """
    coputes envelope_parameters, linopt6 and gets correction strengths for a lattice r0
    :param r0: AT lattice
    :param indexes: indexes from correction.get_lattice_indexes
    :param file_name: file to save data
    :return:
    """
    if indexes == None:
        indexes = get_lattice_indexes(r0)

    start_time = time.time()

    nan_envelope = at.RingParameters()
    nan_envelope.__dict__ = {'E0': np.nan,
                             'U0': np.nan,
                             'tunes6': [np.nan]*3,
                             'emittances': [np.nan]*3,
                             'J': [np.nan]*3,
                             'Tau': [np.nan]*3,
                             'sigma_e': np.nan,
                             'sigma_l': np.nan,
                             'voltage': np.nan,
                             'phi_s': np.nan,
                             'fs': np.nan
                             }

    '''
    vars(nan_envelope).update({'E0': np.nan,
                             'U0': np.nan,
                             'tunes6': [np.nan]*3,
                             'emittances': [np.nan]*3,
                             'J': [np.nan]*3,
                             'Tau': [np.nan]*3,
                             'sigma_e': np.nan,
                             'sigma_l': np.nan,
                             'voltage': np.nan,
                             'phi_s': np.nan,
                             'fs': np.nan
                             })
    '''

    nan_ringdata = np.recarray((1,),
                               dtype=[('tunes', '<f8', (3, )),
                                      ('chromaticity', '<f8', (3, ))])

    nan_linopt6 = np.recarray(indexes['bpms'].shape,
                                 dtype=[('alpha', '<f8', (2,)),
                                         ('beta', '<f8', (2,)),
                                         ('mu', '<f8', (3,)),
                                         ('R', '<f8', (3, 6, 6)),
                                         ('A', '<f8', (6, 6)),
                                         ('dispersion', '<f8', (4,)),
                                         ('closed_orbit', '<f8', (6,)),
                                         ('M', '<f8', (6, 6)),
                                         ('s_pos', '<f8')])
    for el in nan_linopt6:
        el.s_pos = np.nan * el.s_pos
        el.M = np.nan * el.M
        el.alpha = np.nan * el.alpha
        el.beta = np.nan * el.beta
        el.mu = np.nan * el.mu
        el.closed_orbit = np.nan * el.closed_orbit
        el.dispersion = np.nan * el.dispersion
        el.A = np.nan * el.A
        el.R = np.nan * el.R

    print('get lattice parameters')
    try:
        radstate=r0.radiation
        r0.radiation_on()
        p0 = r0.envelope_parameters()
        if not radstate:
            r0.radiation_off()
    except Exception as ve:
        p0 = nan_envelope

    print('get orbit, dispersion, beta functions')
    try:
        _, rd0, l0 = r0.linopt6(refpts=indexes['bpms'])
    except Exception as ve:
        l0 = nan_linopt6
        rd0 = nan_ringdata

    print('get correction strengths')
    c0 = get_correction_strengths(r0, indexes=indexes)

    m_dict = {'parameters': p0,
              'optics': l0,
              'correctors': c0,
              'ring_data': rd0}

    if save_matlab_file:
        savemat(file_name.replace('.pkl', '.mat'), mdict=m_dict)

    pickle.dump(m_dict, open(file_name, 'wb'))

    end_time = time.time()
    print('Parameters computations took: {} s. \nSaved to: {}'.format(end_time - start_time, file_name))

    return p0, l0, c0


def load_data_file(data_file):
    """
    load data file (.pkl or .mat) saves by compute_lattice_parameters.compute_lattice_parameters

    :param data_file:
    :return: parameters, opitcs, correctors
    """
    _, file_extension = os.path.splitext(data_file)

    if file_extension=='.mat':
        mat_contents = loadmat(data_file)
    elif file_extension=='.pkl':
        mat_contents = pickle.load(open(data_file, 'rb'))

    p0 = mat_contents['parameters']
    l0 = mat_contents['optics']
    c0 = mat_contents['correctors']

    return p0, l0, c0


def print_lattice_parameters(data_file_r0,
                             data_file_r_errors,
                             data_file_r_corrected,
                             file_name='./ComparativeData.txt',
                             save_matlab_file=False):
    """
    write to file comparative lattice parameters such as beta beating and dispersion deviation compared to model

    :param data_file_r0: refernce optics. file output of commissioningsimulations.evaluation.compute_lattice_parameters
    :param data_file_r_errors: optics with errors. file output of commissioningsimulations.evaluation.compute_lattice_parameters
    :param data_file_r_corrected: optics with errors and corrections. file output of commissioningsimulations.evaluation.compute_lattice_parameters
    :param file_name: output file name (default: ./ComparativeData.txt)
    :param save_matlab_file: save also a file in .mat format
    :return: None
    """

    p0, l0, c0 = load_data_file(data_file_r0)
    pe, le, ce = load_data_file(data_file_r_errors)
    pc, lc, cc = load_data_file(data_file_r_corrected)

    # closed orbit
    cod_h_0 = np.array([x.closed_orbit[0] for x in l0])
    cod_h_e = np.array([x.closed_orbit[0] for x in le])
    cod_h_c = np.array([x.closed_orbit[0] for x in lc])
    
    std_Dcod_h_e = np.std(cod_h_e - cod_h_0) * 1e6
    std_Dcod_h_c = np.std(cod_h_c - cod_h_0) * 1e6

    cod_v_0 = np.array([x.closed_orbit[2] for x in l0])
    cod_v_e = np.array([x.closed_orbit[2] for x in le])
    cod_v_c = np.array([x.closed_orbit[2] for x in lc])

    std_Dcod_v_e = np.std(cod_v_e - cod_v_0) * 1e6
    std_Dcod_v_c = np.std(cod_v_c - cod_v_0) * 1e6

    # dispersion
    eta_h_0 = np.array([x.dispersion[0] for x in l0])
    eta_h_e = np.array([x.dispersion[0] for x in le])
    eta_h_c = np.array([x.dispersion[0] for x in lc])

    std_Deta_h_e = np.std(eta_h_e - eta_h_0) * 1e3
    std_Deta_h_c = np.std(eta_h_c - eta_h_0) * 1e3

    eta_v_0 = np.array([x.dispersion[2] for x in l0])
    eta_v_e = np.array([x.dispersion[2] for x in le])
    eta_v_c = np.array([x.dispersion[2] for x in lc])

    std_Deta_v_e = np.std(eta_v_e - eta_v_0) * 1e3
    std_Deta_v_c = np.std(eta_v_c - eta_v_0) * 1e3

    # beta
    beta_h_0 = np.array([x.beta[0] for x in l0])
    beta_h_e = np.array([x.beta[0] for x in le])
    beta_h_c = np.array([x.beta[0] for x in lc])

    std_Dbeta_h_e = np.std((beta_h_e - beta_h_0) / beta_h_0) * 1e2
    std_Dbeta_h_c = np.std((beta_h_c - beta_h_0) / beta_h_0) * 1e2

    beta_v_0 = np.array([x.beta[1] for x in l0])
    beta_v_e = np.array([x.beta[1] for x in le])
    beta_v_c = np.array([x.beta[1] for x in lc])

    std_Dbeta_v_e = np.std((beta_v_e - beta_v_0) / beta_v_0) * 1e2
    std_Dbeta_v_c = np.std((beta_v_c - beta_v_0) / beta_v_0) * 1e2

    # W

    # RDTs


    with open(file_name, 'w') as f:

        f.write('- - - - - - - - - - - - - - - - \n')
        f.write(' Optics  parameters             \n')

        f.write('\t:\t\t errors \t\t corrected \t\t design \n')
        for k in p0.__dict__.keys():
            try:
                f.write('{}\t:\t\t {} \t\t {} \t\t {} \n'.format(k, pe.__dict__[k], pc.__dict__[k], p0.__dict__[k]))
            except Exception as e:
                print(e)

    with open(file_name, 'a') as f:
        f.write('\tLinear optics :\t\t errors \t\t corrected \n')
        f.write('closed_orbit H [um]:\t\t {:.3f} \t\t {:.3f} \n'.format(std_Dcod_h_e ,std_Dcod_h_c ))
        f.write('closed_orbit V [um]:\t\t {:.3f} \t\t {:.3f} \n'.format( std_Dcod_v_e, std_Dcod_v_c ))

    with open(file_name, 'a') as f:
        f.write('eta_h -  eta_h_0  [mm]:\t\t {:.3f} \t\t {:.3f} \n'.format( std_Deta_h_e, std_Deta_h_c))
        f.write('eta_v -  eta_v_0 [mm]:\t\t {:.3f} \t\t {:.3f} \n'.format(std_Deta_v_e, std_Deta_v_c))

    with open(file_name, 'a') as f:
        f.write('beta-beating H [%]:\t\t {:.3f} \t\t {:.3f} \n'.format(std_Dbeta_h_e, std_Dbeta_h_c))
        f.write('beta-beating V [%]:\t\t {:.3f} \t\t {:.3f} \n'.format(std_Dbeta_v_e, std_Dbeta_v_c))

    with open(file_name, 'a') as f:

        f.write('- - - - - - - - - - - - - - - - \n')
        f.write('Integrated error/correction strengths \n')
        f.write('\t: \t\t errors \t\t corrected \n')
        for k in c0.keys():
            f.write('{}:\t\t {:.3e} \t\t {:.3e} \n'.format(k, np.std(ce[k]-c0[k]), np.std(cc[k]-ce[k])))

    # reduced set of parameters
    m_dict = {'bb_h_e': std_Dbeta_h_e, # %
              'bb_h_c': std_Dbeta_h_c,
              'bb_v_e': std_Dbeta_v_e,
              'bb_v_c': std_Dbeta_v_c,
              'dd_h_e': std_Deta_h_e, # mm
              'dd_h_c': std_Deta_h_c,
              'dd_v_e': std_Deta_v_e,
              'dd_v_c': std_Deta_v_c,
              'oo_h_e': std_Dcod_h_e, # mum
              'oo_h_c': std_Dcod_h_c,
              'oo_v_e': std_Dcod_v_e,
              'oo_v_c': std_Dcod_v_c,
              }

    if save_matlab_file:
        savemat(file_name.replace('.txt', '.mat'), mdict=m_dict)

    pickle.dump(m_dict, open(file_name.replace('.txt', '.pkl'), 'wb'))

    pass


def loadmat(filename):
    '''
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    '''
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)


def _check_keys(dict):
    '''
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    '''
    for key in dict:
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict

def _todict(matobj):
    '''
    A recursive function which constructs from matobjects nested dictionaries
    '''
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict


if __name__ == '__main__':

    import commissioningsimulations.config as config

    ring = at.load_mat(config.lattice_file_for_test, mat_key=config.lattice_variable_name)
    compute_lattice_parameters(ring, file_name='./LatticeData.pkl')
    print_lattice_parameters('./LatticeData.pkl', './LatticeData.pkl', './LatticeData.pkl')

    pass


