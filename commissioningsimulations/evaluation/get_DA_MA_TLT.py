import at
from matplotlib import pyplot as plt
import at.plot
from os.path import exists
from scipy.io import savemat
import pickle
import numpy as np


folder_data = '.'
show_figures = False


######  compute  DA and MA
def get_DA_TL(ring,
           show_figures=show_figures,
           specname='',
           folder_data=folder_data,
           amplitudes=[5e-4, 1e-5],
           nturns=2**9,
           emit_v=10e-12,       # 10 pm
           cur_per_bunch=0.001,  # 1 mA
           zn=0.67,
           mom_acc_refpts=[],
           use_mp=False,
           save_matlab_file=False,
           save_figures=False):
    """
    computed DA, MA, Touschek  lifetime, saved the results to file and saves figures.

    :param ring:
    :param show_figures:
    :param specname:
    :param folder_data:
    :param amplitudes:
    :param nturns:
    :param emit_v:
    :param cur_per_bunch:
    :param mom_acc_refpts:
    :param use_mp:
    :return:
    """
    force_computation = False

    planes = ['x', 'y']
    npoints = [21, 21]

    if len(mom_acc_refpts)==0:
        print('Use default locations for lifetime computation: 1/30th of the the ring')
        mom_acc_refpts = range(int(len(ring) / 30))

    fignam = folder_data + '/DATLT' + specname
    fignamda = folder_data + '/DA' + specname
    fignamtlt = folder_data + '/TLT' + specname

    if not(exists(fignam + '.pkl') or force_computation):

        # x-y Dynamic Aperture
        try:
            b, s, g = ring.get_acceptance(planes,
                                          npoints,
                                          amplitudes,
                                          grid_mode=at.GridMode.RADIAL,
                                          nturns=nturns,
                                          use_mp=use_mp)
        except ValueError as ve:
            print(ve)
            print('DA computation failed')
            b = np.zeros((2, npoints[0]))*np.nan
            s = np.zeros((2, npoints[0]))*np.nan
            g = np.zeros((2, npoints[0]))*np.nan

        # Momentum acceptance
        try:
            Tau, MomAcc, indsMA = ring.get_lifetime(emit_v,
                                                    cur_per_bunch,
                                                    zn=zn,
                                                    nturns=nturns,
                                                    refpts=mom_acc_refpts,
                                                    verbose=True,
                                                    use_mp=use_mp)
        except ValueError as ve:
            print(ve)
            print('MA computation failed')
            indsMA = np.array(mom_acc_refpts)
            MomAcc = np.zeros((len(indsMA), 2))*np.nan
            Tau = np.nan

        # save data
        m_dict = {'border': b,
         'survived': s,
         'grid_of_tested_initial_coordinates': g,
         'n_turns': nturns,
         'vertical_emittance': emit_v,
         'current_per_bunch': cur_per_bunch,
         'zn': zn,
         'n_turns': nturns,
         'n_points': npoints,
         'rad': ring.radiation,
         'touschek_lifetime': Tau,
         'momentum_acceptance': MomAcc,
         'ind_mom_acc': indsMA}

        if save_matlab_file:
            savemat(fignam + '.mat', mdict=m_dict)

        pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

    else:
        print('{} already exist, skipping computation'.format(fignam + '.pkl'))
        mat_contents = pickle.load(open(fignam + '.pkl', 'rb'))
        b = mat_contents['border']
        s = mat_contents['survived']
        g = mat_contents['grid_of_tested_initial_coordinates']
        nturns = mat_contents['n_turns']
        Tau = mat_contents['touschek_lifetime']
        MomAcc = mat_contents['momentum_acceptance']
        indsMA = mat_contents['ind_mom_acc']

    # plot DA / TLT
    fig, ax = plt.subplots()

    plt.plot(*g, '.', label='tested')
    plt.plot(*s, 'o', label='survived')
    plt.plot(*b, label='edge')
    ax.legend()
    plt.xlabel('x [mm]')
    plt.ylabel('y [mm]')
    plt.text(0.9, 0.9, '{} turns\n rad {}'.format(nturns, ring.radiation==1), ha='left', va='top')
    if show_figures:
        plt.show()
    if save_figures:
        plt.savefig(fignamda + '.png')
    plt.close()

    fig, ax = plt.subplots()
    s = ring.get_s_pos(indsMA)  # at.get_s_pos(ring,indsMA) returns wrong size, (2,300) instead of (134,)
    plt.plot(s, [m*100 for m in MomAcc[:, 0]], '.-')
    plt.plot(s, [m*100 for m in MomAcc[:, 1]], '.-')
    #plt.xlabel('index')
    plt.xlabel('s [m]')
    plt.ylabel('$\delta$ [%]')
    plt.text(0.9,
             0.9,
             '$\\tau$ = {:.2f} h\n$\epsilon_v=$ {:.2f} pm, $I_b =$ {:.2f} mA\n {} turns\n rad {}'.format(
                 Tau/3600, emit_v*1e12, cur_per_bunch*1e3, nturns, ring.radiation==1),
             ha='left', va='top')
    if show_figures:
        plt.show()
    if save_figures:
        plt.savefig(fignamtlt + '.png')
    plt.close()

    return b, MomAcc, Tau, indsMA


def plot_DA_comparison(ring, b, b_e, b_c, folder_data=folder_data, save_figures=False):

    # check dimensionality
    if b.ndim == 1:
        b = np.atleast_2d(b).T
    if b_e.ndim == 1:
        b_e = np.atleast_2d(b_e).T
    if b_c.ndim == 1:
        b_c = np.atleast_2d(b_c).T

    # get sigmas
    ring.radiation_on(dipole_pass='BndMPoleSymplectic4RadPass')
    params = ring.envelope_parameters()
    # get global lattice parameters
    emittance_h = params.emittances[0]
    emittance_v = params.emittances[1]
    sigma_delta =  params.sigma_e
    l, _, _ = ring.linopt6()
    sig_h = (emittance_h*l.beta[0] + (sigma_delta*l.dispersion[0])**2)**0.5
    sig_v = (emittance_h/100*l.beta[1] + (sigma_delta*l.dispersion[2])**2)**0.5

    # plot DA and MA reference, with errors and corrected, in sigmas
    fig, ax = plt.subplots()
    plt.plot([bb/sig_h for bb in b[0, :]], [bb/sig_v for bb in b[1, :]], '-s', label='no errors')
    plt.plot([bb/sig_h for bb in b_e[0, :]], [bb/sig_v for bb in b_e[1, :]], '-x', label='with errors')
    plt.plot([bb/sig_h for bb in b_c[0, :]], [bb/sig_v for bb in b_c[1, :]], '-o', label='with errors + correction')
    ax.legend()
    plt.xlabel('x [sigmas]')
    plt.ylabel('y [sigmas]')

    if show_figures:
        plt.show()
    if save_figures:
        plt.savefig(folder_data + '/DA_compare.png')
    plt.close()


def plot_mom_acc_comparison(ring, indsMA, indsMA_e, indsMA_c, ma, ma_e, ma_c, tau, tau_e, tau_c,
                            folder_data=folder_data, save_figures=False):
    # get sigmas
    ring.radiation_on(dipole_pass='BndMPoleSymplectic4RadPass')
    params = ring.envelope_parameters()
    # get global lattice parameters
    emittance_h = params.emittances[0]
    emittance_v = params.emittances[1]
    sigma_delta = params.sigma_e
    l, _, _ = ring.linopt6()
    sig_h = (emittance_h * l.beta[0] + (sigma_delta * l.dispersion[0]) ** 2) ** 0.5
    sig_v = (emittance_h / 100 * l.beta[1] + (sigma_delta * l.dispersion[2]) ** 2) ** 0.5
    ss = ring.get_s_pos(indsMA)
    ss_e = ring.get_s_pos(indsMA_e) # dimension may change if computation of MA fails
    ss_c = ring.get_s_pos(indsMA_c)

    # plot DA and MA reference, with errors and corrected, in sigmas
    fig, ax = plt.subplots()
    plt.plot(ss, ma[:, 0]*100, '-s', color='blue', label='no errors' + ' $\\tau$ = {:.2f}'.format(tau))
    plt.plot(ss, ma[:, 1]*100, '-s', color='blue')
    plt.plot(ss_e, ma_e[:, 0]*100, '-x', color='blue', label='with errors' + ' $\\tau$ = {:.2f}'.format(tau_e))
    plt.plot(ss_e, ma_e[:, 1]*100, '-x', color='blue')
    plt.plot(ss_c, ma_c[:, 0]*100, '-o', color='blue', label='with errors + correction' + ' $\\tau$ = {:.2f}'.format(tau_c))
    plt.plot(ss_c, ma_c[:, 1]*100, '-o', color='blue')

    ax.legend()
    plt.xlabel('s [m]')
    plt.ylabel('$\delta$ [%]')

    if show_figures:
        plt.show()
    if save_figures:
        plt.savefig(folder_data + '/MA_compare.png')
    plt.close()