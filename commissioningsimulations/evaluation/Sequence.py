import at
from matplotlib import pyplot as plt
import at.plot
from os.path import exists
from scipy.io import savemat
import pickle
import numpy as np
import copy
import commissioningsimulations.config as config
from commissioningsimulations.evaluation.get_angular_acceptance import get_angular_acceptance


"""
config.dict_of_evaluations = { 'DA': {'DA_max_amplitude': [15e-3, 6e-3],
                                      'n_turns': 2**8,
                                      'n_points': [21, 21]},
                               'DAdpp': {
                                      'DA_max_amplitude': [5e-2, 15e-3],
                                      'n_turns': 2**8,
                                      'n_points': [21, 21]},
                               'MA': {'mom_acc_refpts': range(10),
                                      'n_turns': 2**8,
                                      'emit_v': 10e-12,       # 10 pm
                                      'cur_per_bunch': 0.001,  # 1 mA
                                      'zn': 0.67},
                               'AngularAcceptance': {
                                      'refpts': range(10),
                                      'n_turns': 2**8
                                      },
                               'FrequencyMap': {
                                      'DA_max_amplitude': [15e-3, 6e-3]
                                      'refpts': range(10),
                                      'n_turns': 2**8
                                      },
                               'Detunings': {'DA_max_amplitude': [15e-3, 6e-3],
                                      'n_turns': 2**8,
                                      'n_points': [21, 21]},
                               'IE': {},
                               }
                               """


def evaluations_sequence(
        rerr,  # lattice to be corrected
        dict_of_evaluations=config.dict_of_evaluations,
        specname='',
        folder_data = config.main_folder_data,
        use_mp=True,
        show_figures=False,
        save_figures=True):
    """

    :param rerr: AT lattice with errors to be corrected
    :param indexes: if None it is computed. dictionary of indexes from get_lattice_indexes
    :param dict_of_corrections: dictionary of corrections and correction parameters
           Example: dict_of_evaluations = { 'DA': {'DA_max_amplitude': [15e-3, 6e-3],
                                      'n_turns': 2**8},
                               'DAdpp': {'DA_max_amplitude': [15e-3, 6e-3],
                                      'n_turns': 2**8},
                               'MA': {'mom_acc_refpts': range(10),
                                      'n_turns': 2**8},
                               'IE': {},
                               }
    :return: DA_border, MomAcc, Tau, indsMA, InjEff, DA_dpp_border
    """

    force_computation = False
    save_matlab_file = config.save_matlab_files

    # initialize output
    DA_border = np.zeros((2, 13)) * np.nan
    DA_dpp_border = np.zeros((2, 13)) * np.nan
    indsMA = np.array(range(len(rerr)))
    MomAcc = np.zeros((len(indsMA), 2)) * np.nan
    Tau = np.nan
    IE = np.nan
    
    # loop corrections list
    for k in dict_of_evaluations:

        # replace _# by nothing
        k_sel = copy.deepcopy(k)
        k = ''.join([i for i in k_sel if not i.isdigit()])
        k = k.replace(" ", "")

        try:

            print('- * -'*20)
            print('evaluating {}'.format(k))
            print('- - -'*5)
            
            params = dict_of_evaluations[k]
            
            if k == 'DA':

                fignam = folder_data + '/DA' + specname

                if not(exists(fignam + '.pkl')) or force_computation:

                    # x-y Dynamic Aperture
                    try:
                        DA_border, s, g = rerr.get_acceptance(['x', 'y'],
                                                      params['n_points'],
                                                      params['DA_max_amplitude'],
                                                      grid_mode=at.GridMode.RADIAL,
                                                      nturns=params['n_turns'],
                                                      use_mp=use_mp)
                    except ValueError as ve:
                        print(ve)
                        print('DA computation failed')
                        DA_border = np.zeros((2, params['n_points'][0])) * np.nan
                        s = np.zeros((2, params['n_points'][0])) * np.nan
                        g = np.zeros((2, params['n_points'][0])) * np.nan


                    # save data
                    m_dict = {'border': DA_border,
                              'survived': s,
                              'grid_of_tested_initial_coordinates': g,
                              'n_turns': params['n_turns'],
                              'n_points': params['n_points'],
                              'rad': rerr.radiation}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot DA
                    fig, ax = plt.subplots()

                    plt.plot(*g, '.', label='tested')
                    plt.plot(*s, 'o', label='survived')
                    plt.plot(*DA_border, label='edge')
                    ax.legend()
                    plt.xlabel('x [m]')
                    plt.ylabel('y [m]')
                    plt.text(0.9, 0.9, '{} turns\n rad {}'.format(params['n_turns'], rerr.radiation == 1),
                             ha='left', va='top', transform=ax.transAxes)
                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()

                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing
                    dat = pickle.load(open(fignam + '.pkl', 'rb'))
                    DA_border = dat['border']

                    # data = pickle.load(open((fignam + '.pkl'))
                    # g = data['grid_of_tested_initial_coordinates']
                    # s = data['survived']
                    # DA_border = data['border']

            if k == 'MA':
                fignam = folder_data + '/MA' + specname

                if not (exists(fignam + '.pkl')) or force_computation:

                    try:
                        Tau, MomAcc, indsMA = rerr.get_lifetime(params['emit_v'],
                                                                params['cur_per_bunch'],
                                                                zn=params['zn'],
                                                                nturns=params['n_turns'],
                                                                refpts=params['mom_acc_refpts'],
                                                                verbose=True,
                                                                use_mp=use_mp)
                    except ValueError as ve:
                        print(ve)
                        print('MA computation failed')
                        indsMA = np.array(params['mom_acc_refpts'])
                        MomAcc = np.zeros((len(indsMA), 2)) * np.nan
                        Tau = np.nan

                    # save data
                    m_dict = {'n_turns': params['n_turns'],
                              'vertical_emittance': params['emit_v'],
                              'current_per_bunch': params['cur_per_bunch'],
                              'zn': params['zn'],
                              'rad': rerr.radiation,
                              'touschek_lifetime': Tau,
                              'momentum_acceptance': MomAcc,
                              'ind_mom_acc': indsMA}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot MA
                    fig, ax = plt.subplots()
                    s = rerr.get_s_pos(indsMA)  # at.get_s_pos(ring,indsMA) returns wrong size, (2,300) instead of (134,)
                    plt.plot(s, [m * 100 for m in MomAcc[:, 0]], '.-')
                    plt.plot(s, [m * 100 for m in MomAcc[:, 1]], '.-')
                    # plt.xlabel('index')
                    plt.xlabel('s [m]')
                    plt.ylabel('$\delta$ [%]')
                    plt.text(0.9,
                             0.9,
                             '$\\tau$ = {:.2f} h\n$\epsilon_v=$ {:.2f} pm, $I_b =$ {:.2f} mA\n {} turns\n rad {}'.format(
                                 Tau / 3600, params['emit_v'] * 1e12, params['cur_per_bunch'] * 1e3, params['n_turns'], rerr.radiation == 1),
                             ha='left', va='top', transform=ax.transAxes)
                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()
                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing
                    dat = pickle.load(open(fignam + '.pkl', 'rb'))
                    indsMA = dat['ind_mom_acc']
                    MomAcc = dat['momentum_acceptance']
                    Tau = dat['touschek_lifetime']

            if k == 'AngularAcceptance':
                # AngularAcceptance
                fignam = folder_data + '/AngAcc' + specname

                if not (exists(fignam + '.pkl')) or force_computation:

                    try:
                        XpAcc, YpAcc = get_angular_acceptance(
                                                rerr,
                                                resolution=1e-5,
                                                amplitude=1e-3,
                                                nturns=params['n_turns'],
                                                refpts=params['refpts'],
                                                verbose=True,
                                                use_mp=use_mp)

                        indsAcc = np.array(params['refpts'])

                    except ValueError as ve:
                        print(ve)
                        print('Angular Acceptance computation failed')
                        indsAcc = np.array(params['refpts'])
                        XpAcc = np.zeros((len(indsMA), 2)) * np.nan
                        YpAcc = np.zeros((len(indsMA), 2)) * np.nan

                    # save data
                    m_dict = {'n_turns': params['n_turns'],
                              'rad': rerr.radiation,
                              'Xp_acceptance': XpAcc,
                              'Yp_acceptance': YpAcc,
                              'ind_ang_acc': indsAcc}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot XYACC
                    fig, (axh, axv) = plt.subplots(nrows=2)
                    s = rerr.get_s_pos(indsAcc)  # at.get_s_pos(ring,indsMA) returns wrong size, (2,300) instead of (134,)
                    axh.plot(s, [m * 1e6 for m in XpAcc[:, 0]], 'b.-', label='xp')
                    axh.plot(s, [m * 1e6 for m in XpAcc[:, 1]], 'b.-', label='xp')
                    # plt.xlabel('index')
                    axh.set_xlabel('s [m]')
                    axh.set_ylabel('xp [$\mu$rad]')
                    # plt.text(0.9,
                    #          0.9,
                    #          '$\\tau$ = {:.2f} h\n$\epsilon_v=$ {:.2f} pm, $I_b =$ {:.2f} mA\n {} turns\n rad {}'.format(
                    #             Tau / 3600, params['emit_v'] * 1e12, params['cur_per_bunch'] * 1e3, params['n_turns'],
                    #             rerr.radiation == 1),
                    #         ha='left', va='top', transform=ax.transAxes)
                    axv.plot(s, [m * 1e6 for m in YpAcc[:, 0]], 'b.-', label='yp')
                    axv.plot(s, [m * 1e6 for m in YpAcc[:, 1]], 'b.-', label='yp')
                    # plt.xlabel('index')
                    axv.set_xlabel('s [m]')
                    axv.set_ylabel('yp [$\mu$rad]')

                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()
                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing

            if k == 'DAdpp':
                fignam = folder_data + '/DAdpp' + specname
    
                if not(exists(fignam + '.pkl')) or force_computation:
    
                    # x-y Dynamic Aperture
                    try:
                        DA_dpp_border, s, g = rerr.get_acceptance(['dp', 'x'],
                                                              params['n_points'],
                                                              params['DA_max_amplitude'],
                                                              grid_mode=at.GridMode.RADIAL,
                                                              nturns=params['n_turns'],
                                                              use_mp=use_mp)
                    except ValueError as ve:
                        print(ve)
                        print('DA computation failed')
                        DA_dpp_border = np.zeros((2, params['n_points'][0])) * np.nan
                        s = np.zeros((2, params['n_points'][0])) * np.nan
                        g = np.zeros((2, params['n_points'][0])) * np.nan
    
                    # save data
                    m_dict = {'border': DA_dpp_border,
                              'survived': s,
                              'grid_of_tested_initial_coordinates': g,
                              'n_turns': params['n_turns'],
                              'n_points': params['n_points'],
                              'rad': rerr.radiation}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot DA
                    fig, ax = plt.subplots()

                    plt.plot(*g, '.', label='tested')
                    plt.plot(*s, 'o', label='survived')
                    plt.plot(*DA_dpp_border, label='edge')
                    ax.legend()
                    plt.xlabel('\delta []')
                    plt.ylabel('x [m]')
                    plt.text(0.9, 0.9, '{} turns\n rad {}'.format(params['n_turns'], rerr.radiation == 1),
                             ha='left', va='top', transform=ax.transAxes)
                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()
                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing, data and figures already exist
                    dat = pickle.load(open(fignam + '.pkl', 'rb'))
                    DA_dpp_border = dat['border']

            if k == 'FrequencyMapAnalysis':

                fignam = folder_data + '/FMA4D' + specname

                if not(exists(fignam + '.pkl')) or force_computation:

                    # x-y Frequency Map
                    big_ampls = params['DA_max_amplitude']
                    r = copy.deepcopy(rerr)
                    r.disable_6d()
                    try:
                        [xy_tune_nudiff_array, plosses_array] = \
                            at.fmap_parallel_track(r,
                                                   coords=[-big_ampls[0]*1e3,  # in mm
                                                           +big_ampls[0]*1e3,
                                                           0.0,
                                                           big_ampls[1]*1e3],
                                                   steps=params['n_points'],
                                                   scale='linear',
                                                   lossmap=False,
                                                   verbose=False,
                                                   turns=params['n_turns'])
                    except ValueError as ve:
                        print(ve)
                        print('FMA (4D) computation failed')
                        xy_tune_nudiff_array = np.array([None]*5)
                        plosses_array = None


                    # save data
                    m_dict = {'xy_tune_nudiff_array': xy_tune_nudiff_array,
                              'plosses_array': plosses_array,
                              'n_turns': params['n_turns'],
                              'n_points': params['n_points'],
                              'rad': r.radiation}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot FMA
                    fig, ((axc, axt), (axh, axv)) = plt.subplots(ncols=2, nrows=2)
                    plt.subplots_adjust(hspace=0.5, wspace=0.5)

                    ms = 2

                    s = axc.scatter(xy_tune_nudiff_array[:, 0] * 1e3, xy_tune_nudiff_array[:, 1] * 1e3,
                                    s=ms, c=xy_tune_nudiff_array[:, 4], marker='s')
                    axc.set_xlabel('x [$\mu$m]')
                    axc.set_ylabel('y [$\mu$m]')
                    axc.set_title('diff. rate')
                    axc.set_ylim([0, big_ampls[1] * 1e6])
                    axc.set_xlim([-big_ampls[0] * 1e6, big_ampls[0] * 1e6])

                    s = axt.scatter(xy_tune_nudiff_array[:, 2], xy_tune_nudiff_array[:, 3],
                                    s=ms, c=xy_tune_nudiff_array[:, 4], marker='s')
                    axt.set_xlabel('$Q_H$ []')
                    axt.set_ylabel('$Q_V$ []')
                    axt.set_title('diff. rate')

                    s = axh.scatter(xy_tune_nudiff_array[:, 0] * 1e3, xy_tune_nudiff_array[:, 1] * 1e3,
                                    s=ms, c=xy_tune_nudiff_array[:, 2], marker='s')
                    axh.set_xlabel('x [$\mu$m]')
                    axh.set_ylabel('y [$\mu$m]')
                    axh.set_title('$Q_H$')
                    axh.set_ylim([0, big_ampls[1] * 1e6])
                    axh.set_xlim([-big_ampls[0] * 1e6, big_ampls[0] * 1e6])

                    s = axv.scatter(xy_tune_nudiff_array[:, 0] * 1e3, xy_tune_nudiff_array[:, 1] * 1e3,
                                    s=ms, c=xy_tune_nudiff_array[:, 3], marker='s')
                    axv.set_xlabel('x [$\mu$m]')
                    axv.set_ylabel('y [$\mu$m]')
                    axv.set_title('$Q_V$')
                    axv.set_ylim([0, big_ampls[1] * 1e6])
                    axv.set_xlim([-big_ampls[0] * 1e6, big_ampls[0] * 1e6])

                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()

                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing
                    dat = pickle.load(open(fignam + '.pkl', 'rb'))
                    xy_tune_nudiff_array = dat['xy_tune_nudiff_array']

            if k == 'Detunings':
                fignam = folder_data + '/Detuning' + specname

                # QH,QV vs H,V, Delta flat and in tune space
                # path lengthening as well

                if not(exists(fignam + '.pkl')) or force_computation:
                    # x-y detuning with amplitude
                    try:

                        _, _, x, q_x, y, q_y = at.detuning(rerr,
                                                       npoints=params['n_points'][0],
                                                       xm=params['DA_max_amplitude'][0],
                                                       ym=params['DA_max_amplitude'][1],
                                                       nturns=params['n_turns'])
                        rerr4d = copy.deepcopy(rerr)
                        rerr4d.disable_6d()
                        coef, dp, q_dp = at.chromaticity(rerr4d,
                                                         dpm=0.01,
                                                      npoints=params['n_points'][0],
                                                      method='laskar',
                                                      nturns=params['n_turns'])
                        del rerr4d

                    except ValueError as ve:
                        print(ve)
                        print('Detuning with amplitude computation failed')
                        x = np.nan*np.zeros(params['n_points'])
                        y = copy.deepcopy(x)
                        q_x = copy.deepcopy(x)
                        q_y = copy.deepcopy(x)
                        dp = copy.deepcopy(x)
                        q_dp = copy.deepcopy(x)
                        coef = np.array([None, None])

                    # save data
                    m_dict = {'x': x,
                              'y': y,
                              'q_x': q_x,
                              'q_y': q_y,
                              'dp': dp,
                              'q_dp': q_dp,
                              'coef': coef,
                              'n_turns': params['n_turns'],
                              'n_points': params['n_points'],
                              'rad': r.radiation}

                    if save_matlab_file:
                        savemat(fignam + '.mat', mdict=m_dict)

                    pickle.dump(m_dict, open(fignam + '.pkl', 'wb'))

                    # plot detuning with amplitude
                    fig, (axh, axv, axd) = plt.subplots(ncols=1, nrows=3)
                    plt.subplots_adjust(hspace=0.5)
                    axd.plot(dp * 1e2, q_dp[:, 0], label=f'$Q_h$ crhom:{coef[0, 1]:2.1f},{coef[1, 1]:2.1f}')
                    axd.plot(dp * 1e2, q_dp[:, 1], label=f'$Q_v$ crhom:{coef[0, 1]:2.1f},{coef[1, 1]:2.1f}')
                    axd.set_ylabel('$Q_h$, $Q_v$')
                    axd.set_xlabel('dp/p [%]')

                    axh.plot(x, q_x[:, 0], 'x-', label='$Q_h$')
                    axh.plot(x, q_x[:, 1], 'x-', label='$Q_v$')
                    axh.set_ylabel('$Q_h$, $Q_v$')
                    axh.set_xlabel('Jx [mm]')

                    axv.plot(y, q_y[:, 0], 'x-', label='$Q_h$')
                    axv.plot(y, q_y[:, 1], 'x-', label='$Q_v$')
                    axv.set_ylabel('$Q_h$, $Q_v$')
                    axv.set_xlabel('Jy [mm]')

                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + '.png', bbox_inches='tight')
                    plt.close()

                    # footprint figure
                    fig, (axh, axv, axd) = plt.subplots(ncols=1, nrows=3)
                    axd.scatter(q_dp[:, 0], q_dp[:, 1], c=dp)
                    axd.set_ylabel('$Q_h$')
                    axd.set_xlabel('$Q_v$')
                    axv.set_title('dp/p')

                    axh.scatter(q_x[:, 0], q_x[:, 1], c=x)
                    axh.set_ylabel('$Q_h$')
                    axh.set_xlabel('$Q_v$')
                    axv.set_title('$J_x$')

                    axv.scatter(q_y[:, 0], q_y[:, 1], c=y)
                    axv.set_ylabel('$Q_h$')
                    axv.set_xlabel('$Q_v$')
                    axv.set_title('$J_y$')

                    if show_figures:
                        plt.show()
                    if save_figures:
                        plt.savefig(fignam + 'Footprint.png', bbox_inches='tight')
                    plt.close()

                else:
                    print('data already exists in ' + fignam + '*')
                    # do nothing
                    dat = pickle.load(open(fignam + '.pkl', 'rb'))
                    x = dat['x']

            if k == 'IE':
                fignam = folder_data + '/IE' + specname
                print('Injection efficiency not yet implemented. Wait for pyAT implementation? '
                      'need optimal beta at injection, septum, injected beam coordinates and optics tuning ')

            if k == 'VacuumLifetime':
                fignam = folder_data + '/VacLT' + specname
                print('Vacuum Lifetime not yet implemented. Wait for pyAT implementation? '
                      'need angular acceptance (see AngularAcceptance), vacuum composition')

        except Exception as e:
            print(e)
            print(f'evaluation of {k} failed')

    return DA_border, MomAcc, Tau, indsMA, DA_dpp_border, IE


def _test_eval_sequence():

    pass


if __name__=='__main__':
    _test_eval_sequence()
    pass